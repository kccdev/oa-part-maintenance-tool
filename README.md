# OA Rev6 Part Maintenance Tool

This windows forms application lists all parts available and allows the user to create/update rules for each part.

## Project Structure

### Rev6 OAU_Parts Maintenance Tool
This project is a .NET Framework 4.5 windows application project. It inserts/updates the KCC database via ADO.NET.

## Data Access
All data access to the KCC database is done via ADO.NET and stored procedures. The following stored procedures are used:

* R6_OA_GetCompressorDataDtl
* R6_OA_GetCompressorDetailData
* R6_OA_UpdateCompressorDataDTL
* R6_OA_DeleteCompressorDataDtl
* R6_OA_InsertCompressorDataDtl
* R6_OA_GetCompressorChargeData
* R6_OA_InsertCompressorChargeDataHead
* R6_OA_UpdateCompressorChargeDataHead
* R6_OA_DeleteCompressorChargeDataHead
* R6_OA_GetCompCircuitChargeData
* R6_OA_GetDuplicateCompChargeDataEntries
* R6_OA_InsertCompCircuitChargeData
* R6_OA_UpdateCompCircuitChargeData
* R6_OA_DeleteCompCircuitData
* R6_OA_GetLineSizeList
* R6_OA_GetLineSizeCoolingCap
* R6_OA_InsertLineSize
* R6_OA_UpdateLineSize
* R6_OA_DeleteLineSizeCoolingCap
* R6_OA_GetFurnaceDataDTL
* R6_OA_UpdateFurnaceDataDTL
* R6_OA_InsertFurnaceDataDTL
* R6_OA_DeleteFurnaceDataDTL
* R6_OA_GetCoolingCapByRevision
* R6_OA_GetVoltageByRevision
* R6_OA_GetModelNumDesc
* VKG_GetModelNumDesc
* R6_VKG_GetModelNumDesc
* R6_OA_GetModelNumDescByRevision
* R6_OA_UpdateModelNumDesc
* R6_OA_InsertModelNumDesc
* R6_OA_DeleteModelNumDesc
* R6_OA_GetMotorDataDTL
* R6_OA_GetAllMotorDataDTL
* R6_OA_UpdateMotorDataDTL
* R6_OA_InsertMotorDataDTL
* R6_OA_DeleteMotorDataDTL
* R6_OA_GetPartCategoryHead
* R6_OA_UpdatePartCategoryHead
* R6_OA_InsertPartCategoryHead
* R6_OA_DeletePartCategoryHead
* R6_OA_GetPartsAndRulesList
* R6_OA_GetVMEASM_PartsWithRules
* R6_OA_GetPartRulesByPartNum
* R6_OA_GetPartRulesDataByPartNum
* R6_OA_GetPartRulesByPartNumber
* R6_OA_GetPartDetail
* R6_OA_GetRuleBatchesForPartNum_UnitType
* R6_OA_GetPartRulesByRuleHeadID_AndRuleBatchID
* R6_OA_GetPartCategoryList
* R6_OA_GetModelNoDigitList
* R6_OA_GetMainPartList
* R6_OA_GetPartNumberInfo
* R6_OA_GetPartNumbersByCategory
* R6_OA_GetCompressorPartNumbers
* R6_OA_UpdatePartConditionsDTL
* R6_OA_InsertPartConditionsDTL
* R6_OA_InsertPartMaster
* VKG_InsertPartMaster
* R6_OA_DeleteFromPartMaster
* R6_OA_InsertPartRulesHead
* R6_OA_GetPartRulesHead
* R6_OA_DeletePartRulesHead
* R6_OA_UpdatePartRulesHead
* R6_OA_DeleteFromPartConditionsDTL
* R6_OA_GetPartNumbersBasedOnClassID
* VKG_GetModelNoDigitList
* VKG_GetModelNumDesc
* R6_OA_VKG_GetSubAsmPartNums
* R6_OA_VKG_GetInnerAssemblyPartNums
* R6_VKG_GetAssemblyPartNumDetail
* R6_OA_VKG_GetSubAsmParentData
* R6_OA_VKG_InsertSubAsmParentData
* R6_OA_VKG_UpdateSubAsmParentData
* R6_OA_VKG_DeleteSubAsmParentData
* VKG_DeleteFromPartMaster

## How to run locally
Assuming a VPN connection, make sure solution is ready to run in Debug configuration. Change the connection string in frmMain.cs #if Debug section to point to the appropriate local or dev KCC database.
Build the project and run it. 

## How to deploy
One-click publish: [https://docs.microsoft.com/en-us/visualstudio/deployment/how-to-publish-a-clickonce-application-using-the-publish-wizard?view=vs-2019]

## Author/Devs
Tony Thoman