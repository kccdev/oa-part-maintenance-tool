﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.IO;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmMain : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        ModelNumDescBO objModel = new ModelNumDescBO();
        CompressorBO objComp = new CompressorBO();
        MotorBO objMotor = new MotorBO();
        FurnaceBO objFurn = new FurnaceBO();
        PartCategoryBO objCat = new PartCategoryBO();

        int curSelRowIdx = 0;
        int curSearchDigitNo = 0;
        int curVoltage = -1;
        string curHeatType = "";
        string curOAUTypeCode = "";
        string curPartNum = "";

        bool initModelRender = true;
        
        // PartNum master fields
        public string mRuleHeadID { get; set; }
        public string mRuleBatchID { get; set; }
        public string mPartNumber { get; set; }
        public string mPartDesc { get; set; }
        public string mRelatedOp { get; set; }
        public string mPartCategory { get; set; }
        public string mUnitType { get; set; }
        public string mDigit { get; set; }
        public string mValueStr { get; set; }
        public string mQty { get; set; } 
        public string mUpdatedValueStr { get; set; }
        public string mAssemblySeq { get; set; }
        public string mPartType { get; set; }
        public string mStationLoc { get; set; }
        public string mPicklist { get; set; }
        public string mConfigPart { get; set; }
        public string mLinesideBin { get; set; }

        // ModelNumDesc fields
        public string mDigitNo { get; set; }
        public string mDigitVal { get; set; }
        public string mHeatType { get; set; }
        public string mDigitDesc { get; set; }
        public string mDigitRep { get; set; }
        public string mOAUTypeCode { get; set; }
        public string mComboBoxItemPos { get; set; }

        // Compressor Data fields
        public string mVoltage { get; set; }
        public string mPhase { get; set; }
        public string mRLA { get; set; }
        public string mLRA { get; set; }
        public string mComp1_1 { get; set; }
        public string mComp1_2 { get; set; }
        public string mComp1_3 { get; set; }
        public string mComp2_1 { get; set; }
        public string mComp2_2 { get; set; }
        public string mComp2_3 { get; set; }
        public string mCircuit1 { get; set;         }
        public string mCircuit1_Reheat { get; set; }
        public string mCircuit2 { get; set; }
        public string mCircuit2_Reheat { get; set; }
        public string mCircuit1Suction { get; set; }
        public string mCircuit1LiquidLine { get; set; }
        public string mCircuit1DischargeLine { get; set; }
        public string mCircuit2Suction { get; set; }
        public string mCircuit2LiquidLine { get; set; }
        public string mCircuit2DischargeLine { get; set; }
        public string mCoolingCap { get; set; }
        public string mRfgComponent { get; set; }   
        public string mModBy { get; set; }
        public string mLastModDate { get; set; }
        public string mRevision { get; set; }
        public string mCompType { get; set; }
        public string mIndoorCoilType { get; set; }
        public string mOutdoorCoilType { get; set; }
        public string mHotGasReheat { get; set; }

        // Motor Data fields
        public string mMotorType { get; set; }
        public string mFLA { get; set; }
        public string mHertz { get; set; }
        public string mMCC { get; set; }
        public string mHP { get; set; }

        // Furnace Data fields
        public string mAmps { get; set; }
        public string mVolts { get; set; }
        public string mBurnerRatio { get; set; }
        public string mFuelType { get; set; }
        public string mMBHkW { get; set; }

        // Part Category Head Fields
        public string mCategoryName { get; set; }       
        public string mCategoryDesc { get; set; }
        public string mPullOrder { get; set; }
        public string mCritComp { get; set; }

        public string EnvironmentStr { get; set; }
        public Color EnvColor { get; set; }

        public frmMain()
        {
            InitializeComponent();


#if DEBUG
            try           // If in the debug environment then set the Database connection string to the test database.
            {               
                EnvColor = Color.LightCoral;
                EnvironmentStr = "Development Environment";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";               
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
            }
#if TEST2
            try           // If in the debug environment then set the Database connection string to the test database.
            {              
                EnvColor = Color.SpringGreen;
                EnvironmentStr = "Test 2 Environment";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";                
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
            }
#endif
#endif

            //Load PartRulesHead DataGridView
            objPart.PartNum = "ALL";
            DataTable dt = objPart.GetPartRulesHead();
            populatePartRuleHeadList(dt, true);           

            //Load PartConditionsDTL DataGridView
            dt = objPart.GetOAU_PartsAndRulesList();
            populateMainPartsList(dt);

            //Load Model No Desc DataGridView
            curSearchDigitNo = -1;
            curHeatType = "ALL";
            curOAUTypeCode = "ALL";

            dt = objModel.GetOAU_ModelNoValues(curSearchDigitNo, curHeatType);
            populateModelNoList(dt, true);

            //Load Compressor Detail DataGridView
            curVoltage = -1;
            curPartNum = "";
            dt = objComp.GetROA_CompressorDataDtl(curPartNum, curVoltage);
            populateCompressorDetailList(dt, true);

            ////Load Compressor Charge DataGridView
            //curVoltage = -1;
            //curPartNum = "";
            //objComp.UnitModel = "ALL";
            //objComp.CoolingCap = "ALL";
            //objComp.HeatType = "";
            //dt = objComp.GetROA_CompressorChargeData();
            //populateCompressorChargeList(dt, true);

            //Load Motor Data DataGridView

            objMotor.RuleHeadID = "-1";
            dt = objMotor.R6_OA_GetAllMotorDataDTL();
            populateMotorDataList(dt, true);

            //Load Furnace Data DataGridView

            objFurn.PartNum = "";
            dt = objFurn.GetROA_FurnaceDataDTL();
            populateFurnaceDataList(dt, true);

            //Load Part Category Head DataGridView

            objCat.PullOrder = "-1";
            dt = objCat.GetROA_PartCategoryHead();
            populatePartCategoryDataList(dt, true);

            this.txtPRH_SearchPartNum.Focus();
            lbMainTitle.Text = "Part Rule Head";

            //Load Compressor Circuit Charge DataGridView                        
            objComp.Digit3 = null;
            objComp.Digit4 = null;
            objComp.Digit567 = "0";
            objComp.Digit9 = null;
            objComp.Digit11 = null;
            objComp.Digit13 = null;
            objComp.Digit14 = null;
            dt = objComp.GetCompCircuitChargeData();
            populateCompressorCircuitChargeList(dt, true);

            objComp.Digit3 = "ALL";
            objComp.Digit4 = null;
            objComp.Digit567 = null;           
            objComp.Digit11 = null;            
            dt = objComp.GetLineSizeList();
            populateLineSizeList(dt, true);

            lbEnvironment.Text = EnvironmentStr;
            this.BackColor = EnvColor;
            
        }


        #region Main Form Events

        private void btnMainExit_Click(object sender, EventArgs e)
        {
             DataTable dtDups = objComp.R6_OA_GetDuplicateCompChargeDataEntries();
             if (dtDups.Rows.Count > 0)
             {
                 MessageBox.Show("Duplicate Cooling Capacity data exist and you can not exit before removing the duplicates.");
             }
             else
             {
                 Application.Exit();
             }            
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            int tabIdx = 0;
            tabIdx = tabMain.SelectedIndex;

            switch (tabIdx)
            {
                case 0: lbMainTitle.Text = "Part Rules Head";
                    break;
                case 1: lbMainTitle.Text = "Part Rules Conditions";
                    break;
                case 2: lbMainTitle.Text = "Model No Description";
                    break;
                case 3: lbMainTitle.Text = "Compressor Data Detail";
                    break;
                case 4: lbMainTitle.Text = "Compressor Charge Data";
                    break;
                case 5: lbMainTitle.Text = "Motor Data Detail";
                    break;
                case 6: lbMainTitle.Text = "Furnace Data Detail";
                    break;
                case 7: lbMainTitle.Text = "Part Category";
                    break;
                case 8: lbMainTitle.Text = "Rev6 Compressor Charge Data";
                    break;
                default: break;
            }
        }
        #endregion

        #region OAU Part Conditions Tab Events

        private void dgvPartsMain_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string strValue = "";

            int iRuleId = 0;
            int iBatchId = 0;
            int iCurRuleId = -1;
            int iCurBatchId = -1;          

            Color prevBackground = Color.White;
            Color foreGround = Color.Black;
            Color curBackGround = Color.LightGray;            
            Color nextBackGround = Color.White;           

            foreach (DataGridViewRow row in dgvPartsMain.Rows)
            {
                
                strValue = row.Cells[0].Value.ToString();
                iRuleId = Int32.Parse(strValue);
                strValue = row.Cells[1].Value.ToString();
                iBatchId = Int32.Parse(strValue);
                
                if ((iCurRuleId != iRuleId) || (iCurBatchId != iBatchId))
                {
                    iCurRuleId = iRuleId;
                    iCurBatchId = iBatchId;

                    prevBackground = curBackGround;
                    curBackGround = nextBackGround;
                    nextBackGround = prevBackground;
                }

                row.DefaultCellStyle.BackColor = curBackGround;
                row.DefaultCellStyle.ForeColor = foreGround;
            }
        }

        private void dgvPartsMain_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string partNum;
            string partDesc;
            string partCategory;
            string unitType;
            string value;
            string heatType;
            string modBy;
            string modDate;
            string immedParent;
            string relatedOp;
            string vkgRelOp;
            string comments;

            int ruleHeadID;
            int ruleBatchID;           
            int digit;
           
            decimal reqQty;            

            bool rfgComp = false;
            bool circuit1 = false;
            bool circuit2 = false;
            bool subAssemblyPart = false;
            bool subAsmMtlPart = false;           

            //var dataIndexNo = dgvPartsMain.Rows[e.RowIndex].Index.ToString();
            //MessageBox.Show("Row == " + dataIndexNo.ToString());
            ruleHeadID = (int) dgvPartsMain.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
            ruleBatchID = (int) dgvPartsMain.Rows[e.RowIndex].Cells["RuleBatchID"].Value;
            partNum = dgvPartsMain.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
            partDesc = dgvPartsMain.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
            relatedOp =  dgvPartsMain.Rows[e.RowIndex].Cells["RelatedOperation"].Value.ToString();            
            partCategory = dgvPartsMain.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();
            unitType = dgvPartsMain.Rows[e.RowIndex].Cells["Unit"].Value.ToString();
            digit = (int) dgvPartsMain.Rows[e.RowIndex].Cells["Digit"].Value;
            heatType = dgvPartsMain.Rows[e.RowIndex].Cells["HeatType"].Value.ToString();
            value = dgvPartsMain.Rows[e.RowIndex].Cells["Value"].Value.ToString();
            reqQty = (decimal)dgvPartsMain.Rows[e.RowIndex].Cells["Qty"].Value;
            rfgComp = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["RfgComponent"].Value;
            circuit1 = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["Circuit1"].Value;
            circuit2 = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["Circuit2"].Value;            
            modDate = dgvPartsMain.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            modBy = dgvPartsMain.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            subAssemblyPart = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["SubAssemblyPart"].Value;
            subAsmMtlPart = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["SubAsmMtlPart"].Value;
            immedParent = dgvPartsMain.Rows[e.RowIndex].Cells["ImmediateParent"].Value.ToString();
            comments = dgvPartsMain.Rows[e.RowIndex].Cells["Comments"].Value.ToString();  

            frmUpdate frmUpd = new frmUpdate(this);
            frmUpd.cbPartNum.Text = partNum;
            frmUpd.txtPartDesc.Text = partDesc;
            frmUpd.txtOARelOp.Text = relatedOp;            
            frmUpd.lbModBy.Text = modBy;
            frmUpd.lbModDate.Text = modDate;

            DataTable dt1 = objPart.GetModelNoDigitList();
            frmUpd.cbDigit.DisplayMember = "DigitHeat";
            frmUpd.cbDigit.ValueMember = "HeatType";

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "0/NA = CircuitBreaker";
            dr1["HeatType"] = "NA";
            dt1.Rows.InsertAt(dr1, 0);

            frmUpd.cbDigit.DataSource = dt1;
            frmUpd.cbDigit.SelectedIndex = 0;
            frmUpd.cbDigit.Text = digit.ToString();
            frmUpd.txtPartCategory.Text = partCategory;
            
            if (unitType.Contains("OA") == true)
            {                
                objPart.RevType = "REV6";
            }

            //if (unitType.Contains("VKG") == true)
            //{
            //    frmUpd.chkBoxViking.Checked = true;
            //    objPart.RevType = "VKG";
            //}

            //if (unitType.Contains("OA") == true && unitType.Contains("VKG") == true)
            //{
            //    objPart.RevType = "REV6,VKG";
            //}
            
            frmUpd.txtRuleID.Text = ruleHeadID.ToString();
            frmUpd.txtBatchID.Text = ruleBatchID.ToString();
            frmUpd.nudReqQty.Value = reqQty;
            setupValueCheckBoxes(frmUpd, digit, value, heatType, unitType);

            objPart.PartNum = partNum;
            objPart.RuleBatchID = ruleBatchID.ToString();            

            if (subAssemblyPart == true)
            {               
                frmUpd.chkBoxSubAsm.Checked = true;
                frmUpd.cbImmediateParentPartNum.SelectedValue = immedParent;
                
                //DataTable dtSub = objPart.GetSubAsmParentData();
                //if (dtSub.Rows.Count > 0)
                //{
                //    DataRow drSub = dtSub.Rows[0];

                //    frmUpd.cbImmediateParentPartNum.SelectedValue = drSub["PrimaryAssemblyPartNum"].ToString();
                //    bomLevel = (int)drSub["BOM_Level"];
                //    frmUpd.nudBOM_Level.Value = bomLevel;

                //    if (!drSub.IsNull("InnerAssemblyPartNum"))                    
                //    {
                //        frmUpd.cbInnerAsmPartNum.SelectedValue = drSub["InnerAssemblyPartNum"].ToString();                        
                //    }
                //    else
                //    {
                //        frmUpd.cbInnerAsmPartNum.SelectedIndex = 0;
                //    }

                //    if (!drSub.IsNull("ImmediateParentPartNum"))                    
                //    {
                //        frmUpd.cbImmediateParentPartNum.SelectedValue = drSub["ImmediateParentPartNum"].ToString();                        
                //    }
                //    else
                //    {
                //        frmUpd.cbImmediateParentPartNum.SelectedIndex = 0;
                //    }
                //}
            }

            if (subAsmMtlPart == true)
            {
                frmUpd.chkBoxMtlPart.Checked = true;
            }

            frmUpd.rtbComments.Text = comments;
            //if (preConfigAsm == true)
            //{
            //    frmUpd.chkBoxPreConfigAsm.Checked = true;
            //}

            DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

            foreach(DataRow dr in dt.Rows)
            {
                int index = frmUpd.dgvUpdateRules.Rows.Add();

                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleHeadID"].Value = dr["RuleHeadID"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleBatchID"].Value = dr["RuleBatchID"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartNumber"].Value = dr["PartNum"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartDesc"].Value = dr["PartDescription"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRelatedOp"].Value = dr["RelatedOperation"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartCat"].Value = dr["PartCategory"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUnitType"].Value = dr["Unit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColDigit"].Value = dr["Digit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColHeatType"].Value = dr["HeatType"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColValue"].Value = dr["Value"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColQty"].Value = dr["Qty"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUpdateType"].Value = "Update";                

                //frmUpd.dgvUpdateRules.Rows[index].Cells["ColID"].Value = dr["ID"].ToString();
      
                if (value == dr["Value"].ToString())
                {
                    frmUpd.lbSelectedRow.Text = index.ToString();
                }
            }

            if (rfgComp == true)
            {
                frmUpd.chkBoxRfgComp.Checked = true;
                frmUpd.gbCircuits.Visible = true;
                if (circuit1 == true)
                {
                    frmUpd.chkBoxCircuit1.Checked = true;
                }
                if (circuit2 == true)
                {
                    frmUpd.chkBoxCircuit2.Checked = true;
                }               
            }
            
            frmUpd.lbScreenMode.Text = "UpdateMode";
            this.btnMainExit.Enabled = false;
            frmUpd.ShowDialog();

            if (mUpdatedValueStr != "NoUpdate")
            {
                //dgvPartsMain.Rows[e.RowIndex].Cells[6].Value = Int32.Parse(mRelatedOp);
                //dgvPartsMain.Rows[e.RowIndex].Cells[10].Value = mUpdatedValueStr;
                //dgvPartsMain.Rows[e.RowIndex].Cells[11].Value = Decimal.Parse(mQty);
                dt = objPart.GetOAU_PartsAndRulesList();
                populateMainPartsList(dt);

                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, null); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }

            this.btnMainExit.Enabled = true;
        }
              
        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtSearchPartNum.Text = "";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtSearchPartNum.Text = "";           
            dgvPartsMain.DataSource = null;
            DataTable dt = objPart.GetOAU_PartsAndRulesList();
            dgvPartsMain.DataSource = dt;            
            populateMainPartsList(dt);
        }

        private void dgvPartsMain_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {          
            //int rowIdx = e.RowIndex;           
            //int ruleHeadID = Int32.Parse(dgvPartsMain.Rows[rowIdx].Cells[2].Value.ToString());
            //int ruleBatchID = Int32.Parse(dgvPartsMain.Rows[rowIdx].Cells[3].Value.ToString());

            //string partNumberStr = dgvPartsMain.Rows[rowIdx].Cells[4].Value.ToString();

            //DataTable rulesDT = dgvPartsMain.DataSource as DataTable;

            //objPart.RuleHeadID = ruleHeadID.ToString();
            //objPart.RuleBatchID = ruleBatchID.ToString();

            //if (e.ColumnIndex == 0)  // Add button within datagridview.
            //{
            //    frmAddRule frmAdd = new frmAddRule(this);
            //    DataTable dt = objPart.GetPartCategoryList();

            //    mPartNumber = dgvPartsMain.Rows[rowIdx].Cells[4].Value.ToString();
            //    mPartDesc = dgvPartsMain.Rows[rowIdx].Cells[5].Value.ToString();
            //    mRelatedOp = dgvPartsMain.Rows[rowIdx].Cells[6].Value.ToString();
            //    mPartCategory = dgvPartsMain.Rows[rowIdx].Cells[7].Value.ToString();
            //    mUnitType = dgvPartsMain.Rows[rowIdx].Cells[8].Value.ToString();                
               
            //    //frmAdd.cbPartCategory.DisplayMember = "CategoryName";
            //    //frmAdd.cbPartCategory.ValueMember = "CategoryName";               
            //    //frmAdd.cbPartCategory.DataSource = dt;

            //    objPart.PartCategory = null;
            //    dt = objPart.GetOAU_MainPartList();
            //    frmAdd.cbPartNum.DisplayMember = "PartNum";
            //    frmAdd.cbPartNum.ValueMember = "ID";

            //    DataRow dr = dt.NewRow();
            //    dr["ID"] = 0;
            //    dr["PartNum"] = "Select a Part Number.";
            //    dt.Rows.InsertAt(dr, 0);
            //    frmAdd.cbPartNum.DataSource = dt;                
            //    frmAdd.cbPartNum.SelectedIndex = 0;
            //    frmAdd.cbPartNum.Text = partNumberStr;
            //    frmAdd.cbPartNum.Enabled = false;
            //    frmAdd.txtPartDesc.Text = dgvPartsMain.Rows[rowIdx].Cells[5].Value.ToString(); 

            //    frmAdd.txtRuleID.Text = ruleHeadID.ToString();
            //    frmAdd.txtBatchID.Text =ruleBatchID.ToString();
            //    frmAdd.txtBatchID.Enabled = false;
            //    //frmAdd.cbRelatedOp.Text = dgvPartsMain.Rows[rowIdx].Cells[6].Value.ToString();
            //    //frmAdd.cbRelatedOp.Enabled = false;
            //    //frmAdd.cbPartCategory.Text = dgvPartsMain.Rows[rowIdx].Cells[7].Value.ToString();
            //    //frmAdd.cbPartCategory.Enabled = false;
            //    frmAdd.cbUnitType.Text = dgvPartsMain.Rows[rowIdx].Cells[8].Value.ToString();
            //    frmAdd.cbUnitType.Enabled = false;
            //    frmAdd.nudReqQty.Value = decimal.Parse(dgvPartsMain.Rows[rowIdx].Cells[11].Value.ToString());
            //    frmAdd.nudReqQty.Enabled = false;
            //    frmAdd.cbDigit.Focus();
                
            //    frmAdd.ShowDialog();

            //    if (mQty != "-1")
            //    {
            //        DataRow row = rulesDT.NewRow();                                            
                   
            //        row[0] = mRuleHeadID;
            //        row[1] = mRuleBatchID;
            //        row[2] = mPartNumber;
            //        row[3] = mPartDesc;
            //        row[4] = Int32.Parse(mRelatedOp);
            //        row[5] = mPartCategory;
            //        row[6] = mUnitType;
            //        row[7] = Int32.Parse(mDigit);
            //        row[8] = mValueStr;
            //        row[9] = decimal.Parse(mQty);

            //        rulesDT.Rows.InsertAt(row, (rowIdx + 1));

            //        dgvPartsMain.DataSource = rulesDT;
            //        dgvPartsMain.Refresh();
            //    }
            //}
            //else if (e.ColumnIndex == 1)  // Delete button internally is really in column #1 not 11 as it appears on the screen.
            //{
            //    objPart.Digit = dgvPartsMain.Rows[rowIdx].Cells[9].Value.ToString();                

            //    DialogResult result1 = MessageBox.Show("Attempting to Delete a rule (RuleHeadID/RuleBatchID/Digit): " + objPart.RuleHeadID + "/" + objPart.RuleBatchID + "/" + objPart.Digit +
            //                                           " for Part Number: " + partNumberStr + " from the ROA_PartConditionsDTL Table. Are you sure you want to do this?",
            //                                           "Deleting a Rule",
            //                                           MessageBoxButtons.YesNo);
            //    if (result1 == DialogResult.Yes)
            //    {
            //        objPart.DeleteFromROA_PartConditionsDTL();
            //        DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

            //        if (dt.Rows.Count > 0)
            //        {
            //            objPart.UpdateOAU_PartMaster(dt);
            //        }
            //        dgvPartsMain.DataSource = null;
            //        DataTable dtRules = objPart.GetOAU_PartsAndRulesList();
            //        dgvPartsMain.DataSource = dtRules;
            //        populateMainPartsList(dtRules);
            //        dgvPartsMain.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
            //        dgvPartsMain.Rows[--rowIdx].Selected = true;
            //        dgvPartsMain.FirstDisplayedScrollingRowIndex = rowIdx;
            //        dgvPartsMain.Focus();
            //    }               
            //}   
            //else
            //{
            //    curSelRowIdx = e.RowIndex;
            //}
        }

        private void btnAddPart_Click(object sender, EventArgs e)
        {
            //frmAddRule frmNew = new frmAddRule(this);           
            frmUpdate frmNew = new frmUpdate(this);
            
            DataTable dt = objPart.GetOAU_MainPartList();
            frmNew.cbPartNum.DisplayMember = "PartNum";
            frmNew.cbPartNum.ValueMember = "PartNum";
            
            DataRow dr = dt.NewRow();
            dr["ID"] = 0;
            dr["PartNum"] = "Select a Part Number.";
            dt.Rows.InsertAt(dr, 0);

            DataTable dt1 = objPart.GetModelNoDigitList();
            frmNew.cbDigit.DisplayMember = "DigitHeat";
            frmNew.cbDigit.ValueMember = "HeatType";
            frmNew.cbDigit.Enabled = true;

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "Select Digit Number Please.";
            dr1["HeatType"] = "NA";
            dt1.Rows.InsertAt(dr1, 0);

            frmNew.cbDigit.DataSource = dt1;
            frmNew.cbDigit.SelectedIndex = 0;

            frmNew.gbValues.Visible = false;
            //frmNew.lbAddMsg.Text = "";
            frmNew.lbScreenMode.Text = "NewPartMode";
            frmNew.cbPartNum.Enabled = true;
            frmNew.cbPartNum.DataSource = dt;
            frmNew.cbPartNum.SelectedIndex = 0;            
            frmNew.cbPartNum.Select();
            //frmNew.lbScreenMode.Text = "AddNewPart";
            frmNew.btnAddNewRule.Text = "Save New Rule";
            frmNew.btnUpdate.Enabled = false;
            frmNew.Text = "Add New Part";
            
            frmNew.ShowDialog();

            if (mQty != "-1")
            {
                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, null); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }

        }

        private void btnCopy_Click(object sender, EventArgs e)
        {           
            bool rfgComp = false;
            bool circuit1 = false;
            bool circuit2 = false;
            bool firstRow = true;

            int rowIdx = dgvPartsMain.CurrentRow.Index;
            int iDigit = 0;            
          
            frmUpdate frmUpd = new frmUpdate(this);
            //DataTable dt = objPart.GetPartCategoryList();

            mRuleHeadID = dgvPartsMain.Rows[rowIdx].Cells["RuleHeadID"].Value.ToString();
            mRuleBatchID = dgvPartsMain.Rows[rowIdx].Cells["RuleBatchID"].Value.ToString();
            mPartNumber = dgvPartsMain.Rows[rowIdx].Cells["PartNum"].Value.ToString();
            mPartDesc = dgvPartsMain.Rows[rowIdx].Cells["PartDescription"].Value.ToString();
            mRelatedOp = dgvPartsMain.Rows[rowIdx].Cells["RelatedOperation"].Value.ToString();
            mPartCategory = dgvPartsMain.Rows[rowIdx].Cells["PartCategory"].Value.ToString();
            mUnitType = dgvPartsMain.Rows[rowIdx].Cells["Unit"].Value.ToString();
            mDigit = dgvPartsMain.Rows[rowIdx].Cells["Digit"].Value.ToString();
            mHeatType = dgvPartsMain.Rows[rowIdx].Cells["HeatType"].Value.ToString();
            mValueStr = dgvPartsMain.Rows[rowIdx].Cells["Value"].Value.ToString();
            mQty = dgvPartsMain.Rows[rowIdx].Cells["Qty"].Value.ToString();
            rfgComp = (bool)dgvPartsMain.Rows[rowIdx].Cells["RfgComponent"].Value;
            circuit1 = (bool)dgvPartsMain.Rows[rowIdx].Cells["Circuit1"].Value;
            circuit2 = (bool)dgvPartsMain.Rows[rowIdx].Cells["Circuit2"].Value;

            objPart.PartNum = mPartNumber;
            objPart.GetOAU_PartNumberInfo();

            objPart.PartCategory = null;
            DataTable dt = objPart.GetOAU_MainPartList();
            frmUpd.cbPartNum.DisplayMember = "PartNum";
            frmUpd.cbPartNum.ValueMember = "ID";

            DataRow dr = dt.NewRow();
            dr["ID"] = 0;
            dr["PartNum"] = "Select a Part Number.";
            dt.Rows.InsertAt(dr, 0);
            frmUpd.cbPartNum.DataSource = dt;
            frmUpd.cbPartNum.SelectedIndex = 0;
            frmUpd.cbPartNum.Text = mPartNumber;
            frmUpd.cbPartNum.Enabled = true;
            //frmUpd.cbUnitType.Text = mUnitType;
            frmUpd.txtOARelOp.Text = mRelatedOp;
            frmUpd.txtPartDesc.Text = mPartDesc;

            frmUpd.txtRuleID.Text = mRuleHeadID;
            frmUpd.txtBatchID.Text = objPart.RuleBatchID.ToString();                     
           
            DataTable dt1 = objPart.GetModelNoDigitList();
            frmUpd.cbDigit.DisplayMember = "DigitHeat";
            frmUpd.cbDigit.ValueMember = "HeatType";
            frmUpd.cbDigit.DataSource = dt1;           

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "0/NA = CircuitBreaker";
            dr1["HeatType"] = "NA";
            dt1.Rows.InsertAt(dr1, 0);           

            //objPart.GetOAU_PartNumberInfo();

            frmUpd.txtPartDesc.Text = mPartDesc;
            frmUpd.txtPartCategory.Text = mPartCategory;
            frmUpd.txtRuleID.Text = mRuleHeadID.ToString();
            frmUpd.txtBatchID.Text = "00";
            frmUpd.nudReqQty.Value = decimal.Parse(mQty);            
           
            objPart.PartNum = mPartNumber;
            objPart.RuleBatchID = mRuleBatchID;
            dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();           

            foreach (DataRow dr2 in dt.Rows)
            {
                if (firstRow)
                {
                    mDigit = dr2["Digit"].ToString();
                    mHeatType = dr2["HeatType"].ToString();
                    mValueStr = dr2["Value"].ToString();
                    mQty = dr2["Qty"].ToString();
                    firstRow = false;
                }

                int index = frmUpd.dgvUpdateRules.Rows.Add();

                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleHeadID"].Value = dr2["RuleHeadID"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleBatchID"].Value = frmUpd.txtBatchID.Text;
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartNumber"].Value = dr2["PartNum"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartDesc"].Value = dr2["PartDescription"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRelatedOp"].Value = dr2["RelatedOperation"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartCat"].Value = dr2["PartCategory"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUnitType"].Value = dr2["Unit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColDigit"].Value = dr2["Digit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColHeatType"].Value = dr2["HeatType"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColValue"].Value = dr2["Value"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColQty"].Value = dr2["Qty"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUpdateType"].Value = "Insert";                              
            }

            int selIdx = objPart.getDigitSelectedIndex(mDigit, mHeatType, frmUpd.cbDigit);

            frmUpd.cbDigit.SelectedIndex = selIdx;
            iDigit = Int32.Parse(mDigit);

            frmUpd.gbValues.Enabled = true;
            frmUpd.gbValues.Visible = true;

            setupValueCheckBoxesCopyRule(frmUpd, iDigit, mValueStr, mHeatType);

            if (rfgComp == true)
            {
                frmUpd.chkBoxRfgComp.Checked = true;
                frmUpd.gbCircuits.Visible = true;
                if (circuit1 == true)
                {
                    frmUpd.chkBoxCircuit1.Checked = true;
                }
                if (circuit2 == true)
                {
                    frmUpd.chkBoxCircuit2.Checked = true;
                }
            }            

            this.btnMainExit.Enabled = false;
            frmUpd.lbScreenMode.Text = "CopyMode";
            frmUpd.btnUpdate.Enabled = false;
            frmUpd.btnAllRuleSets.Enabled = true;
            frmUpd.btnSave.Enabled = false;

            frmUpd.cbDigit.Enabled = false;

            frmUpd.Text = "Copy Rule Set";
            frmUpd.ShowDialog();

            btnMainExit.Enabled = true;
            
            if (mQty != "-1")
            {
                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, null); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }
        }

        private void btnCreateSSRules_Click(object sender, EventArgs e)
        {
            frmCreateSS_Rules frmCrt = new frmCreateSS_Rules();
            DataTable dtPart = objPart.GetVMEASM_PartsWithRules();
            frmCrt.dgvVmeParts.DataSource = dtPart;
            frmCrt.dgvVmeParts.Columns["PartNum"].Width = 135;           // Part Number
            frmCrt.dgvVmeParts.Columns["PartNum"].HeaderText = "Part Number";
            frmCrt.dgvVmeParts.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmCrt.dgvVmeParts.Columns["PartNum"].DisplayIndex = 0;
            frmCrt.dgvVmeParts.Columns["PartDescription"].Width = 400;           // Part Description
            frmCrt.dgvVmeParts.Columns["PartDescription"].HeaderText = "Part Description";
            frmCrt.dgvVmeParts.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmCrt.dgvVmeParts.Columns["PartDescription"].DisplayIndex = 1;
            frmCrt.ShowDialog();

            //Load PartConditionsDTL DataGridView
            DataTable dt = objPart.GetOAU_PartsAndRulesList();
            populateMainPartsList(dt);

        }

        #endregion

        #region Other Methods

        private void setupValueCheckBoxes(frmUpdate frmUpd, int digit, string actValueStr, string heatType, string unitType)
        {
            int idx = 0;
            string valueStr = "";
            DataTable dt;

            if (unitType.Contains("OA") == true && unitType.Contains("VKG") == true)
            {
                dt = objModel.GetR6_Viking_ModelNoValues(digit, heatType);
            }
            else if (unitType.Contains("VKG") == true)
            {
                dt = objModel.GetViking_ModelNoValues(digit, heatType);
            }
            else
            {
                dt = objModel.GetOAU_ModelNoValues(digit, heatType);
            }           

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(frmUpd.lbVal1);
            labelControls.Add(frmUpd.lbVal2);
            labelControls.Add(frmUpd.lbVal3);
            labelControls.Add(frmUpd.lbVal4);
            labelControls.Add(frmUpd.lbVal5);
            labelControls.Add(frmUpd.lbVal6);
            labelControls.Add(frmUpd.lbVal7);
            labelControls.Add(frmUpd.lbVal8);
            labelControls.Add(frmUpd.lbVal9);
            labelControls.Add(frmUpd.lbVal10);
            labelControls.Add(frmUpd.lbVal11);
            labelControls.Add(frmUpd.lbVal12);
            labelControls.Add(frmUpd.lbVal13);
            labelControls.Add(frmUpd.lbVal14);
            labelControls.Add(frmUpd.lbVal15);
            labelControls.Add(frmUpd.lbVal16);
            labelControls.Add(frmUpd.lbVal17);
            labelControls.Add(frmUpd.lbVal18);
            labelControls.Add(frmUpd.lbVal19);
            labelControls.Add(frmUpd.lbVal20);
            labelControls.Add(frmUpd.lbVal21);
            labelControls.Add(frmUpd.lbVal22);
            labelControls.Add(frmUpd.lbVal23);
            labelControls.Add(frmUpd.lbVal24);
            labelControls.Add(frmUpd.lbVal25);
            labelControls.Add(frmUpd.lbVal26);
            labelControls.Add(frmUpd.lbVal27);
            labelControls.Add(frmUpd.lbVal28);
            labelControls.Add(frmUpd.lbVal29);
            labelControls.Add(frmUpd.lbVal30);

            checkBoxControls.Add(frmUpd.cbVal1);
            checkBoxControls.Add(frmUpd.cbVal2);
            checkBoxControls.Add(frmUpd.cbVal3);
            checkBoxControls.Add(frmUpd.cbVal4);
            checkBoxControls.Add(frmUpd.cbVal5);
            checkBoxControls.Add(frmUpd.cbVal6);
            checkBoxControls.Add(frmUpd.cbVal7);
            checkBoxControls.Add(frmUpd.cbVal8);
            checkBoxControls.Add(frmUpd.cbVal9);
            checkBoxControls.Add(frmUpd.cbVal10);
            checkBoxControls.Add(frmUpd.cbVal11);
            checkBoxControls.Add(frmUpd.cbVal12);
            checkBoxControls.Add(frmUpd.cbVal13);
            checkBoxControls.Add(frmUpd.cbVal14);
            checkBoxControls.Add(frmUpd.cbVal15);
            checkBoxControls.Add(frmUpd.cbVal16);
            checkBoxControls.Add(frmUpd.cbVal17);
            checkBoxControls.Add(frmUpd.cbVal18);
            checkBoxControls.Add(frmUpd.cbVal19);
            checkBoxControls.Add(frmUpd.cbVal20);
            checkBoxControls.Add(frmUpd.cbVal21);
            checkBoxControls.Add(frmUpd.cbVal22);
            checkBoxControls.Add(frmUpd.cbVal23);
            checkBoxControls.Add(frmUpd.cbVal24);
            checkBoxControls.Add(frmUpd.cbVal25);
            checkBoxControls.Add(frmUpd.cbVal26);
            checkBoxControls.Add(frmUpd.cbVal27);
            checkBoxControls.Add(frmUpd.cbVal28);
            checkBoxControls.Add(frmUpd.cbVal29);
            checkBoxControls.Add(frmUpd.cbVal30);

            foreach (DataRow dr in dt.Rows)
            {
                valueStr = dr["DigitVal"].ToString();
                labelControls[idx].Text = valueStr;
                labelControls[idx].Visible = true;

                if (actValueStr.Contains(valueStr))
                {
                    checkBoxControls[idx].Checked = true;
                }
                checkBoxControls[idx].Visible = true;
                frmUpd.toolTip1.SetToolTip(labelControls[idx], dr["DigitDescription"].ToString());
                frmUpd.toolTip1.SetToolTip(checkBoxControls[idx++], dr["DigitDescription"].ToString());
                frmUpd.toolTip1.ShowAlways = true;
            }
        }

        private void setupValueCheckBoxesCopyRule(frmUpdate frmAdd, int digit, string actValueStr, string heatType)
        {
            int idx = 0;
            string valueStr = "";

            DataTable dt = objModel.GetModelNoValues(digit, heatType, mUnitType);

            //if (mUnitType.Contains("OA") == true && mUnitType.Contains("VKG") == true) // Wanting to change the color of the display values to indicate OA or Viking or Both.
            //{
            //    DataTable dtOA = objModel.GetModelNoValues(digit, heatType, "OA");
            //    DataTable dtVKG = objModel.GetModelNoValues(digit, heatType, "VKG");
            //}

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(frmAdd.lbVal1);
            labelControls.Add(frmAdd.lbVal2);
            labelControls.Add(frmAdd.lbVal3);
            labelControls.Add(frmAdd.lbVal4);
            labelControls.Add(frmAdd.lbVal5);
            labelControls.Add(frmAdd.lbVal6);
            labelControls.Add(frmAdd.lbVal7);
            labelControls.Add(frmAdd.lbVal8);
            labelControls.Add(frmAdd.lbVal9);
            labelControls.Add(frmAdd.lbVal10);
            labelControls.Add(frmAdd.lbVal11);
            labelControls.Add(frmAdd.lbVal12);
            labelControls.Add(frmAdd.lbVal13);
            labelControls.Add(frmAdd.lbVal14);
            labelControls.Add(frmAdd.lbVal15);
            labelControls.Add(frmAdd.lbVal16);
            labelControls.Add(frmAdd.lbVal17);
            labelControls.Add(frmAdd.lbVal18);
            labelControls.Add(frmAdd.lbVal19);
            labelControls.Add(frmAdd.lbVal20);
            labelControls.Add(frmAdd.lbVal21);
            labelControls.Add(frmAdd.lbVal22);
            labelControls.Add(frmAdd.lbVal23);
            labelControls.Add(frmAdd.lbVal24);
            labelControls.Add(frmAdd.lbVal25);
            labelControls.Add(frmAdd.lbVal26);
            labelControls.Add(frmAdd.lbVal27);
            labelControls.Add(frmAdd.lbVal28);
            labelControls.Add(frmAdd.lbVal29);
            labelControls.Add(frmAdd.lbVal30);

            checkBoxControls.Add(frmAdd.cbVal1);
            checkBoxControls.Add(frmAdd.cbVal2);
            checkBoxControls.Add(frmAdd.cbVal3);
            checkBoxControls.Add(frmAdd.cbVal4);
            checkBoxControls.Add(frmAdd.cbVal5);
            checkBoxControls.Add(frmAdd.cbVal6);
            checkBoxControls.Add(frmAdd.cbVal7);
            checkBoxControls.Add(frmAdd.cbVal8);
            checkBoxControls.Add(frmAdd.cbVal9);
            checkBoxControls.Add(frmAdd.cbVal10);
            checkBoxControls.Add(frmAdd.cbVal11);
            checkBoxControls.Add(frmAdd.cbVal12);
            checkBoxControls.Add(frmAdd.cbVal13);
            checkBoxControls.Add(frmAdd.cbVal14);
            checkBoxControls.Add(frmAdd.cbVal15);
            checkBoxControls.Add(frmAdd.cbVal16);
            checkBoxControls.Add(frmAdd.cbVal17);
            checkBoxControls.Add(frmAdd.cbVal18);
            checkBoxControls.Add(frmAdd.cbVal19);
            checkBoxControls.Add(frmAdd.cbVal20);
            checkBoxControls.Add(frmAdd.cbVal21);
            checkBoxControls.Add(frmAdd.cbVal22);
            checkBoxControls.Add(frmAdd.cbVal23);
            checkBoxControls.Add(frmAdd.cbVal24);
            checkBoxControls.Add(frmAdd.cbVal25);
            checkBoxControls.Add(frmAdd.cbVal26);
            checkBoxControls.Add(frmAdd.cbVal27);
            checkBoxControls.Add(frmAdd.cbVal28);
            checkBoxControls.Add(frmAdd.cbVal29);
            checkBoxControls.Add(frmAdd.cbVal30);

            foreach (DataRow dr in dt.Rows)
            {
                valueStr = dr["DigitVal"].ToString();
                labelControls[idx].Text = valueStr;
                labelControls[idx].Visible = true;

                if (actValueStr.Contains(valueStr))
                {
                    checkBoxControls[idx].Checked = true;
                }
                checkBoxControls[idx].Enabled = true;
                checkBoxControls[idx++].Visible = true;
            }

        }       

        private void positionDataGrid(string partNum, int? ruleBatchID)
        {
            int rowIdx = 0;

            dgvPartsMain.DataSource = null;
            DataTable dtPRL = objPart.GetOAU_PartsAndRulesList();
            dgvPartsMain.DataSource = dtPRL;
            populateMainPartsList(dtPRL);

            if (ruleBatchID != null)
            {
                foreach (DataGridViewRow row in dgvPartsMain.Rows)
                {
                    if (row.Cells["PartNum"].Value != null)
                    {
                        if (row.Cells["PartNum"].Value.ToString() == partNum)
                        {
                            rowIdx = row.Index;
                            dgvPartsMain.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                            dgvPartsMain.Rows[rowIdx].Selected = true;
                            dgvPartsMain.FirstDisplayedScrollingRowIndex = rowIdx;
                            dgvPartsMain.Focus();
                            break;
                        }
                    }
                }
            }
            else
            {
                dgvPartsMain.Rows[0].Selected = true;
            }
        }

        private void populatePartRuleHeadList(DataTable dt, bool firstPass)
        {

            dgvPartRulesHead.DataSource = dt;

            dgvPartRulesHead.Columns["PartNum"].Width = 200;           // Part Number
            dgvPartRulesHead.Columns["PartNum"].HeaderText = "Part Number";
            dgvPartRulesHead.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartNum"].DisplayIndex = 0;
            dgvPartRulesHead.Columns["PartDescription"].Width = 390;           // Part Description
            dgvPartRulesHead.Columns["PartDescription"].HeaderText = "Part Description";
            dgvPartRulesHead.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartDescription"].DisplayIndex = 1;
            dgvPartRulesHead.Columns["RelatedOperation"].Width = 60;            // Related Operation
            dgvPartRulesHead.Columns["RelatedOperation"].HeaderText = "Related\nOperation";
            dgvPartRulesHead.Columns["RelatedOperation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartRulesHead.Columns["RelatedOperation"].DisplayIndex = 2;
            dgvPartRulesHead.Columns["PartCategory"].Width = 125;           // PartCategory   
            dgvPartRulesHead.Columns["PartCategory"].HeaderText = "Part Category";
            dgvPartRulesHead.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartCategory"].DisplayIndex = 3;
            dgvPartRulesHead.Columns["AssemblySeq"].Width = 50;            // AssemblySeq
            dgvPartRulesHead.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            dgvPartRulesHead.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartRulesHead.Columns["AssemblySeq"].DisplayIndex = 4;
            dgvPartRulesHead.Columns["PartType"].Width = 100;            // PartType
            dgvPartRulesHead.Columns["PartType"].HeaderText = "Part Type";
            dgvPartRulesHead.Columns["PartType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartType"].DisplayIndex = 5;          
            dgvPartRulesHead.Columns["LinesideBin"].Width = 60;            // LinesideBin
            dgvPartRulesHead.Columns["LinesideBin"].HeaderText = "Lineside\nBin";
            dgvPartRulesHead.Columns["LinesideBin"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["LinesideBin"].DisplayIndex = 6;
            dgvPartRulesHead.Columns["Picklist"].Width = 55;            // Picklist
            dgvPartRulesHead.Columns["Picklist"].HeaderText = "Picklist";
            dgvPartRulesHead.Columns["Picklist"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["Picklist"].DisplayIndex = 7;
            dgvPartRulesHead.Columns["ConfigurablePart"].Width = 60;            // ConfigurablePart
            dgvPartRulesHead.Columns["ConfigurablePart"].HeaderText = "Config\nPart";
            dgvPartRulesHead.Columns["ConfigurablePart"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["ConfigurablePart"].DisplayIndex = 8;
            dgvPartRulesHead.Columns["UserName"].Width = 125;            // UserName
            dgvPartRulesHead.Columns["UserName"].HeaderText = "UserName";
            dgvPartRulesHead.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["UserName"].DisplayIndex = 9;
            dgvPartRulesHead.Columns["DateMod"].Width = 90;            // DateMod
            dgvPartRulesHead.Columns["DateMod"].HeaderText = "DateMod";
            dgvPartRulesHead.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartRulesHead.Columns["DateMod"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvPartRulesHead.Columns["DateMod"].DisplayIndex = 10;
            dgvPartRulesHead.Columns["StationLocation"].Visible = false;            // StationLocation
            dgvPartRulesHead.Columns["ID"].Visible = false;            // ID
          
        }
       
        private void populateMainPartsList(DataTable dt)
        {
            dgvPartsMain.DataSource = dt;
           
            dgvPartsMain.Columns["RuleBatchID"].Width = 60;            // RuleBatchID
            dgvPartsMain.Columns["RuleBatchID"].HeaderText = "Rule\nBatch ID";
            dgvPartsMain.Columns["RuleBatchID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["RuleBatchID"].DisplayIndex = 0;
            dgvPartsMain.Columns["PartNum"].Width = 100;           // Part Number
            dgvPartsMain.Columns["PartNum"].HeaderText = "Part Number";
            dgvPartsMain.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["PartNum"].DisplayIndex = 1;
            dgvPartsMain.Columns["PartDescription"].Width = 225;           // Part Description
            dgvPartsMain.Columns["PartDescription"].HeaderText = "Part Description";
            dgvPartsMain.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["PartDescription"].DisplayIndex = 2;
            dgvPartsMain.Columns["RelatedOperation"].Width = 60;            // Related Operation
            dgvPartsMain.Columns["RelatedOperation"].HeaderText = "OAU\nRelOp";
            dgvPartsMain.Columns["RelatedOperation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["RelatedOperation"].DisplayIndex = 3;            
            dgvPartsMain.Columns["PartCategory"].Width = 130;           // PartCategory   
            dgvPartsMain.Columns["PartCategory"].HeaderText = "Part Category";
            dgvPartsMain.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["PartCategory"].DisplayIndex = 4;
            dgvPartsMain.Columns["Unit"].Width = 50;            // Unit Type          
            dgvPartsMain.Columns["Unit"].HeaderText = "Unit\nType";
            dgvPartsMain.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["Unit"].DisplayIndex = 5;
            dgvPartsMain.Columns["Digit"].Width = 50;            // Digit
            dgvPartsMain.Columns["Digit"].HeaderText = "Digit";
            dgvPartsMain.Columns["Digit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["Digit"].DisplayIndex = 6;
            dgvPartsMain.Columns["HeatType"].Width = 65;            // Heat Type  
            dgvPartsMain.Columns["HeatType"].HeaderText = "HeatType";
            dgvPartsMain.Columns["HeatType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["HeatType"].DisplayIndex = 7;
            dgvPartsMain.Columns["Value"].Width = 150;            // Value  
            dgvPartsMain.Columns["Value"].HeaderText = "Value";
            dgvPartsMain.Columns["Value"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["Value"].DisplayIndex = 8;
            dgvPartsMain.Columns["Qty"].Width = 75;            // Qty
            dgvPartsMain.Columns["Qty"].HeaderText = "Quantity";
            dgvPartsMain.Columns["Qty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["Qty"].DisplayIndex = 9;
            dgvPartsMain.Columns["RfgComponent"].HeaderText = "Rfg Comp";
            dgvPartsMain.Columns["RfgComponent"].Width = 65;            // RfgComponent
            dgvPartsMain.Columns["RfgComponent"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["RfgComponent"].DisplayIndex = 10;
            dgvPartsMain.Columns["Circuit1"].HeaderText = "Circuit1";
            dgvPartsMain.Columns["Circuit1"].Width = 65;            // Circuit1
            dgvPartsMain.Columns["Circuit1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["Circuit1"].DisplayIndex = 11;
            dgvPartsMain.Columns["Circuit2"].HeaderText = "Circuit2";
            dgvPartsMain.Columns["Circuit2"].Width = 65;            // Circuit2
            dgvPartsMain.Columns["Circuit2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["Circuit2"].DisplayIndex = 12;
            dgvPartsMain.Columns["SubAssemblyPart"].HeaderText = "SubAsm\nPart";
            dgvPartsMain.Columns["SubAssemblyPart"].Width = 65;            // SubAssemblyPart
            dgvPartsMain.Columns["SubAssemblyPart"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["SubAssemblyPart"].DisplayIndex = 13;
            dgvPartsMain.Columns["Comments"].Visible = false;
            dgvPartsMain.Columns["SubAsmMtlPart"].Visible = false;
            dgvPartsMain.Columns["ImmediateParent"].Visible = false;
            dgvPartsMain.Columns["RuleHeadID"].Visible = false;
            dgvPartsMain.Columns["DateMod"].Visible = false;
            dgvPartsMain.Columns["UserName"].Visible = false;            
        }

        private void populateModelNoList(DataTable dt, bool firstPass)
        {
            dgvModelNo.DataSource = dt;

            if (firstPass)
            {
                DataGridViewButtonColumn AddButtonCell = new DataGridViewButtonColumn();
                AddButtonCell.Name = "Add";
                AddButtonCell.HeaderText = "Add";
                AddButtonCell.Text = "Add";
                AddButtonCell.UseColumnTextForButtonValue = true;
                dgvModelNo.Columns.Insert(0, AddButtonCell);
            }

            dgvModelNo.Columns["Add"].Width = 60;            // Add Button
            dgvModelNo.Columns["Add"].DisplayIndex = 0;
            dgvModelNo.Columns["DigitNo"].Width = 50;            // DigitNo
            dgvModelNo.Columns["DigitNo"].HeaderText = "Digit No";
            dgvModelNo.Columns["DigitNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvModelNo.Columns["DigitNo"].DisplayIndex = 1;
            dgvModelNo.Columns["DigitVal"].Width = 50;            // Digit Value
            dgvModelNo.Columns["DigitVal"].HeaderText = "Digit\nValue";
            dgvModelNo.Columns["DigitVal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitVal"].DisplayIndex = 2;
            dgvModelNo.Columns["DigitDescription"].Width = 250;           // Digit Description
            dgvModelNo.Columns["DigitDescription"].HeaderText = "Digit Description";
            dgvModelNo.Columns["DigitDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitDescription"].DisplayIndex = 3;
            dgvModelNo.Columns["HeatType"].Width = 75;           //Heat Type
            dgvModelNo.Columns["HeatType"].HeaderText = "Heat Type";
            dgvModelNo.Columns["HeatType"].DisplayIndex = 4;
            dgvModelNo.Columns["DigitRepresentation"].Width = 200;            //DigitRepresentation
            dgvModelNo.Columns["DigitRepresentation"].HeaderText = "Digit Representation";
            dgvModelNo.Columns["DigitRepresentation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitRepresentation"].DisplayIndex = 5;           
            dgvModelNo.Columns["ID"].Visible = false;
            dgvModelNo.Columns["RowIndex"].Visible = false;
            dgvModelNo.Columns["DigitValDesc"].Visible = false; 

            if (firstPass)
            {
                DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
                DelButtonCell.Name = "Del";
                DelButtonCell.HeaderText = "Del";
                DelButtonCell.Text = "Del";
                DelButtonCell.UseColumnTextForButtonValue = true;
                dgvModelNo.Columns.Insert(6, DelButtonCell);
                dgvModelNo.Columns["Del"].Width = 60;            // Delete Button
                dgvModelNo.Columns["Del"].DisplayIndex = 6;
            }
            //dgvModelNo.Columns["ID"].Visible = false;

        }

        private void populateCompressorDetailList(DataTable dt, bool firstPass)
        {
            dgvCompDetail.DataSource = dt;

            //if (firstPass)
            //{
            //    DataGridViewButtonColumn AddButtonCell = new DataGridViewButtonColumn();
            //    AddButtonCell.Name = "Add";
            //    AddButtonCell.HeaderText = "Add";
            //    AddButtonCell.Text = "Add";
            //    AddButtonCell.UseColumnTextForButtonValue = true;
            //    dgvCompDetail.Columns.Insert(0, AddButtonCell);
            //}

            //dgvCompDetail.Columns["Add"].Width = 60;            // Add Button
            //dgvCompDetail.Columns["Add"].DisplayIndex = 0;
            dgvCompDetail.Columns["RuleHeadID"].Width = 60;            // RuleHeadID
            dgvCompDetail.Columns["RuleHeadID"].HeaderText = "Rule\nHead ID";
            dgvCompDetail.Columns["RuleHeadID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCompDetail.Columns["RuleHeadID"].DisplayIndex = 0;
            dgvCompDetail.Columns["PartNum"].Width = 150;            // Part Number
            dgvCompDetail.Columns["PartNum"].HeaderText = "Part Number";
            dgvCompDetail.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCompDetail.Columns["PartNum"].DisplayIndex = 1;
            dgvCompDetail.Columns["PartDescription"].Width = 300;            // Part Description
            dgvCompDetail.Columns["PartDescription"].HeaderText = "Part Description";
            dgvCompDetail.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;            
            dgvCompDetail.Columns["PartDescription"].DisplayIndex = 2;
            dgvCompDetail.Columns["PartCategory"].Width = 125;           // PartCategory
            dgvCompDetail.Columns["PartCategory"].HeaderText = "Part Category";
            dgvCompDetail.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCompDetail.Columns["PartCategory"].DisplayIndex = 3;
            dgvCompDetail.Columns["Voltage"].Width = 60;           // Voltage
            dgvCompDetail.Columns["Voltage"].HeaderText = "Voltage";
            dgvCompDetail.Columns["Voltage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompDetail.Columns["Voltage"].DisplayIndex = 4;
            dgvCompDetail.Columns["Phase"].Width = 50;           //Phase
            dgvCompDetail.Columns["Phase"].HeaderText = "Phase";            
            dgvCompDetail.Columns["Phase"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompDetail.Columns["Phase"].DisplayIndex = 5;
            dgvCompDetail.Columns["RLA"].Width = 60;           // RLA
            dgvCompDetail.Columns["RLA"].HeaderText = "RLA";
            dgvCompDetail.Columns["RLA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCompDetail.Columns["RLA"].DisplayIndex = 6;
            dgvCompDetail.Columns["LRA"].Width = 60;           // LRA
            dgvCompDetail.Columns["LRA"].HeaderText = "LRA";
            dgvCompDetail.Columns["LRA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCompDetail.Columns["LRA"].DisplayIndex = 7;
            dgvCompDetail.Columns["UserName"].Width = 100;            //UserName
            dgvCompDetail.Columns["UserName"].HeaderText = "UserName";
            dgvCompDetail.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCompDetail.Columns["UserName"].DisplayIndex = 8;
            dgvCompDetail.Columns["DateMod"].Width = 150;           // Date Modified  
            dgvCompDetail.Columns["DateMod"].HeaderText = "Date Modified";
            dgvCompDetail.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCompDetail.Columns["DateMod"].DisplayIndex = 9;
            dgvCompDetail.Columns["ID"].Width = 75;            // ID         
            dgvCompDetail.Columns["ID"].HeaderText = "ID";
            dgvCompDetail.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCompDetail.Columns["ID"].DisplayIndex = 10;           

            if (firstPass)
            {
                DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
                DelButtonCell.Name = "Del";
                DelButtonCell.HeaderText = "Del";
                DelButtonCell.Text = "Del";
                DelButtonCell.UseColumnTextForButtonValue = true;
                dgvCompDetail.Columns.Insert(11, DelButtonCell);
                dgvCompDetail.Columns["Del"].Width = 60;            // Delete Button
                dgvCompDetail.Columns["Del"].DisplayIndex = 11;
            }
        }

        //private void populateCompressorChargeList(DataTable dt, bool firstPass)
        //{

        //    dgvCompChargeData.DataSource = dt;


        //    //if (firstPass)
        //    //{
        //    //    DataGridViewButtonColumn AddButtonCell = new DataGridViewButtonColumn();
        //    //    AddButtonCell.Name = "Add";
        //    //    AddButtonCell.HeaderText = "Add";
        //    //    AddButtonCell.Text = "Add";
        //    //    AddButtonCell.UseColumnTextForButtonValue = true;
        //    //    dgvCompChargeData.Columns.Insert(0, AddButtonCell);
        //    //}

        //    //dgvCompChargeData.Columns["Add"].Width = 40;            // Add Button
        //    //dgvCompChargeData.Columns["Add"].DisplayIndex = 0;
        //    dgvCompChargeData.Columns["UnitModel"].Width = 60;            // Unit Model
        //    dgvCompChargeData.Columns["UnitModel"].HeaderText = "Unit Model";
        //    dgvCompChargeData.Columns["UnitModel"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompChargeData.Columns["UnitModel"].DisplayIndex = 0;
        //    dgvCompChargeData.Columns["CoolingCap"].Width = 75;           // Cooling Cap
        //    dgvCompChargeData.Columns["CoolingCap"].HeaderText = "Cooling\nCapacity";
        //    dgvCompChargeData.Columns["CoolingCap"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvCompChargeData.Columns["CoolingCap"].DisplayIndex = 1;
        //    dgvCompChargeData.Columns["HeatType"].Width = 75;           // heat Type
        //    dgvCompChargeData.Columns["HeatType"].HeaderText = "Heat Type";
        //    dgvCompChargeData.Columns["HeatType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompChargeData.Columns["HeatType"].DisplayIndex = 2;
        //    dgvCompChargeData.Columns["Circuit1"].Width = 75;            // Circuit 1 Charge
        //    dgvCompChargeData.Columns["Circuit1"].HeaderText = "Circuit 1";
        //    dgvCompChargeData.Columns["Circuit1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompChargeData.Columns["Circuit1"].DisplayIndex = 3;
        //    dgvCompChargeData.Columns["Circuit1_Reheat"].Width = 75;           // Circuit 1 Charge Reheat 
        //    dgvCompChargeData.Columns["Circuit1_Reheat"].HeaderText = "Circuit1\nReheat";
        //    dgvCompChargeData.Columns["Circuit1_Reheat"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompChargeData.Columns["Circuit1_Reheat"].DisplayIndex = 4;            
        //    dgvCompChargeData.Columns["Circuit2"].Width = 75;            // Circuit 2          
        //    dgvCompChargeData.Columns["Circuit2"].HeaderText = "Circuit 2";
        //    dgvCompChargeData.Columns["Circuit2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompChargeData.Columns["Circuit2"].DisplayIndex = 5;
        //    dgvCompChargeData.Columns["Circuit2_Reheat"].Width = 75;           // Circuit 2 Charge Reheat 
        //    dgvCompChargeData.Columns["Circuit2_Reheat"].HeaderText = "Circuit2\nReheat";
        //    dgvCompChargeData.Columns["Circuit2_Reheat"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompChargeData.Columns["Circuit2_Reheat"].DisplayIndex = 6;            
        //    dgvCompChargeData.Columns["ModifiedBy"].Width = 150;            // Modified By
        //    dgvCompChargeData.Columns["ModifiedBy"].HeaderText = "Modified By";
        //    dgvCompChargeData.Columns["ModifiedBy"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompChargeData.Columns["ModifiedBy"].DisplayIndex = 7;
        //    dgvCompChargeData.Columns["DateMod"].Width = 150;            // Modified By
        //    dgvCompChargeData.Columns["DateMod"].HeaderText = "Date Modified";
        //    dgvCompChargeData.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompChargeData.Columns["DateMod"].DisplayIndex = 8;
        //    dgvCompChargeData.Columns["ID"].Width = 60;            // ID
        //    dgvCompChargeData.Columns["ID"].HeaderText = "ID";
        //    dgvCompChargeData.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompChargeData.Columns["ID"].DisplayIndex = 9;
        //    dgvCompChargeData.Columns["ID"].Visible = false;

        //    if (firstPass)
        //    {
        //        DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
        //        DelButtonCell.Name = "Del";
        //        DelButtonCell.HeaderText = "Del";
        //        DelButtonCell.Text = "Del";
        //        DelButtonCell.UseColumnTextForButtonValue = true;
        //        dgvCompChargeData.Columns.Insert(10, DelButtonCell);
        //        dgvCompChargeData.Columns["Del"].Width = 40;            // Delete Button
        //        dgvCompChargeData.Columns["Del"].DisplayIndex = 10;
        //    }
        //}

        private void populateMotorDataList(DataTable dt, bool firstPass)
        {
            dgvMotorData.DataSource = dt;
           
            dgvMotorData.Columns["RuleHeadID"].Width = 60;            // RuleHeadID
            dgvMotorData.Columns["RuleHeadID"].HeaderText = "Rule\nHead ID";
            dgvMotorData.Columns["RuleHeadID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvMotorData.Columns["RuleHeadID"].DisplayIndex = 0;
            dgvMotorData.Columns["PartNum"].Width = 100;            // Part Number
            dgvMotorData.Columns["PartNum"].HeaderText = "Part Number";
            dgvMotorData.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvMotorData.Columns["PartNum"].DisplayIndex = 1;
            dgvMotorData.Columns["PartDescription"].Width = 250;            // Part Description
            dgvMotorData.Columns["PartDescription"].HeaderText = "Part Description";
            dgvMotorData.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvMotorData.Columns["PartDescription"].DisplayIndex = 2;
            dgvMotorData.Columns["MotorType"].Width = 80;           // MotorType
            dgvMotorData.Columns["MotorType"].HeaderText = "Motor Type";
            dgvMotorData.Columns["MotorType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvMotorData.Columns["MotorType"].DisplayIndex = 3;
            dgvMotorData.Columns["FLA"].Width = 60;           // FLA
            dgvMotorData.Columns["FLA"].HeaderText = "FLA";
            dgvMotorData.Columns["FLA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvMotorData.Columns["FLA"].DisplayIndex = 4;
            dgvMotorData.Columns["Hertz"].Width = 50;           // Hertz
            dgvMotorData.Columns["Hertz"].HeaderText = "Hertz";
            dgvMotorData.Columns["Hertz"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvMotorData.Columns["Hertz"].DisplayIndex = 5;
            dgvMotorData.Columns["Phase"].Width = 50;           //Phase
            dgvMotorData.Columns["Phase"].HeaderText = "Phase";
            dgvMotorData.Columns["Phase"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvMotorData.Columns["Phase"].DisplayIndex = 6;
            dgvMotorData.Columns["MCC"].Width = 40;           // MCC
            dgvMotorData.Columns["MCC"].HeaderText = "MCC";
            dgvMotorData.Columns["MCC"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvMotorData.Columns["MCC"].DisplayIndex = 7;
            dgvMotorData.Columns["RLA"].Width = 40;           // RLA
            dgvMotorData.Columns["RLA"].HeaderText = "RLA";
            dgvMotorData.Columns["RLA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvMotorData.Columns["RLA"].DisplayIndex = 8;
            dgvMotorData.Columns["Voltage"].Width = 90;            //Voltage
            dgvMotorData.Columns["Voltage"].HeaderText = "Voltage";
            dgvMotorData.Columns["Voltage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvMotorData.Columns["Voltage"].DisplayIndex = 9;
            dgvMotorData.Columns["HP"].Width = 60;           // HP
            dgvMotorData.Columns["HP"].HeaderText = "HP";
            dgvMotorData.Columns["HP"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvMotorData.Columns["HP"].DisplayIndex = 10;
            dgvMotorData.Columns["UserName"].Width = 100;            //UserName
            dgvMotorData.Columns["UserName"].HeaderText = "UserName";
            dgvMotorData.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvMotorData.Columns["UserName"].DisplayIndex = 11;
            dgvMotorData.Columns["DateMod"].Width = 150;           // Date Modified  
            dgvMotorData.Columns["DateMod"].HeaderText = "Date Modified";
            dgvMotorData.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvMotorData.Columns["DateMod"].DisplayIndex = 12;
            dgvMotorData.Columns["ID"].Visible = false;

            if (firstPass)
            {
                DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
                DelButtonCell.Name = "Del";
                DelButtonCell.HeaderText = "Del";
                DelButtonCell.Text = "Del";
                DelButtonCell.UseColumnTextForButtonValue = true;
                dgvMotorData.Columns.Insert(14, DelButtonCell);
                dgvMotorData.Columns["Del"].Width = 50;            // Delete Button
                dgvMotorData.Columns["Del"].DisplayIndex = 14;
            }
        }

        private void populateFurnaceDataList(DataTable dt, bool firstPass)
        {
            dgvFurnaceData.DataSource = dt;

            dgvFurnaceData.Columns["RuleHeadID"].Width = 60;            // RuleHeadID
            dgvFurnaceData.Columns["RuleHeadID"].HeaderText = "Rule\nHead ID";
            dgvFurnaceData.Columns["RuleHeadID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvFurnaceData.Columns["RuleHeadID"].DisplayIndex = 0;
            dgvFurnaceData.Columns["PartNum"].Width = 100;            // Part Number
            dgvFurnaceData.Columns["PartNum"].HeaderText = "Part Number";
            dgvFurnaceData.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvFurnaceData.Columns["PartNum"].DisplayIndex = 1;
            dgvFurnaceData.Columns["PartDescription"].Width = 250;            // Part Description
            dgvFurnaceData.Columns["PartDescription"].HeaderText = "Part Description";
            dgvFurnaceData.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvFurnaceData.Columns["PartDescription"].DisplayIndex = 2;
            dgvFurnaceData.Columns["Amps"].Width = 60;           // Amps
            dgvFurnaceData.Columns["Amps"].HeaderText = "Amps";
            dgvFurnaceData.Columns["Amps"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvFurnaceData.Columns["Amps"].DisplayIndex = 3;
            dgvFurnaceData.Columns["Volts"].Width = 70;           // Volts
            dgvFurnaceData.Columns["Volts"].HeaderText = "Volts";
            dgvFurnaceData.Columns["Volts"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvFurnaceData.Columns["Volts"].DisplayIndex = 4;
            dgvFurnaceData.Columns["BurnerRatio"].Width = 75;           // BurnerRation
            dgvFurnaceData.Columns["BurnerRatio"].HeaderText = "Burner Ratio";
            dgvFurnaceData.Columns["BurnerRatio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvFurnaceData.Columns["BurnerRatio"].DisplayIndex = 5;
            dgvFurnaceData.Columns["Phase"].Width = 50;           //Phase
            dgvFurnaceData.Columns["Phase"].HeaderText = "Phase";
            dgvFurnaceData.Columns["Phase"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvFurnaceData.Columns["Phase"].DisplayIndex = 6;
            dgvFurnaceData.Columns["FuelType"].Width = 80;           // FuelType
            dgvFurnaceData.Columns["FuelType"].HeaderText = "Fuel Type";
            dgvFurnaceData.Columns["FuelType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvFurnaceData.Columns["FuelType"].DisplayIndex = 7;
            dgvFurnaceData.Columns["MBHkW"].Width = 75;           // MBHkW
            dgvFurnaceData.Columns["MBHkW"].HeaderText = "MBH/KW";
            dgvFurnaceData.Columns["MBHkW"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvFurnaceData.Columns["MBHkW"].DisplayIndex = 8;
            dgvFurnaceData.Columns["UserName"].Width = 100;            //UserName
            dgvFurnaceData.Columns["UserName"].HeaderText = "UserName";
            dgvFurnaceData.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvFurnaceData.Columns["UserName"].DisplayIndex = 9;
            dgvFurnaceData.Columns["DateMod"].Width = 150;           // Date Modified  
            dgvFurnaceData.Columns["DateMod"].HeaderText = "Date Modified";
            dgvFurnaceData.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvFurnaceData.Columns["DateMod"].DisplayIndex = 10;
            dgvFurnaceData.Columns["ID"].Visible = false;

            if (firstPass)
            {
                DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
                DelButtonCell.Name = "Del";
                DelButtonCell.HeaderText = "Del";
                DelButtonCell.Text = "Del";
                DelButtonCell.UseColumnTextForButtonValue = true;
                dgvFurnaceData.Columns.Insert(12, DelButtonCell);
                dgvFurnaceData.Columns["Del"].Width = 50;            // Delete Button
                dgvFurnaceData.Columns["Del"].DisplayIndex = 12;
            }
        }

        private void populatePartCategoryDataList(DataTable dt, bool firstPass)
        {
            dgvPartCat.DataSource = dt;

            dgvPartCat.Columns["CategoryName"].Width = 150;            // CategoryName
            dgvPartCat.Columns["CategoryName"].HeaderText = "Category Name";
            dgvPartCat.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["CategoryName"].DisplayIndex = 0;
            dgvPartCat.Columns["CategoryDesc"].Width = 200;            // CategoryDesc
            dgvPartCat.Columns["CategoryDesc"].HeaderText = "Category Desc";
            dgvPartCat.Columns["CategoryDesc"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["CategoryDesc"].DisplayIndex = 1;
            dgvPartCat.Columns["PullOrder"].Width = 45;            // Pull Order
            dgvPartCat.Columns["PullOrder"].HeaderText = "Pull\nOrder";
            dgvPartCat.Columns["PullOrder"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartCat.Columns["PullOrder"].DisplayIndex = 2;
            dgvPartCat.Columns["CriticalComp"].Width = 70;            //CriticalComponent
            dgvPartCat.Columns["CriticalComp"].HeaderText = "Critical\nComponent";
            dgvPartCat.Columns["CriticalComp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartCat.Columns["CriticalComp"].DisplayIndex = 3;
            dgvPartCat.Columns["UserName"].Width = 100;            //UserName
            dgvPartCat.Columns["UserName"].HeaderText = "UserName";
            dgvPartCat.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["UserName"].DisplayIndex = 4;
            dgvPartCat.Columns["DateMod"].Width = 125;           // Date Modified  
            dgvPartCat.Columns["DateMod"].HeaderText = "Date Modified";
            dgvPartCat.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["DateMod"].DisplayIndex = 5;                    
            dgvPartCat.Columns["ID"].Visible = false;

            if (firstPass)
            {
                DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
                DelButtonCell.Name = "Del";
                DelButtonCell.HeaderText = "Del";
                DelButtonCell.Text = "Del";
                DelButtonCell.UseColumnTextForButtonValue = true;
                dgvPartCat.Columns.Insert(6, DelButtonCell);
                dgvPartCat.Columns["Del"].Width = 50;            // Delete Button
                dgvPartCat.Columns["Del"].DisplayIndex = 6;
            }
        }

        private void populateCompressorCircuitChargeList(DataTable dt, bool firstPass)             
        {

            dgvCompCircuitData.DataSource = dt;
          
            dgvCompCircuitData.Columns["Digit3"].Width = 40;            // Digit3
            dgvCompCircuitData.Columns["Digit3"].HeaderText = "Digit3";
            dgvCompCircuitData.Columns["Digit3"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Digit3"].DisplayIndex = 0;
            dgvCompCircuitData.Columns["Digit4"].Width = 40;           // Digit3
            dgvCompCircuitData.Columns["Digit4"].HeaderText = "Digit4";
            dgvCompCircuitData.Columns["Digit4"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Digit4"].DisplayIndex = 1;
            dgvCompCircuitData.Columns["Digit567"].Width = 50;           // Digit567
            dgvCompCircuitData.Columns["Digit567"].HeaderText = "Digit567";
            dgvCompCircuitData.Columns["Digit567"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Digit567"].DisplayIndex = 2;
            dgvCompCircuitData.Columns["Digit9"].Width = 40;           // Digit9
            dgvCompCircuitData.Columns["Digit9"].HeaderText = "Digit9";
            dgvCompCircuitData.Columns["Digit9"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Digit9"].DisplayIndex = 3;
            dgvCompCircuitData.Columns["Digit11"].Width = 45;           // Digit11
            dgvCompCircuitData.Columns["Digit11"].HeaderText = "Digit11";
            dgvCompCircuitData.Columns["Digit11"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Digit11"].DisplayIndex = 4;
            dgvCompCircuitData.Columns["Digit12"].Width = 45;           // Digit12
            dgvCompCircuitData.Columns["Digit12"].HeaderText = "Digit12";
            dgvCompCircuitData.Columns["Digit12"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Digit12"].DisplayIndex = 5;
            dgvCompCircuitData.Columns["Digit13"].Width = 45;           // Digit13
            dgvCompCircuitData.Columns["Digit13"].HeaderText = "Digit13";
            dgvCompCircuitData.Columns["Digit13"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Digit13"].DisplayIndex = 6;
            dgvCompCircuitData.Columns["Digit14"].Width = 45;           // Digit14
            dgvCompCircuitData.Columns["Digit14"].HeaderText = "Digit14";
            dgvCompCircuitData.Columns["Digit14"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Digit14"].DisplayIndex = 7;
            dgvCompCircuitData.Columns["Circuit1_1_Compressor"].Width = 85;           // Circuit1_1_Compressor
            dgvCompCircuitData.Columns["Circuit1_1_Compressor"].HeaderText = "Comp 1-1";
            dgvCompCircuitData.Columns["Circuit1_1_Compressor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Circuit1_1_Compressor"].DisplayIndex = 8;
            dgvCompCircuitData.Columns["Circuit1_2_Compressor"].Width = 85;           // Circuit1_2_Compressor
            dgvCompCircuitData.Columns["Circuit1_2_Compressor"].HeaderText = "Comp 1-2";
            dgvCompCircuitData.Columns["Circuit1_2_Compressor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Circuit1_2_Compressor"].DisplayIndex = 9;
            dgvCompCircuitData.Columns["Circuit1_3_Compressor"].Width = 85;           // Circuit1_3_Compressor
            dgvCompCircuitData.Columns["Circuit1_3_Compressor"].HeaderText = "Comp 1-3";
            dgvCompCircuitData.Columns["Circuit1_3_Compressor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Circuit1_3_Compressor"].DisplayIndex = 10;
            dgvCompCircuitData.Columns["Circuit2_1_Compressor"].Width = 85;           // Circuit1_1_Compressor
            dgvCompCircuitData.Columns["Circuit2_1_Compressor"].HeaderText = "Comp 2-1";
            dgvCompCircuitData.Columns["Circuit2_1_Compressor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Circuit2_1_Compressor"].DisplayIndex = 11;
            dgvCompCircuitData.Columns["Circuit2_2_Compressor"].Width = 85;           // Circuit1_2_Compressor
            dgvCompCircuitData.Columns["Circuit2_2_Compressor"].HeaderText = "Comp 2-2";
            dgvCompCircuitData.Columns["Circuit2_2_Compressor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Circuit2_2_Compressor"].DisplayIndex = 12;
            dgvCompCircuitData.Columns["Circuit2_3_Compressor"].Width = 85;           // Circuit1_3_Compressor
            dgvCompCircuitData.Columns["Circuit2_3_Compressor"].HeaderText = "Comp 2-3";
            dgvCompCircuitData.Columns["Circuit2_3_Compressor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["Circuit2_3_Compressor"].DisplayIndex = 13;
            dgvCompCircuitData.Columns["Circuit1_Charge"].Width = 70;            // Circuit 1 Charge
            dgvCompCircuitData.Columns["Circuit1_Charge"].HeaderText = "Circuit1\nCharge";
            dgvCompCircuitData.Columns["Circuit1_Charge"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCompCircuitData.Columns["Circuit1_Charge"].DisplayIndex = 14;
            dgvCompCircuitData.Columns["Circuit1_ReheatCharge"].Width = 70;           // Circuit 1 Charge Reheat 
            dgvCompCircuitData.Columns["Circuit1_ReheatCharge"].HeaderText = "Circuit1\nReheat";
            dgvCompCircuitData.Columns["Circuit1_ReheatCharge"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCompCircuitData.Columns["Circuit1_ReheatCharge"].DisplayIndex = 15;
            dgvCompCircuitData.Columns["Circuit2_Charge"].Width = 70;            // Circuit 2          
            dgvCompCircuitData.Columns["Circuit2_Charge"].HeaderText = "Circuit2\nCharge";
            dgvCompCircuitData.Columns["Circuit2_Charge"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCompCircuitData.Columns["Circuit2_Charge"].DisplayIndex = 16;
            dgvCompCircuitData.Columns["Circuit2_ReheatCharge"].Width = 70;           // Circuit 2 Charge Reheat 
            dgvCompCircuitData.Columns["Circuit2_ReheatCharge"].HeaderText = "Circuit2\nReheat";
            dgvCompCircuitData.Columns["Circuit2_ReheatCharge"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCompCircuitData.Columns["Circuit2_ReheatCharge"].DisplayIndex = 17;                        
            dgvCompCircuitData.Columns["DateMod"].Width = 125;            // Modified Date
            dgvCompCircuitData.Columns["DateMod"].HeaderText = "Date Modified";
            dgvCompCircuitData.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCompCircuitData.Columns["DateMod"].DisplayIndex = 18;
            dgvCompCircuitData.Columns["ModifiedBy"].Width = 110;            // Modified By
            dgvCompCircuitData.Columns["ModifiedBy"].Visible = false;
            dgvCompCircuitData.Columns["ID"].Width = 60;            // ID                    
            dgvCompCircuitData.Columns["ID"].Visible = false;

            if (firstPass)
            {
                DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
                DelButtonCell.Name = "Del";
                DelButtonCell.HeaderText = "Del";
                DelButtonCell.Text = "Del";
                DelButtonCell.UseColumnTextForButtonValue = true;
                dgvCompCircuitData.Columns.Insert(19, DelButtonCell);
                dgvCompCircuitData.Columns["Del"].Width = 40;            // Delete Button
                dgvCompCircuitData.Columns["Del"].DisplayIndex = 19;
            }
        }

        private void populateLineSizeList(DataTable dt, bool firstPass)
        {

            dgvLineSize.DataSource = dt;

            dgvLineSize.Columns["Digit3"].Width = 40;            // Digit3
            dgvLineSize.Columns["Digit3"].HeaderText = "Digit3";
            dgvLineSize.Columns["Digit3"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Digit3"].DisplayIndex = 0;
            dgvLineSize.Columns["Digit4"].Width = 40;           // Digit3
            dgvLineSize.Columns["Digit4"].HeaderText = "Digit4";
            dgvLineSize.Columns["Digit4"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Digit4"].DisplayIndex = 1;
            dgvLineSize.Columns["CoolingCap"].Width = 50;           // Digit567
            dgvLineSize.Columns["CoolingCap"].HeaderText = "Digit567";
            dgvLineSize.Columns["CoolingCap"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["CoolingCap"].DisplayIndex = 2;
            dgvLineSize.Columns["Digit11"].Width = 45;           // Digit11
            dgvLineSize.Columns["Digit11"].HeaderText = "Digit11";
            dgvLineSize.Columns["Digit11"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Digit11"].DisplayIndex = 3;
            dgvLineSize.Columns["Circuit1Suction"].Width = 85;           // Circuit1Suction
            dgvLineSize.Columns["Circuit1Suction"].HeaderText = "Circuit1\nSuction";
            dgvLineSize.Columns["Circuit1Suction"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Circuit1Suction"].DisplayIndex = 4;
            dgvLineSize.Columns["Circuit1LiquidLine"].Width = 85;           // Circuit1LiquidLine
            dgvLineSize.Columns["Circuit1LiquidLine"].HeaderText = "Circuit1\nLiquidLine";
            dgvLineSize.Columns["Circuit1LiquidLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Circuit1LiquidLine"].DisplayIndex = 5;
            dgvLineSize.Columns["Circuit1DischargeLine"].Width = 85;           // Circuit1DischargeLine
            dgvLineSize.Columns["Circuit1DischargeLine"].HeaderText = "Circuit1\nDischargeLine";
            dgvLineSize.Columns["Circuit1DischargeLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Circuit1DischargeLine"].DisplayIndex = 6;
            dgvLineSize.Columns["Circuit2Suction"].Width = 85;           // Circuit2Suction
            dgvLineSize.Columns["Circuit2Suction"].HeaderText = "Circuit2\nSuction";
            dgvLineSize.Columns["Circuit2Suction"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Circuit2Suction"].DisplayIndex = 7;
            dgvLineSize.Columns["Circuit2LiquidLine"].Width = 85;           // Circuit2LiquidLine
            dgvLineSize.Columns["Circuit2LiquidLine"].HeaderText = "Circuit2\nLiquidLine";
            dgvLineSize.Columns["Circuit2LiquidLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Circuit2LiquidLine"].DisplayIndex = 8;
            dgvLineSize.Columns["Circuit2DischargeLine"].Width = 85;           // Circuit2DischargeLine
            dgvLineSize.Columns["Circuit2DischargeLine"].HeaderText = "Circuit2\nDischargeLine";
            dgvLineSize.Columns["Circuit2DischargeLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["Circuit2DischargeLine"].DisplayIndex = 9;
            dgvLineSize.Columns["ModifiedDate"].Width = 125;            // Modified Date
            dgvLineSize.Columns["ModifiedDate"].HeaderText = "Date Modified";
            dgvLineSize.Columns["ModifiedDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLineSize.Columns["ModifiedDate"].DisplayIndex = 10;
            dgvLineSize.Columns["ModifiedBy"].Width = 110;            // Modified By
            dgvLineSize.Columns["ModifiedBy"].HeaderText = "ModifiedBy";
            dgvLineSize.Columns["ModifiedBy"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvLineSize.Columns["ModifiedBy"].DisplayIndex = 11;
            dgvLineSize.Columns["ID"].Visible = false;
            //dgvLineSize.Columns["ID"].Width = 50;            // ID
            //dgvLineSize.Columns["ID"].HeaderText = "ID";
            //dgvLineSize.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dgvLineSize.Columns["ID"].DisplayIndex = 12;            

            if (firstPass)
            {
                DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
                DelButtonCell.Name = "Del";
                DelButtonCell.HeaderText = "Del";
                DelButtonCell.Text = "Del";
                DelButtonCell.UseColumnTextForButtonValue = true;
                dgvLineSize.Columns.Insert(12, DelButtonCell);
                dgvLineSize.Columns["Del"].Width = 40;            // Delete Button
                dgvLineSize.Columns["Del"].DisplayIndex = 12;
            }
            
            
        }

        private string convertKeyValue(int keyValue)
        {
            string retVal = "";
            // This function converts the integer KeyValue passed into it to its string equivalent.

            switch(keyValue)
            {
                case 48:
                case 96:
                    retVal = "0";
                    break;
                case 49:
                case 97:
                    retVal = "1";
                    break;
                case 50:
                case 98:
                    retVal = "2";
                    break;
                case 51:
                case 99:
                    retVal = "3";
                    break;
                case 52:
                case 100:
                    retVal = "4";
                    break;
                case 53:
                case 101:
                    retVal = "5";
                    break;
                case 54:
                case 102:
                    retVal = "6";
                    break;
                case 55:
                case 103:
                    retVal = "7";
                    break;
                case 56:
                case 104:
                    retVal = "8";
                    break;
                case 57:
                case 105:
                    retVal = "9";
                    break;
                case 65:               
                    retVal = "a";
                    break;               
                case 66:
                    retVal = "b";
                    break;
                case 67:
                    retVal = "c";
                    break;
                case 68:
                    retVal = "d";
                    break;
                case 69:
                    retVal = "e";
                    break;
                case 70:
                    retVal = "f";
                    break;
                case 71:
                    retVal = "g";
                    break;
                case 72:
                    retVal = "h";
                    break;
                case 73:
                    retVal = "i";
                    break;
                case 74:
                    retVal = "j";
                    break;
                case 75:
                    retVal = "k";
                    break;
                case 76:
                    retVal = "l";
                    break;
                case 77:
                    retVal = "m";
                    break;
                case 78:
                    retVal = "n";
                    break;
                case 79:
                    retVal = "o";
                    break;
                case 80:
                    retVal = "p";
                    break;
                case 81:
                    retVal = "q";
                    break;
                case 82:
                    retVal = "r";
                    break;
                case 83:
                    retVal = "s";
                    break;
                case 84:
                    retVal = "t";
                    break;
                case 85:
                    retVal = "u";
                    break;
                case 86:
                    retVal = "v";
                    break;
                case 87:
                    retVal = "w";
                    break;
                case 88:
                    retVal = "x";
                    break;
                case 89:
                    retVal = "y";
                    break;
                case 90:
                    retVal = "z";
                    break;
                default:
                    retVal = "";
                    break;
            }           

            return retVal;
        }

        private void txtSearchPartNum_KeyUp(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtSearchPartNum.Text.ToUpper();
                if (tmpStr.Length >= 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow row in dgvPartsMain.Rows)
                    {
                        if (row.Cells[2].Value != null)
                        {
                            if (row.Cells[2].Value.ToString().StartsWith(tmpStr) || row.Cells[2].Value.ToString() == tmpStr)
                            {
                                rowIdx = row.Index;
                                dgvPartsMain.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvPartsMain.Rows[rowIdx].Selected = true;
                                dgvPartsMain.FirstDisplayedScrollingRowIndex = rowIdx;
                                dgvPartsMain.Focus();
                                break;
                            }
                        }
                    }
                }
                this.txtSearchPartNum.Focus();
            }
        }

        private void modelNoHeatTypeChanged(string digitNoStr, string heatType)
        {
            string unitType = "ALL";
            int digitNo = -1;

            if (txtSearchModelNo.Text != "")
            {
                try
                {
                    digitNo = Int32.Parse(txtSearchModelNo.Text);
                    if (digitNo < 0)
                    {
                        MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                    }
                    else if (digitNo > 59)
                    {
                        if (digitNo != 567 || digitNo != 2324 || digitNo != 2728)
                        {
                            MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                }
            }
           
            dgvModelNo.DataSource = null;

            curSearchDigitNo = digitNo;
            curHeatType = heatType;
            curOAUTypeCode = unitType;

            DataTable dt = objModel.GetOAU_ModelNoValues(digitNo, heatType);
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);
        }

        private void modelNoUnitTypeChanged(string digitNoStr, string unitType)
        {
            string heatType = "ALL";
            int digitNo = -1;

            if (txtSearchModelNo.Text != "")
            {
                try
                {
                    digitNo = Int32.Parse(txtSearchModelNo.Text);
                    if (digitNo < 0)
                    {
                        MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                    }
                    else if (digitNo > 59)
                    {
                        if (digitNo != 567 || digitNo != 2324 || digitNo != 2728)
                        {
                            MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                }
            }

            if (rbModelNoHeatTypeNA.Checked == true)
            {
                heatType = "NA";
            }
            else if (rbModelNoHeatTypeElec.Checked == true)
            {
                heatType = "ELEC";
            }
            else if (rbModelNoHeatTypeIF.Checked == true)
            {
                heatType = "IF";
            }
            else if (rbModelNoHeatTypeDF.Checked == true)
            {
                heatType = "DF";
            }

            dgvModelNo.DataSource = null;

            curSearchDigitNo = digitNo;
            curHeatType = heatType;
            curOAUTypeCode = unitType;

            DataTable dt = objModel.GetOAU_ModelNoValues(digitNo, heatType);
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);
        }

       
        #endregion              

        #region ModelNo Desc Events
        private void dgvModelNo_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string strDigitVal = "";

            int iPos;

            Color prevBackground = Color.White;
            Color foreGround = Color.Black;
            Color curBackGround = Color.LightGray;
            //Color curForeGround = Color.Black;
            Color nextBackGround = Color.White;
            //Color nextForeGround = Color.Black;

            foreach (DataGridViewRow row in dgvModelNo.Rows)
            {
                if (initModelRender)
                {
                    iPos = 0;
                }
                else
                {
                    iPos = 2;
                }

                if (row.Cells[iPos].Value.ToString() != strDigitVal)
                {
                    strDigitVal = row.Cells[iPos].Value.ToString();

                    prevBackground = curBackGround;
                    curBackGround = nextBackGround;
                    nextBackGround = prevBackground;
                }

                row.DefaultCellStyle.BackColor = curBackGround;
                row.DefaultCellStyle.ForeColor = foreGround;
            }
        }

        private void btnModelNoSearch_Click(object sender, EventArgs e)
        {
            int digitNo = -1;
            string heatType = "ALL";           

            if (txtSearchModelNo.Text != "")
            {
                try
                {
                    digitNo = Int32.Parse(txtSearchModelNo.Text);
                    if (digitNo < 0)
                    {
                        MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value from 0 - 69");
                    }
                    else if (digitNo > 69)
                    {
                        if (digitNo != 567 && digitNo != 2324 && digitNo != 2728)
                        {
                            MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value from 0 - 69");
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value from 0 - 69");
                }
            }
            else
            {
                digitNo = -1;
                //MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value from 0 - 69");
            }

            if ( rbModelNoHeatTypeNA.Checked == true)
            {
                heatType = "NA";
            }
            else if (rbModelNoHeatTypeIF.Checked == true)
            {
                heatType = "IF";
            }
            else if (rbModelNoHeatTypeElec.Checked == true)
            {
                heatType = "ELEC";
            }
            else if (rbModelNoHeatTypeDF.Checked == true)
            {
                heatType = "DF";
            }
            else if (rbModelNoHeatTypeHotWater.Checked == true)
            {
                heatType = "HOTWATER";
            }         

            dgvModelNo.DataSource = null;

            curSearchDigitNo = digitNo;
            curHeatType = heatType;            

            DataTable dt = objModel.GetOAU_ModelNoValues(digitNo, heatType);
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);
        }          

        private void btnModelNoReset_Click(object sender, EventArgs e)
        {
            txtSearchModelNo.Text = "";
            rbModelNoHeatTypeAll.Checked = true;

            initModelRender = false;
            
            dgvModelNo.DataSource = null;

            curSearchDigitNo = -1;
            curHeatType = "ALL";
            curOAUTypeCode = "ALL";

            DataTable dt = objModel.GetOAU_ModelNoValues(-1, "ALL");
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);
        }        

        private void rbModelNoHeatTypeAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeAll.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "ALL");
            }
        }

        private void rbModelNoHeatTypeNA_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeNA.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "NA");
            }
        }

        private void rbModelNoHeatTypeElec_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeElec.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "ELEC");
            }
        }

        private void rbModelNoHeatTypeIF_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeIF.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "IF");
            }
        }

        private void rbModelNoHeatTypeDF_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeDF.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "DF");
            }
        }

        private void rbModelNoHeatTypeHotWater_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeHotWater.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "HOTWATER");
            }
        }              

        private void dgvModelNo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string digitValue;
            string heatType;
            string digitDesc;
            string digitRep;
            string idStr;
           
            int digitNo;            
            int ID;                  

            digitNo = (int)dgvModelNo.Rows[e.RowIndex].Cells[2].Value;
            digitValue = dgvModelNo.Rows[e.RowIndex].Cells[3].Value.ToString();
            digitDesc = dgvModelNo.Rows[e.RowIndex].Cells[4].Value.ToString();
            heatType = dgvModelNo.Rows[e.RowIndex].Cells[5].Value.ToString();            
            digitRep = dgvModelNo.Rows[e.RowIndex].Cells[6].Value.ToString();
            idStr = dgvModelNo.Rows[e.RowIndex].Cells[8].Value.ToString();
            ID = Int32.Parse(idStr);
           
            frmUpdateModelNoDesc frmUpd = new frmUpdateModelNoDesc(this);
            frmUpd.txtModelNoUpdDigitNo.Text = digitNo.ToString();
            frmUpd.txtModelNoUpdDigitVal.Text = digitValue;
            frmUpd.txtModelNoUpdDigitDesc.Text = digitDesc;
            frmUpd.txtModelNoUpdDigitRep.Text = digitRep;            
            frmUpd.lbModelNoID.Text = ID.ToString();
            frmUpd.btnSave.Text = "Update";

            if (heatType == "NA")
            {
                frmUpd.rbModelNoHeatTypeNA.Checked = true;
            }
            else if (heatType == "IF")
            {
                frmUpd.rbModelNoHeatTypeIF.Checked = true;
            }
            else if (heatType == "ELEC")
            {
                frmUpd.rbModelNoHeatTypeElec.Checked = true;
            }
            else if (heatType == "DF")
            {
                frmUpd.rbModelNoHeatTypeDF.Checked = true;
            }
            else if (heatType == "HOTWATER")
            {
                frmUpd.rbModelNoHeatTypeHotWater.Checked = true;
            }          
            
            frmUpd.ShowDialog();

            if (mHeatType != "NoUpdate")
            {
                dgvModelNo.Rows[e.RowIndex].Cells[3].Value = mDigitVal;
                dgvModelNo.Rows[e.RowIndex].Cells[4].Value = mDigitDesc;
                dgvModelNo.Rows[e.RowIndex].Cells[5].Value = mHeatType;
                dgvModelNo.Rows[e.RowIndex].Cells[6].Value = mDigitRep;               
                dgvModelNo.Refresh();
            }
        }       

        private void dgvModelNo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIdx = e.RowIndex;

            string digitValue;
            string heatType;
            string digitDesc;
            string digitRep;            

            int digitNo;           
            int ID;


            if (e.ColumnIndex == 0 || e.ColumnIndex == 1)
            {
                DataTable dt = new DataTable();

                digitNo = (int)dgvModelNo.Rows[e.RowIndex].Cells[2].Value;
                digitValue = dgvModelNo.Rows[e.RowIndex].Cells[3].Value.ToString();
                digitDesc = dgvModelNo.Rows[e.RowIndex].Cells[4].Value.ToString();
                heatType = dgvModelNo.Rows[e.RowIndex].Cells[5].Value.ToString();
                digitRep = dgvModelNo.Rows[e.RowIndex].Cells[6].Value.ToString();
                ID = (int)dgvModelNo.Rows[e.RowIndex].Cells[8].Value;

                frmUpdateModelNoDesc frmUpd = new frmUpdateModelNoDesc(this);
                frmUpd.txtModelNoUpdDigitNo.Text = digitNo.ToString();
                frmUpd.txtModelNoUpdDigitVal.Text = digitValue;
                frmUpd.txtModelNoUpdDigitDesc.Text = digitDesc;
                frmUpd.txtModelNoUpdDigitRep.Text = digitRep;
                frmUpd.lbModelNoID.Text = ID.ToString();

                if (heatType == "NA")
                {
                    frmUpd.rbModelNoHeatTypeNA.Checked = true;
                }
                else if (heatType == "IF")
                {
                    frmUpd.rbModelNoHeatTypeIF.Checked = true;
                }
                else if (heatType == "ELEC")
                {
                    frmUpd.rbModelNoHeatTypeElec.Checked = true;
                }
                else if (heatType == "DF")
                {
                    frmUpd.rbModelNoHeatTypeDF.Checked = true;
                }
                else if (heatType == "HOTWATER")
                {
                    frmUpd.rbModelNoHeatTypeHotWater.Checked = true;
                }

                if (e.ColumnIndex == 0)  // Add button within datagridview.
                {
                    frmUpd.btnSave.Text = "Add";
                    frmUpd.ShowDialog();

                }
                else if (e.ColumnIndex == 1)  // Delete button internally is really in column #1 not 11 as it appears on the screen.
                {
                    frmUpd.btnSave.Text = "Delete";
                    frmUpd.ShowDialog();
                }
                else
                {
                    //curSelRowIdx = e.RowIndex;
                }
                dgvModelNo.DataSource = null;
                dt = objModel.GetOAU_ModelNoValues(curSearchDigitNo, curHeatType);
                dgvModelNo.DataSource = dt;
                populateModelNoList(dt, false);
            }
        }            

        private void btnModelNoAddRow_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            frmUpdateModelNoDesc frmUpd = new frmUpdateModelNoDesc(this);
            frmUpd.txtModelNoUpdDigitNo.Enabled = true;
            frmUpd.btnSave.Text = "Add";
            frmUpd.Text = "Add ModelNo";
            frmUpd.ShowDialog();
            
            dgvModelNo.DataSource = null;
            dt = objModel.GetOAU_ModelNoValues(curSearchDigitNo, curHeatType);
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);

        }

        #endregion      

        #region Compressor Detail Events
        private void rbCompDtlVoltAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCompDtlVoltAll.Checked == true)
            {
                if (this.txtCompDtlSearchPartNum.Text == "")
                {
                    compressorDataSearch(txtCompDtlSearchPartNum.Text, -1);
                }
            }
        }

        private void rbCompDtlVolt208_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCompDtlVolt208.Checked == true)
            {
                if (this.txtCompDtlSearchPartNum.Text == "")
                {
                    compressorDataSearch(txtCompDtlSearchPartNum.Text, 208);
                }
            }
        }

        private void rbCompDtlVolt460_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCompDtlVolt460.Checked == true)
            {
                if (this.txtCompDtlSearchPartNum.Text == "")
                {
                    compressorDataSearch(txtCompDtlSearchPartNum.Text, 460);
                }
            }
        }

        private void rbCompDtlVolt575_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCompDtlVolt575.Checked == true)
            {
                if (this.txtCompDtlSearchPartNum.Text == "")
                {
                    compressorDataSearch(txtCompDtlSearchPartNum.Text, 575);
                }
            }
        }

        private void compressorDataSearch(string partNum, int voltage)
        {                        

            dgvCompDetail.DataSource = null;

            curVoltage = voltage;
            curPartNum = partNum;

            DataTable dt = objComp.GetROA_CompressorDataDtl(curPartNum, voltage);
            dgvCompDetail.DataSource = dt;
            populateCompressorDetailList(dt, false);
        }              

        private void btnCompDtlReset_Click(object sender, EventArgs e)
        {
            this.txtCompDtlSearchPartNum.Text = "";
            curPartNum = "";
            curVoltage = -1;

            dgvCompDetail.DataSource = null;
            DataTable dt = objComp.GetROA_CompressorDataDtl(curPartNum, curVoltage);
            dgvCompDetail.DataSource = dt;
            populateCompressorDetailList(dt, false);
            
        }

        private void dgvCompDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string sPartNum;
            string sPartDesc;
            string sPartCategory;
            string sModBy;
            string sLastModDate;

            int iRuleHeadID;
            int iVoltage;
            int iPhase;
            int iID;

            decimal dRLA;
            decimal dLRA;

            if (e.ColumnIndex == 0)  // Add button within datagridview.
            {               
                iRuleHeadID = (int)dgvCompDetail.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
                sPartNum = dgvCompDetail.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
                sPartDesc = dgvCompDetail.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
                sPartCategory = dgvCompDetail.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();
                iVoltage = (int)dgvCompDetail.Rows[e.RowIndex].Cells["Voltage"].Value;
                iPhase = (int)dgvCompDetail.Rows[e.RowIndex].Cells["Phase"].Value;
                dRLA = (decimal)dgvCompDetail.Rows[e.RowIndex].Cells["RLA"].Value;
                dLRA = (decimal)dgvCompDetail.Rows[e.RowIndex].Cells["LRA"].Value;
                sModBy = dgvCompDetail.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
                sLastModDate = dgvCompDetail.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
                iID = (int)dgvCompDetail.Rows[e.RowIndex].Cells["ID"].Value;

                frmUpdateCompDataDtl frmUpd = new frmUpdateCompDataDtl(this);
                frmUpd.txtCompDtlRuleHeadID.Text = iRuleHeadID.ToString();
                frmUpd.cbCompDtl_PartNum.Text = sPartNum;
                frmUpd.txtCompDtlPartDesc.Text = sPartDesc;
                frmUpd.txtCompDtlPartCategory.Text = sPartCategory;
                frmUpd.txtCompDtlPhase.Text = iPhase.ToString();
                frmUpd.txtCompDtlRLA.Text = dRLA.ToString();
                frmUpd.txtCompDtlLRA.Text = dLRA.ToString();
                frmUpd.txtCompDtlModifedBy.Text = sModBy;
                frmUpd.txtCompDtlLastModDate.Text = sLastModDate;
                frmUpd.lbCompDtlID.Text = iID.ToString();
                frmUpd.btnCompDtlSave.Text = "Update";

                if (iVoltage == 208)
                {
                    frmUpd.rbCompDtlVolt208.Checked = true;
                }
                else if (iVoltage == 460)
                {
                    frmUpd.rbCompDtlVolt460.Checked = true;
                }
                else if (iVoltage == 575)
                {
                    frmUpd.rbCompDtlVolt575.Checked = true;
                }

                frmUpd.btnCompDtlSave.Text = "Delete";
                frmUpd.ShowDialog();

                if (mVoltage != "NoUpdate")
                {
                    //dgvCompDetail.Rows[e.RowIndex].Cells["Voltage"].Value = mVoltage;
                    //dgvCompDetail.Rows[e.RowIndex].Cells["Phase"].Value = mPhase;
                    //dgvCompDetail.Rows[e.RowIndex].Cells["RLA"].Value = mRLA;
                    //dgvCompDetail.Rows[e.RowIndex].Cells["LRA"].Value = mLRA;
                    //dgvCompDetail.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                    //dgvCompDetail.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                    //dgvCompDetail.Refresh();
                    curVoltage = -1;
                    curPartNum = "";
                    DataTable dt = objComp.GetROA_CompressorDataDtl(curPartNum, curVoltage);
                    populateCompressorDetailList(dt, false);
                }
            }
        }

        private void dgvCompDetail_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sPartNum;
            string sPartDesc;
            string sPartCategory;
            string sModBy;
            string sLastModDate;

            int iRuleHeadID;
            int iVoltage;
            int iPhase;
            int iID;

            decimal dRLA;
            decimal dLRA;

            iRuleHeadID = (int)dgvCompDetail.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
            sPartNum = dgvCompDetail.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
            sPartDesc = dgvCompDetail.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
            sPartCategory = dgvCompDetail.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();
            iVoltage = (int)dgvCompDetail.Rows[e.RowIndex].Cells["Voltage"].Value;
            iPhase = (int)dgvCompDetail.Rows[e.RowIndex].Cells["Phase"].Value;
            dRLA = (decimal)dgvCompDetail.Rows[e.RowIndex].Cells["RLA"].Value;
            dLRA = (decimal)dgvCompDetail.Rows[e.RowIndex].Cells["LRA"].Value;
            sModBy = dgvCompDetail.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            sLastModDate = dgvCompDetail.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            iID = (int)dgvCompDetail.Rows[e.RowIndex].Cells["ID"].Value;

            frmUpdateCompDataDtl frmUpd = new frmUpdateCompDataDtl(this);
            frmUpd.txtCompDtlRuleHeadID.Text = iRuleHeadID.ToString();
            frmUpd.txtCompDtlPartNum.Text = sPartNum;
            frmUpd.txtCompDtlPartNum.Visible = true;
            frmUpd.cbCompDtl_PartNum.Visible = false;
            frmUpd.txtCompDtlPartDesc.Text = sPartDesc;
            frmUpd.txtCompDtlPartCategory.Text = sPartCategory;            
            frmUpd.txtCompDtlPhase.Text = iPhase.ToString();
            frmUpd.txtCompDtlRLA.Text = dRLA.ToString();
            frmUpd.txtCompDtlLRA.Text = dLRA.ToString();
            frmUpd.txtCompDtlModifedBy.Text = sModBy;
            frmUpd.txtCompDtlLastModDate.Text = sLastModDate;
            frmUpd.lbCompDtlID.Text = iID.ToString();
            frmUpd.btnCompDtlSave.Text = "Update";

            if (iVoltage == 208)
            {
                frmUpd.rbCompDtlVolt208.Checked = true;
            }
            else  if (iVoltage == 460)
            {
                frmUpd.rbCompDtlVolt460.Checked = true;
            }
            else  if (iVoltage == 575)
            {
                frmUpd.rbCompDtlVolt575.Checked = true;
            }           

            frmUpd.ShowDialog();

            if (mVoltage != "NoUpdate")
            {
                dgvCompDetail.Rows[e.RowIndex].Cells["Voltage"].Value = mVoltage;
                dgvCompDetail.Rows[e.RowIndex].Cells["Phase"].Value = mPhase;
                dgvCompDetail.Rows[e.RowIndex].Cells["RLA"].Value = mRLA;
                dgvCompDetail.Rows[e.RowIndex].Cells["LRA"].Value = mLRA;
                dgvCompDetail.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                dgvCompDetail.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                dgvCompDetail.Refresh();
            }
        }

        private void btnCompDtlAddRow_Click(object sender, EventArgs e)
        {
            frmUpdateCompDataDtl frmAdd = new frmUpdateCompDataDtl(this);

            DataTable dt = objPart.GetOAU_PartNumbersByCategory("Compressor,DigitalScroll,DigitalScrollTandem");
            frmAdd.cbCompDtl_PartNum.DataSource = dt;
            frmAdd.cbCompDtl_PartNum.ValueMember = "PartNum";
            frmAdd.cbCompDtl_PartNum.DisplayMember = "PartNumDesc";
            
            //DataRow dr = dt.NewRow();
            //dr["RuleHeadID"] = 0;
            //dr["PartNumDesc"] = "Select a Part Number.";
            //dt.Rows.InsertAt(dr, 0);
            //frmAdd.cbCompDtl_PartNum.SelectedIndex = 0;
            frmAdd.cbCompDtl_PartNum.Enabled = true;
            frmAdd.btnCompDtlSave.Text = "Add";
            frmAdd.Text = "Compressor Data Detail Add";
            frmAdd.ShowDialog();

            if (mVoltage != "NoUpdate")
            {
                //dgvCompDetail.Rows[e.RowIndex].Cells["Voltage"].Value = mVoltage;
                //dgvCompDetail.Rows[e.RowIndex].Cells["Phase"].Value = mPhase;
                //dgvCompDetail.Rows[e.RowIndex].Cells["RLA"].Value = mRLA;
                //dgvCompDetail.Rows[e.RowIndex].Cells["LRA"].Value = mLRA;
                //dgvCompDetail.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                //dgvCompDetail.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                //dgvCompDetail.Refresh();
                curVoltage = -1;
                curPartNum = "";
                dt = objComp.GetROA_CompressorDataDtl(curPartNum, curVoltage);
                populateCompressorDetailList(dt, false);
            }
        }

        private void txtCompDtlSearchPartNum_KeyUp(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtCompDtlSearchPartNum.Text.ToUpper();
                if (tmpStr.Length > 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow dgr in dgvCompDetail.Rows)
                    {
                        if (dgr.Cells[2].Value != null)
                        {
                            if (dgr.Cells[2].Value.ToString().StartsWith(tmpStr) || dgr.Cells[2].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvCompDetail.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvCompDetail.Rows[rowIdx].Selected = true;
                                dgvCompDetail.FirstDisplayedScrollingRowIndex = rowIdx;
                                dgvCompDetail.Focus();
                                break;
                            }
                        }
                    }
                }
            }
            this.txtCompDtlSearchPartNum.Focus();
        }
        #endregion            
        
        #region PartRuleHead Events

        private void btnPRH_AddPart_Click(object sender, EventArgs e)
        {
            string partNum = "";
            int rowIdx = 0;
            frmAddPartRuleHead frmAdd = new frmAddPartRuleHead(this);

            DataTable dt = objPart.GetOA_PartNumbersBasedOnClassID();

            frmAdd.cbAddPRH_PartNum.DataSource = dt;
            frmAdd.cbAddPRH_PartNum.DisplayMember = "PartNumDesc";
            frmAdd.cbAddPRH_PartNum.ValueMember = "PartNum";

            DataRow dr = dt.NewRow();
            dr["PartNum"] = 0;
            dr["PartNumDesc"] = "Select a Part Number.";
            dt.Rows.InsertAt(dr, 0);
            frmAdd.cbAddPRH_PartNum.SelectedIndex = 0;

            dt = objPart.GetPartCategoryList();
            frmAdd.cbAddPRH_PartCategory.DataSource = dt;
            frmAdd.cbAddPRH_PartCategory.DisplayMember = "CategoryName";
            frmAdd.cbAddPRH_PartCategory.ValueMember = "CategoryName";

            frmAdd.cbAddPRH_RelatedOp.SelectedIndex = 0;

            frmAdd.txtAddPRH_AsmSeq.Text = "0";

            frmAdd.ShowDialog();

            partNum = mPartNumber;
            objPart.PartNum = "ALL";
            dgvPartRulesHead.DataSource = null;
            dt = objPart.GetPartRulesHead();         
            populatePartRuleHeadList(dt, false);
           
            foreach (DataGridViewRow dgr in dgvPartRulesHead.Rows)
            {
                if (dgr.Cells["PartNum"].Value != null)
                {
                    if (dgr.Cells["PartNum"].Value.ToString() == partNum)
                    {
                        rowIdx = dgr.Index;
                        dgvPartRulesHead.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                        dgvPartRulesHead.Rows[rowIdx].Selected = true;
                        dgvPartRulesHead.FirstDisplayedScrollingRowIndex = rowIdx;
                        dgvPartRulesHead.Focus();
                        break;
                    }
                }
            }            
        }

        //private void dgvPartRulesHead_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    int rowIdx = e.RowIndex;
            
        //    string partNum = dgvPartRulesHead.Rows[e.RowIndex].Cells[2].Value.ToString();
                        
        //    if (e.ColumnIndex == 0)  // Delete button within datagridview.
        //    {
        //        DialogResult result1 = MessageBox.Show("Are you sure you want to do this Part Number: " +   partNum + "? Press Yes to delete; No to cancel!",
        //                                               "Deleting a PartRulesHead Row",
        //                                               MessageBoxButtons.YesNo);
        //        if (result1 == DialogResult.Yes)
        //        {
        //            objPart.RuleHeadID = dgvPartRulesHead.Rows[e.RowIndex].Cells[1].Value.ToString();
        //            objPart.DeleteROA_PartRulesHead();
        //        }

        //    }

        //    dgvPartRulesHead.DataSource = null;
        //    DataTable dt = objPart.GetPartRulesHead();            
        //    populatePartRuleHeadList(dt, false);          
        //}

        private void dgvPartRulesHead_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string partNum;
            string partDesc;
            string partCategory;
            string modBy;
            string lastModDate;
            string assemblySeq;
            string partType;
            string stationLoc;
            string linesideBin;
            string pickList;
            string configPart;

            int ruleHeadID;           
            int relatedOp;
                      
            ruleHeadID = (int)dgvPartRulesHead.Rows[e.RowIndex].Cells["ID"].Value;           
            partNum = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
            partDesc = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
            relatedOp = (int)dgvPartRulesHead.Rows[e.RowIndex].Cells["RelatedOperation"].Value;
            partCategory = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();
            modBy = dgvPartRulesHead.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            lastModDate = dgvPartRulesHead.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            assemblySeq = dgvPartRulesHead.Rows[e.RowIndex].Cells["AssemblySeq"].Value.ToString();
            linesideBin = dgvPartRulesHead.Rows[e.RowIndex].Cells["LinesideBin"].Value.ToString();
            partType = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartType"].Value.ToString();
            stationLoc = dgvPartRulesHead.Rows[e.RowIndex].Cells["StationLocation"].Value.ToString();
            pickList = dgvPartRulesHead.Rows[e.RowIndex].Cells["Picklist"].Value.ToString();
            configPart = dgvPartRulesHead.Rows[e.RowIndex].Cells["ConfigurablePart"].Value.ToString();

            frmUpdatePartRuleHead frmUpd = new frmUpdatePartRuleHead(this);
            frmUpd.txtPRH_PartNum.Text = partNum;
            frmUpd.txtPRH_PartDesc.Text = partDesc;
            frmUpd.cbPRH_RelatedOp.Text = relatedOp.ToString();
            frmUpd.txtPRH_AsmSeq.Text = assemblySeq;
            frmUpd.txtPRH_PartType.Text = partType;
            frmUpd.cbStationLoc.Text = stationLoc;
            frmUpd.txtLinesideBin.Text = linesideBin;

            if (pickList == "Yes")
            {
                frmUpd.chkBoxPicklist.Checked = true;
            }

            if (configPart == "Yes")
            {
                frmUpd.chkBoxConfigPart.Checked = true;
            }

            DataTable dt = objPart.GetPartCategoryList();

            frmUpd.cbPRH_PartCategory.DisplayMember = "CategoryName";
            frmUpd.cbPRH_PartCategory.ValueMember = "CategoryName";
            frmUpd.cbPRH_PartCategory.DataSource = dt;    

            frmUpd.cbPRH_PartCategory.Text = partCategory;            
            frmUpd.lbPRH_ID.Text = ruleHeadID.ToString();
            frmUpd.txtPRH_ModBy.Text = modBy;
            frmUpd.txtPRH_DateModified.Text = lastModDate;

            frmUpd.ShowDialog();

            if (mPartCategory == "Delete")
            {
                txtPRH_SearchPartNum.Text = "";
                dgvPartRulesHead.DataSource = null;
                objPart.PartCategory = null;
                objPart.PartNum = "ALL";
                dt = objPart.GetPartRulesHead();
                populatePartRuleHeadList(dt, false);
            }
            else if (mPartCategory != "NoUpdate")
            {
                dgvPartRulesHead.Rows[e.RowIndex].Cells["RelatedOperation"].Value = Int32.Parse(mRelatedOp);
                dgvPartRulesHead.Rows[e.RowIndex].Cells["PartCategory"].Value = mPartCategory;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["AssemblySeq"].Value = mAssemblySeq;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["PartType"].Value = mPartType;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["StationLocation"].Value = mStationLoc;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["LinesideBin"].Value = mLinesideBin;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["Picklist"].Value = mPicklist;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["ConfigurablePart"].Value = mConfigPart;
                dgvPartRulesHead.Refresh();
            }

        }

        private void txtPRH_SearchPartNum_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;
            bool partNumFound = false;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtPRH_SearchPartNum.Text + convertKeyValue(e.KeyValue);
                tmpStr = tmpStr.ToUpper();
                //tmpStr = this.txtPRH_SearchPartNum.Text.ToUpper();
                
                if (tmpStr.Length > 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow dgr in dgvPartRulesHead.Rows)
                    {
                        if (dgr.Cells["PartNum"].Value != null)
                        {
                            if (dgr.Cells["PartNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["PartNum"].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvPartRulesHead.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvPartRulesHead.Rows[rowIdx].Selected = true;
                                dgvPartRulesHead.FirstDisplayedScrollingRowIndex = rowIdx;
                                //dgvPartRulesHead.Focus();
                                partNumFound = true;
                                break;
                            }
                        }
                    }
                }
            }
            //if (partNumFound == true)
            //{
            //    dgvPartRulesHead.Rows[rowIdx].Selected = true;
            //    dgvPartRulesHead.FirstDisplayedScrollingRowIndex = rowIdx;
            //    //this.txtPRH_SearchPartNum.Focus();
            //}
        }

        //private void txtPRH_SearchPartNum_KeyUp(object sender, KeyEventArgs e)
        //{
        //    string tmpStr = "";
        //    int rowIdx = 0;
        //    bool partNumFound = false;

        //    // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
        //    // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
        //    // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
        //    if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
        //    {
        //        return;
        //    }
        //    // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
        //    else
        //    {

        //        // Because the key value has not been added to the StartAt textbox
        //        // at this point it must be added in order to search properly.
        //        //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

        //        tmpStr = this.txtPRH_SearchPartNum.Text.ToUpper();
                
        //        if (tmpStr.Length > 3)
        //        {
        //            //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
        //            //{
        //            //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
        //            //}                    
        //            foreach (DataGridViewRow dgr in dgvPartRulesHead.Rows)
        //            {
        //                if (dgr.Cells[1].Value != null)
        //                {
        //                    if (dgr.Cells[1].Value.ToString().StartsWith(tmpStr) || dgr.Cells[1].Value.ToString() == tmpStr)
        //                    {
        //                        rowIdx = dgr.Index;
        //                        //dgvPartRulesHead.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
        //                        dgr.Selected = true;
        //                        dgvPartRulesHead.FirstDisplayedScrollingRowIndex = rowIdx;
        //                        //dgvPartRulesHead.Focus();
        //                        partNumFound = true;
        //                        break;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    if (partNumFound == true)
        //    {
        //        dgvPartRulesHead.Refresh();
        //    }
        //    this.txtPRH_SearchPartNum.Focus();
            
        //}        

        private void btnPRH_Reset_Click(object sender, EventArgs e)
        {
            txtPRH_SearchPartNum.Text = "";
            dgvPartRulesHead.DataSource = null;
            //dgvPartRulesHead.ClearSelection();
            objPart.PartCategory = null;
            objPart.PartNum = "ALL";
            DataTable dt = objPart.GetPartRulesHead();
            populatePartRuleHeadList(dt, false);   
        }
        #endregion        

        #region CompressorChargeData Events
       
        //private void btnCompChrg_Reset_Click(object sender, EventArgs e)
        //{
        //    txtCCD_SearchUnitType.Text = "";
        //    dgvCompChargeData.DataSource = null;
        //    objComp.UnitModel = "ALL";
        //    objComp.CoolingCap = "ALL";
        //    objComp.HeatType = "";
        //    DataTable dt = objComp.GetROA_CompressorChargeData();
        //    populateCompressorChargeList(dt, false);
        //}

        //private void dgvCompChargeData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    int rowIdx = e.RowIndex;

        //    string unitModel;
        //    string coolingCap;
        //    string heatType;
        //    string modifiedBy;
        //    string lastModDate;

        //    int ID;

        //    decimal circuit1;
        //    decimal circuit1_Reheat;
        //    decimal circuit2;
        //    decimal circuit2_Reheat;

        //    ID = (int)dgvCompChargeData.Rows[e.RowIndex].Cells[1].Value;
        //    unitModel = dgvCompChargeData.Rows[e.RowIndex].Cells[2].Value.ToString();
        //    coolingCap = dgvCompChargeData.Rows[e.RowIndex].Cells[3].Value.ToString();
        //    heatType = dgvCompChargeData.Rows[e.RowIndex].Cells[4].Value.ToString();
        //    circuit1 = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[5].Value;
        //    circuit1_Reheat = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[6].Value;
        //    circuit2 = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[7].Value;
        //    circuit2_Reheat = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[8].Value;
        //    modifiedBy = dgvCompChargeData.Rows[e.RowIndex].Cells[9].Value.ToString();
        //    lastModDate = dgvCompChargeData.Rows[e.RowIndex].Cells[10].Value.ToString();

        //    frmUpdateCompCharge frmUpd = new frmUpdateCompCharge(this);
        //    frmUpd.txtUpdCCD_ID.Text = ID.ToString();
        //    frmUpd.cbUpdCCD_UnitModel.Text = unitModel;
        //    frmUpd.cbUpdCCD_CoolingCapacity.Text = coolingCap;
        //    frmUpd.txtUpdCCD_Circuit1.Text = circuit1.ToString();
        //    frmUpd.txtUpdCCD_Circuit1Reheat.Text = circuit1_Reheat.ToString();
        //    frmUpd.txtUpdCCD_Circuit2.Text = circuit2.ToString();
        //    frmUpd.txtUpdCCD_Circuit2Reheat.Text = circuit2_Reheat.ToString();
        //    frmUpd.txtUpdCCD_ModifedBy.Text = modifiedBy;
        //    frmUpd.txtUpdCCD_LastModDate.Text = lastModDate;

        //    if (heatType == "Air")
        //    {
        //        frmUpd.rbUpdCCD_Air.Checked = true;
        //    }
        //    else if (heatType == "Normal")
        //    {
        //        frmUpd.rbUpdCCD_Normal.Checked = true;
        //    }
        //    else
        //    {
        //        frmUpd.rbUpdCCD_Water.Checked = true;
        //    }
            

        //    if (e.ColumnIndex == 0)  // Delete button internally is really in column #0 not 10 as it appears on the screen.
        //    {
        //        frmUpd.btnUpdCCD_Save.Text = "Delete";
        //        frmUpd.ShowDialog();

        //    }
        //    else
        //    {
        //        //curSelRowIdx = e.RowIndex;
        //    }

        //    if (mUnitType != "NoUpdate")
        //    {
        //        txtCCD_SearchUnitType.Text = "";
        //        dgvCompChargeData.DataSource = null;
        //        objComp.UnitModel = "ALL";
        //        objComp.CoolingCap = "ALL";
        //        objComp.HeatType = "";
        //        DataTable dt = objComp.GetROA_CompressorChargeData();
        //        populateCompressorChargeList(dt, false);
        //    }          
        //}

        //private void btnCCD_Search_Click(object sender, EventArgs e)
        //{
        //    txtCCD_SearchUnitType.Text = txtCCD_SearchUnitType.Text.ToUpper();
        //    objComp.UnitModel = txtCCD_SearchUnitType.Text;
        //    objComp.CoolingCap = "ALL";
        //    objComp.HeatType = "";
        //    DataTable dt = objComp.GetROA_CompressorChargeData();
        //    if (dt.Rows.Count > 0)
        //    {
        //        dgvCompChargeData.DataSource = null;
        //        populateCompressorChargeList(dt, false);
        //    }

        //}                            

        //private void dgvCompChargeData_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    string unitModel;
        //    string coolingCap;
        //    string heatType;
        //    string modifiedBy;
        //    string lastModDate;

        //    int ID;

        //    decimal circuit1;
        //    decimal circuit1_Reheat;
        //    decimal circuit2;
        //    decimal circuit2_Reheat;

        //    ID = (int)dgvCompChargeData.Rows[e.RowIndex].Cells[1].Value;
        //    unitModel = dgvCompChargeData.Rows[e.RowIndex].Cells[2].Value.ToString();
        //    coolingCap = dgvCompChargeData.Rows[e.RowIndex].Cells[3].Value.ToString();
        //    heatType = dgvCompChargeData.Rows[e.RowIndex].Cells[4].Value.ToString();
        //    circuit1 = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[5].Value;
        //    circuit1_Reheat = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[6].Value;
        //    circuit2 = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[7].Value;
        //    circuit2_Reheat = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[8].Value;
        //    modifiedBy = dgvCompChargeData.Rows[e.RowIndex].Cells[9].Value.ToString();
        //    lastModDate = dgvCompChargeData.Rows[e.RowIndex].Cells[10].Value.ToString();

        //    frmUpdateCompCharge frmUpd = new frmUpdateCompCharge(this);
        //    frmUpd.txtUpdCCD_ID.Text = ID.ToString();
        //    frmUpd.cbUpdCCD_UnitModel.Text = unitModel;
        //    frmUpd.cbUpdCCD_CoolingCapacity.Text = coolingCap;
        //    frmUpd.txtUpdCCD_Circuit1.Text = circuit1.ToString();
        //    frmUpd.txtUpdCCD_Circuit1Reheat.Text = circuit1_Reheat.ToString();
        //    frmUpd.txtUpdCCD_Circuit2.Text = circuit2.ToString();
        //    frmUpd.txtUpdCCD_Circuit2Reheat.Text = circuit2_Reheat.ToString();
        //    frmUpd.txtUpdCCD_ModifedBy.Text = modifiedBy;
        //    frmUpd.txtUpdCCD_LastModDate.Text = lastModDate;
        //    frmUpd.btnUpdCCD_Save.Text = "Update";

        //    if (heatType == "Air")
        //    {
        //        frmUpd.rbUpdCCD_Air.Checked = true;
        //    }
        //    else if (heatType == "Normal")
        //    {
        //        frmUpd.rbUpdCCD_Normal.Checked = true;
        //    }
        //    else
        //    {
        //        frmUpd.rbUpdCCD_Water.Checked = true;
        //    }

        //    frmUpd.ShowDialog();

        //    if (mUnitType != "NoUpdate")
        //    {
        //        dgvCompChargeData.Rows[e.RowIndex].Cells[5].Value = Decimal.Parse(mCircuit1);
        //        dgvCompChargeData.Rows[e.RowIndex].Cells[6].Value = Decimal.Parse(mCircuit1_Reheat);
        //        dgvCompChargeData.Rows[e.RowIndex].Cells[7].Value = Decimal.Parse(mCircuit2);
        //        dgvCompChargeData.Rows[e.RowIndex].Cells[8].Value = Decimal.Parse(mCircuit2_Reheat);
        //        dgvCompChargeData.Rows[e.RowIndex].Cells[9].Value = mModBy;
        //        dgvCompChargeData.Rows[e.RowIndex].Cells[10].Value = mLastModDate;
        //        dgvCompChargeData.Refresh();
        //    }
        //}

        //private void btnCompChargeAdd_Click(object sender, EventArgs e)
        //{
        //    frmUpdateCompCharge frmUpd = new frmUpdateCompCharge(this);

        //    frmUpd.cbUpdCCD_UnitModel.Enabled = true;
        //    frmUpd.cbUpdCCD_CoolingCapacity.Enabled = true;
        //    frmUpd.gbUpdCCD_HeatType.Enabled = true;
        //    frmUpd.btnUpdCCD_Save.Text = "Add";
        //    frmUpd.Text = "Add Compressor Charge Data";
        //    frmUpd.ShowDialog();

        //    if (mUnitType != "NoUpdate")
        //    {
        //        txtCCD_SearchUnitType.Text = "";
        //        dgvCompChargeData.DataSource = null;
        //        objComp.UnitModel = "ALL";
        //        objComp.CoolingCap = "ALL";
        //        objComp.HeatType = "";
        //        DataTable dt = objComp.GetROA_CompressorChargeData();
        //        populateCompressorChargeList(dt, false);
        //    }
        //}
        #endregion       
      
        #region ROA_MotorDataDTL Events
        private void txtMotorDataSearchPartNum_KeyUp(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtMotorDataSearchPartNum.Text.ToUpper();
                if (tmpStr.Length > 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow dgr in dgvMotorData.Rows)
                    {
                        if (dgr.Cells[2].Value != null)
                        {
                            if (dgr.Cells[2].Value.ToString().StartsWith(tmpStr) || dgr.Cells[2].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvMotorData.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvMotorData.Rows[rowIdx].Selected = true;
                                dgvMotorData.FirstDisplayedScrollingRowIndex = rowIdx;
                                dgvMotorData.Focus();
                                break;
                            }
                        }
                    }
                }
            }
            this.txtMotorDataSearchPartNum.Focus();
        }      

        private void dgvMotorData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string sPartNum;
            string sPartDesc;
            string sMotorType;
            string sModBy;
            string sLastModDate;
            string sVoltage;
            string sFLA;

            int iRuleHeadID;

            int iPhase;
            int iID;

            decimal dMCC;
            decimal dRLA;
            decimal dHertz;
            decimal dHP;

            iRuleHeadID = (int)dgvMotorData.Rows[e.RowIndex].Cells[1].Value;
            sPartNum = dgvMotorData.Rows[e.RowIndex].Cells[2].Value.ToString();
            sPartDesc = dgvMotorData.Rows[e.RowIndex].Cells[3].Value.ToString();
            sMotorType = dgvMotorData.Rows[e.RowIndex].Cells[4].Value.ToString();
            sFLA = dgvMotorData.Rows[e.RowIndex].Cells[5].Value.ToString();
            dHertz = (decimal)dgvMotorData.Rows[e.RowIndex].Cells[6].Value;
            iPhase = (int)dgvMotorData.Rows[e.RowIndex].Cells[7].Value;
            dMCC = (decimal)dgvMotorData.Rows[e.RowIndex].Cells[8].Value;
            dRLA = (decimal)dgvMotorData.Rows[e.RowIndex].Cells[9].Value;
            sVoltage = dgvMotorData.Rows[e.RowIndex].Cells[10].Value.ToString();
            dHP = (decimal)dgvMotorData.Rows[e.RowIndex].Cells[11].Value;
            sModBy = dgvMotorData.Rows[e.RowIndex].Cells[12].Value.ToString();
            sLastModDate = dgvMotorData.Rows[e.RowIndex].Cells[13].Value.ToString();
            iID = (int)dgvMotorData.Rows[e.RowIndex].Cells[14].Value;

            frmUpdateMotorData frmUpd = new frmUpdateMotorData(this);
            frmUpd.txtUpdMotorRuleHeadID.Text = iRuleHeadID.ToString();
            frmUpd.cbUpdMotorPartNum.Text = sPartNum;
            frmUpd.txtUpdMotorPartDesc.Text = sPartDesc;
            frmUpd.cbUpdMotor_MotorType.Text = sMotorType;
            frmUpd.txtUpdMotorFLA.Text = sFLA;
            frmUpd.txtUpdMotorHertz.Text = dHertz.ToString();
            frmUpd.txtUpdMotorPhase.Text = iPhase.ToString();
            frmUpd.txtUpdMotorMCC.Text = dMCC.ToString();
            frmUpd.txtUpdMotorRLA.Text = dRLA.ToString();
            if (sVoltage == "208" && dHertz == 60 && iPhase == 3)
            {
                frmUpd.rbUpdMotor208_60_3.Checked = true;
            }
            else if (sVoltage == "460" && dHertz == 60 && iPhase == 3)
            {
                frmUpd.rbUpdMotor460_60_3.Checked = true;
            }
            else if (sVoltage == "575" && dHertz == 60 && iPhase == 3)
            {
                frmUpd.rbUpdMotor575_60_3.Checked = true;
            }
            else if (sVoltage == "230" && dHertz == 60 && iPhase == 3)
            {
                frmUpd.rbUpdMotor230_240_60_3.Checked = true;
            }
            else if (sVoltage == "115" && dHertz == 60 && iPhase == 1)
            {
                frmUpd.rbUpdMotor115_60_1.Checked = true;
            }
            else if (sVoltage == "208" && dHertz == 60 && iPhase == 1)
            {
                frmUpd.rbUpdMotor208_60_1.Checked = true;
            }
            else if (sVoltage == "208" && dHertz == 50 && iPhase == 3)
            {
                frmUpd.rbUpdMotor208_50_3.Checked = true;
            }
            else if (sVoltage == "380" && dHertz == 50 && iPhase == 3)
            {
                frmUpd.rbUpdMotor380_50_3.Checked = true;
            }
            else if (sVoltage == "208" && dHertz == 50 && iPhase == 1)
            {
                frmUpd.rbUpdMotor208_50_1.Checked = true;
            }
            frmUpd.txtUpdMotorHP.Text = dHP.ToString();
            frmUpd.txtUpdMotorModifedBy.Text = sModBy;
            frmUpd.txtUpdMotorLastModDate.Text = sLastModDate;
            frmUpd.lbUpdMotorID.Text = iID.ToString();
            
            if (e.ColumnIndex == 0)  // Delete button internally is really in column #0 not 10 as it appears on the screen.
            {
                frmUpd.btnUpdMotorSave.Text = "Delete";
                frmUpd.ShowDialog();

            }          

            if (mVoltage != "NoUpdate")
            {
                txtMotorDataSearchPartNum.Text = "";
                objMotor.RuleHeadID = "-1";
                //DataTable dtMtr = objMotor.R6_OA_GetAllMotorDataDTL();
                //populateMotorDataList(dtMtr, false);
            }          
        }

        private void dgvMotorData_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sPartNum;
            string sPartDesc;
            string sMotorType;
            string sModBy;
            string sLastModDate;
            string sVoltage;
            string sFLA;

            int iRuleHeadID;

            int iPhase;
            int iID;

            decimal dMCC;
            decimal dRLA;
            decimal dHertz;
            decimal dHP;

            iRuleHeadID = (int)dgvMotorData.Rows[e.RowIndex].Cells[1].Value;
            sPartNum = dgvMotorData.Rows[e.RowIndex].Cells[2].Value.ToString();
            sPartDesc = dgvMotorData.Rows[e.RowIndex].Cells[3].Value.ToString();
            sMotorType = dgvMotorData.Rows[e.RowIndex].Cells[4].Value.ToString();
            sFLA = dgvMotorData.Rows[e.RowIndex].Cells[5].Value.ToString();
            dHertz = (decimal)dgvMotorData.Rows[e.RowIndex].Cells[6].Value;
            iPhase = (int)dgvMotorData.Rows[e.RowIndex].Cells[7].Value;
            dMCC = (decimal)dgvMotorData.Rows[e.RowIndex].Cells[8].Value;
            dRLA = (decimal)dgvMotorData.Rows[e.RowIndex].Cells[9].Value;
            sVoltage = dgvMotorData.Rows[e.RowIndex].Cells[10].Value.ToString();
            dHP = (decimal)dgvMotorData.Rows[e.RowIndex].Cells[11].Value;
            sModBy = dgvMotorData.Rows[e.RowIndex].Cells[12].Value.ToString();
            sLastModDate = dgvMotorData.Rows[e.RowIndex].Cells[13].Value.ToString();
            iID = (int)dgvMotorData.Rows[e.RowIndex].Cells[14].Value;

            frmUpdateMotorData frmUpd = new frmUpdateMotorData(this);
            frmUpd.txtUpdMotorRuleHeadID.Text = iRuleHeadID.ToString();
            frmUpd.cbUpdMotorPartNum.Text = sPartNum;
            frmUpd.txtUpdMotorPartDesc.Text = sPartDesc;
            frmUpd.cbUpdMotor_MotorType.Text = sMotorType;
            
            if (sVoltage == "208" && dHertz == 60 && iPhase == 3) 
            {
                frmUpd.rbUpdMotor208_60_3.Checked = true;
            }
            else if (sVoltage == "460" && dHertz == 60 && iPhase == 3)
            {
                frmUpd.rbUpdMotor460_60_3.Checked = true;
            }
            else if (sVoltage == "575" && dHertz == 60 && iPhase == 3)
            {
                frmUpd.rbUpdMotor575_60_3.Checked = true;
            }
            else if (sVoltage == "230" && dHertz == 60 && iPhase == 3)
            {
                frmUpd.rbUpdMotor230_240_60_3.Checked = true;
            }
            else if (sVoltage == "115" && dHertz == 60 && iPhase == 1)
            {
                frmUpd.rbUpdMotor115_60_1.Checked = true;
            }
            else if (sVoltage == "208" && dHertz == 60 && iPhase == 1)
            {
                frmUpd.rbUpdMotor208_60_1.Checked = true;
            }
            else if (sVoltage == "208" && dHertz == 50 && iPhase == 3)
            {
                frmUpd.rbUpdMotor208_50_3.Checked = true;
            }
            else if (sVoltage == "380" && dHertz == 50 && iPhase == 3)
            {
                frmUpd.rbUpdMotor380_50_3.Checked = true;
            }
            else if (sVoltage == "208" && dHertz == 50 && iPhase == 1)
            {
                frmUpd.rbUpdMotor208_50_1.Checked = true;
            }

            frmUpd.txtUpdMotorFLA.Text = sFLA;
            frmUpd.txtUpdMotorHertz.Text = dHertz.ToString();
            frmUpd.txtUpdMotorPhase.Text = iPhase.ToString();
            frmUpd.txtUpdMotorMCC.Text = dMCC.ToString();
            frmUpd.txtUpdMotorRLA.Text = dRLA.ToString();           
            frmUpd.txtUpdMotorHP.Text = dHP.ToString();
            frmUpd.txtUpdMotorModifedBy.Text = sModBy;
            frmUpd.txtUpdMotorLastModDate.Text = sLastModDate;
            frmUpd.lbUpdMotorID.Text = iID.ToString();
            frmUpd.btnUpdMotorSave.Text = "Update";

            frmUpd.ShowDialog();

            if (mVoltage != "NoUpdate")
            {
                dgvMotorData.Rows[e.RowIndex].Cells[4].Value = mMotorType;
                dgvMotorData.Rows[e.RowIndex].Cells[5].Value = Decimal.Parse(mFLA);
                dgvMotorData.Rows[e.RowIndex].Cells[6].Value = Decimal.Parse(mHertz);
                dgvMotorData.Rows[e.RowIndex].Cells[7].Value = Int32.Parse(mPhase);
                dgvMotorData.Rows[e.RowIndex].Cells[8].Value = Decimal.Parse(mMCC);
                dgvMotorData.Rows[e.RowIndex].Cells[9].Value = Decimal.Parse(mRLA);
                dgvMotorData.Rows[e.RowIndex].Cells[10].Value = Int32.Parse(mVoltage);
                dgvMotorData.Rows[e.RowIndex].Cells[11].Value = Decimal.Parse(mHP);
                dgvMotorData.Rows[e.RowIndex].Cells[12].Value = mModBy;
                dgvMotorData.Rows[e.RowIndex].Cells[13].Value = mLastModDate;
                dgvMotorData.Refresh();
            }
        }       

        private void btnMotorDataAdd_Click(object sender, EventArgs e)
        {
            frmUpdateMotorData frmAdd = new frmUpdateMotorData(this);
            DataTable dt = objPart.GetOAU_PartNumbersByCategory("Motor,Off-Site Mtr");
            frmAdd.cbUpdMotorPartNum.DataSource = dt;
            frmAdd.cbUpdMotorPartNum.ValueMember = "PartNum";
            frmAdd.cbUpdMotorPartNum.DisplayMember = "PartNumDesc";

            frmAdd.cbUpdMotorPartNum.Enabled = true;
            frmAdd.cbUpdMotor_MotorType.Enabled = true;
            frmAdd.btnUpdMotorSave.Text = "Add";
            frmAdd.ShowDialog();
            if (mVoltage != "NoUpdate")
            {
                txtMotorDataSearchPartNum.Text = "";
                objMotor.RuleHeadID = "-1";
                DataTable dtMtr = objMotor.R6_OA_GetAllMotorDataDTL();
                populateMotorDataList(dtMtr, false);
            }
        }        

        private void btnMotorDataReset_Click(object sender, EventArgs e)
        {
            txtMotorDataSearchPartNum.Text = "";
            objMotor.RuleHeadID = "-1";
            DataTable dt = objMotor.R6_OA_GetAllMotorDataDTL();
            populateMotorDataList(dt, false);
        }

        #endregion       

        #region FurnaceDataDTL Events
        private void txtFurnaceDataSearchPartNum_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;
            bool partNumFound = false;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtFurnaceDataSearchPartNum.Text + convertKeyValue(e.KeyValue);
                tmpStr = tmpStr.ToUpper();
                //tmpStr = this.txtPRH_SearchPartNum.Text.ToUpper();

                if (tmpStr.Length > 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow dgr in dgvFurnaceData.Rows)
                    {
                        if (dgr.Cells["PartNum"].Value != null)
                        {
                            if (dgr.Cells["PartNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["PartNum"].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvFurnaceData.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvFurnaceData.Rows[rowIdx].Selected = true;
                                dgvFurnaceData.FirstDisplayedScrollingRowIndex = rowIdx;
                                //dgvPartRulesHead.Focus();
                                partNumFound = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void btnFurnaceDataReset_Click(object sender, EventArgs e)
        {
            txtFurnaceDataSearchPartNum.Text = "";
            objFurn.PartNum = "";
            DataTable dt = objFurn.GetROA_FurnaceDataDTL();
            populateFurnaceDataList(dt, false);
        }

        private void btnFurnaceDataAdd_Click(object sender, EventArgs e)
        {
            frmUpdateFurnaceData frmAdd = new frmUpdateFurnaceData(this);
            DataTable dt = objPart.GetOAU_PartNumbersByCategory("Furnace/Heater,Pre-Heat");
            frmAdd.cbUpdFurnacePartNum.DataSource = dt;
            frmAdd.cbUpdFurnacePartNum.ValueMember = "PartNum";
            frmAdd.cbUpdFurnacePartNum.DisplayMember = "PartNumDesc";

            frmAdd.cbUpdFurnacePartNum.Enabled = true;
            frmAdd.btnUpdFurnaceSave.Text = "Add";
            frmAdd.Text = "Add Furnace Data Detail";
            frmAdd.ShowDialog();
            if (mVolts != "NoUpdate")
            {
                txtFurnaceDataSearchPartNum.Text = "";
                objFurn.PartNum = "";
                DataTable dtFurn = objFurn.GetROA_FurnaceDataDTL();
                populateFurnaceDataList(dtFurn, false);
            }
        }

        private void dgvFurnaceData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string sPartNum;
            string sPartDesc;
            string sBurnerRatio;
            string sModBy;
            string sLastModDate;
            string sFuelType;
            string sMBHkW;            

            int iRuleHeadID = 0;
            int iPhase;
            int iID;

            decimal dAmps;
            decimal dVolts;


            if (dgvFurnaceData.Rows[e.RowIndex].Cells[1].Value != null)
            {
                iRuleHeadID = (int)dgvFurnaceData.Rows[e.RowIndex].Cells[1].Value;
            }
            sPartNum = dgvFurnaceData.Rows[e.RowIndex].Cells[2].Value.ToString();
            sPartDesc = dgvFurnaceData.Rows[e.RowIndex].Cells[3].Value.ToString();
            dAmps = (decimal)dgvFurnaceData.Rows[e.RowIndex].Cells[4].Value;
            dVolts = (int)dgvFurnaceData.Rows[e.RowIndex].Cells[5].Value;
            sBurnerRatio = dgvFurnaceData.Rows[e.RowIndex].Cells[6].Value.ToString();
            iPhase = (int)dgvFurnaceData.Rows[e.RowIndex].Cells[7].Value;
            sFuelType = dgvFurnaceData.Rows[e.RowIndex].Cells[8].Value.ToString();
            sMBHkW = dgvFurnaceData.Rows[e.RowIndex].Cells[9].Value.ToString();
            sModBy = dgvFurnaceData.Rows[e.RowIndex].Cells[10].Value.ToString();
            sLastModDate = dgvFurnaceData.Rows[e.RowIndex].Cells[11].Value.ToString();
            iID = (int)dgvFurnaceData.Rows[e.RowIndex].Cells[12].Value;

            frmUpdateFurnaceData frmUpd = new frmUpdateFurnaceData(this);
            frmUpd.txtUpdFurnaceRuleHeadID.Text = iRuleHeadID.ToString();
            frmUpd.cbUpdFurnacePartNum.Text = sPartNum;
            frmUpd.txtUpdFurnacePartDesc.Text = sPartDesc;
            frmUpd.txtUpdFurnaceAmps.Text = dAmps.ToString();
            frmUpd.txtUpdFurnaceBurnerRatio.Text = sBurnerRatio;
            frmUpd.txtUpdFurnacePhase.Text = iPhase.ToString();

            if (dVolts == 120.00M)
            {
                frmUpd.rbUpdFurnaceVolts120.Checked = true;
            }
            else if (dVolts == 208.00M)
            {
                frmUpd.rbUpdFurnaceVolts208.Checked = true;
            }
            else if (dVolts == 460.00M)
            {
                frmUpd.rbUpdFurnaceVolts460.Checked = true;
            }
            else
            {
                frmUpd.rbUpdFurnaceVolts575.Checked = true;
            }

            if (sFuelType == "ELEC")
            {
                frmUpd.rbUpdFurnaceElectric.Checked = true;
            }
            else if (sFuelType == "LP")
            {
                frmUpd.rbUpdFurnacePropane.Checked = true;
            }
            else
            {
                frmUpd.rbUpdFurnaceNaturalGas.Checked = true;
            }

            frmUpd.txtUpdFurnaceMBHkW.Text = sMBHkW;
            frmUpd.txtUpdFurnaceModifedBy.Text = sModBy;
            frmUpd.txtUpdFurnaceLastModDate.Text = sLastModDate;
            frmUpd.lbUpdFurnaceID.Text = iID.ToString();                    

            if (e.ColumnIndex == 0)  // Delete button internally is really in column #0 not 10 as it appears on the screen.
            {
                frmUpd.btnUpdFurnaceSave.Text = "Delete";                 
                frmUpd.ShowDialog();
            }

            if (mVolts != "NoUpdate")
            {
                txtFurnaceDataSearchPartNum.Text = "";
                objFurn.PartNum = "";
                DataTable dtFurn = objFurn.GetROA_FurnaceDataDTL();
                populateFurnaceDataList(dtFurn, false);
            }
        }

        private void dgvFurnaceData_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sPartNum;
            string sPartDesc;
            string sBurnerRatio;
            string sModBy;
            string sLastModDate;
            string sFuelType;
            string sMBHkW;

            int iRuleHeadID;
            int iPhase;
            int iID;

            decimal dAmps;
            decimal dVolts;                       

            iRuleHeadID = (int)dgvFurnaceData.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
            sPartNum = dgvFurnaceData.Rows[e.RowIndex].Cells["PArtNum"].Value.ToString();
            sPartDesc = dgvFurnaceData.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
            dAmps = (decimal)dgvFurnaceData.Rows[e.RowIndex].Cells["Amps"].Value;
            dVolts = (int)dgvFurnaceData.Rows[e.RowIndex].Cells["Volts"].Value;
            sBurnerRatio = dgvFurnaceData.Rows[e.RowIndex].Cells["BurnerRatio"].Value.ToString();
            iPhase = (int)dgvFurnaceData.Rows[e.RowIndex].Cells["Phase"].Value;
            sFuelType = dgvFurnaceData.Rows[e.RowIndex].Cells["FuelType"].Value.ToString();
            sMBHkW = dgvFurnaceData.Rows[e.RowIndex].Cells["MBHkW"].Value.ToString();           
            sModBy = dgvFurnaceData.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            sLastModDate = dgvFurnaceData.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            iID = (int)dgvFurnaceData.Rows[e.RowIndex].Cells["ID"].Value;

            frmUpdateFurnaceData frmUpd = new frmUpdateFurnaceData(this);
            frmUpd.txtUpdFurnaceRuleHeadID.Text = iRuleHeadID.ToString();
            frmUpd.cbUpdFurnacePartNum.Text = sPartNum;
            frmUpd.txtUpdFurnacePartDesc.Text = sPartDesc;
            frmUpd.txtUpdFurnaceAmps.Text = dAmps.ToString();           
            frmUpd.txtUpdFurnaceBurnerRatio.Text = sBurnerRatio;
            frmUpd.txtUpdFurnacePhase.Text = iPhase.ToString();

            if (dVolts == 120.00M)
            {
                frmUpd.rbUpdFurnaceVolts120.Checked = true;
            }
            else  if (dVolts == 208.00M)
            {
                frmUpd.rbUpdFurnaceVolts208.Checked = true;
            }
            else if (dVolts == 230.00M)
            {
                frmUpd.rbUpdFurnaceVolts230.Checked = true;
            }
            else  if (dVolts == 460.00M)
            {
                frmUpd.rbUpdFurnaceVolts460.Checked = true;
            }
            else 
            {
                frmUpd.rbUpdFurnaceVolts575.Checked = true;
            }

            if (sFuelType == "ELEC")
            {
                frmUpd.rbUpdFurnaceElectric.Checked = true;
            }
            else if (sFuelType == "LP")
            {
                frmUpd.rbUpdFurnacePropane.Checked = true;
            }
            else
            {
                frmUpd.rbUpdFurnaceNaturalGas.Checked = true;
            }
           
            frmUpd.txtUpdFurnaceMBHkW.Text = sMBHkW;            
            frmUpd.txtUpdFurnaceModifedBy.Text = sModBy;
            frmUpd.txtUpdFurnaceLastModDate.Text = sLastModDate;
            frmUpd.lbUpdFurnaceID.Text = iID.ToString();
            frmUpd.btnUpdFurnaceSave.Text = "Update";

            frmUpd.ShowDialog();

            if (mVolts != "NoUpdate")
            {
                dgvFurnaceData.Rows[e.RowIndex].Cells["Amps"].Value = Decimal.Parse(mAmps);
                dgvFurnaceData.Rows[e.RowIndex].Cells["Volts"].Value = Decimal.Parse(mVolts);
                dgvFurnaceData.Rows[e.RowIndex].Cells["BurnerRatio"].Value = mBurnerRatio;
                dgvFurnaceData.Rows[e.RowIndex].Cells["Phase"].Value = Int32.Parse(mPhase);
                dgvFurnaceData.Rows[e.RowIndex].Cells["FuelType"].Value = mFuelType;
                dgvFurnaceData.Rows[e.RowIndex].Cells["MBHkW"].Value = mMBHkW;                
                dgvFurnaceData.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                dgvFurnaceData.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                dgvFurnaceData.Refresh();
            }
        }

        #endregion  
      
        #region PartCategoryHead Events

        private void btnPartCatReset_Click(object sender, EventArgs e)
        {
            objCat.PullOrder = "-1";
            DataTable dt = objCat.GetROA_PartCategoryHead();
            populatePartCategoryDataList(dt, false);
        }

        private void btnPartCatAdd_Click(object sender, EventArgs e)
        {
            frmUpdatePartCat frmUpd = new frmUpdatePartCat(this);
            frmUpd.btnUpdPartCatSave.Text = "Add";
            frmUpd.Text = "Add Part Category Head";
            frmUpd.ShowDialog();
           

            if (mVolts != "NoUpdate")
            {
                objCat.PullOrder = "-1";
                DataTable dt = objCat.GetROA_PartCategoryHead();
                populatePartCategoryDataList(dt, false);
            }
        }       

        private void dgvPartCat_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            string sCategoryName;
            string sCategoryDesc;
            string sModBy;
            string sLastModDate;
            string sCritComp;

            int iPullOrder;
            int iID;

            sCategoryName = dgvPartCat.Rows[e.RowIndex].Cells[1].Value.ToString();
            sCategoryDesc = dgvPartCat.Rows[e.RowIndex].Cells[2].Value.ToString();
            iPullOrder = (int)dgvPartCat.Rows[e.RowIndex].Cells[3].Value;
            sCritComp = dgvPartCat.Rows[e.RowIndex].Cells[4].Value.ToString();
            sModBy = dgvPartCat.Rows[e.RowIndex].Cells[5].Value.ToString();
            sLastModDate = dgvPartCat.Rows[e.RowIndex].Cells[6].Value.ToString();
            iID = (int)dgvPartCat.Rows[e.RowIndex].Cells[7].Value;

            frmUpdatePartCat frmUpd = new frmUpdatePartCat(this);
            frmUpd.lbUpdPartCatID.Text = iID.ToString();
            frmUpd.txtUpdPartCat_CategoryName.Text = sCategoryName;
            frmUpd.txtUpdPartCat_CategoryDesc.Text = sCategoryDesc;
            frmUpd.txtUpdPartCatPullOrder.Text = iPullOrder.ToString();
            if (sCritComp == "True")
            {
                frmUpd.cbUpdPartCatCritComp.Checked = true;
            }
            else
            {
                frmUpd.cbUpdPartCatCritComp.Checked = false;
            }
            frmUpd.txtUpdPartCatModBy.Text = sModBy;
            frmUpd.txtUpdPartCatLastModDate.Text = sLastModDate;                     

            if (e.ColumnIndex == 0)  // Delete button internally is really in column #0 not 10 as it appears on the screen.
            {
                frmUpd.btnUpdPartCatSave.Text = "Delete";
                frmUpd.ShowDialog();
            }

            if (mPullOrder != "NoUpdate")
            {
                objCat.PullOrder = "-1";
                DataTable dt = objCat.GetROA_PartCategoryHead();
                populatePartCategoryDataList(dt, false);
            }
        }

        private void dgvPartCat_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sCategoryName;
            string sCategoryDesc;            
            string sModBy;
            string sLastModDate;
            string sCritComp;
           
            int iPullOrder;
            int iID;

            sCategoryName = dgvPartCat.Rows[e.RowIndex].Cells["CategoryName"].Value.ToString();
            sCategoryDesc = dgvPartCat.Rows[e.RowIndex].Cells["CategoryDesc"].Value.ToString();
            iPullOrder = (int)dgvPartCat.Rows[e.RowIndex].Cells["PullOrder"].Value;
            sCritComp = dgvPartCat.Rows[e.RowIndex].Cells["CriticalComp"].Value.ToString();
            sModBy = dgvPartCat.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            sLastModDate = dgvPartCat.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            iID = (int)dgvPartCat.Rows[e.RowIndex].Cells["ID"].Value;

            frmUpdatePartCat frmUpd = new frmUpdatePartCat(this);
            frmUpd.lbUpdPartCatID.Text = iID.ToString();
            frmUpd.txtUpdPartCat_CategoryName.Text = sCategoryName;
            frmUpd.txtUpdPartCat_CategoryDesc.Text = sCategoryDesc;
            frmUpd.txtUpdPartCatPullOrder.Text = iPullOrder.ToString();
            if (sCritComp == "True")
            {
                frmUpd.cbUpdPartCatCritComp.Checked = true;
            }
            else
            {
                frmUpd.cbUpdPartCatCritComp.Checked = false;
            }

            frmUpd.txtUpdPartCatModBy.Text = sModBy;
            frmUpd.txtUpdPartCatLastModDate.Text = sLastModDate;            
            frmUpd.btnUpdPartCatSave.Text = "Update";

            frmUpd.ShowDialog();

            if (mPullOrder != "NoUpdate")
            {
                dgvPartCat.Rows[e.RowIndex].Cells["CategoryName"].Value = mCategoryName;
                dgvPartCat.Rows[e.RowIndex].Cells["CategoryDesc"].Value = mCategoryDesc;
                dgvPartCat.Rows[e.RowIndex].Cells["PullOrder"].Value = iPullOrder;
                dgvPartCat.Rows[e.RowIndex].Cells["CriticalComp"].Value = mCritComp;
                dgvPartCat.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                dgvPartCat.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                dgvPartCat.Refresh();
            }
        }
        #endregion                                   
       
        #region CompressorCircuitChargeDataEvents

        private void btnCompDataAdd_Click(object sender, EventArgs e)
        {
            frmUpdateCompCircuit frmAdd = new frmUpdateCompCircuit(this);

            frmAdd.Text = "Add Compressor Circuit Charge Data";

            frmAdd.cbUpdCCD_Digit3.SelectedIndex = 0;
            frmAdd.cbUpdCCD_Digit4.SelectedIndex = 0;

            //DataTable dtDigit = objModel.GetOAU_ModelNoValues(9, "NA");
            //DataRow dr = dtDigit.NewRow();
            //dr["DigitVal"] = -1;
            //dr["DigitValDesc"] = "Select a Value.";
            //dtDigit.Rows.InsertAt(dr, 0);
            //frmAdd.cbUpdCCD_Digit9.ValueMember = "DigitVal";
            //frmAdd.cbUpdCCD_Digit9.DisplayMember = "DigitValDesc";
            //frmAdd.cbUpdCCD_Digit9.DataSource = dtDigit;
            frmAdd.cbUpdCCD_Digit9.Enabled = false;

            //dtDigit = objModel.GetOAU_ModelNoValues(11, "NA");
            //dr = dtDigit.NewRow();
            //dr["DigitVal"] = -1;
            //dr["DigitValDesc"] = "Select a Value.";
            //dtDigit.Rows.InsertAt(dr, 0);
            //frmAdd.cbUpdCCD_Digit11.ValueMember = "DigitVal";
            //frmAdd.cbUpdCCD_Digit11.DisplayMember = "DigitValDesc";
            //frmAdd.cbUpdCCD_Digit11.DataSource = dtDigit;
            frmAdd.cbUpdCCD_Digit11.Enabled = false;

            //dtDigit = objModel.GetOAU_ModelNoValues(12, "NA");
            //dr = dtDigit.NewRow();
            //dr["DigitVal"] = -1;
            //dr["DigitValDesc"] = "Select a Value.";
            //dtDigit.Rows.InsertAt(dr, 0);
            //frmAdd.cbUpdCCD_Digit12.ValueMember = "DigitVal";
            //frmAdd.cbUpdCCD_Digit12.DisplayMember = "DigitValDesc";
            //frmAdd.cbUpdCCD_Digit12.DataSource = dtDigit;
            frmAdd.cbUpdCCD_Digit12.Enabled = false;

            //dtDigit = objModel.GetOAU_ModelNoValues(13, "NA");
            //dr = dtDigit.NewRow();
            //dr["DigitVal"] = -1;
            //dr["DigitValDesc"] = "Select a Value.";    
            //dtDigit.Rows.InsertAt(dr, 0);
            //frmAdd.cbUpdCCD_Digit13.ValueMember = "DigitVal";
            //frmAdd.cbUpdCCD_Digit13.DisplayMember = "DigitValDesc";
            //frmAdd.cbUpdCCD_Digit13.DataSource = dtDigit;
            frmAdd.cbUpdCCD_Digit13.Enabled = false;

            //dtDigit = objModel.GetOAU_ModelNoValues(14, "NA");
            //dr = dtDigit.NewRow();
            //dr["DigitVal"] = -1;
            //dr["DigitValDesc"] = "Select a Value.";
            //dtDigit.Rows.InsertAt(dr, 0);
            //frmAdd.cbUpdCCD_Digit14.ValueMember = "DigitVal";
            //frmAdd.cbUpdCCD_Digit14.DisplayMember = "DigitValDesc";
            //frmAdd.cbUpdCCD_Digit14.DataSource = dtDigit;
            frmAdd.cbUpdCCD_Digit13.Enabled = false;

            DataTable dtComp1 = objPart.GetCompressorPartNumbers();

            if (dtComp1.Rows.Count > 0)
            {
                frmAdd.cbUpdCCDComp1_1.DataSource = dtComp1;
                frmAdd.cbUpdCCDComp1_1.ValueMember = "PartNum";  
                frmAdd.cbUpdCCDComp1_1.DisplayMember = "PartNumDesc";

                DataTable dtComp2 = new DataTable();
                dtComp2 = dtComp1.Copy();
                frmAdd.cbUpdCCDComp1_2.DataSource = dtComp2;
                frmAdd.cbUpdCCDComp1_2.ValueMember = "PartNum";  
                frmAdd.cbUpdCCDComp1_2.DisplayMember = "PartNumDesc";

                DataTable dtComp3 = new DataTable();
                dtComp3 = dtComp1.Copy();
                frmAdd.cbUpdCCDComp1_3.DataSource = dtComp3;
                frmAdd.cbUpdCCDComp1_3.ValueMember = "PartNum";  
                frmAdd.cbUpdCCDComp1_3.DisplayMember = "PartNumDesc";

                DataTable dtComp4 = new DataTable();
                dtComp4 = dtComp1.Copy();               
                frmAdd.cbUpdCCDComp2_1.DataSource = dtComp4;
                frmAdd.cbUpdCCDComp2_1.ValueMember = "PartNum";  
                frmAdd.cbUpdCCDComp2_1.DisplayMember = "PartNumDesc";

                DataTable dtComp5 = new DataTable();
                dtComp5 = dtComp1.Copy();
                frmAdd.cbUpdCCDComp2_2.DataSource = dtComp5;
                frmAdd.cbUpdCCDComp2_2.ValueMember = "PartNum";  
                frmAdd.cbUpdCCDComp2_2.DisplayMember = "PartNumDesc";

                DataTable dtComp6 = new DataTable();
                dtComp6 = dtComp1.Copy();
                frmAdd.cbUpdCCDComp2_3.DataSource = dtComp6;
                frmAdd.cbUpdCCDComp2_3.ValueMember = "PartNum";  
                frmAdd.cbUpdCCDComp2_3.DisplayMember = "PartNumDesc";                            
            }

            frmAdd.btnUpdCCD_Save.Text = "Save";

            frmAdd.ShowDialog();

            if (mCoolingCap != "0")
            {
                objComp.Digit3 = null;
                objComp.Digit4 = null;
                objComp.Digit567 = "0";
                objComp.Digit9 = null;
                objComp.Digit11 = null;
                objComp.Digit12 = null;
                objComp.Digit13 = null;
                objComp.Digit14 = null;
                DataTable dt = objComp.GetCompCircuitChargeData();
                populateCompressorCircuitChargeList(dt, false);
            }

        }

        private void dgvCompCircuitData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIdx = e.RowIndex;

            string coolingCap = "";
            string revision = "";

            if (e.ColumnIndex == 0)  // Add button within datagridview.
            {
                if (dgvCompCircuitData.Rows[rowIdx].Cells[3].Value.ToString() == "D")
                {
                    revision = "Rev 5";
                }
                else
                {
                    revision = "Rev 6";
                }
                coolingCap = dgvCompCircuitData.Rows[rowIdx].Cells[4].Value.ToString();
                DialogResult result1 = MessageBox.Show("Are you sure you want to delete the " + revision + " - Cooling Capacity - '" + coolingCap + "' Compressor Circuit Row? Press Yes to delete!",
                                                       "Deleting a Compressor Circuit Row",
                                                       MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    objComp.ID = dgvCompCircuitData.Rows[rowIdx].Cells[1].Value.ToString();
                    objComp.DeleteR6_OA_CompCircuitChargeData();
                    dgvModelNo.DataSource = null;
                    DataTable dt = objComp.GetCompCircuitChargeData();
                    populateCompressorCircuitChargeList(dt, false);
                }
            }
        }

        private void dgvCompCircuitData_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = e.RowIndex;

            string revision = "";
            string unitType = "";

            frmUpdateCompCircuit frmUpd = new frmUpdateCompCircuit(this);

            frmUpd.Text = "Update Compressor Circuit Charge Data";
            frmUpd.lbUpdCCD_ID.Text = dgvCompCircuitData.Rows[rowIdx].Cells[1].Value.ToString();

            frmUpd.lbUpdCCDEventFire.Text = "No";

            unitType = dgvCompCircuitData.Rows[rowIdx].Cells[2].Value.ToString();
            frmUpd.cbUpdCCD_Digit3.Text = unitType;
            frmUpd.cbUpdCCD_Digit3.Enabled = false;
            frmUpd.cbUpdCCD_Digit4.Text = dgvCompCircuitData.Rows[rowIdx].Cells[3].Value.ToString();
            frmUpd.cbUpdCCD_Digit4.Enabled = false;

            if (unitType == "B" || unitType == "G")
            {
                objModel.OAUTypeCode = "OALBG";
            }
            else
            {
                objModel.OAUTypeCode = "OAU123DKN";
            }

            if (unitType != "G")
            {
                revision = "REV5";
            }
            else
            {
                revision = "REV6";
                objModel.OAUTypeCode = "";
            }

            DataTable dtDigit = objModel.GetCoolingCapByRevision(revision, 567);
            DataRow dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbUpdCCD_Digit567.ValueMember = "DigitVal";
            frmUpd.cbUpdCCD_Digit567.DisplayMember = "DigitValDesc";
            frmUpd.cbUpdCCD_Digit567.DataSource = dtDigit;
            //frmUpd.cbUpdCCD_Digit567.Enabled = true;
            frmUpd.cbUpdCCD_Digit567.SelectedIndex = findSelectIndex(dtDigit, dgvCompCircuitData.Rows[rowIdx].Cells[4].Value.ToString());

            dtDigit = objModel.GetVoltageByRevision(revision, 9);
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbUpdCCD_Digit9.ValueMember = "DigitVal";
            frmUpd.cbUpdCCD_Digit9.DisplayMember = "DigitValDesc";
            frmUpd.cbUpdCCD_Digit9.DataSource = dtDigit;
            //frmUpd.cbUpdCCD_Digit9.Enabled = true;
            frmUpd.cbUpdCCD_Digit9.SelectedIndex = findSelectIndex(dtDigit, dgvCompCircuitData.Rows[rowIdx].Cells[5].Value.ToString());

            objModel.DigitNo = "11";
            objModel.HeatType = "NA";            
            objModel.Revision = revision;

            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbUpdCCD_Digit11.ValueMember = "DigitVal";
            frmUpd.cbUpdCCD_Digit11.DisplayMember = "DigitValDesc";
            frmUpd.cbUpdCCD_Digit11.DataSource = dtDigit;
            //frmUpd.cbUpdCCD_Digit11.Enabled = true;
            frmUpd.cbUpdCCD_Digit11.SelectedIndex = findSelectIndex(dtDigit, dgvCompCircuitData.Rows[rowIdx].Cells[6].Value.ToString());

            objModel.DigitNo = "12";
            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbUpdCCD_Digit12.ValueMember = "DigitVal";
            frmUpd.cbUpdCCD_Digit12.DisplayMember = "DigitValDesc";
            frmUpd.cbUpdCCD_Digit12.DataSource = dtDigit;
            //frmUpd.cbUpdCCD_Digit12.Enabled = true;
            frmUpd.cbUpdCCD_Digit12.SelectedIndex = findSelectIndex(dtDigit, dgvCompCircuitData.Rows[rowIdx].Cells[7].Value.ToString());

            objModel.DigitNo = "13";
            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbUpdCCD_Digit13.ValueMember = "DigitVal";
            frmUpd.cbUpdCCD_Digit13.DisplayMember = "DigitValDesc";
            frmUpd.cbUpdCCD_Digit13.DataSource = dtDigit;
            //frmUpd.cbUpdCCD_Digit13.Enabled = true;
            frmUpd.cbUpdCCD_Digit13.SelectedIndex = findSelectIndex(dtDigit, dgvCompCircuitData.Rows[rowIdx].Cells[8].Value.ToString());

            objModel.DigitNo = "14";
            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbUpdCCD_Digit14.ValueMember = "DigitVal";
            frmUpd.cbUpdCCD_Digit14.DisplayMember = "DigitValDesc";
            frmUpd.cbUpdCCD_Digit14.DataSource = dtDigit;
            //frmUpd.cbUpdCCD_Digit14.Enabled = true;
            frmUpd.cbUpdCCD_Digit14.SelectedIndex = findSelectIndex(dtDigit, dgvCompCircuitData.Rows[rowIdx].Cells[9].Value.ToString());

            DataTable dtComp1 = objPart.GetCompressorPartNumbers();

            if (dtComp1.Rows.Count > 0)
            {
                frmUpd.cbUpdCCDComp1_1.DataSource = dtComp1;
                frmUpd.cbUpdCCDComp1_1.ValueMember = "PartNum";
                frmUpd.cbUpdCCDComp1_1.DisplayMember = "PartNumDesc";
                frmUpd.cbUpdCCDComp1_1.SelectedIndex = findCompSelectIndex(dtComp1, dgvCompCircuitData.Rows[rowIdx].Cells[10].Value.ToString());

                DataTable dtComp2 = new DataTable();
                dtComp2 = dtComp1.Copy();
                frmUpd.cbUpdCCDComp1_2.DataSource = dtComp2;
                frmUpd.cbUpdCCDComp1_2.ValueMember = "PartNum";
                frmUpd.cbUpdCCDComp1_2.DisplayMember = "PartNumDesc";
                if (dgvCompCircuitData.Rows[rowIdx].Cells[11].Value.ToString() != "NA")
                {
                    frmUpd.cbUpdCCDComp1_2.SelectedIndex = findCompSelectIndex(dtComp2, dgvCompCircuitData.Rows[rowIdx].Cells[11].Value.ToString());
                }

                DataTable dtComp3 = new DataTable();
                dtComp3 = dtComp1.Copy();
                frmUpd.cbUpdCCDComp1_3.DataSource = dtComp3;
                frmUpd.cbUpdCCDComp1_3.ValueMember = "PartNum";
                frmUpd.cbUpdCCDComp1_3.DisplayMember = "PartNumDesc";
                if (dgvCompCircuitData.Rows[rowIdx].Cells[12].Value.ToString() != "NA")
                {
                    frmUpd.cbUpdCCDComp1_3.SelectedIndex = findCompSelectIndex(dtComp3, dgvCompCircuitData.Rows[rowIdx].Cells[12].Value.ToString());
                }

                DataTable dtComp4 = new DataTable();
                dtComp4 = dtComp1.Copy();
                frmUpd.cbUpdCCDComp2_1.DataSource = dtComp4;
                frmUpd.cbUpdCCDComp2_1.ValueMember = "PartNum";
                frmUpd.cbUpdCCDComp2_1.DisplayMember = "PartNumDesc";
                if (dgvCompCircuitData.Rows[rowIdx].Cells[13].Value.ToString() != "NA")
                {
                    frmUpd.cbUpdCCDComp2_1.SelectedIndex = findCompSelectIndex(dtComp4, dgvCompCircuitData.Rows[rowIdx].Cells[13].Value.ToString());
                }

                DataTable dtComp5 = new DataTable();
                dtComp5 = dtComp1.Copy();
                frmUpd.cbUpdCCDComp2_2.DataSource = dtComp5;
                frmUpd.cbUpdCCDComp2_2.ValueMember = "PartNum";
                frmUpd.cbUpdCCDComp2_2.DisplayMember = "PartNumDesc";
                if (dgvCompCircuitData.Rows[rowIdx].Cells[14].Value.ToString() != "NA")
                {
                    frmUpd.cbUpdCCDComp2_2.SelectedIndex = findCompSelectIndex(dtComp5, dgvCompCircuitData.Rows[rowIdx].Cells[14].Value.ToString());
                }

                DataTable dtComp6 = new DataTable();
                dtComp6 = dtComp1.Copy();
                frmUpd.cbUpdCCDComp2_3.DataSource = dtComp6;
                frmUpd.cbUpdCCDComp2_3.ValueMember = "PartNum";
                frmUpd.cbUpdCCDComp2_3.DisplayMember = "PartNumDesc";
                if (dgvCompCircuitData.Rows[rowIdx].Cells[15].Value.ToString() != "NA")
                {
                    frmUpd.cbUpdCCDComp2_3.SelectedIndex = findCompSelectIndex(dtComp6, dgvCompCircuitData.Rows[rowIdx].Cells[15].Value.ToString());
                }
            }

            frmUpd.txtUpdCCD_Circuit1.Text = dgvCompCircuitData.Rows[rowIdx].Cells[16].Value.ToString();
            frmUpd.txtUpdCCD_Circuit1Reheat.Text = dgvCompCircuitData.Rows[rowIdx].Cells[17].Value.ToString();
            frmUpd.txtUpdCCD_Circuit2.Text = dgvCompCircuitData.Rows[rowIdx].Cells[18].Value.ToString();
            frmUpd.txtUpdCCD_Circuit2Reheat.Text = dgvCompCircuitData.Rows[rowIdx].Cells[19].Value.ToString();            
            frmUpd.txtUpdCCD_LastModDate.Text = dgvCompCircuitData.Rows[rowIdx].Cells[20].Value.ToString();

            frmUpd.btnUpdCCD_Save.Text = "Update";

            frmUpd.ShowDialog();

            if (mCoolingCap != "0")
            {
                dgvCompCircuitData.Rows[rowIdx].Cells[2].Value = mUnitType;
                dgvCompCircuitData.Rows[rowIdx].Cells[3].Value = mRevision;
                dgvCompCircuitData.Rows[rowIdx].Cells[4].Value = mCoolingCap;
                dgvCompCircuitData.Rows[rowIdx].Cells[5].Value = mVoltage;
                dgvCompCircuitData.Rows[rowIdx].Cells[6].Value = mIndoorCoilType;
                dgvCompCircuitData.Rows[rowIdx].Cells[7].Value = mHotGasReheat;
                dgvCompCircuitData.Rows[rowIdx].Cells[8].Value = mCompType;
                dgvCompCircuitData.Rows[rowIdx].Cells[9].Value = mOutdoorCoilType;
                dgvCompCircuitData.Rows[rowIdx].Cells[10].Value = mComp1_1;
                dgvCompCircuitData.Rows[rowIdx].Cells[11].Value = mComp1_2;
                dgvCompCircuitData.Rows[rowIdx].Cells[12].Value = mComp1_3;
                dgvCompCircuitData.Rows[rowIdx].Cells[13].Value = mComp2_1;
                dgvCompCircuitData.Rows[rowIdx].Cells[14].Value = mComp2_2;
                dgvCompCircuitData.Rows[rowIdx].Cells[15].Value = mComp2_3;
                dgvCompCircuitData.Rows[rowIdx].Cells[16].Value = mCircuit1;
                dgvCompCircuitData.Rows[rowIdx].Cells[17].Value = mCircuit1_Reheat;
                dgvCompCircuitData.Rows[rowIdx].Cells[18].Value = mCircuit2;
                dgvCompCircuitData.Rows[rowIdx].Cells[19].Value = mCircuit2_Reheat;                
                dgvCompCircuitData.Rows[rowIdx].Cells[20].Value = mLastModDate;
                dgvCompCircuitData.Refresh();
            }            
        }

        private int findSelectIndex(DataTable dt, string searchVal)
        {
            int retIdx = 0;
            string digitVal = "";

            foreach(DataRow dr in dt.Rows)
            {
                digitVal = dr["DigitVal"].ToString();

                if (searchVal == digitVal)
                {
                    break;
                }
                ++retIdx;
            }

            return retIdx;
        }

        private int findCompSelectIndex(DataTable dt, string searchVal)
        {
            int retIdx = 0;

            foreach (DataRow dr in dt.Rows)
            {
                if (searchVal == dr["PartNum"].ToString())
                {
                    break;
                }
                ++retIdx;
            }

            return retIdx;
        }

        private void btnMainCCCD_Copy_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in dgvCompCircuitData.Rows)
            {
                if (dgvr.Selected)
                {
                    objComp.Digit3 = dgvr.Cells[2].Value.ToString();
                    objComp.Digit4 = dgvr.Cells[3].Value.ToString();
                    objComp.Digit567 = dgvr.Cells[4].Value.ToString();
                    objComp.Digit9 = dgvr.Cells[5].Value.ToString();
                    objComp.Digit13 = dgvr.Cells[6].Value.ToString();
                    objComp.Digit14 = dgvr.Cells[7].Value.ToString();
                    objComp.Circuit1_1_Compressor = dgvr.Cells[8].Value.ToString();
                    objComp.Circuit1_2_Compressor = dgvr.Cells[9].Value.ToString();
                    objComp.Circuit1_3_Compressor = dgvr.Cells[10].Value.ToString();
                    objComp.Circuit2_1_Compressor = dgvr.Cells[11].Value.ToString();
                    objComp.Circuit2_2_Compressor = dgvr.Cells[12].Value.ToString();
                    objComp.Circuit2_3_Compressor = dgvr.Cells[13].Value.ToString();
                    objComp.Circuit1 = dgvr.Cells[14].Value.ToString();
                    objComp.Circuit1_Reheat = dgvr.Cells[15].Value.ToString();
                    objComp.Circuit2 = dgvr.Cells[16].Value.ToString();
                    objComp.Circuit2_Reheat = dgvr.Cells[17].Value.ToString();
                    objComp.ModBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                    objComp.InsertR6_OA_CompCircuitChargeData();
                }
            }
            dgvModelNo.DataSource = null;
            objComp.Digit3 = null;
            objComp.Digit4 = null;
            objComp.Digit567 = "0";
            objComp.Digit9 = null;
            objComp.Digit13 = null;
            objComp.Digit14 = null;
            DataTable dt = objComp.GetCompCircuitChargeData();
            populateCompressorCircuitChargeList(dt, false);
        }

        private void btnMainCCCD_DelDups_Click(object sender, EventArgs e)
        {
            bool skipFirst = true;

            DataTable dtDups = objComp.R6_OA_GetDuplicateCompChargeDataEntries();
            if (dtDups.Rows.Count > 0)
            {

                DialogResult result1 = MessageBox.Show("You have chosen to check for and delete any duplicate Cooling Capacity's. If you wish to continue press 'Yes'!",
                                                       "Deleting Duplicate Rows of Data",
                                                        MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    foreach (DataRow dr in dtDups.Rows)
                    {
                        objComp.Digit3 = dr["Digit3"].ToString();
                        objComp.Digit4 = dr["Digit4"].ToString(); ;
                        objComp.Digit567 = dr["Digit567"].ToString();
                        objComp.Digit9 = dr["Digit9"].ToString();
                        objComp.Digit13 = dr["Digit13"].ToString();
                        objComp.Digit14 = dr["Digit14"].ToString();
                        DataTable dt = objComp.GetCompCircuitChargeData();


                        skipFirst = true;

                        foreach (DataRow dr1 in dt.Rows)
                        {
                            if (skipFirst == false)
                            {
                                objComp.ID = dr1["ID"].ToString();
                                objComp.DeleteR6_OA_CompCircuitChargeData();
                            }
                            else
                            {
                                skipFirst = false;
                            }
                        }

                    }
                    dgvModelNo.DataSource = null;
                    objComp.Digit3 = null;
                    objComp.Digit4 = null;
                    objComp.Digit567 = "0";
                    objComp.Digit9 = null;
                    objComp.Digit13 = null;
                    objComp.Digit14 = null;
                    DataTable dtComp = objComp.GetCompCircuitChargeData();
                    populateCompressorCircuitChargeList(dtComp, false);
                }
            }
            else
            {
                MessageBox.Show("No duplicate rows of data exist!");
            }
        }

        private void txtSearchCoolCap_KeyUp(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            string revision = "";
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {
                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtSearchCoolCap.Text.ToUpper();
                if (tmpStr.Length < 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow dgr in dgvCompCircuitData.Rows)
                    {
                        if (dgr.Cells[4].Value != null)
                        {
                            if (dgr.Cells[4].Value.ToString().StartsWith(tmpStr) || dgr.Cells[4].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvCompCircuitData.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvCompCircuitData.Rows[rowIdx].Selected = true;
                                dgvCompCircuitData.FirstDisplayedScrollingRowIndex = rowIdx;
                                dgvCompCircuitData.Focus();
                                break;
                            }
                        }
                    }
                }
            }
            this.txtSearchCoolCap.Focus();
        }

        private void searchCoolingCapacityByRevision(string revision)
        {
            string tmpStr = this.txtSearchCoolCap.Text.ToUpper();

            int rowIdx = 0;

            foreach (DataGridViewRow dgr in dgvCompCircuitData.Rows)
            {
                if (dgr.Cells[4].Value != null)
                {
                    if ((dgr.Cells[4].Value.ToString().StartsWith(tmpStr) || dgr.Cells[4].Value.ToString() == tmpStr) &&
                        (dgr.Cells[3].Value.ToString() == revision))
                    {
                        rowIdx = dgr.Index;
                        dgvCompCircuitData.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                        dgvCompCircuitData.Rows[rowIdx].Selected = true;
                        dgvCompCircuitData.FirstDisplayedScrollingRowIndex = rowIdx;
                        dgvCompCircuitData.Focus();
                        break;
                    }
                }
            }
            this.txtSearchCoolCap.Focus();
        }

        private void btnUpdCDD_Reset_Click(object sender, EventArgs e)
        {
            txtSearchCoolCap.Text = "";
            DataTable dtComp = objComp.GetCompCircuitChargeData();
            populateCompressorCircuitChargeList(dtComp, false);
        }

        private void btnMassUpdate_Click(object sender, EventArgs e)
        {
            frmMassUpd frmMas = new frmMassUpd();

            frmMas.ShowDialog();
        }

        private void btnExportData_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = "CompCircuitChargeData.csv";
            saveFileDialog1.Filter = "Excel csv files (CSV)|*.csv";
            saveFileDialog1.ShowDialog();           
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            int rowCount = 0;
            int updInterval = 0;

            string digit3 = "Digit3";
            string digit4 = "Digit4";
            string digit567 = "Digit567";
            string digit9 = "Digit9";
            string digit11 = "Digit11";
            string digit12 = "Digit12";
            string digit13 = "Digit13";
            string digit14 = "Digit14";

            string circuit1_1_Compressor = "Circuit1_1_Compressor";
            string circuit1_2_Compressor = "Circuit1_2_Compressor";
            string circuit1_3_Compressor = "Circuit1_3_Compressor";
            string circuit2_1_Compressor = "Circuit2_1_Compressor";
            string circuit2_2_Compressor = "Circuit2_2_Compressor";
            string circuit2_3_Compressor = "Circuit2_3_Compressor";

            string circuit1 = "Circuit1";
            string circuit1_Reheat = "Circuit1_Reheat";
            string circuit2 = "Circuit2";
            string circuit2_Reheat = "Circuit2_Reheat";

            string filename = saveFileDialog1.FileName;

            objComp.Digit3 = null;
            objComp.Digit4 = null;
            objComp.Digit567 = "0";
            objComp.Digit9 = null;
            objComp.Digit11 = null;
            objComp.Digit12 = null;
            objComp.Digit13 = null;
            objComp.Digit14 = null;

            DataTable dtComp = objComp.GetCompCircuitChargeData();

            if (dtComp.Rows.Count > 0)
            {               
                using (StreamWriter sw = new StreamWriter(@filename))
                {
                    sw.WriteLine(digit3 + "," + digit4 + "," + digit567 + "," + digit9 + "," + digit11 + "," + digit12 + "," + digit13 + "," + digit14 + "," + circuit1_1_Compressor + "," +
                                 circuit1_2_Compressor + "," + circuit1_3_Compressor + "," + circuit2_1_Compressor + "," + circuit2_2_Compressor + "," + circuit2_3_Compressor + "," + circuit1 + "," +
                                 circuit1_Reheat + "," + circuit2 + "," + circuit2_Reheat);

                    foreach (DataRow row in dtComp.Rows)
                    {
                        sw.WriteLine(row["Digit3"].ToString() + "," + row["Digit4"].ToString() + "," + row["Digit567"].ToString() + "," + row["Digit9"].ToString() + "," + row["Digit11"].ToString() + "," +
                                     row["Digit12"].ToString() + "," + row["Digit13"].ToString() + "," + row["Digit14"].ToString() + "," + row["Circuit1_1_Compressor"].ToString() + "," +
                                     row["Circuit1_2_Compressor"].ToString() + "," + row["Circuit1_3_Compressor"].ToString() + "," + row["Circuit2_1_Compressor"].ToString() + "," +
                                     row["Circuit2_2_Compressor"].ToString() + "," + row["Circuit2_3_Compressor"].ToString() + "," + row["Circuit1_Charge"].ToString() + "," +
                                     row["Circuit1_ReheatCharge"].ToString() + "," + row["Circuit2_Charge"].ToString() + "," + row["Circuit2_ReheatCharge"].ToString());                       
                    }
                }
                MessageBox.Show("Export Complete!");
            }
            else
            {
                MessageBox.Show("Error occurred with export.");
            }
        }
        #endregion       
                                
        #region LineSize Events
        private void txtLineSizeCoolingCap_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {
                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtLineSizeCoolingCap.Text.ToUpper();
                if (tmpStr.Length < 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow dgr in dgvLineSize.Rows)
                    {
                        if (dgr.Cells["CoolingCap"].Value != null)
                        {
                            if (dgr.Cells["CoolingCap"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["CoolingCap"].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvLineSize.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvLineSize.Rows[rowIdx].Selected = true;
                                dgvLineSize.FirstDisplayedScrollingRowIndex = rowIdx;
                                dgvLineSize.Focus();
                                break;
                            }
                        }
                    }
                }
            }
            this.txtLineSizeCoolingCap.Focus();            
        }

        private void dgvLineSize_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = e.RowIndex;

            string revision = "";
            string unitType = "";
            string majorDesignSeq = "";
            string coilType = "";


            frmUpdateLineSize frmUpd = new frmUpdateLineSize(this);

            frmUpd.Text = "Update Line Size Data";
            frmUpd.lbLineSizeID.Text = dgvLineSize.Rows[rowIdx].Cells["ID"].Value.ToString();

            frmUpd.lbLineSizeEventFire.Text = "No";

            unitType = dgvLineSize.Rows[rowIdx].Cells["Digit3"].Value.ToString();
            frmUpd.cbLineSizeDigit3.Text = unitType;
            frmUpd.cbLineSizeDigit3.Enabled = false;

            majorDesignSeq = dgvLineSize.Rows[rowIdx].Cells["Digit4"].Value.ToString();
            frmUpd.cbLineSizeDigit4.Text = majorDesignSeq;
            
            frmUpd.cbLineSizeDigit4.Enabled = false;

            if (unitType == "B" || unitType == "G")
            {
                objModel.OAUTypeCode = "OALBG";
            }
            else
            {
                objModel.OAUTypeCode = "OAU123DKN";
            }

            if (majorDesignSeq != "G")
            {
                revision = "REV5";
            }
            else
            {
                revision = "REV6";
                objModel.OAUTypeCode = "";
            }

            DataTable dtDigit = objModel.GetCoolingCapByRevision(revision, 567);
            DataRow dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbLineSizeDigit567.ValueMember = "DigitVal";
            frmUpd.cbLineSizeDigit567.DisplayMember = "DigitValDesc";
            frmUpd.cbLineSizeDigit567.DataSource = dtDigit;
            //frmUpd.cbUpdCCD_Digit567.Enabled = true;
            frmUpd.cbLineSizeDigit567.SelectedIndex = findSelectIndex(dtDigit, dgvLineSize.Rows[rowIdx].Cells["CoolingCap"].Value.ToString());          

            objModel.DigitNo = "11";
            objModel.HeatType = "NA";
            objModel.Revision = revision;

            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbLineSizeDigit11.ValueMember = "DigitVal";
            frmUpd.cbLineSizeDigit11.DisplayMember = "DigitValDesc";
            frmUpd.cbLineSizeDigit11.DataSource = dtDigit;

            coilType = dgvLineSize.Rows[rowIdx].Cells["Digit11"].Value.ToString();

            if (coilType != "")
            {
                frmUpd.cbLineSizeDigit11.SelectedIndex = findSelectIndex(dtDigit, coilType);
            }
            else
            {
                frmUpd.cbLineSizeDigit11.SelectedIndex = 0;
                frmUpd.cbLineSizeDigit11.Enabled = true;
            }                      

            frmUpd.txtLineSizeCircuit1Suction.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit1Suction"].Value.ToString();
            frmUpd.txtLineSizeCircuit1LiquidLine.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit1LiquidLine"].Value.ToString();
            frmUpd.txtLineSizeCircuit1DischargeLine.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit1DischargeLine"].Value.ToString();
            frmUpd.txtLineSizeCircuit2Suction.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit2Suction"].Value.ToString();
            frmUpd.txtLineSizeCircuit2LiquidLine.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit2LiquidLine"].Value.ToString();
            frmUpd.txtLineSizeCircuit2DischargeLine.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit2DischargeLine"].Value.ToString();

            frmUpd.btnLineSizeSave.Text = "Update";

            frmUpd.ShowDialog();

            if (mCoolingCap != "0")
            {
                dgvLineSize.Rows[rowIdx].Cells["Digit3"].Value = mUnitType;
                dgvLineSize.Rows[rowIdx].Cells["Digit4"].Value = mRevision;
                dgvLineSize.Rows[rowIdx].Cells["CoolingCap"].Value = mCoolingCap;                
                dgvLineSize.Rows[rowIdx].Cells["Digit11"].Value = mIndoorCoilType;
                dgvLineSize.Rows[rowIdx].Cells["Circuit1Suction"].Value = mCircuit1Suction;
                dgvLineSize.Rows[rowIdx].Cells["Circuit1LiquidLine"].Value = mCircuit1LiquidLine;
                dgvLineSize.Rows[rowIdx].Cells["Circuit1DischargeLine"].Value = mCircuit1DischargeLine;
                dgvLineSize.Rows[rowIdx].Cells["Circuit2Suction"].Value = mCircuit2Suction;
                dgvLineSize.Rows[rowIdx].Cells["Circuit2LiquidLine"].Value = mCircuit2LiquidLine;
                dgvLineSize.Rows[rowIdx].Cells["Circuit2DischargeLine"].Value = mCircuit2DischargeLine;                
                dgvLineSize.Rows[rowIdx].Cells["ModifiedDate"].Value = mLastModDate;
                dgvLineSize.Rows[rowIdx].Cells["ModifiedBy"].Value = mModBy;
                dgvLineSize.Refresh();
            }            
        }

        private void btnLineSizeCopy_Click(object sender, EventArgs e)
        {
            int rowIdx = dgvLineSize.CurrentRow.Index;

            string revision = "";
            string unitType = "";
            string majorDesignSeq = "";
            string coilType = "";

            frmUpdateLineSize frmUpd = new frmUpdateLineSize(this);

            frmUpd.Text = "Copy Line Size Data";
            frmUpd.lbLineSizeID.Text = dgvLineSize.Rows[rowIdx].Cells["ID"].Value.ToString();

            frmUpd.lbLineSizeEventFire.Text = "No";

            unitType = dgvLineSize.Rows[rowIdx].Cells["Digit3"].Value.ToString();
            frmUpd.cbLineSizeDigit3.Text = unitType;
            frmUpd.cbLineSizeDigit3.Enabled = true;
            majorDesignSeq = dgvLineSize.Rows[rowIdx].Cells["Digit4"].Value.ToString();
            frmUpd.cbLineSizeDigit4.Text = majorDesignSeq;
            frmUpd.cbLineSizeDigit4.Enabled = true;
            

            if (unitType == "B" || unitType == "G")
            {
                objModel.OAUTypeCode = "OALBG";
            }
            else
            {
                objModel.OAUTypeCode = "OAU123DKN";
            }

            if (majorDesignSeq != "G")
            {
                revision = "REV5";
            }
            else
            {
                revision = "REV6";
                objModel.OAUTypeCode = "";
            }

            DataTable dtDigit = objModel.GetCoolingCapByRevision(revision, 567);
            DataRow dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbLineSizeDigit567.ValueMember = "DigitVal";
            frmUpd.cbLineSizeDigit567.DisplayMember = "DigitValDesc";
            frmUpd.cbLineSizeDigit567.DataSource = dtDigit;
            frmUpd.cbLineSizeDigit567.Enabled = true;
            frmUpd.cbLineSizeDigit567.SelectedIndex = findSelectIndex(dtDigit, dgvLineSize.Rows[rowIdx].Cells["CoolingCap"].Value.ToString());

            objModel.DigitNo = "11";
            objModel.HeatType = "NA";
            objModel.Revision = revision;

            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            frmUpd.cbLineSizeDigit11.ValueMember = "DigitVal";
            frmUpd.cbLineSizeDigit11.DisplayMember = "DigitValDesc";
            frmUpd.cbLineSizeDigit11.DataSource = dtDigit;
            frmUpd.cbLineSizeDigit11.Enabled = true;
            
            coilType = dgvLineSize.Rows[rowIdx].Cells["Digit11"].Value.ToString();

            if (coilType != "")
            {
                frmUpd.cbLineSizeDigit11.SelectedIndex = findSelectIndex(dtDigit, coilType);
            }
            else
            {
                frmUpd.cbLineSizeDigit11.SelectedIndex = 0;
            }

            frmUpd.txtLineSizeCircuit1Suction.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit1Suction"].Value.ToString();
            frmUpd.txtLineSizeCircuit1LiquidLine.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit1LiquidLine"].Value.ToString();
            frmUpd.txtLineSizeCircuit1DischargeLine.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit1DischargeLine"].Value.ToString();
            frmUpd.txtLineSizeCircuit2Suction.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit2Suction"].Value.ToString();
            frmUpd.txtLineSizeCircuit2LiquidLine.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit2LiquidLine"].Value.ToString();
            frmUpd.txtLineSizeCircuit2DischargeLine.Text = dgvLineSize.Rows[rowIdx].Cells["Circuit2DischargeLine"].Value.ToString();

            frmUpd.btnLineSizeSave.Text = "Add";

            frmUpd.ShowDialog();

            if (mCoolingCap != "0")
            {
                ResetLineSize();
            }            
        }

        private void dgvLineSize_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIdx = e.RowIndex;

            string coolingCap = "";
           
            if (e.ColumnIndex == 0)  // Add button within datagridview.
            {
                coolingCap = dgvLineSize.Rows[rowIdx].Cells["CoolingCap"].Value.ToString();
                DialogResult result1 = MessageBox.Show("Are you sure you want to delete the Cooling Capacity - '" + coolingCap + "' Line Size Row? Press Yes to delete & press 'No' to cancel!",
                                                       "Deleting a Line Size Row",
                                                       MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    objComp.ID = dgvLineSize.Rows[rowIdx].Cells["ID"].Value.ToString();
                    objComp.DeleteLineSizeCoolingCap();
                    ResetLineSize();
                }
            }                       
        }
       
        private void btnLineSizeReset_Click(object sender, EventArgs e)
        {
            ResetLineSize();
            this.txtLineSizeCoolingCap.Text = "";
            this.txtLineSizeCoolingCap.Focus();  
        }

        private void btnLineSizeNewCooling_Click(object sender, EventArgs e)
        {

            frmUpdateLineSize frmAdd = new frmUpdateLineSize(this);

            frmAdd.Text = "Add Cooling Capacity Line Size";
            frmAdd.btnLineSizeSave.Text = "Add";

            frmAdd.cbLineSizeDigit3.SelectedIndex = 0;
            frmAdd.cbLineSizeDigit4.SelectedIndex = 0;

            frmAdd.txtLineSizeLastModDate.Visible = false;
            frmAdd.txtLineSizeModifedBy.Visible = false;

            frmAdd.ShowDialog();

        }

        private void ResetLineSize()
        {
            objComp.Digit3 = "ALL";
            objComp.Digit4 = null;
            objComp.Digit567 = null;
            objComp.Digit11 = null;
            DataTable dt = objComp.GetLineSizeList();
            populateLineSizeList(dt, false);
        }

        #endregion                                           

       
              
    }
}
