﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;


namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmMassUpd : Form
    {
        CompressorBO objComp = new CompressorBO();

        public frmMassUpd()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {            
            //openFileDialog1.ShowDialog();
            openFileDialog1.Filter = "Excel file (CSV)|*.csv";
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txtFilename.Text = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR = " + ex);
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string fileName = txtFilename.Text;
            //string modBy = "";

            if (fileName.Length > 0)
            {
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet = null;
                Excel.Range range;

                xlApp = new Excel.Application();

                xlWorkBook = xlApp.Workbooks.Open(@fileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets[1];
                string bookNameStr = xlWorkSheet.Name.ToString();
                range = xlWorkSheet.UsedRange;

                //modBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();            

                processCompCircuitData(xlWorkSheet, range);
            }
            else
            {
                MessageBox.Show("ERROR - No file has been selected!");
            }
        }

        private void processCompCircuitData(Excel.Worksheet xlWorkSheet, Excel.Range range)
        {
            int startRow = 2;
            int rowsProcessed = 1;
            int updInterval = 0;

            objComp.ModBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            updInterval = range.Rows.Count / 100 + 1;
            progBarSave.Value = 0;
            lbSave.Visible = true;
            progBarSave.Visible = true;

            for (int rCnt = startRow; rCnt <= range.Rows.Count; rCnt++)
            {
                //if ((range.Cells[rCnt, 1] as Excel.Range).Value2 != null && (range.Cells[rCnt, 2] as Excel.Range).Value2 != null && 
                //    (range.Cells[rCnt, 3] as Excel.Range).Value2 != null && (range.Cells[rCnt, 4] as Excel.Range).Value2 != null && 
                //    (range.Cells[rCnt, 5] as Excel.Range).Value2 != null && (range.Cells[rCnt, 6] as Excel.Range).Value2 != null && 
                //    (range.Cells[rCnt, 7] as Excel.Range).Value2 != null && (range.Cells[rCnt, 8] as Excel.Range).Value2 != null)
                objComp.Digit3 = (range.Cells[rCnt, 1] as Excel.Range).Value2.ToString();
                if ((range.Cells[rCnt, 1] as Excel.Range).Value2 != null )
                {
                    string[] values = objComp.Digit3.Split(',');  
 
                    objComp.Digit3 = values[0];
                    objComp.Digit4 = values[1];
                    objComp.Digit567 = values[2];
                    objComp.Digit9 = values[3];
                    objComp.Digit11 = values[4];
                    objComp.Digit12 = values[5];
                    objComp.Digit13 = values[6];
                    objComp.Digit14 = values[7];

                    objComp.Circuit1_1_Compressor = values[8];
                    objComp.Circuit1_2_Compressor = values[9];
                    objComp.Circuit1_3_Compressor = values[10];
                    objComp.Circuit2_1_Compressor = values[11];
                    objComp.Circuit2_2_Compressor = values[12];
                    objComp.Circuit2_3_Compressor = values[13];

                    objComp.Circuit1 = values[14];
                    objComp.Circuit1_Reheat = values[15];
                    objComp.Circuit2 = values[16];
                    objComp.Circuit2_Reheat = values[17];

                    //if ((range.Cells[rCnt, 9] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit1_1_Compressor = (range.Cells[rCnt, 9] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit1_1_Compressor = "NA";
                    //}

                    //if ((range.Cells[rCnt, 10] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit1_2_Compressor = (range.Cells[rCnt, 10] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit1_2_Compressor = "NA";
                    //}

                    //if ((range.Cells[rCnt, 11] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit1_3_Compressor = (range.Cells[rCnt, 11] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit1_3_Compressor = "NA";
                    //}
                    
                    //if ((range.Cells[rCnt, 9] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit2_1_Compressor = (range.Cells[rCnt, 12] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit2_1_Compressor = "NA";
                    //}

                    //if ((range.Cells[rCnt, 13] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit2_2_Compressor = (range.Cells[rCnt, 13] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit2_2_Compressor = "NA";
                    //}

                    //if ((range.Cells[rCnt, 14] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit2_3_Compressor = (range.Cells[rCnt, 14] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit2_3_Compressor = "NA";
                    //}

                    //if ((range.Cells[rCnt, 15] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit1 = (range.Cells[rCnt, 15] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit1 = "0";
                    //}

                    //if ((range.Cells[rCnt, 16] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit1_Reheat = (range.Cells[rCnt, 16] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit1_Reheat = "0";
                    //}

                    //if ((range.Cells[rCnt, 17] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit2 = (range.Cells[rCnt, 17] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit2 = "0";
                    //}

                    //if ((range.Cells[rCnt, 18] as Excel.Range).Value2 != null)
                    //{
                    //    objComp.Circuit2_Reheat = (range.Cells[rCnt, 18] as Excel.Range).Value2.ToString();
                    //}
                    //else
                    //{
                    //    objComp.Circuit2_Reheat = "0";
                    //}
                    
                    DataTable dtComp = objComp.GetCompCircuitChargeData();
                    
                    if (dtComp.Rows.Count > 0)
                    {
                        DataRow row = dtComp.Rows[0];
                        objComp.ID = row["ID"].ToString();
                        objComp.UpdateR6_OA_CompCircuitChargeData();
                    }
                    else
                    {
                        objComp.InsertR6_OA_CompCircuitChargeData();
                    }

                    ++rowsProcessed;

                    if (rowsProcessed > updInterval)
                    {
                        progBarSave.Value += 1;
                        rowsProcessed = 1;
                    }
                }
            }
            progBarSave.Value = 100;
            MessageBox.Show("Import complete!");
        }

        private void bindDataToModel(DataRow row)
        {
            
            objComp.ModBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
        }
    }
}
