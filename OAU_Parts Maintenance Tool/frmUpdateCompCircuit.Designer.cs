﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdateCompCircuit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbUpdCCD_Digit567 = new System.Windows.Forms.ComboBox();
            this.txtUpdCCD_Circuit1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUpdCCD_LastModDate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUpdCCD_ModifedBy = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUpdCCD_Circuit2Reheat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUpdCCD_Circuit2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUpdCCD_Circuit1Reheat = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnUpdCCD_Save = new System.Windows.Forms.Button();
            this.btnUpdCCD_Cancel = new System.Windows.Forms.Button();
            this.cbUpdCCD_Digit13 = new System.Windows.Forms.ComboBox();
            this.cbUpdCCD_Digit9 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbUpdCCD_Digit14 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbUpdCCDComp1_1 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cbUpdCCDComp1_2 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbUpdCCDComp1_3 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbUpdCCDComp2_3 = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbUpdCCDComp2_2 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbUpdCCDComp2_1 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cbUpdCCD_Digit3 = new System.Windows.Forms.ComboBox();
            this.lbUpdCCD_ID = new System.Windows.Forms.Label();
            this.lbUpdCCDEventFire = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.cbUpdCCD_Digit4 = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cbUpdCCD_Digit11 = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.cbUpdCCD_Digit12 = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbUpdCCD_Digit567
            // 
            this.cbUpdCCD_Digit567.DropDownWidth = 300;
            this.cbUpdCCD_Digit567.Enabled = false;
            this.cbUpdCCD_Digit567.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_Digit567.FormattingEnabled = true;
            this.cbUpdCCD_Digit567.Items.AddRange(new object[] {
            "060",
            "072",
            "084",
            "096",
            "120",
            "144",
            "180",
            "210",
            "240",
            "264",
            "300",
            "360",
            "420",
            "480",
            "540",
            "600",
            "648"});
            this.cbUpdCCD_Digit567.Location = new System.Drawing.Point(190, 80);
            this.cbUpdCCD_Digit567.Name = "cbUpdCCD_Digit567";
            this.cbUpdCCD_Digit567.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCD_Digit567.TabIndex = 3;
            // 
            // txtUpdCCD_Circuit1
            // 
            this.txtUpdCCD_Circuit1.Location = new System.Drawing.Point(190, 348);
            this.txtUpdCCD_Circuit1.Name = "txtUpdCCD_Circuit1";
            this.txtUpdCCD_Circuit1.Size = new System.Drawing.Size(66, 20);
            this.txtUpdCCD_Circuit1.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(28, 347);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 20);
            this.label10.TabIndex = 174;
            this.label10.Text = "Circuit1 Charge:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_LastModDate
            // 
            this.txtUpdCCD_LastModDate.Enabled = false;
            this.txtUpdCCD_LastModDate.Location = new System.Drawing.Point(190, 468);
            this.txtUpdCCD_LastModDate.Name = "txtUpdCCD_LastModDate";
            this.txtUpdCCD_LastModDate.Size = new System.Drawing.Size(147, 20);
            this.txtUpdCCD_LastModDate.TabIndex = 173;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(28, 468);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 20);
            this.label9.TabIndex = 172;
            this.label9.Text = "Last Modified:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_ModifedBy
            // 
            this.txtUpdCCD_ModifedBy.Enabled = false;
            this.txtUpdCCD_ModifedBy.Location = new System.Drawing.Point(190, 444);
            this.txtUpdCCD_ModifedBy.Name = "txtUpdCCD_ModifedBy";
            this.txtUpdCCD_ModifedBy.Size = new System.Drawing.Size(147, 20);
            this.txtUpdCCD_ModifedBy.TabIndex = 171;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(28, 444);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 20);
            this.label8.TabIndex = 170;
            this.label8.Text = "Modified By:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_Circuit2Reheat
            // 
            this.txtUpdCCD_Circuit2Reheat.Location = new System.Drawing.Point(190, 420);
            this.txtUpdCCD_Circuit2Reheat.Name = "txtUpdCCD_Circuit2Reheat";
            this.txtUpdCCD_Circuit2Reheat.Size = new System.Drawing.Size(66, 20);
            this.txtUpdCCD_Circuit2Reheat.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(28, 420);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 20);
            this.label7.TabIndex = 169;
            this.label7.Text = "Circuit2 Reheat:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_Circuit2
            // 
            this.txtUpdCCD_Circuit2.Location = new System.Drawing.Point(190, 396);
            this.txtUpdCCD_Circuit2.Name = "txtUpdCCD_Circuit2";
            this.txtUpdCCD_Circuit2.Size = new System.Drawing.Size(66, 20);
            this.txtUpdCCD_Circuit2.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(28, 396);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 20);
            this.label6.TabIndex = 168;
            this.label6.Text = "Circuit2 Charge:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_Circuit1Reheat
            // 
            this.txtUpdCCD_Circuit1Reheat.Location = new System.Drawing.Point(190, 372);
            this.txtUpdCCD_Circuit1Reheat.Name = "txtUpdCCD_Circuit1Reheat";
            this.txtUpdCCD_Circuit1Reheat.Size = new System.Drawing.Size(66, 20);
            this.txtUpdCCD_Circuit1Reheat.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(28, 372);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 20);
            this.label4.TabIndex = 167;
            this.label4.Text = "Circuit1 Reheat:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(28, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 20);
            this.label2.TabIndex = 166;
            this.label2.Text = "Digit 567:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(28, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 20);
            this.label1.TabIndex = 165;
            this.label1.Text = "Digit 4:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(28, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 20);
            this.label5.TabIndex = 163;
            this.label5.Text = "Digit 3:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnUpdCCD_Save
            // 
            this.btnUpdCCD_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdCCD_Save.ForeColor = System.Drawing.Color.Green;
            this.btnUpdCCD_Save.Location = new System.Drawing.Point(540, 414);
            this.btnUpdCCD_Save.Name = "btnUpdCCD_Save";
            this.btnUpdCCD_Save.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCCD_Save.TabIndex = 18;
            this.btnUpdCCD_Save.Text = "Update";
            this.btnUpdCCD_Save.UseVisualStyleBackColor = true;
            this.btnUpdCCD_Save.Click += new System.EventHandler(this.btnUpdCCD_Save_Click);
            // 
            // btnUpdCCD_Cancel
            // 
            this.btnUpdCCD_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdCCD_Cancel.ForeColor = System.Drawing.Color.Red;
            this.btnUpdCCD_Cancel.Location = new System.Drawing.Point(540, 458);
            this.btnUpdCCD_Cancel.Name = "btnUpdCCD_Cancel";
            this.btnUpdCCD_Cancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCCD_Cancel.TabIndex = 19;
            this.btnUpdCCD_Cancel.Text = "Cancel";
            this.btnUpdCCD_Cancel.UseVisualStyleBackColor = true;
            this.btnUpdCCD_Cancel.Click += new System.EventHandler(this.btnUpdCCD_Cancel_Click);
            // 
            // cbUpdCCD_Digit13
            // 
            this.cbUpdCCD_Digit13.DropDownWidth = 300;
            this.cbUpdCCD_Digit13.Enabled = false;
            this.cbUpdCCD_Digit13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_Digit13.FormattingEnabled = true;
            this.cbUpdCCD_Digit13.Items.AddRange(new object[] {
            "060",
            "072",
            "084",
            "096",
            "120",
            "144",
            "180",
            "210",
            "240",
            "264",
            "300",
            "360",
            "420",
            "480",
            "540",
            "600",
            "648"});
            this.cbUpdCCD_Digit13.Location = new System.Drawing.Point(190, 188);
            this.cbUpdCCD_Digit13.Name = "cbUpdCCD_Digit13";
            this.cbUpdCCD_Digit13.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCD_Digit13.TabIndex = 5;
            // 
            // cbUpdCCD_Digit9
            // 
            this.cbUpdCCD_Digit9.DropDownWidth = 300;
            this.cbUpdCCD_Digit9.Enabled = false;
            this.cbUpdCCD_Digit9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_Digit9.FormattingEnabled = true;
            this.cbUpdCCD_Digit9.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUpdCCD_Digit9.Location = new System.Drawing.Point(190, 107);
            this.cbUpdCCD_Digit9.Name = "cbUpdCCD_Digit9";
            this.cbUpdCCD_Digit9.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCD_Digit9.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(28, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 20);
            this.label3.TabIndex = 178;
            this.label3.Text = "Digit 13:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label11.Location = new System.Drawing.Point(28, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(156, 20);
            this.label11.TabIndex = 177;
            this.label11.Text = "Digit 9:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdCCD_Digit14
            // 
            this.cbUpdCCD_Digit14.DropDownWidth = 300;
            this.cbUpdCCD_Digit14.Enabled = false;
            this.cbUpdCCD_Digit14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_Digit14.FormattingEnabled = true;
            this.cbUpdCCD_Digit14.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUpdCCD_Digit14.Location = new System.Drawing.Point(190, 215);
            this.cbUpdCCD_Digit14.Name = "cbUpdCCD_Digit14";
            this.cbUpdCCD_Digit14.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCD_Digit14.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label13.Location = new System.Drawing.Point(28, 217);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(156, 20);
            this.label13.TabIndex = 181;
            this.label13.Text = "Digit 14:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdCCDComp1_1
            // 
            this.cbUpdCCDComp1_1.DropDownWidth = 400;
            this.cbUpdCCDComp1_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCDComp1_1.FormattingEnabled = true;
            this.cbUpdCCDComp1_1.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUpdCCDComp1_1.Location = new System.Drawing.Point(190, 265);
            this.cbUpdCCDComp1_1.Name = "cbUpdCCDComp1_1";
            this.cbUpdCCDComp1_1.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCDComp1_1.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label12.Location = new System.Drawing.Point(28, 267);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(156, 20);
            this.label12.TabIndex = 183;
            this.label12.Text = "Circuit 1 Compressors:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label14.Location = new System.Drawing.Point(241, 243);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 20);
            this.label14.TabIndex = 184;
            this.label14.Text = "1 - 1";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label15.Location = new System.Drawing.Point(385, 243);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 20);
            this.label15.TabIndex = 186;
            this.label15.Text = "1 - 2";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdCCDComp1_2
            // 
            this.cbUpdCCDComp1_2.DropDownWidth = 400;
            this.cbUpdCCDComp1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCDComp1_2.FormattingEnabled = true;
            this.cbUpdCCDComp1_2.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUpdCCDComp1_2.Location = new System.Drawing.Point(339, 265);
            this.cbUpdCCDComp1_2.Name = "cbUpdCCDComp1_2";
            this.cbUpdCCDComp1_2.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCDComp1_2.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label16.Location = new System.Drawing.Point(537, 243);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 20);
            this.label16.TabIndex = 188;
            this.label16.Text = "1 - 3";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdCCDComp1_3
            // 
            this.cbUpdCCDComp1_3.DropDownWidth = 400;
            this.cbUpdCCDComp1_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCDComp1_3.FormattingEnabled = true;
            this.cbUpdCCDComp1_3.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUpdCCDComp1_3.Location = new System.Drawing.Point(488, 265);
            this.cbUpdCCDComp1_3.Name = "cbUpdCCDComp1_3";
            this.cbUpdCCDComp1_3.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCDComp1_3.TabIndex = 9;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label17.Location = new System.Drawing.Point(537, 293);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 20);
            this.label17.TabIndex = 195;
            this.label17.Text = "2 - 3";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdCCDComp2_3
            // 
            this.cbUpdCCDComp2_3.DropDownWidth = 400;
            this.cbUpdCCDComp2_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCDComp2_3.FormattingEnabled = true;
            this.cbUpdCCDComp2_3.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUpdCCDComp2_3.Location = new System.Drawing.Point(488, 315);
            this.cbUpdCCDComp2_3.Name = "cbUpdCCDComp2_3";
            this.cbUpdCCDComp2_3.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCDComp2_3.TabIndex = 12;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label18.Location = new System.Drawing.Point(385, 293);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 20);
            this.label18.TabIndex = 193;
            this.label18.Text = "2 - 2";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdCCDComp2_2
            // 
            this.cbUpdCCDComp2_2.DropDownWidth = 400;
            this.cbUpdCCDComp2_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCDComp2_2.FormattingEnabled = true;
            this.cbUpdCCDComp2_2.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUpdCCDComp2_2.Location = new System.Drawing.Point(339, 315);
            this.cbUpdCCDComp2_2.Name = "cbUpdCCDComp2_2";
            this.cbUpdCCDComp2_2.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCDComp2_2.TabIndex = 11;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label19.Location = new System.Drawing.Point(241, 293);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 20);
            this.label19.TabIndex = 191;
            this.label19.Text = "2 - 1";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdCCDComp2_1
            // 
            this.cbUpdCCDComp2_1.DropDownWidth = 400;
            this.cbUpdCCDComp2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCDComp2_1.FormattingEnabled = true;
            this.cbUpdCCDComp2_1.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUpdCCDComp2_1.Location = new System.Drawing.Point(190, 315);
            this.cbUpdCCDComp2_1.Name = "cbUpdCCDComp2_1";
            this.cbUpdCCDComp2_1.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCDComp2_1.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label20.Location = new System.Drawing.Point(28, 317);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(156, 20);
            this.label20.TabIndex = 190;
            this.label20.Text = "Circuit 2 Compressors:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdCCD_Digit3
            // 
            this.cbUpdCCD_Digit3.DropDownWidth = 60;
            this.cbUpdCCD_Digit3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_Digit3.FormattingEnabled = true;
            this.cbUpdCCD_Digit3.Items.AddRange(new object[] {
            "Select",
            "B",
            "D",
            "G",
            "K",
            "N"});
            this.cbUpdCCD_Digit3.Location = new System.Drawing.Point(190, 26);
            this.cbUpdCCD_Digit3.Name = "cbUpdCCD_Digit3";
            this.cbUpdCCD_Digit3.Size = new System.Drawing.Size(66, 23);
            this.cbUpdCCD_Digit3.TabIndex = 1;
            this.cbUpdCCD_Digit3.SelectedIndexChanged += new System.EventHandler(this.cbUpdCCD_Digit3_SelectedIndexChanged);
            // 
            // lbUpdCCD_ID
            // 
            this.lbUpdCCD_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpdCCD_ID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbUpdCCD_ID.Location = new System.Drawing.Point(542, 110);
            this.lbUpdCCD_ID.Name = "lbUpdCCD_ID";
            this.lbUpdCCD_ID.Size = new System.Drawing.Size(60, 20);
            this.lbUpdCCD_ID.TabIndex = 198;
            this.lbUpdCCD_ID.Text = "ID";
            this.lbUpdCCD_ID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbUpdCCD_ID.Visible = false;
            // 
            // lbUpdCCDEventFire
            // 
            this.lbUpdCCDEventFire.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpdCCDEventFire.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbUpdCCDEventFire.Location = new System.Drawing.Point(514, 130);
            this.lbUpdCCDEventFire.Name = "lbUpdCCDEventFire";
            this.lbUpdCCDEventFire.Size = new System.Drawing.Size(88, 20);
            this.lbUpdCCDEventFire.TabIndex = 199;
            this.lbUpdCCDEventFire.Text = "Yes";
            this.lbUpdCCDEventFire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbUpdCCDEventFire.Visible = false;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label21.Location = new System.Drawing.Point(265, 29);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 20);
            this.label21.TabIndex = 200;
            this.label21.Text = "Unit Type";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label22.Location = new System.Drawing.Point(331, 83);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(143, 20);
            this.label22.TabIndex = 201;
            this.label22.Text = "Cooling Capacity";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label23.Location = new System.Drawing.Point(331, 110);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(143, 20);
            this.label23.TabIndex = 202;
            this.label23.Text = "Voltage";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label24.Location = new System.Drawing.Point(331, 189);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(143, 20);
            this.label24.TabIndex = 203;
            this.label24.Text = "Compressor Type";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label25.Location = new System.Drawing.Point(331, 217);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(143, 20);
            this.label25.TabIndex = 204;
            this.label25.Text = "Outdoor Coil Type";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbUpdCCD_Digit4
            // 
            this.cbUpdCCD_Digit4.DropDownWidth = 60;
            this.cbUpdCCD_Digit4.Enabled = false;
            this.cbUpdCCD_Digit4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_Digit4.FormattingEnabled = true;
            this.cbUpdCCD_Digit4.Items.AddRange(new object[] {
            "Select",
            "D - Revision 5",
            "E - Heat Pump",
            "F - Indoor WSHP",
            "G - Revision 6"});
            this.cbUpdCCD_Digit4.Location = new System.Drawing.Point(190, 53);
            this.cbUpdCCD_Digit4.Name = "cbUpdCCD_Digit4";
            this.cbUpdCCD_Digit4.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCD_Digit4.TabIndex = 205;
            this.cbUpdCCD_Digit4.SelectedIndexChanged += new System.EventHandler(this.cbUpdCCD_Digit4_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label26.Location = new System.Drawing.Point(331, 55);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(168, 20);
            this.label26.TabIndex = 206;
            this.label26.Text = "Major Design Sequence";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label27.Location = new System.Drawing.Point(331, 135);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(143, 20);
            this.label27.TabIndex = 209;
            this.label27.Text = "Indoor Coil Type";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbUpdCCD_Digit11
            // 
            this.cbUpdCCD_Digit11.DropDownWidth = 300;
            this.cbUpdCCD_Digit11.Enabled = false;
            this.cbUpdCCD_Digit11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_Digit11.FormattingEnabled = true;
            this.cbUpdCCD_Digit11.Items.AddRange(new object[] {
            "060",
            "072",
            "084",
            "096",
            "120",
            "144",
            "180",
            "210",
            "240",
            "264",
            "300",
            "360",
            "420",
            "480",
            "540",
            "600",
            "648"});
            this.cbUpdCCD_Digit11.Location = new System.Drawing.Point(190, 134);
            this.cbUpdCCD_Digit11.Name = "cbUpdCCD_Digit11";
            this.cbUpdCCD_Digit11.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCD_Digit11.TabIndex = 207;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label28.Location = new System.Drawing.Point(28, 135);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(156, 20);
            this.label28.TabIndex = 208;
            this.label28.Text = "Digit 11:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label29.Location = new System.Drawing.Point(331, 162);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(143, 20);
            this.label29.TabIndex = 212;
            this.label29.Text = "Hot Gas Reheat";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbUpdCCD_Digit12
            // 
            this.cbUpdCCD_Digit12.DropDownWidth = 300;
            this.cbUpdCCD_Digit12.Enabled = false;
            this.cbUpdCCD_Digit12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_Digit12.FormattingEnabled = true;
            this.cbUpdCCD_Digit12.Items.AddRange(new object[] {
            "060",
            "072",
            "084",
            "096",
            "120",
            "144",
            "180",
            "210",
            "240",
            "264",
            "300",
            "360",
            "420",
            "480",
            "540",
            "600",
            "648"});
            this.cbUpdCCD_Digit12.Location = new System.Drawing.Point(190, 161);
            this.cbUpdCCD_Digit12.Name = "cbUpdCCD_Digit12";
            this.cbUpdCCD_Digit12.Size = new System.Drawing.Size(135, 23);
            this.cbUpdCCD_Digit12.TabIndex = 210;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label30.Location = new System.Drawing.Point(28, 162);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(156, 20);
            this.label30.TabIndex = 211;
            this.label30.Text = "Digit 12:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmUpdateCompCircuit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 517);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.cbUpdCCD_Digit12);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.cbUpdCCD_Digit11);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.cbUpdCCD_Digit4);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.lbUpdCCDEventFire);
            this.Controls.Add(this.lbUpdCCD_ID);
            this.Controls.Add(this.cbUpdCCD_Digit3);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cbUpdCCDComp2_3);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.cbUpdCCDComp2_2);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.cbUpdCCDComp2_1);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.cbUpdCCDComp1_3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cbUpdCCDComp1_2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cbUpdCCDComp1_1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cbUpdCCD_Digit14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cbUpdCCD_Digit13);
            this.Controls.Add(this.cbUpdCCD_Digit9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbUpdCCD_Digit567);
            this.Controls.Add(this.txtUpdCCD_Circuit1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtUpdCCD_LastModDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtUpdCCD_ModifedBy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtUpdCCD_Circuit2Reheat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtUpdCCD_Circuit2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtUpdCCD_Circuit1Reheat);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnUpdCCD_Save);
            this.Controls.Add(this.btnUpdCCD_Cancel);
            this.Name = "frmUpdateCompCircuit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Comp Circuit Data";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox cbUpdCCD_Digit567;
        public System.Windows.Forms.TextBox txtUpdCCD_Circuit1;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtUpdCCD_LastModDate;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtUpdCCD_ModifedBy;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtUpdCCD_Circuit2Reheat;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtUpdCCD_Circuit2;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtUpdCCD_Circuit1Reheat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button btnUpdCCD_Save;
        private System.Windows.Forms.Button btnUpdCCD_Cancel;
        public System.Windows.Forms.ComboBox cbUpdCCD_Digit13;
        public System.Windows.Forms.ComboBox cbUpdCCD_Digit9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.ComboBox cbUpdCCD_Digit14;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.ComboBox cbUpdCCDComp1_1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.ComboBox cbUpdCCDComp1_2;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.ComboBox cbUpdCCDComp1_3;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.ComboBox cbUpdCCDComp2_3;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.ComboBox cbUpdCCDComp2_2;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.ComboBox cbUpdCCDComp2_1;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.ComboBox cbUpdCCD_Digit3;
        public System.Windows.Forms.Label lbUpdCCDEventFire;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lbUpdCCD_ID;
        public System.Windows.Forms.ComboBox cbUpdCCD_Digit4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        public System.Windows.Forms.ComboBox cbUpdCCD_Digit11;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        public System.Windows.Forms.ComboBox cbUpdCCD_Digit12;
        private System.Windows.Forms.Label label30;
    }
}