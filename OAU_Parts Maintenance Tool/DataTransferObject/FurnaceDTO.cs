﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAU_Parts_Maintenance_Tool
{
    class FurnaceDTO
    {
        public string Amps { get; set; }

        public string BurnerRatio { get; set; }

        public string DateMod { get; set; }        

        public string FuelType { get; set; }

        public string ID { get; set; }

        public string MBHkW { get; set; }

        public string PartNum { get; set; } 

        public string Phase { get; set; }       

        public string RuleHeadID { get; set; }

        public string Username { get; set; }

        public string Volts { get; set; }
               
    }
}
