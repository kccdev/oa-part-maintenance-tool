﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAU_Parts_Maintenance_Tool
{
    class PartCategoryDTO
    {

        public string CategoryName { get; set; }

        public string CategoryDesc { get; set; }

        public string CriticalComp { get; set; }

        public string DateMod { get; set; }

        public string ID { get; set; }

        public string ModBy { get; set; }

        public string PullOrder { get; set; }

    }
}
