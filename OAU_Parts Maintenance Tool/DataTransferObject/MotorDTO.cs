﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAU_Parts_Maintenance_Tool
{
    class MotorDTO
    {
        public string DateMod { get; set; }

        public string FLA { get; set; }

        public string Hertz { get; set; }

        public string HP { get; set; }

        public string ID { get; set; }

        public string MCC { get; set; }

        public string MotorType { get; set; }

        public string PartNum { get; set; }

        public string Phase { get; set; }

        public string RLA { get; set; }

        public string RuleHeadID { get; set; }

        public string Username { get; set; }

        public string Voltage { get; set; }              
    }
}
