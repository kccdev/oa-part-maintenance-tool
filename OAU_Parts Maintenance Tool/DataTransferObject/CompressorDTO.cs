﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAU_Parts_Maintenance_Tool
{
    class CompressorDTO
    {
        public string Circuit1 { get; set; }
        public string Circuit1LiquidLine { get; set; }
        public string Circuit1DischargeLine { get; set; }
        public string Circuit1Suction { get; set; }

        public string Circuit1_Reheat { get; set; }

        public string Circuit2 { get; set; }
        public string Circuit2LiquidLine { get; set; }
        public string Circuit2DischargeLine { get; set; }
        public string Circuit2Suction { get; set; }

        public string Circuit2_Reheat { get; set; }

        public string Circuit1_1_Compressor { get; set; }
        public string Circuit1_2_Compressor { get; set; }
        public string Circuit1_3_Compressor { get; set; }
        public string Circuit2_1_Compressor { get; set; }
        public string Circuit2_2_Compressor { get; set; }
        public string Circuit2_3_Compressor { get; set; }

        public string CoolingCap { get; set; }

        public string DateMod { get; set; }

        public string Digit3 { get; set; }
        public string Digit4 { get; set; }
        public string Digit567 { get; set; }
        public string Digit9 { get; set; }
        public string Digit11 { get; set; }
        public string Digit12 { get; set; }
        public string Digit13 { get; set; }
        public string Digit14 { get; set; }

        public string Filename { get; set; }

        public string HeatType { get; set; }

        public string ID { get; set; }

        public string LRA { get; set; }

        public string ModBy { get; set; }

        public string PartCategory { get; set; }        

        public string PartDescription { get; set; }

        public string PartNum { get; set; }

        public string Phase { get; set; }

        public string RLA { get; set; }

        public string RuleHeadID { get; set; }

        public string UnitModel { get; set; }

        public string Voltage { get; set; }                         
       
    }
}
