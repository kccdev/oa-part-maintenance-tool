﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdateLineSize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbLineSizeCoilTypeRev = new System.Windows.Forms.Label();
            this.cbLineSizeDigit11 = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.cbLineSizeDigit4 = new System.Windows.Forms.ComboBox();
            this.lbLineSizeCoolingCapRev = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lbLineSizeEventFire = new System.Windows.Forms.Label();
            this.lbLineSizeID = new System.Windows.Forms.Label();
            this.cbLineSizeDigit3 = new System.Windows.Forms.ComboBox();
            this.cbLineSizeDigit567 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLineSizeCircuit1Suction = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtLineSizeLastModDate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtLineSizeModifedBy = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLineSizeCircuit2Suction = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtLineSizeCircuit1DischargeLine = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLineSizeCircuit1LiquidLine = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLineSizeCircuit2DischargeLine = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLineSizeCircuit2LiquidLine = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnLineSizeSave = new System.Windows.Forms.Button();
            this.btnLineSizeCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbLineSizeCoilTypeRev
            // 
            this.lbLineSizeCoilTypeRev.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLineSizeCoilTypeRev.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbLineSizeCoilTypeRev.Location = new System.Drawing.Point(341, 103);
            this.lbLineSizeCoilTypeRev.Name = "lbLineSizeCoilTypeRev";
            this.lbLineSizeCoilTypeRev.Size = new System.Drawing.Size(143, 20);
            this.lbLineSizeCoilTypeRev.TabIndex = 226;
            this.lbLineSizeCoilTypeRev.Text = "Indoor Coil Type";
            this.lbLineSizeCoilTypeRev.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbLineSizeDigit11
            // 
            this.cbLineSizeDigit11.DropDownWidth = 300;
            this.cbLineSizeDigit11.Enabled = false;
            this.cbLineSizeDigit11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLineSizeDigit11.FormattingEnabled = true;
            this.cbLineSizeDigit11.Items.AddRange(new object[] {
            "060",
            "072",
            "084",
            "096",
            "120",
            "144",
            "180",
            "210",
            "240",
            "264",
            "300",
            "360",
            "420",
            "480",
            "540",
            "600",
            "648"});
            this.cbLineSizeDigit11.Location = new System.Drawing.Point(200, 103);
            this.cbLineSizeDigit11.Name = "cbLineSizeDigit11";
            this.cbLineSizeDigit11.Size = new System.Drawing.Size(135, 23);
            this.cbLineSizeDigit11.TabIndex = 224;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label28.Location = new System.Drawing.Point(20, 103);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(175, 20);
            this.label28.TabIndex = 225;
            this.label28.Text = "Digit 11:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label26.Location = new System.Drawing.Point(341, 51);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(168, 20);
            this.label26.TabIndex = 223;
            this.label26.Text = "Major Design Sequence";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbLineSizeDigit4
            // 
            this.cbLineSizeDigit4.DropDownWidth = 60;
            this.cbLineSizeDigit4.Enabled = false;
            this.cbLineSizeDigit4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLineSizeDigit4.FormattingEnabled = true;
            this.cbLineSizeDigit4.Items.AddRange(new object[] {
            "Select",
            "D - Revision 5",
            "E - Heat Pump",
            "F - Indoor WSHP",
            "G - Revision 6"});
            this.cbLineSizeDigit4.Location = new System.Drawing.Point(200, 49);
            this.cbLineSizeDigit4.Name = "cbLineSizeDigit4";
            this.cbLineSizeDigit4.Size = new System.Drawing.Size(135, 23);
            this.cbLineSizeDigit4.TabIndex = 222;
            this.cbLineSizeDigit4.SelectedIndexChanged += new System.EventHandler(this.cbLineSizeDigit4_SelectedIndexChanged);
            // 
            // lbLineSizeCoolingCapRev
            // 
            this.lbLineSizeCoolingCapRev.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLineSizeCoolingCapRev.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbLineSizeCoolingCapRev.Location = new System.Drawing.Point(341, 79);
            this.lbLineSizeCoolingCapRev.Name = "lbLineSizeCoolingCapRev";
            this.lbLineSizeCoolingCapRev.Size = new System.Drawing.Size(168, 20);
            this.lbLineSizeCoolingCapRev.TabIndex = 220;
            this.lbLineSizeCoolingCapRev.Text = "Cooling Capacity";
            this.lbLineSizeCoolingCapRev.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label21.Location = new System.Drawing.Point(275, 25);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 20);
            this.label21.TabIndex = 219;
            this.label21.Text = "Unit Type";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLineSizeEventFire
            // 
            this.lbLineSizeEventFire.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLineSizeEventFire.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbLineSizeEventFire.Location = new System.Drawing.Point(396, 165);
            this.lbLineSizeEventFire.Name = "lbLineSizeEventFire";
            this.lbLineSizeEventFire.Size = new System.Drawing.Size(88, 20);
            this.lbLineSizeEventFire.TabIndex = 218;
            this.lbLineSizeEventFire.Text = "Yes";
            this.lbLineSizeEventFire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbLineSizeEventFire.Visible = false;
            // 
            // lbLineSizeID
            // 
            this.lbLineSizeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLineSizeID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbLineSizeID.Location = new System.Drawing.Point(424, 145);
            this.lbLineSizeID.Name = "lbLineSizeID";
            this.lbLineSizeID.Size = new System.Drawing.Size(60, 20);
            this.lbLineSizeID.TabIndex = 217;
            this.lbLineSizeID.Text = "ID";
            this.lbLineSizeID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbLineSizeID.Visible = false;
            // 
            // cbLineSizeDigit3
            // 
            this.cbLineSizeDigit3.DropDownWidth = 60;
            this.cbLineSizeDigit3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLineSizeDigit3.FormattingEnabled = true;
            this.cbLineSizeDigit3.Items.AddRange(new object[] {
            "Select",
            "B",
            "D",
            "G",
            "K",
            "N"});
            this.cbLineSizeDigit3.Location = new System.Drawing.Point(200, 22);
            this.cbLineSizeDigit3.Name = "cbLineSizeDigit3";
            this.cbLineSizeDigit3.Size = new System.Drawing.Size(66, 23);
            this.cbLineSizeDigit3.TabIndex = 210;
            this.cbLineSizeDigit3.SelectedIndexChanged += new System.EventHandler(this.cbLineSizeDigit3_SelectedIndexChanged);
            // 
            // cbLineSizeDigit567
            // 
            this.cbLineSizeDigit567.DropDownWidth = 300;
            this.cbLineSizeDigit567.Enabled = false;
            this.cbLineSizeDigit567.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLineSizeDigit567.FormattingEnabled = true;
            this.cbLineSizeDigit567.Items.AddRange(new object[] {
            "060",
            "072",
            "084",
            "096",
            "120",
            "144",
            "180",
            "210",
            "240",
            "264",
            "300",
            "360",
            "420",
            "480",
            "540",
            "600",
            "648"});
            this.cbLineSizeDigit567.Location = new System.Drawing.Point(200, 76);
            this.cbLineSizeDigit567.Name = "cbLineSizeDigit567";
            this.cbLineSizeDigit567.Size = new System.Drawing.Size(135, 23);
            this.cbLineSizeDigit567.TabIndex = 211;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(20, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 20);
            this.label2.TabIndex = 215;
            this.label2.Text = "Digit 567:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(20, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 20);
            this.label1.TabIndex = 214;
            this.label1.Text = "Digit 4:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(20, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(175, 20);
            this.label5.TabIndex = 213;
            this.label5.Text = "Digit 3:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLineSizeCircuit1Suction
            // 
            this.txtLineSizeCircuit1Suction.Location = new System.Drawing.Point(200, 131);
            this.txtLineSizeCircuit1Suction.Name = "txtLineSizeCircuit1Suction";
            this.txtLineSizeCircuit1Suction.Size = new System.Drawing.Size(66, 20);
            this.txtLineSizeCircuit1Suction.TabIndex = 227;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(20, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(175, 20);
            this.label10.TabIndex = 238;
            this.label10.Text = "Circuit 1 Suction:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLineSizeLastModDate
            // 
            this.txtLineSizeLastModDate.Enabled = false;
            this.txtLineSizeLastModDate.Location = new System.Drawing.Point(200, 299);
            this.txtLineSizeLastModDate.Name = "txtLineSizeLastModDate";
            this.txtLineSizeLastModDate.Size = new System.Drawing.Size(147, 20);
            this.txtLineSizeLastModDate.TabIndex = 237;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(20, 299);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 20);
            this.label9.TabIndex = 236;
            this.label9.Text = "Last Modified:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLineSizeModifedBy
            // 
            this.txtLineSizeModifedBy.Enabled = false;
            this.txtLineSizeModifedBy.Location = new System.Drawing.Point(200, 275);
            this.txtLineSizeModifedBy.Name = "txtLineSizeModifedBy";
            this.txtLineSizeModifedBy.Size = new System.Drawing.Size(147, 20);
            this.txtLineSizeModifedBy.TabIndex = 235;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(20, 273);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(175, 20);
            this.label8.TabIndex = 234;
            this.label8.Text = "Modified By:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLineSizeCircuit2Suction
            // 
            this.txtLineSizeCircuit2Suction.Location = new System.Drawing.Point(200, 203);
            this.txtLineSizeCircuit2Suction.Name = "txtLineSizeCircuit2Suction";
            this.txtLineSizeCircuit2Suction.Size = new System.Drawing.Size(66, 20);
            this.txtLineSizeCircuit2Suction.TabIndex = 230;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(20, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(175, 20);
            this.label7.TabIndex = 233;
            this.label7.Text = "Circuit 2 Suction:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLineSizeCircuit1DischargeLine
            // 
            this.txtLineSizeCircuit1DischargeLine.Location = new System.Drawing.Point(200, 179);
            this.txtLineSizeCircuit1DischargeLine.Name = "txtLineSizeCircuit1DischargeLine";
            this.txtLineSizeCircuit1DischargeLine.Size = new System.Drawing.Size(66, 20);
            this.txtLineSizeCircuit1DischargeLine.TabIndex = 229;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(20, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 20);
            this.label6.TabIndex = 232;
            this.label6.Text = "Circuit 1 Discharge Line:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLineSizeCircuit1LiquidLine
            // 
            this.txtLineSizeCircuit1LiquidLine.Location = new System.Drawing.Point(200, 155);
            this.txtLineSizeCircuit1LiquidLine.Name = "txtLineSizeCircuit1LiquidLine";
            this.txtLineSizeCircuit1LiquidLine.Size = new System.Drawing.Size(66, 20);
            this.txtLineSizeCircuit1LiquidLine.TabIndex = 228;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(20, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 20);
            this.label4.TabIndex = 231;
            this.label4.Text = "Circuit 1 Liquid Line:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLineSizeCircuit2DischargeLine
            // 
            this.txtLineSizeCircuit2DischargeLine.Location = new System.Drawing.Point(200, 251);
            this.txtLineSizeCircuit2DischargeLine.Name = "txtLineSizeCircuit2DischargeLine";
            this.txtLineSizeCircuit2DischargeLine.Size = new System.Drawing.Size(66, 20);
            this.txtLineSizeCircuit2DischargeLine.TabIndex = 240;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(20, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 20);
            this.label3.TabIndex = 242;
            this.label3.Text = "Circuit 2 Discharge Line:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLineSizeCircuit2LiquidLine
            // 
            this.txtLineSizeCircuit2LiquidLine.Location = new System.Drawing.Point(200, 227);
            this.txtLineSizeCircuit2LiquidLine.Name = "txtLineSizeCircuit2LiquidLine";
            this.txtLineSizeCircuit2LiquidLine.Size = new System.Drawing.Size(66, 20);
            this.txtLineSizeCircuit2LiquidLine.TabIndex = 239;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label11.Location = new System.Drawing.Point(20, 227);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(175, 20);
            this.label11.TabIndex = 241;
            this.label11.Text = "Circuit 2 Liquid Line:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnLineSizeSave
            // 
            this.btnLineSizeSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLineSizeSave.ForeColor = System.Drawing.Color.Green;
            this.btnLineSizeSave.Location = new System.Drawing.Point(427, 245);
            this.btnLineSizeSave.Name = "btnLineSizeSave";
            this.btnLineSizeSave.Size = new System.Drawing.Size(75, 30);
            this.btnLineSizeSave.TabIndex = 243;
            this.btnLineSizeSave.Text = "Update";
            this.btnLineSizeSave.UseVisualStyleBackColor = true;
            this.btnLineSizeSave.Click += new System.EventHandler(this.btnLineSizeSave_Click);
            // 
            // btnLineSizeCancel
            // 
            this.btnLineSizeCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLineSizeCancel.ForeColor = System.Drawing.Color.Red;
            this.btnLineSizeCancel.Location = new System.Drawing.Point(427, 289);
            this.btnLineSizeCancel.Name = "btnLineSizeCancel";
            this.btnLineSizeCancel.Size = new System.Drawing.Size(75, 30);
            this.btnLineSizeCancel.TabIndex = 244;
            this.btnLineSizeCancel.Text = "Cancel";
            this.btnLineSizeCancel.UseVisualStyleBackColor = true;
            this.btnLineSizeCancel.Click += new System.EventHandler(this.btnLineSizeCancel_Click);
            // 
            // frmUpdateLineSize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 343);
            this.Controls.Add(this.btnLineSizeSave);
            this.Controls.Add(this.btnLineSizeCancel);
            this.Controls.Add(this.txtLineSizeCircuit2DischargeLine);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtLineSizeCircuit2LiquidLine);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtLineSizeCircuit1Suction);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtLineSizeLastModDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtLineSizeModifedBy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtLineSizeCircuit2Suction);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtLineSizeCircuit1DischargeLine);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtLineSizeCircuit1LiquidLine);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbLineSizeCoilTypeRev);
            this.Controls.Add(this.cbLineSizeDigit11);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.cbLineSizeDigit4);
            this.Controls.Add(this.lbLineSizeCoolingCapRev);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.lbLineSizeEventFire);
            this.Controls.Add(this.lbLineSizeID);
            this.Controls.Add(this.cbLineSizeDigit3);
            this.Controls.Add(this.cbLineSizeDigit567);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Name = "frmUpdateLineSize";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLineSize";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbLineSizeCoilTypeRev;
        public System.Windows.Forms.ComboBox cbLineSizeDigit11;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.ComboBox cbLineSizeDigit4;
        private System.Windows.Forms.Label lbLineSizeCoolingCapRev;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.Label lbLineSizeEventFire;
        public System.Windows.Forms.Label lbLineSizeID;
        public System.Windows.Forms.ComboBox cbLineSizeDigit3;
        public System.Windows.Forms.ComboBox cbLineSizeDigit567;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtLineSizeCircuit1Suction;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtLineSizeLastModDate;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtLineSizeModifedBy;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtLineSizeCircuit2Suction;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtLineSizeCircuit1DischargeLine;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtLineSizeCircuit1LiquidLine;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtLineSizeCircuit2DischargeLine;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtLineSizeCircuit2LiquidLine;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Button btnLineSizeSave;
        private System.Windows.Forms.Button btnLineSizeCancel;
    }
}