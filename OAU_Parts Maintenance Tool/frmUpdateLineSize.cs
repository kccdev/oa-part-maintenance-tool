﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdateLineSize : Form
    {
        ModelNumDescBO objModel = new ModelNumDescBO();
        CompressorBO objComp = new CompressorBO();
        
        private frmMain m_parent;

        string majorDesignSeq = "";
        string revision = "REV5";
       
        public frmUpdateLineSize(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
        }

        #region LineSize Events

        private void btnLineSizeCancel_Click(object sender, EventArgs e)
        {
            m_parent.mCoolingCap = "0";
            this.Close();
        }

        private void cbLineSizeDigit4_SelectedIndexChanged(object sender, EventArgs e)
        {
            string unitType = cbLineSizeDigit3.Text;
            string OAUTypeCode = "";

            bool enableAll = false;

            if (unitType == "B" || unitType == "G")
            {
                OAUTypeCode = "OALBG";
            }
            else
            {
                OAUTypeCode = "OAU123DKN";
            }

            if (cbLineSizeDigit4.SelectedIndex != 0)
            {
                lbLineSizeCoolingCapRev.Text = "Cooling Capacity - Rev 5";
                lbLineSizeCoilTypeRev.Text = "Coil Type - Rev 5";
                switch (cbLineSizeDigit4.SelectedIndex)
                {
                    case 1:
                        majorDesignSeq = "D";
                        setupDigitValuesComboBoxes("REV5", OAUTypeCode);
                        cbLineSizeDigit567.Focus();
                        enableAll = true;
                        break;
                    case 2:
                        majorDesignSeq = "E";
                        setupDigitValuesComboBoxes("REV5", OAUTypeCode);
                        cbLineSizeDigit567.Focus();
                        enableAll = true;
                        break;
                    case 3:
                        majorDesignSeq = "F";
                        setupDigitValuesComboBoxes("REV5", OAUTypeCode);
                        cbLineSizeDigit567.Focus();
                        enableAll = true;
                        break;
                    case 4:
                        majorDesignSeq = "G";
                        lbLineSizeCoolingCapRev.Text = "Cooling Capacity - Rev 6";
                        lbLineSizeCoilTypeRev.Text = "Coil Type - Rev 6";
                        setupDigitValuesComboBoxes("REV6", "");
                        cbLineSizeDigit567.Focus();
                        enableAll = true;
                        break;
                    default:
                        break;
                }

                if (enableAll == true)
                {
                    cbLineSizeDigit567.Enabled = true;                   
                    cbLineSizeDigit11.Enabled = true;                  
                }

            }
        }

        private void cbLineSizeDigit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbLineSizeDigit3.SelectedIndex != 0)
            {
                cbLineSizeDigit4.Enabled = true;
            }
            else
            {
                cbLineSizeDigit4.Enabled = false;
            }
        }

        private void btnLineSizeSave_Click(object sender, EventArgs e)
        {

            if (btnLineSizeSave.Text == "Add")
            {
                saveNewCoolingCap();
            }
            else
            {
                updateCoolingCap();
            }

            this.Close();
        }


        #endregion

        #region Other Methods

        private void updateCoolingCap()
        {
            string errors = validateInputAndBindData();

            if (errors.Length == 0)
            {                
                objComp.ID = lbLineSizeID.Text;
                objComp.UpdateLineSize();

                // Update m_parent to return updated data to main screen.
                m_parent.mUnitType = objComp.Digit3;
                m_parent.mRevision = objComp.Digit4;
                m_parent.mCoolingCap = objComp.Digit567;              
                m_parent.mIndoorCoilType = objComp.Digit11;
                m_parent.mCircuit1Suction = objComp.Circuit1Suction;
                m_parent.mCircuit1LiquidLine = objComp.Circuit1LiquidLine;
                m_parent.mCircuit1DischargeLine = objComp.Circuit1DischargeLine;
                m_parent.mCircuit2Suction = objComp.Circuit2Suction;
                m_parent.mCircuit2LiquidLine = objComp.Circuit2LiquidLine;
                m_parent.mCircuit2DischargeLine = objComp.Circuit2DischargeLine;               
                m_parent.mModBy = objComp.ModBy;
                m_parent.mLastModDate = DateTime.Now.ToShortDateString();               
            }
            else
            {
                MessageBox.Show(errors);
            }
        }

        private void saveNewCoolingCap()
        {
            string errors = validateInputAndBindData();

            if (errors.Length == 0)
            {                
                objComp.InsertLineSize();
                m_parent.mCoolingCap = objComp.Digit567;
            }
            else
            {
                MessageBox.Show(errors);
            }
        }        

        private string validateInputAndBindData()
        {
            string errors = "";

            if (cbLineSizeDigit3.SelectedIndex == 0 || cbLineSizeDigit3.Text.Length == 0)
            {
                errors += "No selection made for Unit Type (Digit 3).\n";
            }

            if (cbLineSizeDigit4.SelectedIndex == 0 || cbLineSizeDigit4.Text.Length == 0)
            {
                errors += "No selection made for Major Design Sequence (Digit 4).\n";
            }

            if (cbLineSizeDigit567.SelectedIndex == 0 || cbLineSizeDigit567.Text.Length == 0)
            {
                errors += "No selection made for Cooling Capacity (Digit 567).\n";
            }           

            if (cbLineSizeDigit11.SelectedIndex == 0 || cbLineSizeDigit11.Text.Length == 0)
            {
                errors += "No selection made for Indoor Coil Type (Digit 11).\n";
            }

            if (txtLineSizeCircuit1Suction.Text.Length == 0)
            {
                errors += "Circuit 1 Suction value required.\n";
            }

            if (txtLineSizeCircuit1LiquidLine.Text.Length == 0)
            {
                errors += "Circuit 1 Liquid Line value required.\n";
            }

            if (txtLineSizeCircuit1DischargeLine.Text.Length == 0)
            {
                errors += "Circuit 1 Discharge Line value required.\n";
            }            

            if (errors.Length == 0)
            {
                string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

                int slashPos = modByStr.IndexOf("\\");

                modByStr = modByStr.Substring((slashPos + 1), modByStr.Length - (slashPos + 1));

                objComp.Digit3 = cbLineSizeDigit3.Text.ToString();                
                objComp.Digit4 = cbLineSizeDigit4.Text.Substring(0, 1);
                objComp.Digit567 = cbLineSizeDigit567.SelectedValue.ToString();
                objComp.Digit11 = cbLineSizeDigit11.SelectedValue.ToString();

                objComp.Circuit1Suction = txtLineSizeCircuit1Suction.Text;
                objComp.Circuit1LiquidLine = txtLineSizeCircuit1LiquidLine.Text;
                objComp.Circuit1DischargeLine = txtLineSizeCircuit1DischargeLine.Text;
                objComp.Circuit2Suction = txtLineSizeCircuit2Suction.Text;
                objComp.Circuit2LiquidLine = txtLineSizeCircuit2LiquidLine.Text;
                objComp.Circuit2DischargeLine = txtLineSizeCircuit2DischargeLine.Text;

                objComp.ModBy = modByStr;

                if (btnLineSizeSave.Text == "Add")
                {
                    DataTable dt = objComp.GetLineSizeList();
                    if (dt.Rows.Count > 0)
                    {
                        errors += "ERROR - Duplicate record exist utiizing this digit combination.\n";
                        m_parent.mCoolingCap = "0";
                    }
                }
            }

            return errors;
        }

        private void setupDigitValuesComboBoxes(string revision, string unitType)
        {
            if (unitType == "B" || unitType == "G")
            {
                objModel.OAUTypeCode = "OALBG";
            }
            else
            {
                objModel.OAUTypeCode = "OAU123DKN";
            }

            DataTable dtDigit = objModel.GetCoolingCapByRevision(revision, 567);
            DataRow dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            cbLineSizeDigit567.ValueMember = "DigitVal";
            cbLineSizeDigit567.DisplayMember = "DigitValDesc";
            cbLineSizeDigit567.DataSource = dtDigit;
          
            objModel.DigitNo = "11";
            objModel.HeatType = "NA";
            objModel.Revision = revision;

            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            cbLineSizeDigit11.ValueMember = "DigitVal";
            cbLineSizeDigit11.DisplayMember = "DigitValDesc";
            cbLineSizeDigit11.DataSource = dtDigit;          

        }

        #endregion

       
       

    }
}
