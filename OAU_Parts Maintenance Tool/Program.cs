﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
//#if DEBUG

//            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
//            connectionStringsSection.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=DEVSVR01;Initial Catalog=fubar;Integrated Security=True";
//            config.Save();
//            ConfigurationManager.RefreshSection("connectionStrings");

//            string testStr = connectionStringsSection.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
//#endif

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
