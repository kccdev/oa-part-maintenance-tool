﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdateModelNoDesc : Form
    {
        ModelNumDescBO objModel = new ModelNumDescBO();

        private frmMain m_parent;

        public frmUpdateModelNoDesc(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
        }

        private void btnUpdCancel_Click(object sender, EventArgs e)
        {
            m_parent.mHeatType = "NoUpdate";
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string errors = string.Empty;
           
            if (validModelNumUpdates() == true)
            {
                objModel.DigitNo = txtModelNoUpdDigitNo.Text;
                objModel.DigitValue = txtModelNoUpdDigitVal.Text;
                objModel.DigitDescription = txtModelNoUpdDigitDesc.Text;
                objModel.DigitRepresentation = txtModelNoUpdDigitRep.Text;                
                objModel.ID = lbModelNoID.Text;

                m_parent.mDigitNo = objModel.DigitNo;
                m_parent.mDigitVal = objModel.DigitValue;                
                m_parent.mDigitDesc = objModel.DigitDescription;
                m_parent.mDigitRep = objModel.DigitRepresentation;                               

                if (rbModelNoHeatTypeNA.Checked == true)
                {
                    objModel.HeatType = "NA";
                }
                else if (rbModelNoHeatTypeIF.Checked == true)
                {
                    objModel.HeatType = "IF";
                }
                else if (rbModelNoHeatTypeElec.Checked == true)
                {
                    objModel.HeatType = "ELEC";
                }
                else if (rbModelNoHeatTypeDF.Checked == true)
                {
                    objModel.HeatType = "DF";
                }
                else if (rbModelNoHeatTypeHotWater.Checked == true)
                {
                    objModel.HeatType = "HOTWATER";
                }          

                m_parent.mHeatType = objModel.HeatType;                

                if (this.btnSave.Text == "Update")
                {
                    objModel.UpdateModelNumDesc();
                   
                }
                else if (this.btnSave.Text == "Add")
                {
                    objModel.ID = "";
                    objModel.InsertModelNumDesc();
                }
                else
                {
                    DialogResult result1 = MessageBox.Show("Are you sure you want to do this ModelNumDesc Row? Press Yes to delete!",
                                                       "Deleting a ModelNumDesc Row",
                                                       MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.Yes)
                    {                       
                        objModel.DeleteModelNumDesc();                       
                    }
                }
               
                this.Close();

            }
        }

        private bool validModelNumUpdates()
        {
            bool retVal = true;
            
            string errors = String.Empty;

            if (txtModelNoUpdDigitVal.Text.Length == 0)
            {
                errors += "Error - Digit Val is a required value.\n";
                retVal = false;
            }

            if (txtModelNoUpdDigitDesc.Text.Length == 0)
            {
                errors += "Error - Digit Description is a required value.\n";                
            }

            if (txtModelNoUpdDigitRep.Text.Length == 0)
            {
                errors += "Error - Digit Representation is a required value.\n";                
            }


            if ((rbModelNoHeatTypeNA.Checked == false) && (rbModelNoHeatTypeIF.Checked == false) && (rbModelNoHeatTypeElec.Checked == false) &&
                (rbModelNoHeatTypeDF.Checked == false) && (rbModelNoHeatTypeHotWater.Checked == false))
            {
                errors += "Error - No Heat Type button has been selected.\n";
            }
            
            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }       
        
    }
}
