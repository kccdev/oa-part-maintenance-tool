﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdateModelNoDesc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtModelNoUpdDigitNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdCancel = new System.Windows.Forms.Button();
            this.txtModelNoUpdDigitVal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtModelNoUpdDigitDesc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtModelNoUpdDigitRep = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gbModelNoHeatType = new System.Windows.Forms.GroupBox();
            this.rbModelNoHeatTypeNA = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeDF = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeElec = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeIF = new System.Windows.Forms.RadioButton();
            this.lbModelNoID = new System.Windows.Forms.Label();
            this.rbModelNoHeatTypeHotWater = new System.Windows.Forms.RadioButton();
            this.gbModelNoHeatType.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtModelNoUpdDigitNo
            // 
            this.txtModelNoUpdDigitNo.Enabled = false;
            this.txtModelNoUpdDigitNo.Location = new System.Drawing.Point(181, 21);
            this.txtModelNoUpdDigitNo.Name = "txtModelNoUpdDigitNo";
            this.txtModelNoUpdDigitNo.Size = new System.Drawing.Size(45, 20);
            this.txtModelNoUpdDigitNo.TabIndex = 95;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(19, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 20);
            this.label5.TabIndex = 94;
            this.label5.Text = "Digit No:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(598, 37);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 93;
            this.btnSave.Text = "Update";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdCancel
            // 
            this.btnUpdCancel.Location = new System.Drawing.Point(598, 74);
            this.btnUpdCancel.Name = "btnUpdCancel";
            this.btnUpdCancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCancel.TabIndex = 92;
            this.btnUpdCancel.Text = "Cancel";
            this.btnUpdCancel.UseVisualStyleBackColor = true;
            this.btnUpdCancel.Click += new System.EventHandler(this.btnUpdCancel_Click);
            // 
            // txtModelNoUpdDigitVal
            // 
            this.txtModelNoUpdDigitVal.Location = new System.Drawing.Point(181, 47);
            this.txtModelNoUpdDigitVal.Name = "txtModelNoUpdDigitVal";
            this.txtModelNoUpdDigitVal.Size = new System.Drawing.Size(45, 20);
            this.txtModelNoUpdDigitVal.TabIndex = 97;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(19, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 20);
            this.label1.TabIndex = 96;
            this.label1.Text = "Digit Value:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModelNoUpdDigitDesc
            // 
            this.txtModelNoUpdDigitDesc.Location = new System.Drawing.Point(181, 73);
            this.txtModelNoUpdDigitDesc.Name = "txtModelNoUpdDigitDesc";
            this.txtModelNoUpdDigitDesc.Size = new System.Drawing.Size(169, 20);
            this.txtModelNoUpdDigitDesc.TabIndex = 99;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(19, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 20);
            this.label2.TabIndex = 98;
            this.label2.Text = "Digit Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModelNoUpdDigitRep
            // 
            this.txtModelNoUpdDigitRep.Location = new System.Drawing.Point(181, 99);
            this.txtModelNoUpdDigitRep.Name = "txtModelNoUpdDigitRep";
            this.txtModelNoUpdDigitRep.Size = new System.Drawing.Size(169, 20);
            this.txtModelNoUpdDigitRep.TabIndex = 101;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(19, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 20);
            this.label3.TabIndex = 100;
            this.label3.Text = "Digit Representation:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbModelNoHeatType
            // 
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeHotWater);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeNA);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeDF);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeElec);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeIF);
            this.gbModelNoHeatType.ForeColor = System.Drawing.Color.Red;
            this.gbModelNoHeatType.Location = new System.Drawing.Point(370, 21);
            this.gbModelNoHeatType.Name = "gbModelNoHeatType";
            this.gbModelNoHeatType.Size = new System.Drawing.Size(201, 83);
            this.gbModelNoHeatType.TabIndex = 104;
            this.gbModelNoHeatType.TabStop = false;
            this.gbModelNoHeatType.Text = "Heat Type";
            // 
            // rbModelNoHeatTypeNA
            // 
            this.rbModelNoHeatTypeNA.AutoSize = true;
            this.rbModelNoHeatTypeNA.Location = new System.Drawing.Point(7, 19);
            this.rbModelNoHeatTypeNA.Name = "rbModelNoHeatTypeNA";
            this.rbModelNoHeatTypeNA.Size = new System.Drawing.Size(40, 17);
            this.rbModelNoHeatTypeNA.TabIndex = 9;
            this.rbModelNoHeatTypeNA.Text = "NA";
            this.rbModelNoHeatTypeNA.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeDF
            // 
            this.rbModelNoHeatTypeDF.AutoSize = true;
            this.rbModelNoHeatTypeDF.Location = new System.Drawing.Point(107, 19);
            this.rbModelNoHeatTypeDF.Name = "rbModelNoHeatTypeDF";
            this.rbModelNoHeatTypeDF.Size = new System.Drawing.Size(79, 17);
            this.rbModelNoHeatTypeDF.TabIndex = 8;
            this.rbModelNoHeatTypeDF.Text = "Direct Fired";
            this.rbModelNoHeatTypeDF.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeElec
            // 
            this.rbModelNoHeatTypeElec.AutoSize = true;
            this.rbModelNoHeatTypeElec.Location = new System.Drawing.Point(7, 37);
            this.rbModelNoHeatTypeElec.Name = "rbModelNoHeatTypeElec";
            this.rbModelNoHeatTypeElec.Size = new System.Drawing.Size(60, 17);
            this.rbModelNoHeatTypeElec.TabIndex = 8;
            this.rbModelNoHeatTypeElec.Text = "Electric";
            this.rbModelNoHeatTypeElec.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeIF
            // 
            this.rbModelNoHeatTypeIF.AutoSize = true;
            this.rbModelNoHeatTypeIF.Location = new System.Drawing.Point(7, 55);
            this.rbModelNoHeatTypeIF.Name = "rbModelNoHeatTypeIF";
            this.rbModelNoHeatTypeIF.Size = new System.Drawing.Size(86, 17);
            this.rbModelNoHeatTypeIF.TabIndex = 8;
            this.rbModelNoHeatTypeIF.Text = "Indirect Fired";
            this.rbModelNoHeatTypeIF.UseVisualStyleBackColor = true;
            // 
            // lbModelNoID
            // 
            this.lbModelNoID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNoID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbModelNoID.Location = new System.Drawing.Point(374, 95);
            this.lbModelNoID.Name = "lbModelNoID";
            this.lbModelNoID.Size = new System.Drawing.Size(156, 20);
            this.lbModelNoID.TabIndex = 106;
            this.lbModelNoID.Text = "DigitNo:";
            this.lbModelNoID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbModelNoID.Visible = false;
            // 
            // rbModelNoHeatTypeHotWater
            // 
            this.rbModelNoHeatTypeHotWater.AutoSize = true;
            this.rbModelNoHeatTypeHotWater.Location = new System.Drawing.Point(107, 37);
            this.rbModelNoHeatTypeHotWater.Name = "rbModelNoHeatTypeHotWater";
            this.rbModelNoHeatTypeHotWater.Size = new System.Drawing.Size(74, 17);
            this.rbModelNoHeatTypeHotWater.TabIndex = 10;
            this.rbModelNoHeatTypeHotWater.Text = "Hot Water";
            this.rbModelNoHeatTypeHotWater.UseVisualStyleBackColor = true;
            // 
            // frmUpdateModelNoDesc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 140);
            this.Controls.Add(this.lbModelNoID);
            this.Controls.Add(this.gbModelNoHeatType);
            this.Controls.Add(this.txtModelNoUpdDigitRep);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtModelNoUpdDigitDesc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtModelNoUpdDigitVal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtModelNoUpdDigitNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpdCancel);
            this.Name = "frmUpdateModelNoDesc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Model No Update";
            this.gbModelNoHeatType.ResumeLayout(false);
            this.gbModelNoHeatType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtModelNoUpdDigitNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnUpdCancel;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitVal;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitDesc;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitRep;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbModelNoHeatType;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeNA;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeDF;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeElec;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeIF;
        public System.Windows.Forms.Label lbModelNoID;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeHotWater;
    }
}