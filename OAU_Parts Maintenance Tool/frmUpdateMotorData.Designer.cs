﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdateMotorData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbUpdMotorPartNum = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUpdMotorLastModDate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUpdMotorModifedBy = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUpdMotorPhase = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUpdMotorHertz = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lbUpdMotorID = new System.Windows.Forms.Label();
            this.txtUpdMotorPartDesc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUpdMotorRuleHeadID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnUpdMotorSave = new System.Windows.Forms.Button();
            this.btnUpdMotorCancel = new System.Windows.Forms.Button();
            this.txtUpdMotorRLA = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUpdMotorMCC = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtUpdMotorHP = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbUpdMotor_MotorType = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbUpdMotor208_50_1 = new System.Windows.Forms.RadioButton();
            this.rbUpdMotor380_50_3 = new System.Windows.Forms.RadioButton();
            this.rbUpdMotor208_50_3 = new System.Windows.Forms.RadioButton();
            this.rbUpdMotor208_60_1 = new System.Windows.Forms.RadioButton();
            this.rbUpdMotor115_60_1 = new System.Windows.Forms.RadioButton();
            this.rbUpdMotor230_240_60_3 = new System.Windows.Forms.RadioButton();
            this.rbUpdMotor575_60_3 = new System.Windows.Forms.RadioButton();
            this.rbUpdMotor460_60_3 = new System.Windows.Forms.RadioButton();
            this.rbUpdMotor208_60_3 = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUpdMotorFLA = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbUpdMotorPartNum
            // 
            this.cbUpdMotorPartNum.DropDownWidth = 500;
            this.cbUpdMotorPartNum.Enabled = false;
            this.cbUpdMotorPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdMotorPartNum.FormattingEnabled = true;
            this.cbUpdMotorPartNum.Location = new System.Drawing.Point(136, 23);
            this.cbUpdMotorPartNum.Name = "cbUpdMotorPartNum";
            this.cbUpdMotorPartNum.Size = new System.Drawing.Size(164, 23);
            this.cbUpdMotorPartNum.TabIndex = 1;
            this.cbUpdMotorPartNum.SelectedIndexChanged += new System.EventHandler(this.cbUpdMotorPartNum_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(16, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 20);
            this.label10.TabIndex = 153;
            this.label10.Text = "Motor Type:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdMotorLastModDate
            // 
            this.txtUpdMotorLastModDate.Enabled = false;
            this.txtUpdMotorLastModDate.Location = new System.Drawing.Point(136, 317);
            this.txtUpdMotorLastModDate.Name = "txtUpdMotorLastModDate";
            this.txtUpdMotorLastModDate.Size = new System.Drawing.Size(122, 20);
            this.txtUpdMotorLastModDate.TabIndex = 152;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(16, 317);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 20);
            this.label9.TabIndex = 151;
            this.label9.Text = "Last Modified:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdMotorModifedBy
            // 
            this.txtUpdMotorModifedBy.Enabled = false;
            this.txtUpdMotorModifedBy.Location = new System.Drawing.Point(136, 291);
            this.txtUpdMotorModifedBy.Name = "txtUpdMotorModifedBy";
            this.txtUpdMotorModifedBy.Size = new System.Drawing.Size(122, 20);
            this.txtUpdMotorModifedBy.TabIndex = 150;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(16, 291);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 20);
            this.label8.TabIndex = 149;
            this.label8.Text = "Modified By:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdMotorPhase
            // 
            this.txtUpdMotorPhase.Location = new System.Drawing.Point(136, 159);
            this.txtUpdMotorPhase.Name = "txtUpdMotorPhase";
            this.txtUpdMotorPhase.Size = new System.Drawing.Size(90, 20);
            this.txtUpdMotorPhase.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(16, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 20);
            this.label7.TabIndex = 148;
            this.label7.Text = "Phase:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdMotorHertz
            // 
            this.txtUpdMotorHertz.Location = new System.Drawing.Point(136, 133);
            this.txtUpdMotorHertz.Name = "txtUpdMotorHertz";
            this.txtUpdMotorHertz.Size = new System.Drawing.Size(90, 20);
            this.txtUpdMotorHertz.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(16, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 20);
            this.label6.TabIndex = 147;
            this.label6.Text = "Hertz:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbUpdMotorID
            // 
            this.lbUpdMotorID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpdMotorID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbUpdMotorID.Location = new System.Drawing.Point(319, 21);
            this.lbUpdMotorID.Name = "lbUpdMotorID";
            this.lbUpdMotorID.Size = new System.Drawing.Size(75, 20);
            this.lbUpdMotorID.TabIndex = 146;
            this.lbUpdMotorID.Text = "ID Hidden:";
            this.lbUpdMotorID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbUpdMotorID.Visible = false;
            // 
            // txtUpdMotorPartDesc
            // 
            this.txtUpdMotorPartDesc.Enabled = false;
            this.txtUpdMotorPartDesc.Location = new System.Drawing.Point(136, 52);
            this.txtUpdMotorPartDesc.Name = "txtUpdMotorPartDesc";
            this.txtUpdMotorPartDesc.Size = new System.Drawing.Size(246, 20);
            this.txtUpdMotorPartDesc.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(16, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 20);
            this.label2.TabIndex = 144;
            this.label2.Text = "Part Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(16, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 143;
            this.label1.Text = "Part Number:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdMotorRuleHeadID
            // 
            this.txtUpdMotorRuleHeadID.Enabled = false;
            this.txtUpdMotorRuleHeadID.Location = new System.Drawing.Point(136, 78);
            this.txtUpdMotorRuleHeadID.Name = "txtUpdMotorRuleHeadID";
            this.txtUpdMotorRuleHeadID.Size = new System.Drawing.Size(59, 20);
            this.txtUpdMotorRuleHeadID.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(16, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 20);
            this.label5.TabIndex = 141;
            this.label5.Text = "Rule Head ID:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnUpdMotorSave
            // 
            this.btnUpdMotorSave.Location = new System.Drawing.Point(139, 356);
            this.btnUpdMotorSave.Name = "btnUpdMotorSave";
            this.btnUpdMotorSave.Size = new System.Drawing.Size(75, 30);
            this.btnUpdMotorSave.TabIndex = 12;
            this.btnUpdMotorSave.Text = "Update";
            this.btnUpdMotorSave.UseVisualStyleBackColor = true;
            this.btnUpdMotorSave.Click += new System.EventHandler(this.btnUpdMotorSave_Click);
            // 
            // btnUpdMotorCancel
            // 
            this.btnUpdMotorCancel.Location = new System.Drawing.Point(267, 356);
            this.btnUpdMotorCancel.Name = "btnUpdMotorCancel";
            this.btnUpdMotorCancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdMotorCancel.TabIndex = 13;
            this.btnUpdMotorCancel.Text = "Cancel";
            this.btnUpdMotorCancel.UseVisualStyleBackColor = true;
            this.btnUpdMotorCancel.Click += new System.EventHandler(this.btnUpdMotorCancel_Click);
            // 
            // txtUpdMotorRLA
            // 
            this.txtUpdMotorRLA.Location = new System.Drawing.Point(136, 238);
            this.txtUpdMotorRLA.Name = "txtUpdMotorRLA";
            this.txtUpdMotorRLA.Size = new System.Drawing.Size(90, 20);
            this.txtUpdMotorRLA.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label11.Location = new System.Drawing.Point(16, 238);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 20);
            this.label11.TabIndex = 158;
            this.label11.Text = "RLA:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdMotorMCC
            // 
            this.txtUpdMotorMCC.Location = new System.Drawing.Point(136, 186);
            this.txtUpdMotorMCC.Name = "txtUpdMotorMCC";
            this.txtUpdMotorMCC.Size = new System.Drawing.Size(90, 20);
            this.txtUpdMotorMCC.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label12.Location = new System.Drawing.Point(16, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 20);
            this.label12.TabIndex = 157;
            this.label12.Text = "MCC:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdMotorHP
            // 
            this.txtUpdMotorHP.Location = new System.Drawing.Point(136, 264);
            this.txtUpdMotorHP.Name = "txtUpdMotorHP";
            this.txtUpdMotorHP.Size = new System.Drawing.Size(90, 20);
            this.txtUpdMotorHP.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label13.Location = new System.Drawing.Point(16, 264);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 20);
            this.label13.TabIndex = 161;
            this.label13.Text = "HP:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdMotor_MotorType
            // 
            this.cbUpdMotor_MotorType.DropDownWidth = 500;
            this.cbUpdMotor_MotorType.Enabled = false;
            this.cbUpdMotor_MotorType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdMotor_MotorType.FormattingEnabled = true;
            this.cbUpdMotor_MotorType.Items.AddRange(new object[] {
            "Condensor",
            "ERV",
            "FAN",
            "VFD"});
            this.cbUpdMotor_MotorType.Location = new System.Drawing.Point(136, 103);
            this.cbUpdMotor_MotorType.Name = "cbUpdMotor_MotorType";
            this.cbUpdMotor_MotorType.Size = new System.Drawing.Size(90, 23);
            this.cbUpdMotor_MotorType.TabIndex = 4;
            this.cbUpdMotor_MotorType.Tag = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbUpdMotor208_50_1);
            this.groupBox1.Controls.Add(this.rbUpdMotor380_50_3);
            this.groupBox1.Controls.Add(this.rbUpdMotor208_50_3);
            this.groupBox1.Controls.Add(this.rbUpdMotor208_60_1);
            this.groupBox1.Controls.Add(this.rbUpdMotor115_60_1);
            this.groupBox1.Controls.Add(this.rbUpdMotor230_240_60_3);
            this.groupBox1.Controls.Add(this.rbUpdMotor575_60_3);
            this.groupBox1.Controls.Add(this.rbUpdMotor460_60_3);
            this.groupBox1.Controls.Add(this.rbUpdMotor208_60_3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Red;
            this.groupBox1.Location = new System.Drawing.Point(280, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(139, 259);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Voltage";
            // 
            // rbUpdMotor208_50_1
            // 
            this.rbUpdMotor208_50_1.AutoSize = true;
            this.rbUpdMotor208_50_1.Location = new System.Drawing.Point(23, 207);
            this.rbUpdMotor208_50_1.Name = "rbUpdMotor208_50_1";
            this.rbUpdMotor208_50_1.Size = new System.Drawing.Size(79, 17);
            this.rbUpdMotor208_50_1.TabIndex = 154;
            this.rbUpdMotor208_50_1.TabStop = true;
            this.rbUpdMotor208_50_1.Text = "208/50/1";
            this.rbUpdMotor208_50_1.UseVisualStyleBackColor = true;
            this.rbUpdMotor208_50_1.CheckedChanged += new System.EventHandler(this.rbUpdMotor208_50_1_CheckedChanged);
            // 
            // rbUpdMotor380_50_3
            // 
            this.rbUpdMotor380_50_3.AutoSize = true;
            this.rbUpdMotor380_50_3.Location = new System.Drawing.Point(23, 184);
            this.rbUpdMotor380_50_3.Name = "rbUpdMotor380_50_3";
            this.rbUpdMotor380_50_3.Size = new System.Drawing.Size(79, 17);
            this.rbUpdMotor380_50_3.TabIndex = 153;
            this.rbUpdMotor380_50_3.TabStop = true;
            this.rbUpdMotor380_50_3.Text = "380/50/3";
            this.rbUpdMotor380_50_3.UseVisualStyleBackColor = true;
            this.rbUpdMotor380_50_3.CheckedChanged += new System.EventHandler(this.rbUpdMotor380_50_3_CheckedChanged);
            // 
            // rbUpdMotor208_50_3
            // 
            this.rbUpdMotor208_50_3.AutoSize = true;
            this.rbUpdMotor208_50_3.Location = new System.Drawing.Point(23, 161);
            this.rbUpdMotor208_50_3.Name = "rbUpdMotor208_50_3";
            this.rbUpdMotor208_50_3.Size = new System.Drawing.Size(79, 17);
            this.rbUpdMotor208_50_3.TabIndex = 152;
            this.rbUpdMotor208_50_3.TabStop = true;
            this.rbUpdMotor208_50_3.Text = "208/50/3";
            this.rbUpdMotor208_50_3.UseVisualStyleBackColor = true;
            this.rbUpdMotor208_50_3.CheckedChanged += new System.EventHandler(this.rbUpdMotor208_50_3_CheckedChanged);
            // 
            // rbUpdMotor208_60_1
            // 
            this.rbUpdMotor208_60_1.AutoSize = true;
            this.rbUpdMotor208_60_1.Location = new System.Drawing.Point(23, 139);
            this.rbUpdMotor208_60_1.Name = "rbUpdMotor208_60_1";
            this.rbUpdMotor208_60_1.Size = new System.Drawing.Size(79, 17);
            this.rbUpdMotor208_60_1.TabIndex = 151;
            this.rbUpdMotor208_60_1.TabStop = true;
            this.rbUpdMotor208_60_1.Text = "208/60/1";
            this.rbUpdMotor208_60_1.UseVisualStyleBackColor = true;
            this.rbUpdMotor208_60_1.CheckedChanged += new System.EventHandler(this.rbUpdMotor208_60_1_CheckedChanged);
            // 
            // rbUpdMotor115_60_1
            // 
            this.rbUpdMotor115_60_1.AutoSize = true;
            this.rbUpdMotor115_60_1.Location = new System.Drawing.Point(23, 116);
            this.rbUpdMotor115_60_1.Name = "rbUpdMotor115_60_1";
            this.rbUpdMotor115_60_1.Size = new System.Drawing.Size(79, 17);
            this.rbUpdMotor115_60_1.TabIndex = 150;
            this.rbUpdMotor115_60_1.TabStop = true;
            this.rbUpdMotor115_60_1.Text = "115/60/1";
            this.rbUpdMotor115_60_1.UseVisualStyleBackColor = true;
            this.rbUpdMotor115_60_1.CheckedChanged += new System.EventHandler(this.rbUpdMotor115_60_1_CheckedChanged);
            // 
            // rbUpdMotor230_240_60_3
            // 
            this.rbUpdMotor230_240_60_3.AutoSize = true;
            this.rbUpdMotor230_240_60_3.Location = new System.Drawing.Point(23, 94);
            this.rbUpdMotor230_240_60_3.Name = "rbUpdMotor230_240_60_3";
            this.rbUpdMotor230_240_60_3.Size = new System.Drawing.Size(104, 17);
            this.rbUpdMotor230_240_60_3.TabIndex = 149;
            this.rbUpdMotor230_240_60_3.TabStop = true;
            this.rbUpdMotor230_240_60_3.Text = "230-240/60/3";
            this.rbUpdMotor230_240_60_3.UseVisualStyleBackColor = true;
            this.rbUpdMotor230_240_60_3.CheckedChanged += new System.EventHandler(this.rbUpdMotor230_240_60_3_CheckedChanged);
            // 
            // rbUpdMotor575_60_3
            // 
            this.rbUpdMotor575_60_3.AutoSize = true;
            this.rbUpdMotor575_60_3.Location = new System.Drawing.Point(23, 71);
            this.rbUpdMotor575_60_3.Name = "rbUpdMotor575_60_3";
            this.rbUpdMotor575_60_3.Size = new System.Drawing.Size(79, 17);
            this.rbUpdMotor575_60_3.TabIndex = 148;
            this.rbUpdMotor575_60_3.TabStop = true;
            this.rbUpdMotor575_60_3.Text = "575/60/3";
            this.rbUpdMotor575_60_3.UseVisualStyleBackColor = true;
            this.rbUpdMotor575_60_3.CheckedChanged += new System.EventHandler(this.rbUpdMotor575_60_3_CheckedChanged);
            // 
            // rbUpdMotor460_60_3
            // 
            this.rbUpdMotor460_60_3.AutoSize = true;
            this.rbUpdMotor460_60_3.Location = new System.Drawing.Point(23, 49);
            this.rbUpdMotor460_60_3.Name = "rbUpdMotor460_60_3";
            this.rbUpdMotor460_60_3.Size = new System.Drawing.Size(79, 17);
            this.rbUpdMotor460_60_3.TabIndex = 147;
            this.rbUpdMotor460_60_3.TabStop = true;
            this.rbUpdMotor460_60_3.Text = "460/60/3";
            this.rbUpdMotor460_60_3.UseVisualStyleBackColor = true;
            this.rbUpdMotor460_60_3.CheckedChanged += new System.EventHandler(this.rbUpdMotor460_60_3_CheckedChanged);
            // 
            // rbUpdMotor208_60_3
            // 
            this.rbUpdMotor208_60_3.AutoSize = true;
            this.rbUpdMotor208_60_3.Location = new System.Drawing.Point(23, 26);
            this.rbUpdMotor208_60_3.Name = "rbUpdMotor208_60_3";
            this.rbUpdMotor208_60_3.Size = new System.Drawing.Size(79, 17);
            this.rbUpdMotor208_60_3.TabIndex = 146;
            this.rbUpdMotor208_60_3.TabStop = true;
            this.rbUpdMotor208_60_3.Text = "208/60/3";
            this.rbUpdMotor208_60_3.UseVisualStyleBackColor = true;
            this.rbUpdMotor208_60_3.CheckedChanged += new System.EventHandler(this.rbUpdMotor208_60_3_CheckedChanged);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label14.Location = new System.Drawing.Point(16, 212);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(114, 20);
            this.label14.TabIndex = 164;
            this.label14.Text = "FLA:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdMotorFLA
            // 
            this.txtUpdMotorFLA.Location = new System.Drawing.Point(136, 212);
            this.txtUpdMotorFLA.Name = "txtUpdMotorFLA";
            this.txtUpdMotorFLA.Size = new System.Drawing.Size(90, 20);
            this.txtUpdMotorFLA.TabIndex = 8;
            // 
            // frmUpdateMotorData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 463);
            this.Controls.Add(this.txtUpdMotorFLA);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbUpdMotor_MotorType);
            this.Controls.Add(this.txtUpdMotorHP);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtUpdMotorRLA);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtUpdMotorMCC);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cbUpdMotorPartNum);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtUpdMotorLastModDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtUpdMotorModifedBy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtUpdMotorPhase);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtUpdMotorHertz);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbUpdMotorID);
            this.Controls.Add(this.txtUpdMotorPartDesc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUpdMotorRuleHeadID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnUpdMotorSave);
            this.Controls.Add(this.btnUpdMotorCancel);
            this.Name = "frmUpdateMotorData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Motor Data";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox cbUpdMotorPartNum;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtUpdMotorLastModDate;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtUpdMotorModifedBy;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtUpdMotorPhase;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtUpdMotorHertz;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label lbUpdMotorID;
        public System.Windows.Forms.TextBox txtUpdMotorPartDesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtUpdMotorRuleHeadID;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button btnUpdMotorSave;
        private System.Windows.Forms.Button btnUpdMotorCancel;
        public System.Windows.Forms.TextBox txtUpdMotorRLA;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtUpdMotorMCC;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox txtUpdMotorHP;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.ComboBox cbUpdMotor_MotorType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox txtUpdMotorFLA;
        public System.Windows.Forms.RadioButton rbUpdMotor208_50_1;
        public System.Windows.Forms.RadioButton rbUpdMotor380_50_3;
        public System.Windows.Forms.RadioButton rbUpdMotor208_50_3;
        public System.Windows.Forms.RadioButton rbUpdMotor208_60_1;
        public System.Windows.Forms.RadioButton rbUpdMotor115_60_1;
        public System.Windows.Forms.RadioButton rbUpdMotor230_240_60_3;
        public System.Windows.Forms.RadioButton rbUpdMotor575_60_3;
        public System.Windows.Forms.RadioButton rbUpdMotor460_60_3;
        public System.Windows.Forms.RadioButton rbUpdMotor208_60_3;
    }
}