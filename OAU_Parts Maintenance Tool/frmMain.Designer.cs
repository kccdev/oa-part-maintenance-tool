﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPagePartRuleHead = new System.Windows.Forms.TabPage();
            this.btnPRH_Reset = new System.Windows.Forms.Button();
            this.btnPRH_AddPart = new System.Windows.Forms.Button();
            this.txtPRH_SearchPartNum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvPartRulesHead = new System.Windows.Forms.DataGridView();
            this.tabPagePartConditions = new System.Windows.Forms.TabPage();
            this.btnCreateSSRules = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnAddPart = new System.Windows.Forms.Button();
            this.txtSearchPartNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvPartsMain = new System.Windows.Forms.DataGridView();
            this.tabPageModelNumDesc = new System.Windows.Forms.TabPage();
            this.btnModelNoAddRow = new System.Windows.Forms.Button();
            this.gbModelNoHeatType = new System.Windows.Forms.GroupBox();
            this.rbModelNoHeatTypeHotWater = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeNA = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeDF = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeElec = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeIF = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeAll = new System.Windows.Forms.RadioButton();
            this.btnModelNoReset = new System.Windows.Forms.Button();
            this.btnModelNoSearch = new System.Windows.Forms.Button();
            this.txtSearchModelNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvModelNo = new System.Windows.Forms.DataGridView();
            this.tabPagePartCatHead = new System.Windows.Forms.TabPage();
            this.btnPartCatAdd = new System.Windows.Forms.Button();
            this.btnPartCatReset = new System.Windows.Forms.Button();
            this.dgvPartCat = new System.Windows.Forms.DataGridView();
            this.tabPageCompElecData = new System.Windows.Forms.TabPage();
            this.txtCompDtlSearchPartNum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCompDtlAddRow = new System.Windows.Forms.Button();
            this.gbVoltage = new System.Windows.Forms.GroupBox();
            this.rbCompDtlVoltAll = new System.Windows.Forms.RadioButton();
            this.rbCompDtlVolt575 = new System.Windows.Forms.RadioButton();
            this.rbCompDtlVolt460 = new System.Windows.Forms.RadioButton();
            this.rbCompDtlVolt208 = new System.Windows.Forms.RadioButton();
            this.btnCompDtlReset = new System.Windows.Forms.Button();
            this.dgvCompDetail = new System.Windows.Forms.DataGridView();
            this.tabPageMotrData = new System.Windows.Forms.TabPage();
            this.btnMotorDataAdd = new System.Windows.Forms.Button();
            this.txtMotorDataSearchPartNum = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnMotorDataReset = new System.Windows.Forms.Button();
            this.dgvMotorData = new System.Windows.Forms.DataGridView();
            this.tabPageFurnaceData = new System.Windows.Forms.TabPage();
            this.btnFurnaceDataAdd = new System.Windows.Forms.Button();
            this.txtFurnaceDataSearchPartNum = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnFurnaceDataReset = new System.Windows.Forms.Button();
            this.dgvFurnaceData = new System.Windows.Forms.DataGridView();
            this.tabPageR6CompData = new System.Windows.Forms.TabPage();
            this.btnExportData = new System.Windows.Forms.Button();
            this.btnMassUpdate = new System.Windows.Forms.Button();
            this.btnMainCCCD_DelDups = new System.Windows.Forms.Button();
            this.btnUpdCDD_Reset = new System.Windows.Forms.Button();
            this.btnMainCCCD_Copy = new System.Windows.Forms.Button();
            this.btnCompDataAdd = new System.Windows.Forms.Button();
            this.txtSearchCoolCap = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dgvCompCircuitData = new System.Windows.Forms.DataGridView();
            this.tabPageLineSize = new System.Windows.Forms.TabPage();
            this.txtLineSizeCoolingCap = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnLineSizeReset = new System.Windows.Forms.Button();
            this.btnLineSizeCopy = new System.Windows.Forms.Button();
            this.btnLineSizeNewCooling = new System.Windows.Forms.Button();
            this.dgvLineSize = new System.Windows.Forms.DataGridView();
            this.btnMainExit = new System.Windows.Forms.Button();
            this.lbMainTitle = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.rbCompDtlVolt230 = new System.Windows.Forms.RadioButton();
            this.tabMain.SuspendLayout();
            this.tabPagePartRuleHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartRulesHead)).BeginInit();
            this.tabPagePartConditions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartsMain)).BeginInit();
            this.tabPageModelNumDesc.SuspendLayout();
            this.gbModelNoHeatType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelNo)).BeginInit();
            this.tabPagePartCatHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartCat)).BeginInit();
            this.tabPageCompElecData.SuspendLayout();
            this.gbVoltage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompDetail)).BeginInit();
            this.tabPageMotrData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotorData)).BeginInit();
            this.tabPageFurnaceData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFurnaceData)).BeginInit();
            this.tabPageR6CompData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompCircuitData)).BeginInit();
            this.tabPageLineSize.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLineSize)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPagePartRuleHead);
            this.tabMain.Controls.Add(this.tabPagePartConditions);
            this.tabMain.Controls.Add(this.tabPageModelNumDesc);
            this.tabMain.Controls.Add(this.tabPagePartCatHead);
            this.tabMain.Controls.Add(this.tabPageCompElecData);
            this.tabMain.Controls.Add(this.tabPageMotrData);
            this.tabMain.Controls.Add(this.tabPageFurnaceData);
            this.tabMain.Controls.Add(this.tabPageR6CompData);
            this.tabMain.Controls.Add(this.tabPageLineSize);
            this.tabMain.Location = new System.Drawing.Point(8, 60);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1415, 646);
            this.tabMain.TabIndex = 0;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabPagePartRuleHead
            // 
            this.tabPagePartRuleHead.Controls.Add(this.btnPRH_Reset);
            this.tabPagePartRuleHead.Controls.Add(this.btnPRH_AddPart);
            this.tabPagePartRuleHead.Controls.Add(this.txtPRH_SearchPartNum);
            this.tabPagePartRuleHead.Controls.Add(this.label4);
            this.tabPagePartRuleHead.Controls.Add(this.dgvPartRulesHead);
            this.tabPagePartRuleHead.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartRuleHead.Name = "tabPagePartRuleHead";
            this.tabPagePartRuleHead.Size = new System.Drawing.Size(1407, 620);
            this.tabPagePartRuleHead.TabIndex = 8;
            this.tabPagePartRuleHead.Text = "Part Rule Head";
            this.tabPagePartRuleHead.UseVisualStyleBackColor = true;
            // 
            // btnPRH_Reset
            // 
            this.btnPRH_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPRH_Reset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPRH_Reset.Location = new System.Drawing.Point(1188, 6);
            this.btnPRH_Reset.Name = "btnPRH_Reset";
            this.btnPRH_Reset.Size = new System.Drawing.Size(88, 23);
            this.btnPRH_Reset.TabIndex = 10;
            this.btnPRH_Reset.Text = "Reset";
            this.btnPRH_Reset.UseVisualStyleBackColor = true;
            this.btnPRH_Reset.Click += new System.EventHandler(this.btnPRH_Reset_Click);
            // 
            // btnPRH_AddPart
            // 
            this.btnPRH_AddPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPRH_AddPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPRH_AddPart.Location = new System.Drawing.Point(1309, 6);
            this.btnPRH_AddPart.Name = "btnPRH_AddPart";
            this.btnPRH_AddPart.Size = new System.Drawing.Size(88, 23);
            this.btnPRH_AddPart.TabIndex = 11;
            this.btnPRH_AddPart.Text = "Add Part";
            this.btnPRH_AddPart.UseVisualStyleBackColor = true;
            this.btnPRH_AddPart.Click += new System.EventHandler(this.btnPRH_AddPart_Click);
            // 
            // txtPRH_SearchPartNum
            // 
            this.txtPRH_SearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRH_SearchPartNum.Location = new System.Drawing.Point(625, 8);
            this.txtPRH_SearchPartNum.Name = "txtPRH_SearchPartNum";
            this.txtPRH_SearchPartNum.Size = new System.Drawing.Size(235, 21);
            this.txtPRH_SearchPartNum.TabIndex = 9;
            this.txtPRH_SearchPartNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPRH_SearchPartNum_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(457, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Search Part Number:";
            // 
            // dgvPartRulesHead
            // 
            this.dgvPartRulesHead.AllowUserToAddRows = false;
            this.dgvPartRulesHead.AllowUserToDeleteRows = false;
            this.dgvPartRulesHead.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgvPartRulesHead.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPartRulesHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartRulesHead.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPartRulesHead.Location = new System.Drawing.Point(7, 36);
            this.dgvPartRulesHead.MultiSelect = false;
            this.dgvPartRulesHead.Name = "dgvPartRulesHead";
            this.dgvPartRulesHead.ReadOnly = true;
            this.dgvPartRulesHead.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartRulesHead.Size = new System.Drawing.Size(1390, 576);
            this.dgvPartRulesHead.TabIndex = 6;
            this.dgvPartRulesHead.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartRulesHead_CellMouseDoubleClick);
            // 
            // tabPagePartConditions
            // 
            this.tabPagePartConditions.Controls.Add(this.btnCreateSSRules);
            this.tabPagePartConditions.Controls.Add(this.btnClear);
            this.tabPagePartConditions.Controls.Add(this.btnReset);
            this.tabPagePartConditions.Controls.Add(this.btnCopy);
            this.tabPagePartConditions.Controls.Add(this.btnAddPart);
            this.tabPagePartConditions.Controls.Add(this.txtSearchPartNum);
            this.tabPagePartConditions.Controls.Add(this.label1);
            this.tabPagePartConditions.Controls.Add(this.dgvPartsMain);
            this.tabPagePartConditions.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartConditions.Name = "tabPagePartConditions";
            this.tabPagePartConditions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePartConditions.Size = new System.Drawing.Size(1407, 620);
            this.tabPagePartConditions.TabIndex = 0;
            this.tabPagePartConditions.Text = "Part Conditions";
            this.tabPagePartConditions.UseVisualStyleBackColor = true;
            // 
            // btnCreateSSRules
            // 
            this.btnCreateSSRules.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateSSRules.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCreateSSRules.Location = new System.Drawing.Point(16, 7);
            this.btnCreateSSRules.Name = "btnCreateSSRules";
            this.btnCreateSSRules.Size = new System.Drawing.Size(125, 23);
            this.btnCreateSSRules.TabIndex = 6;
            this.btnCreateSSRules.Text = "Create SS Rules";
            this.btnCreateSSRules.UseVisualStyleBackColor = true;
            this.btnCreateSSRules.Click += new System.EventHandler(this.btnCreateSSRules_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnClear.Location = new System.Drawing.Point(1077, 7);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 23);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear ";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnReset.Location = new System.Drawing.Point(958, 7);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(88, 23);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopy.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCopy.Location = new System.Drawing.Point(1302, 7);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(88, 23);
            this.btnCopy.TabIndex = 5;
            this.btnCopy.Text = "Copy ";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnAddPart
            // 
            this.btnAddPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnAddPart.Location = new System.Drawing.Point(1190, 7);
            this.btnAddPart.Name = "btnAddPart";
            this.btnAddPart.Size = new System.Drawing.Size(88, 23);
            this.btnAddPart.TabIndex = 4;
            this.btnAddPart.Text = "New Part";
            this.btnAddPart.UseVisualStyleBackColor = true;
            this.btnAddPart.Click += new System.EventHandler(this.btnAddPart_Click);
            // 
            // txtSearchPartNum
            // 
            this.txtSearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchPartNum.Location = new System.Drawing.Point(607, 8);
            this.txtSearchPartNum.Name = "txtSearchPartNum";
            this.txtSearchPartNum.Size = new System.Drawing.Size(235, 21);
            this.txtSearchPartNum.TabIndex = 2;
            this.txtSearchPartNum.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchPartNum_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(439, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Search Part Number:";
            // 
            // dgvPartsMain
            // 
            this.dgvPartsMain.AllowUserToAddRows = false;
            this.dgvPartsMain.AllowUserToDeleteRows = false;
            this.dgvPartsMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartsMain.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPartsMain.Location = new System.Drawing.Point(7, 36);
            this.dgvPartsMain.MultiSelect = false;
            this.dgvPartsMain.Name = "dgvPartsMain";
            this.dgvPartsMain.ReadOnly = true;
            this.dgvPartsMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartsMain.Size = new System.Drawing.Size(1393, 595);
            this.dgvPartsMain.TabIndex = 0;
            this.dgvPartsMain.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPartsMain_CellContentClick);
            this.dgvPartsMain.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartsMain_CellMouseDoubleClick);
            this.dgvPartsMain.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPartsMain_DataBindingComplete);
            // 
            // tabPageModelNumDesc
            // 
            this.tabPageModelNumDesc.Controls.Add(this.btnModelNoAddRow);
            this.tabPageModelNumDesc.Controls.Add(this.gbModelNoHeatType);
            this.tabPageModelNumDesc.Controls.Add(this.btnModelNoReset);
            this.tabPageModelNumDesc.Controls.Add(this.btnModelNoSearch);
            this.tabPageModelNumDesc.Controls.Add(this.txtSearchModelNo);
            this.tabPageModelNumDesc.Controls.Add(this.label2);
            this.tabPageModelNumDesc.Controls.Add(this.dgvModelNo);
            this.tabPageModelNumDesc.Location = new System.Drawing.Point(4, 22);
            this.tabPageModelNumDesc.Name = "tabPageModelNumDesc";
            this.tabPageModelNumDesc.Size = new System.Drawing.Size(1407, 620);
            this.tabPageModelNumDesc.TabIndex = 3;
            this.tabPageModelNumDesc.Text = "ModelNoDesc";
            this.tabPageModelNumDesc.UseVisualStyleBackColor = true;
            // 
            // btnModelNoAddRow
            // 
            this.btnModelNoAddRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoAddRow.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnModelNoAddRow.Location = new System.Drawing.Point(740, 62);
            this.btnModelNoAddRow.Name = "btnModelNoAddRow";
            this.btnModelNoAddRow.Size = new System.Drawing.Size(88, 23);
            this.btnModelNoAddRow.TabIndex = 9;
            this.btnModelNoAddRow.Text = "Add Row";
            this.btnModelNoAddRow.UseVisualStyleBackColor = true;
            this.btnModelNoAddRow.Click += new System.EventHandler(this.btnModelNoAddRow_Click);
            // 
            // gbModelNoHeatType
            // 
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeHotWater);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeNA);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeDF);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeElec);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeIF);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeAll);
            this.gbModelNoHeatType.ForeColor = System.Drawing.Color.Red;
            this.gbModelNoHeatType.Location = new System.Drawing.Point(350, 13);
            this.gbModelNoHeatType.Name = "gbModelNoHeatType";
            this.gbModelNoHeatType.Size = new System.Drawing.Size(274, 61);
            this.gbModelNoHeatType.TabIndex = 7;
            this.gbModelNoHeatType.TabStop = false;
            this.gbModelNoHeatType.Text = "Heat Type";
            // 
            // rbModelNoHeatTypeHotWater
            // 
            this.rbModelNoHeatTypeHotWater.AutoSize = true;
            this.rbModelNoHeatTypeHotWater.Location = new System.Drawing.Point(188, 16);
            this.rbModelNoHeatTypeHotWater.Name = "rbModelNoHeatTypeHotWater";
            this.rbModelNoHeatTypeHotWater.Size = new System.Drawing.Size(74, 17);
            this.rbModelNoHeatTypeHotWater.TabIndex = 10;
            this.rbModelNoHeatTypeHotWater.Text = "Hot Water";
            this.rbModelNoHeatTypeHotWater.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeHotWater.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeHotWater_CheckedChanged);
            // 
            // rbModelNoHeatTypeNA
            // 
            this.rbModelNoHeatTypeNA.AutoSize = true;
            this.rbModelNoHeatTypeNA.Location = new System.Drawing.Point(7, 39);
            this.rbModelNoHeatTypeNA.Name = "rbModelNoHeatTypeNA";
            this.rbModelNoHeatTypeNA.Size = new System.Drawing.Size(40, 17);
            this.rbModelNoHeatTypeNA.TabIndex = 9;
            this.rbModelNoHeatTypeNA.Text = "NA";
            this.rbModelNoHeatTypeNA.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeNA.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeNA_CheckedChanged);
            // 
            // rbModelNoHeatTypeDF
            // 
            this.rbModelNoHeatTypeDF.AutoSize = true;
            this.rbModelNoHeatTypeDF.Location = new System.Drawing.Point(188, 39);
            this.rbModelNoHeatTypeDF.Name = "rbModelNoHeatTypeDF";
            this.rbModelNoHeatTypeDF.Size = new System.Drawing.Size(79, 17);
            this.rbModelNoHeatTypeDF.TabIndex = 8;
            this.rbModelNoHeatTypeDF.Text = "Direct Fired";
            this.rbModelNoHeatTypeDF.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeDF.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeDF_CheckedChanged);
            // 
            // rbModelNoHeatTypeElec
            // 
            this.rbModelNoHeatTypeElec.AutoSize = true;
            this.rbModelNoHeatTypeElec.Location = new System.Drawing.Point(80, 16);
            this.rbModelNoHeatTypeElec.Name = "rbModelNoHeatTypeElec";
            this.rbModelNoHeatTypeElec.Size = new System.Drawing.Size(60, 17);
            this.rbModelNoHeatTypeElec.TabIndex = 8;
            this.rbModelNoHeatTypeElec.Text = "Electric";
            this.rbModelNoHeatTypeElec.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeElec.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeElec_CheckedChanged);
            // 
            // rbModelNoHeatTypeIF
            // 
            this.rbModelNoHeatTypeIF.AutoSize = true;
            this.rbModelNoHeatTypeIF.Location = new System.Drawing.Point(80, 39);
            this.rbModelNoHeatTypeIF.Name = "rbModelNoHeatTypeIF";
            this.rbModelNoHeatTypeIF.Size = new System.Drawing.Size(86, 17);
            this.rbModelNoHeatTypeIF.TabIndex = 8;
            this.rbModelNoHeatTypeIF.Text = "Indirect Fired";
            this.rbModelNoHeatTypeIF.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeIF.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeIF_CheckedChanged);
            // 
            // rbModelNoHeatTypeAll
            // 
            this.rbModelNoHeatTypeAll.AutoSize = true;
            this.rbModelNoHeatTypeAll.Checked = true;
            this.rbModelNoHeatTypeAll.Location = new System.Drawing.Point(7, 16);
            this.rbModelNoHeatTypeAll.Name = "rbModelNoHeatTypeAll";
            this.rbModelNoHeatTypeAll.Size = new System.Drawing.Size(44, 17);
            this.rbModelNoHeatTypeAll.TabIndex = 0;
            this.rbModelNoHeatTypeAll.TabStop = true;
            this.rbModelNoHeatTypeAll.Text = "ALL";
            this.rbModelNoHeatTypeAll.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeAll.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeAll_CheckedChanged);
            // 
            // btnModelNoReset
            // 
            this.btnModelNoReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnModelNoReset.Location = new System.Drawing.Point(740, 35);
            this.btnModelNoReset.Name = "btnModelNoReset";
            this.btnModelNoReset.Size = new System.Drawing.Size(88, 23);
            this.btnModelNoReset.TabIndex = 6;
            this.btnModelNoReset.Text = "Reset";
            this.btnModelNoReset.UseVisualStyleBackColor = true;
            this.btnModelNoReset.Click += new System.EventHandler(this.btnModelNoReset_Click);
            // 
            // btnModelNoSearch
            // 
            this.btnModelNoSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoSearch.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnModelNoSearch.Location = new System.Drawing.Point(740, 9);
            this.btnModelNoSearch.Name = "btnModelNoSearch";
            this.btnModelNoSearch.Size = new System.Drawing.Size(88, 23);
            this.btnModelNoSearch.TabIndex = 5;
            this.btnModelNoSearch.Text = "Search ";
            this.btnModelNoSearch.UseVisualStyleBackColor = true;
            this.btnModelNoSearch.Click += new System.EventHandler(this.btnModelNoSearch_Click);
            // 
            // txtSearchModelNo
            // 
            this.txtSearchModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchModelNo.Location = new System.Drawing.Point(187, 38);
            this.txtSearchModelNo.Name = "txtSearchModelNo";
            this.txtSearchModelNo.Size = new System.Drawing.Size(61, 21);
            this.txtSearchModelNo.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(62, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Search Digit No:";
            // 
            // dgvModelNo
            // 
            this.dgvModelNo.AllowUserToAddRows = false;
            this.dgvModelNo.AllowUserToDeleteRows = false;
            this.dgvModelNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvModelNo.Location = new System.Drawing.Point(7, 91);
            this.dgvModelNo.MultiSelect = false;
            this.dgvModelNo.Name = "dgvModelNo";
            this.dgvModelNo.ReadOnly = true;
            this.dgvModelNo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvModelNo.Size = new System.Drawing.Size(823, 540);
            this.dgvModelNo.TabIndex = 0;
            this.dgvModelNo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvModelNo_CellContentClick);
            this.dgvModelNo.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvModelNo_CellMouseDoubleClick);
            this.dgvModelNo.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvModelNo_DataBindingComplete);
            // 
            // tabPagePartCatHead
            // 
            this.tabPagePartCatHead.Controls.Add(this.btnPartCatAdd);
            this.tabPagePartCatHead.Controls.Add(this.btnPartCatReset);
            this.tabPagePartCatHead.Controls.Add(this.dgvPartCat);
            this.tabPagePartCatHead.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartCatHead.Name = "tabPagePartCatHead";
            this.tabPagePartCatHead.Size = new System.Drawing.Size(1407, 620);
            this.tabPagePartCatHead.TabIndex = 4;
            this.tabPagePartCatHead.Text = "PartCategory";
            this.tabPagePartCatHead.UseVisualStyleBackColor = true;
            // 
            // btnPartCatAdd
            // 
            this.btnPartCatAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartCatAdd.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPartCatAdd.Location = new System.Drawing.Point(708, 8);
            this.btnPartCatAdd.Name = "btnPartCatAdd";
            this.btnPartCatAdd.Size = new System.Drawing.Size(100, 23);
            this.btnPartCatAdd.TabIndex = 41;
            this.btnPartCatAdd.Text = "Add Category";
            this.btnPartCatAdd.UseVisualStyleBackColor = true;
            this.btnPartCatAdd.Click += new System.EventHandler(this.btnPartCatAdd_Click);
            // 
            // btnPartCatReset
            // 
            this.btnPartCatReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartCatReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPartCatReset.Location = new System.Drawing.Point(574, 8);
            this.btnPartCatReset.Name = "btnPartCatReset";
            this.btnPartCatReset.Size = new System.Drawing.Size(100, 23);
            this.btnPartCatReset.TabIndex = 38;
            this.btnPartCatReset.Text = "Reset";
            this.btnPartCatReset.UseVisualStyleBackColor = true;
            this.btnPartCatReset.Click += new System.EventHandler(this.btnPartCatReset_Click);
            // 
            // dgvPartCat
            // 
            this.dgvPartCat.AllowUserToAddRows = false;
            this.dgvPartCat.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvPartCat.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPartCat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartCat.Location = new System.Drawing.Point(8, 37);
            this.dgvPartCat.MultiSelect = false;
            this.dgvPartCat.Name = "dgvPartCat";
            this.dgvPartCat.ReadOnly = true;
            this.dgvPartCat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartCat.Size = new System.Drawing.Size(800, 575);
            this.dgvPartCat.TabIndex = 37;
            this.dgvPartCat.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPartCat_CellContentClick);
            this.dgvPartCat.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartCat_CellMouseDoubleClick);
            // 
            // tabPageCompElecData
            // 
            this.tabPageCompElecData.Controls.Add(this.txtCompDtlSearchPartNum);
            this.tabPageCompElecData.Controls.Add(this.label3);
            this.tabPageCompElecData.Controls.Add(this.btnCompDtlAddRow);
            this.tabPageCompElecData.Controls.Add(this.gbVoltage);
            this.tabPageCompElecData.Controls.Add(this.btnCompDtlReset);
            this.tabPageCompElecData.Controls.Add(this.dgvCompDetail);
            this.tabPageCompElecData.Location = new System.Drawing.Point(4, 22);
            this.tabPageCompElecData.Name = "tabPageCompElecData";
            this.tabPageCompElecData.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCompElecData.Size = new System.Drawing.Size(1407, 620);
            this.tabPageCompElecData.TabIndex = 1;
            this.tabPageCompElecData.Text = "Compressor Data";
            this.tabPageCompElecData.UseVisualStyleBackColor = true;
            // 
            // txtCompDtlSearchPartNum
            // 
            this.txtCompDtlSearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompDtlSearchPartNum.Location = new System.Drawing.Point(613, 19);
            this.txtCompDtlSearchPartNum.Name = "txtCompDtlSearchPartNum";
            this.txtCompDtlSearchPartNum.Size = new System.Drawing.Size(235, 21);
            this.txtCompDtlSearchPartNum.TabIndex = 19;
            this.txtCompDtlSearchPartNum.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCompDtlSearchPartNum_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(468, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 15);
            this.label3.TabIndex = 18;
            this.label3.Text = "Search Part Number:";
            // 
            // btnCompDtlAddRow
            // 
            this.btnCompDtlAddRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompDtlAddRow.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCompDtlAddRow.Location = new System.Drawing.Point(1254, 39);
            this.btnCompDtlAddRow.Name = "btnCompDtlAddRow";
            this.btnCompDtlAddRow.Size = new System.Drawing.Size(88, 23);
            this.btnCompDtlAddRow.TabIndex = 17;
            this.btnCompDtlAddRow.Text = "Add Comp";
            this.btnCompDtlAddRow.UseVisualStyleBackColor = true;
            this.btnCompDtlAddRow.Click += new System.EventHandler(this.btnCompDtlAddRow_Click);
            // 
            // gbVoltage
            // 
            this.gbVoltage.Controls.Add(this.rbCompDtlVolt230);
            this.gbVoltage.Controls.Add(this.rbCompDtlVoltAll);
            this.gbVoltage.Controls.Add(this.rbCompDtlVolt575);
            this.gbVoltage.Controls.Add(this.rbCompDtlVolt460);
            this.gbVoltage.Controls.Add(this.rbCompDtlVolt208);
            this.gbVoltage.ForeColor = System.Drawing.Color.Green;
            this.gbVoltage.Location = new System.Drawing.Point(988, 3);
            this.gbVoltage.Name = "gbVoltage";
            this.gbVoltage.Size = new System.Drawing.Size(174, 62);
            this.gbVoltage.TabIndex = 16;
            this.gbVoltage.TabStop = false;
            this.gbVoltage.Text = "Voltage";
            // 
            // rbCompDtlVoltAll
            // 
            this.rbCompDtlVoltAll.AutoSize = true;
            this.rbCompDtlVoltAll.Checked = true;
            this.rbCompDtlVoltAll.Location = new System.Drawing.Point(11, 25);
            this.rbCompDtlVoltAll.Name = "rbCompDtlVoltAll";
            this.rbCompDtlVoltAll.Size = new System.Drawing.Size(44, 17);
            this.rbCompDtlVoltAll.TabIndex = 20;
            this.rbCompDtlVoltAll.TabStop = true;
            this.rbCompDtlVoltAll.Text = "ALL";
            this.rbCompDtlVoltAll.UseVisualStyleBackColor = true;
            this.rbCompDtlVoltAll.CheckedChanged += new System.EventHandler(this.rbCompDtlVoltAll_CheckedChanged);
            // 
            // rbCompDtlVolt575
            // 
            this.rbCompDtlVolt575.AutoSize = true;
            this.rbCompDtlVolt575.Location = new System.Drawing.Point(119, 38);
            this.rbCompDtlVolt575.Name = "rbCompDtlVolt575";
            this.rbCompDtlVolt575.Size = new System.Drawing.Size(43, 17);
            this.rbCompDtlVolt575.TabIndex = 8;
            this.rbCompDtlVolt575.Text = "575";
            this.rbCompDtlVolt575.UseVisualStyleBackColor = true;
            this.rbCompDtlVolt575.CheckedChanged += new System.EventHandler(this.rbCompDtlVolt575_CheckedChanged);
            // 
            // rbCompDtlVolt460
            // 
            this.rbCompDtlVolt460.AutoSize = true;
            this.rbCompDtlVolt460.Location = new System.Drawing.Point(119, 16);
            this.rbCompDtlVolt460.Name = "rbCompDtlVolt460";
            this.rbCompDtlVolt460.Size = new System.Drawing.Size(43, 17);
            this.rbCompDtlVolt460.TabIndex = 8;
            this.rbCompDtlVolt460.Text = "460";
            this.rbCompDtlVolt460.UseVisualStyleBackColor = true;
            this.rbCompDtlVolt460.CheckedChanged += new System.EventHandler(this.rbCompDtlVolt460_CheckedChanged);
            // 
            // rbCompDtlVolt208
            // 
            this.rbCompDtlVolt208.AutoSize = true;
            this.rbCompDtlVolt208.Location = new System.Drawing.Point(64, 16);
            this.rbCompDtlVolt208.Name = "rbCompDtlVolt208";
            this.rbCompDtlVolt208.Size = new System.Drawing.Size(43, 17);
            this.rbCompDtlVolt208.TabIndex = 0;
            this.rbCompDtlVolt208.Text = "208";
            this.rbCompDtlVolt208.UseVisualStyleBackColor = true;
            this.rbCompDtlVolt208.CheckedChanged += new System.EventHandler(this.rbCompDtlVolt208_CheckedChanged);
            // 
            // btnCompDtlReset
            // 
            this.btnCompDtlReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompDtlReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCompDtlReset.Location = new System.Drawing.Point(1254, 6);
            this.btnCompDtlReset.Name = "btnCompDtlReset";
            this.btnCompDtlReset.Size = new System.Drawing.Size(88, 23);
            this.btnCompDtlReset.TabIndex = 14;
            this.btnCompDtlReset.Text = "Reset";
            this.btnCompDtlReset.UseVisualStyleBackColor = true;
            this.btnCompDtlReset.Click += new System.EventHandler(this.btnCompDtlReset_Click);
            // 
            // dgvCompDetail
            // 
            this.dgvCompDetail.AllowUserToAddRows = false;
            this.dgvCompDetail.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvCompDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCompDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompDetail.Location = new System.Drawing.Point(7, 68);
            this.dgvCompDetail.MultiSelect = false;
            this.dgvCompDetail.Name = "dgvCompDetail";
            this.dgvCompDetail.ReadOnly = true;
            this.dgvCompDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCompDetail.Size = new System.Drawing.Size(1335, 543);
            this.dgvCompDetail.TabIndex = 10;
            this.dgvCompDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCompDetail_CellContentClick);
            this.dgvCompDetail.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCompDetail_CellMouseDoubleClick);
            // 
            // tabPageMotrData
            // 
            this.tabPageMotrData.Controls.Add(this.btnMotorDataAdd);
            this.tabPageMotrData.Controls.Add(this.txtMotorDataSearchPartNum);
            this.tabPageMotrData.Controls.Add(this.label6);
            this.tabPageMotrData.Controls.Add(this.btnMotorDataReset);
            this.tabPageMotrData.Controls.Add(this.dgvMotorData);
            this.tabPageMotrData.Location = new System.Drawing.Point(4, 22);
            this.tabPageMotrData.Name = "tabPageMotrData";
            this.tabPageMotrData.Size = new System.Drawing.Size(1407, 620);
            this.tabPageMotrData.TabIndex = 2;
            this.tabPageMotrData.Text = "Motor Data";
            this.tabPageMotrData.UseVisualStyleBackColor = true;
            // 
            // btnMotorDataAdd
            // 
            this.btnMotorDataAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMotorDataAdd.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnMotorDataAdd.Location = new System.Drawing.Point(1176, 6);
            this.btnMotorDataAdd.Name = "btnMotorDataAdd";
            this.btnMotorDataAdd.Size = new System.Drawing.Size(88, 23);
            this.btnMotorDataAdd.TabIndex = 31;
            this.btnMotorDataAdd.Text = "Add Motor";
            this.btnMotorDataAdd.UseVisualStyleBackColor = true;
            this.btnMotorDataAdd.Click += new System.EventHandler(this.btnMotorDataAdd_Click);
            // 
            // txtMotorDataSearchPartNum
            // 
            this.txtMotorDataSearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotorDataSearchPartNum.Location = new System.Drawing.Point(649, 8);
            this.txtMotorDataSearchPartNum.Name = "txtMotorDataSearchPartNum";
            this.txtMotorDataSearchPartNum.Size = new System.Drawing.Size(164, 21);
            this.txtMotorDataSearchPartNum.TabIndex = 29;
            this.txtMotorDataSearchPartNum.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMotorDataSearchPartNum_KeyUp);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(503, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 15);
            this.label6.TabIndex = 28;
            this.label6.Text = "Search Part Number:";
            // 
            // btnMotorDataReset
            // 
            this.btnMotorDataReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMotorDataReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnMotorDataReset.Location = new System.Drawing.Point(1066, 6);
            this.btnMotorDataReset.Name = "btnMotorDataReset";
            this.btnMotorDataReset.Size = new System.Drawing.Size(88, 23);
            this.btnMotorDataReset.TabIndex = 27;
            this.btnMotorDataReset.Text = "Reset";
            this.btnMotorDataReset.UseVisualStyleBackColor = true;
            this.btnMotorDataReset.Click += new System.EventHandler(this.btnMotorDataReset_Click);
            // 
            // dgvMotorData
            // 
            this.dgvMotorData.AllowUserToAddRows = false;
            this.dgvMotorData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvMotorData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMotorData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMotorData.Location = new System.Drawing.Point(7, 37);
            this.dgvMotorData.MultiSelect = false;
            this.dgvMotorData.Name = "dgvMotorData";
            this.dgvMotorData.ReadOnly = true;
            this.dgvMotorData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMotorData.Size = new System.Drawing.Size(1257, 575);
            this.dgvMotorData.TabIndex = 26;
            this.dgvMotorData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMotorData_CellContentClick);
            this.dgvMotorData.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMotorData_CellMouseDoubleClick);
            // 
            // tabPageFurnaceData
            // 
            this.tabPageFurnaceData.Controls.Add(this.btnFurnaceDataAdd);
            this.tabPageFurnaceData.Controls.Add(this.txtFurnaceDataSearchPartNum);
            this.tabPageFurnaceData.Controls.Add(this.label7);
            this.tabPageFurnaceData.Controls.Add(this.btnFurnaceDataReset);
            this.tabPageFurnaceData.Controls.Add(this.dgvFurnaceData);
            this.tabPageFurnaceData.Location = new System.Drawing.Point(4, 22);
            this.tabPageFurnaceData.Name = "tabPageFurnaceData";
            this.tabPageFurnaceData.Size = new System.Drawing.Size(1407, 620);
            this.tabPageFurnaceData.TabIndex = 5;
            this.tabPageFurnaceData.Text = "FurnaceData";
            this.tabPageFurnaceData.UseVisualStyleBackColor = true;
            // 
            // btnFurnaceDataAdd
            // 
            this.btnFurnaceDataAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFurnaceDataAdd.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnFurnaceDataAdd.Location = new System.Drawing.Point(1103, 7);
            this.btnFurnaceDataAdd.Name = "btnFurnaceDataAdd";
            this.btnFurnaceDataAdd.Size = new System.Drawing.Size(88, 23);
            this.btnFurnaceDataAdd.TabIndex = 36;
            this.btnFurnaceDataAdd.Text = "Add Furnace";
            this.btnFurnaceDataAdd.UseVisualStyleBackColor = true;
            this.btnFurnaceDataAdd.Click += new System.EventHandler(this.btnFurnaceDataAdd_Click);
            // 
            // txtFurnaceDataSearchPartNum
            // 
            this.txtFurnaceDataSearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFurnaceDataSearchPartNum.Location = new System.Drawing.Point(606, 9);
            this.txtFurnaceDataSearchPartNum.Name = "txtFurnaceDataSearchPartNum";
            this.txtFurnaceDataSearchPartNum.Size = new System.Drawing.Size(164, 21);
            this.txtFurnaceDataSearchPartNum.TabIndex = 35;
            this.txtFurnaceDataSearchPartNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFurnaceDataSearchPartNum_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(460, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 15);
            this.label7.TabIndex = 34;
            this.label7.Text = "Search Part Number:";
            // 
            // btnFurnaceDataReset
            // 
            this.btnFurnaceDataReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFurnaceDataReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnFurnaceDataReset.Location = new System.Drawing.Point(993, 7);
            this.btnFurnaceDataReset.Name = "btnFurnaceDataReset";
            this.btnFurnaceDataReset.Size = new System.Drawing.Size(88, 23);
            this.btnFurnaceDataReset.TabIndex = 33;
            this.btnFurnaceDataReset.Text = "Reset";
            this.btnFurnaceDataReset.UseVisualStyleBackColor = true;
            this.btnFurnaceDataReset.Click += new System.EventHandler(this.btnFurnaceDataReset_Click);
            // 
            // dgvFurnaceData
            // 
            this.dgvFurnaceData.AllowUserToAddRows = false;
            this.dgvFurnaceData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvFurnaceData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvFurnaceData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFurnaceData.Location = new System.Drawing.Point(8, 37);
            this.dgvFurnaceData.MultiSelect = false;
            this.dgvFurnaceData.Name = "dgvFurnaceData";
            this.dgvFurnaceData.ReadOnly = true;
            this.dgvFurnaceData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFurnaceData.Size = new System.Drawing.Size(1185, 575);
            this.dgvFurnaceData.TabIndex = 32;
            this.dgvFurnaceData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFurnaceData_CellContentClick);
            this.dgvFurnaceData.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvFurnaceData_CellMouseDoubleClick);
            // 
            // tabPageR6CompData
            // 
            this.tabPageR6CompData.Controls.Add(this.btnExportData);
            this.tabPageR6CompData.Controls.Add(this.btnMassUpdate);
            this.tabPageR6CompData.Controls.Add(this.btnMainCCCD_DelDups);
            this.tabPageR6CompData.Controls.Add(this.btnUpdCDD_Reset);
            this.tabPageR6CompData.Controls.Add(this.btnMainCCCD_Copy);
            this.tabPageR6CompData.Controls.Add(this.btnCompDataAdd);
            this.tabPageR6CompData.Controls.Add(this.txtSearchCoolCap);
            this.tabPageR6CompData.Controls.Add(this.label8);
            this.tabPageR6CompData.Controls.Add(this.dgvCompCircuitData);
            this.tabPageR6CompData.Location = new System.Drawing.Point(4, 22);
            this.tabPageR6CompData.Name = "tabPageR6CompData";
            this.tabPageR6CompData.Size = new System.Drawing.Size(1407, 620);
            this.tabPageR6CompData.TabIndex = 9;
            this.tabPageR6CompData.Text = "CompCircuitChargeData";
            this.tabPageR6CompData.UseVisualStyleBackColor = true;
            // 
            // btnExportData
            // 
            this.btnExportData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportData.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnExportData.Location = new System.Drawing.Point(149, 7);
            this.btnExportData.Name = "btnExportData";
            this.btnExportData.Size = new System.Drawing.Size(96, 23);
            this.btnExportData.TabIndex = 14;
            this.btnExportData.Text = "Export Data";
            this.btnExportData.UseVisualStyleBackColor = true;
            this.btnExportData.Click += new System.EventHandler(this.btnExportData_Click);
            // 
            // btnMassUpdate
            // 
            this.btnMassUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMassUpdate.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnMassUpdate.Location = new System.Drawing.Point(26, 7);
            this.btnMassUpdate.Name = "btnMassUpdate";
            this.btnMassUpdate.Size = new System.Drawing.Size(96, 23);
            this.btnMassUpdate.TabIndex = 13;
            this.btnMassUpdate.Text = "Mass Update";
            this.btnMassUpdate.UseVisualStyleBackColor = true;
            this.btnMassUpdate.Click += new System.EventHandler(this.btnMassUpdate_Click);
            // 
            // btnMainCCCD_DelDups
            // 
            this.btnMainCCCD_DelDups.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMainCCCD_DelDups.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnMainCCCD_DelDups.Location = new System.Drawing.Point(1304, 7);
            this.btnMainCCCD_DelDups.Name = "btnMainCCCD_DelDups";
            this.btnMainCCCD_DelDups.Size = new System.Drawing.Size(88, 23);
            this.btnMainCCCD_DelDups.TabIndex = 12;
            this.btnMainCCCD_DelDups.Text = "Delete Dups";
            this.btnMainCCCD_DelDups.UseVisualStyleBackColor = true;
            this.btnMainCCCD_DelDups.Click += new System.EventHandler(this.btnMainCCCD_DelDups_Click);
            // 
            // btnUpdCDD_Reset
            // 
            this.btnUpdCDD_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdCDD_Reset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnUpdCDD_Reset.Location = new System.Drawing.Point(967, 7);
            this.btnUpdCDD_Reset.Name = "btnUpdCDD_Reset";
            this.btnUpdCDD_Reset.Size = new System.Drawing.Size(88, 23);
            this.btnUpdCDD_Reset.TabIndex = 9;
            this.btnUpdCDD_Reset.Text = "Reset";
            this.btnUpdCDD_Reset.UseVisualStyleBackColor = true;
            this.btnUpdCDD_Reset.Click += new System.EventHandler(this.btnUpdCDD_Reset_Click);
            // 
            // btnMainCCCD_Copy
            // 
            this.btnMainCCCD_Copy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMainCCCD_Copy.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnMainCCCD_Copy.Location = new System.Drawing.Point(1193, 7);
            this.btnMainCCCD_Copy.Name = "btnMainCCCD_Copy";
            this.btnMainCCCD_Copy.Size = new System.Drawing.Size(88, 23);
            this.btnMainCCCD_Copy.TabIndex = 11;
            this.btnMainCCCD_Copy.Text = "Copy ";
            this.btnMainCCCD_Copy.UseVisualStyleBackColor = true;
            this.btnMainCCCD_Copy.Click += new System.EventHandler(this.btnMainCCCD_Copy_Click);
            // 
            // btnCompDataAdd
            // 
            this.btnCompDataAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompDataAdd.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCompDataAdd.Location = new System.Drawing.Point(1081, 7);
            this.btnCompDataAdd.Name = "btnCompDataAdd";
            this.btnCompDataAdd.Size = new System.Drawing.Size(88, 23);
            this.btnCompDataAdd.TabIndex = 10;
            this.btnCompDataAdd.Text = "New Cooling";
            this.btnCompDataAdd.UseVisualStyleBackColor = true;
            this.btnCompDataAdd.Click += new System.EventHandler(this.btnCompDataAdd_Click);
            // 
            // txtSearchCoolCap
            // 
            this.txtSearchCoolCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchCoolCap.Location = new System.Drawing.Point(672, 9);
            this.txtSearchCoolCap.Name = "txtSearchCoolCap";
            this.txtSearchCoolCap.Size = new System.Drawing.Size(69, 21);
            this.txtSearchCoolCap.TabIndex = 8;
            this.txtSearchCoolCap.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchCoolCap_KeyUp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(489, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(167, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Search Cooling Capacity:";
            // 
            // dgvCompCircuitData
            // 
            this.dgvCompCircuitData.AllowUserToAddRows = false;
            this.dgvCompCircuitData.AllowUserToDeleteRows = false;
            this.dgvCompCircuitData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompCircuitData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCompCircuitData.Location = new System.Drawing.Point(12, 37);
            this.dgvCompCircuitData.Name = "dgvCompCircuitData";
            this.dgvCompCircuitData.ReadOnly = true;
            this.dgvCompCircuitData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCompCircuitData.Size = new System.Drawing.Size(1383, 575);
            this.dgvCompCircuitData.TabIndex = 6;
            this.dgvCompCircuitData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCompCircuitData_CellContentClick);
            this.dgvCompCircuitData.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCompCircuitData_CellMouseDoubleClick);
            // 
            // tabPageLineSize
            // 
            this.tabPageLineSize.Controls.Add(this.txtLineSizeCoolingCap);
            this.tabPageLineSize.Controls.Add(this.label5);
            this.tabPageLineSize.Controls.Add(this.btnLineSizeReset);
            this.tabPageLineSize.Controls.Add(this.btnLineSizeCopy);
            this.tabPageLineSize.Controls.Add(this.btnLineSizeNewCooling);
            this.tabPageLineSize.Controls.Add(this.dgvLineSize);
            this.tabPageLineSize.Location = new System.Drawing.Point(4, 22);
            this.tabPageLineSize.Name = "tabPageLineSize";
            this.tabPageLineSize.Size = new System.Drawing.Size(1407, 620);
            this.tabPageLineSize.TabIndex = 10;
            this.tabPageLineSize.Text = "LineSize";
            this.tabPageLineSize.UseVisualStyleBackColor = true;
            // 
            // txtLineSizeCoolingCap
            // 
            this.txtLineSizeCoolingCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineSizeCoolingCap.Location = new System.Drawing.Point(713, 9);
            this.txtLineSizeCoolingCap.Name = "txtLineSizeCoolingCap";
            this.txtLineSizeCoolingCap.Size = new System.Drawing.Size(69, 21);
            this.txtLineSizeCoolingCap.TabIndex = 16;
            this.txtLineSizeCoolingCap.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLineSizeCoolingCap_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(530, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 15);
            this.label5.TabIndex = 15;
            this.label5.Text = "Search Cooling Capacity:";
            // 
            // btnLineSizeReset
            // 
            this.btnLineSizeReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLineSizeReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnLineSizeReset.Location = new System.Drawing.Point(1034, 7);
            this.btnLineSizeReset.Name = "btnLineSizeReset";
            this.btnLineSizeReset.Size = new System.Drawing.Size(88, 23);
            this.btnLineSizeReset.TabIndex = 12;
            this.btnLineSizeReset.Text = "Reset";
            this.btnLineSizeReset.UseVisualStyleBackColor = true;
            this.btnLineSizeReset.Click += new System.EventHandler(this.btnLineSizeReset_Click);
            // 
            // btnLineSizeCopy
            // 
            this.btnLineSizeCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLineSizeCopy.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnLineSizeCopy.Location = new System.Drawing.Point(1260, 7);
            this.btnLineSizeCopy.Name = "btnLineSizeCopy";
            this.btnLineSizeCopy.Size = new System.Drawing.Size(88, 23);
            this.btnLineSizeCopy.TabIndex = 14;
            this.btnLineSizeCopy.Text = "Copy ";
            this.btnLineSizeCopy.UseVisualStyleBackColor = true;
            this.btnLineSizeCopy.Click += new System.EventHandler(this.btnLineSizeCopy_Click);
            // 
            // btnLineSizeNewCooling
            // 
            this.btnLineSizeNewCooling.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLineSizeNewCooling.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnLineSizeNewCooling.Location = new System.Drawing.Point(1148, 7);
            this.btnLineSizeNewCooling.Name = "btnLineSizeNewCooling";
            this.btnLineSizeNewCooling.Size = new System.Drawing.Size(88, 23);
            this.btnLineSizeNewCooling.TabIndex = 13;
            this.btnLineSizeNewCooling.Text = "New Cooling";
            this.btnLineSizeNewCooling.UseVisualStyleBackColor = true;
            this.btnLineSizeNewCooling.Click += new System.EventHandler(this.btnLineSizeNewCooling_Click);
            // 
            // dgvLineSize
            // 
            this.dgvLineSize.AllowUserToAddRows = false;
            this.dgvLineSize.AllowUserToDeleteRows = false;
            this.dgvLineSize.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLineSize.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvLineSize.Location = new System.Drawing.Point(12, 36);
            this.dgvLineSize.Name = "dgvLineSize";
            this.dgvLineSize.ReadOnly = true;
            this.dgvLineSize.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLineSize.Size = new System.Drawing.Size(1383, 575);
            this.dgvLineSize.TabIndex = 7;
            this.dgvLineSize.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLineSize_CellContentClick);
            this.dgvLineSize.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvLineSize_CellMouseDoubleClick);
            // 
            // btnMainExit
            // 
            this.btnMainExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMainExit.ForeColor = System.Drawing.Color.Black;
            this.btnMainExit.Location = new System.Drawing.Point(1324, 11);
            this.btnMainExit.Name = "btnMainExit";
            this.btnMainExit.Size = new System.Drawing.Size(88, 38);
            this.btnMainExit.TabIndex = 1;
            this.btnMainExit.Text = "Exit";
            this.btnMainExit.UseVisualStyleBackColor = true;
            this.btnMainExit.Click += new System.EventHandler(this.btnMainExit_Click);
            // 
            // lbMainTitle
            // 
            this.lbMainTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainTitle.Location = new System.Drawing.Point(585, 33);
            this.lbMainTitle.Name = "lbMainTitle";
            this.lbMainTitle.Size = new System.Drawing.Size(297, 24);
            this.lbMainTitle.TabIndex = 23;
            this.lbMainTitle.Text = "Rev 6 Part Maintenance Tool";
            this.lbMainTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Title = "Saves as a CSV file";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.ForeColor = System.Drawing.Color.Blue;
            this.lbEnvironment.Location = new System.Drawing.Point(459, 2);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(551, 30);
            this.lbEnvironment.TabIndex = 24;
            this.lbEnvironment.Text = "Production Environment";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rbCompDtlVolt230
            // 
            this.rbCompDtlVolt230.AutoSize = true;
            this.rbCompDtlVolt230.Location = new System.Drawing.Point(64, 36);
            this.rbCompDtlVolt230.Name = "rbCompDtlVolt230";
            this.rbCompDtlVolt230.Size = new System.Drawing.Size(43, 17);
            this.rbCompDtlVolt230.TabIndex = 21;
            this.rbCompDtlVolt230.Text = "230";
            this.rbCompDtlVolt230.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1370, 731);
            this.Controls.Add(this.lbEnvironment);
            this.Controls.Add(this.lbMainTitle);
            this.Controls.Add(this.btnMainExit);
            this.Controls.Add(this.tabMain);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "REV 6 OAU Parts Maintenance Tool";
            this.tabMain.ResumeLayout(false);
            this.tabPagePartRuleHead.ResumeLayout(false);
            this.tabPagePartRuleHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartRulesHead)).EndInit();
            this.tabPagePartConditions.ResumeLayout(false);
            this.tabPagePartConditions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartsMain)).EndInit();
            this.tabPageModelNumDesc.ResumeLayout(false);
            this.tabPageModelNumDesc.PerformLayout();
            this.gbModelNoHeatType.ResumeLayout(false);
            this.gbModelNoHeatType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelNo)).EndInit();
            this.tabPagePartCatHead.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartCat)).EndInit();
            this.tabPageCompElecData.ResumeLayout(false);
            this.tabPageCompElecData.PerformLayout();
            this.gbVoltage.ResumeLayout(false);
            this.gbVoltage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompDetail)).EndInit();
            this.tabPageMotrData.ResumeLayout(false);
            this.tabPageMotrData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotorData)).EndInit();
            this.tabPageFurnaceData.ResumeLayout(false);
            this.tabPageFurnaceData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFurnaceData)).EndInit();
            this.tabPageR6CompData.ResumeLayout(false);
            this.tabPageR6CompData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompCircuitData)).EndInit();
            this.tabPageLineSize.ResumeLayout(false);
            this.tabPageLineSize.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLineSize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPagePartConditions;
        private System.Windows.Forms.DataGridView dgvPartsMain;
        private System.Windows.Forms.TabPage tabPageCompElecData;
        private System.Windows.Forms.TabPage tabPageMotrData;
        private System.Windows.Forms.Button btnMainExit;
        private System.Windows.Forms.TextBox txtSearchPartNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TabPage tabPageFurnaceData;
        private System.Windows.Forms.TabPage tabPageModelNumDesc;
        private System.Windows.Forms.TabPage tabPagePartCatHead;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnAddPart;
        private System.Windows.Forms.DataGridView dgvModelNo;
        private System.Windows.Forms.TextBox txtSearchModelNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnModelNoReset;
        private System.Windows.Forms.Button btnModelNoSearch;
        private System.Windows.Forms.GroupBox gbModelNoHeatType;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeDF;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeElec;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeIF;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeAll;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeNA;
        private System.Windows.Forms.Button btnModelNoAddRow;
        private System.Windows.Forms.TextBox txtCompDtlSearchPartNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCompDtlAddRow;
        private System.Windows.Forms.GroupBox gbVoltage;
        private System.Windows.Forms.RadioButton rbCompDtlVolt575;
        private System.Windows.Forms.RadioButton rbCompDtlVolt460;
        private System.Windows.Forms.RadioButton rbCompDtlVolt208;
        private System.Windows.Forms.Button btnCompDtlReset;
        private System.Windows.Forms.DataGridView dgvCompDetail;
        private System.Windows.Forms.RadioButton rbCompDtlVoltAll;
        private System.Windows.Forms.TabPage tabPagePartRuleHead;
        private System.Windows.Forms.Button btnPRH_Reset;
        private System.Windows.Forms.Button btnPRH_AddPart;
        private System.Windows.Forms.TextBox txtPRH_SearchPartNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvPartRulesHead;
        private System.Windows.Forms.Label lbMainTitle;
        private System.Windows.Forms.Button btnMotorDataAdd;
        private System.Windows.Forms.TextBox txtMotorDataSearchPartNum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnMotorDataReset;
        private System.Windows.Forms.DataGridView dgvMotorData;
        private System.Windows.Forms.Button btnFurnaceDataAdd;
        private System.Windows.Forms.TextBox txtFurnaceDataSearchPartNum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnFurnaceDataReset;
        private System.Windows.Forms.DataGridView dgvFurnaceData;
        private System.Windows.Forms.Button btnPartCatAdd;
        private System.Windows.Forms.Button btnPartCatReset;
        private System.Windows.Forms.DataGridView dgvPartCat;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeHotWater;
        private System.Windows.Forms.TabPage tabPageR6CompData;
        private System.Windows.Forms.Button btnUpdCDD_Reset;
        private System.Windows.Forms.Button btnMainCCCD_Copy;
        private System.Windows.Forms.Button btnCompDataAdd;
        private System.Windows.Forms.TextBox txtSearchCoolCap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dgvCompCircuitData;
        private System.Windows.Forms.Button btnMainCCCD_DelDups;
        private System.Windows.Forms.TabPage tabPageLineSize;
        private System.Windows.Forms.TextBox txtLineSizeCoolingCap;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnLineSizeReset;
        private System.Windows.Forms.Button btnLineSizeCopy;
        private System.Windows.Forms.Button btnLineSizeNewCooling;
        private System.Windows.Forms.DataGridView dgvLineSize;
        private System.Windows.Forms.Button btnMassUpdate;
        private System.Windows.Forms.Button btnExportData;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnCreateSSRules;
        private System.Windows.Forms.Label lbEnvironment;
        private System.Windows.Forms.RadioButton rbCompDtlVolt230;
    }
}

