﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdateMotorData : Form
    {
        private frmMain m_parent;
        MotorBO objMotor = new MotorBO();
        PartsMainBO objPart = new PartsMainBO();
        private string gVoltage { get; set; }
        private string gPartNum { get; set; }
        private string gPhase { get; set; }
        private string gHertz { get; set; }
        private string gRuleHeadId { get; set; }
        private bool gDupPartNumber { get; set; }
        private bool gFireEvent { get; set; }
       
        public frmUpdateMotorData(frmMain frmMn)
        {
            m_parent = frmMn;      
            InitializeComponent();
            gVoltage = "NA";           
            gPhase = "NA";
            gHertz = "NA";
            gDupPartNumber = false;
            gFireEvent = false;            
        }

        private void btnUpdMotorCancel_Click(object sender, EventArgs e)
        {
            m_parent.mVoltage = "NoUpdate";            
            this.Close();
        }

        private void btnUpdMotorSave_Click(object sender, EventArgs e)
        {
            if (validMotorDataUpdates() == true)
            {
                objMotor.ID = lbUpdMotorID.Text;
                objMotor.RuleHeadID = txtUpdMotorRuleHeadID.Text;

                if (cbUpdMotorPartNum.SelectedValue != null)
                {
                    objMotor.PartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                }
                else
                {
                    objMotor.PartNum = cbUpdMotorPartNum.Text;
                }

                objMotor.MotorType = cbUpdMotor_MotorType.Text;
                objMotor.FLA = txtUpdMotorFLA.Text;
                objMotor.Hertz = txtUpdMotorHertz.Text;
                objMotor.Phase = txtUpdMotorPhase.Text;
                objMotor.MCC = txtUpdMotorMCC.Text;
                objMotor.RLA = txtUpdMotorRLA.Text;
                objMotor.Voltage = gVoltage;
                objMotor.HP = txtUpdMotorHP.Text;
                objMotor.Username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                objMotor.DateMod = DateTime.Now.ToString("g");  // 2/27/2009 12:12 PM format     

                m_parent.mMotorType = objMotor.MotorType;
                m_parent.mFLA = objMotor.FLA;
                m_parent.mHertz = objMotor.Hertz;
                m_parent.mPhase = objMotor.Phase;
                m_parent.mMCC = objMotor.MCC;
                m_parent.mRLA = objMotor.RLA;
                m_parent.mVoltage = objMotor.Voltage;
                m_parent.mHP = objMotor.HP;
                m_parent.mModBy = objMotor.Username;
                m_parent.mLastModDate = objMotor.DateMod;

                if (this.btnUpdMotorSave.Text == "Update")
                {
                    objMotor.Update_ROA_MotorDataDTL();                                       
                }
                else if (this.btnUpdMotorSave.Text == "Add")
                {
                    objMotor.InsertROA_MotorDataDTL();                    
                }
                else if (this.btnUpdMotorSave.Text == "Delete")
                {
                    DialogResult result1 = MessageBox.Show("Are you sure you want to Delete Motor: " + objMotor.PartNum + "? Press Yes to delete; No to cancel!",
                                                      "Deleting a ROA_MotorDataDTL Row",
                                                      MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.Yes)
                    {
                        objMotor.DeleteROA_MotorDataDTL();                       
                    }
                }
                this.Close();
            }
        }       

        private void cbUpdMotorPartNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            int pos = 0;
            if (cbUpdMotorPartNum.SelectedIndex != 0)
            {
                objMotor.RuleHeadID = cbUpdMotorPartNum.SelectedValue.ToString();
                gRuleHeadId = objMotor.RuleHeadID;
                objMotor.Voltage = null;

                objPart.PartNum = cbUpdMotorPartNum.Text;
                pos = cbUpdMotorPartNum.Text.IndexOf(" ");
                if (pos > 0)
                {
                    objPart.PartNum = cbUpdMotorPartNum.Text.Substring(0, pos);
                }               
                
                objPart.GetOAU_PartNumberInfo();
                if (objPart.PartDescription != "")
                {
                    txtUpdMotorRuleHeadID.Text = objPart.RuleHeadID;
                    txtUpdMotorPartDesc.Text = objPart.PartDescription;
                    txtUpdMotorHertz.Text = "0.00";
                    txtUpdMotorMCC.Text = "0.00";
                    txtUpdMotorRLA.Text = "0.00";
                    txtUpdMotorPhase.Text = "";      
                    rbUpdMotor115_60_1.Checked = false;
                    rbUpdMotor208_50_1.Checked = false;
                    rbUpdMotor208_50_3.Checked = false;
                    rbUpdMotor208_60_1.Checked = false;
                    rbUpdMotor208_60_3.Checked = false;
                    rbUpdMotor230_240_60_3.Checked = false;
                    rbUpdMotor380_50_3.Checked = false;
                    rbUpdMotor460_60_3.Checked = false;
                    rbUpdMotor575_60_3.Checked = false;
                    gFireEvent = true;

                    cbUpdMotor_MotorType.Select();
                }                                                                   
            }
        }

        private void rbUpdMotor208_60_3_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor208_60_3.Checked == true) && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "208";
                gHertz = "60";
                gPhase = "3";
                txtUpdMotorHertz.Text = "60.00";
                txtUpdMotorPhase.Text = "3";
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }
                 
        }

        private void rbUpdMotor460_60_3_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor460_60_3.Checked == true) && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "460";
                gHertz = "60";
                gPhase = "3";
                txtUpdMotorHertz.Text = "60.00";
                txtUpdMotorPhase.Text = "3";
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }
        }

        private void rbUpdMotor575_60_3_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor575_60_3.Checked == true)  && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "575";
                gHertz = "60";
                gPhase = "3";
                txtUpdMotorHertz.Text = "60.00";
                txtUpdMotorPhase.Text = "3";
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }
        }

        private void rbUpdMotor230_240_60_3_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor230_240_60_3.Checked == true) && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "230";
                gHertz = "60";
                gPhase = "3";
                txtUpdMotorHertz.Text = "60.00";
                txtUpdMotorPhase.Text = "3";
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }
        }

        private void rbUpdMotor115_60_1_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor115_60_1.Checked == true) && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "115";
                gHertz = "60";
                gPhase = "1";
                txtUpdMotorHertz.Text = "60.00";
                txtUpdMotorPhase.Text = "1";
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }
        }

        private void rbUpdMotor208_60_1_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor208_60_1.Checked == true) && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "208";
                gHertz = "60";
                gPhase = "1";
                txtUpdMotorHertz.Text = "60.00";
                txtUpdMotorPhase.Text = "1";
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }

        }

        private void rbUpdMotor208_50_3_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor208_50_3.Checked == true) && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "208";
                gHertz = "50";
                gPhase = "3";
                txtUpdMotorHertz.Text = "50.00";
                txtUpdMotorPhase.Text = "3";
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }
        }

        private void rbUpdMotor380_50_3_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor380_50_3.Checked == true) && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "380";
                gHertz = "50";
                gPhase = "3";
                txtUpdMotorHertz.Text = "50.00";
                txtUpdMotorPhase.Text = "3";
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }
        }

        private void rbUpdMotor208_50_1_CheckedChanged(object sender, EventArgs e)
        {
            if ((rbUpdMotor208_50_3.Checked == true) && (gFireEvent == true))
            {
                gRuleHeadId = txtUpdMotorRuleHeadID.Text;
                gPartNum = cbUpdMotorPartNum.SelectedValue.ToString();
                gVoltage = "208";
                gHertz = "50";
                gPhase = "1";
                txtUpdMotorHertz.Text = "50.00";
                txtUpdMotorPhase.Text = "1";
           
                if (duplicateMotor() == true)
                {
                    gDupPartNumber = true;
                    MessageBox.Show("ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.");
                }
                else
                {
                    gDupPartNumber = false;
                }
            }
            else
            {
                gFireEvent = true;
            }
        }

        private bool validMotorDataUpdates()
        {
            bool retVal = true;
            decimal dPhase;
            decimal dHertz;
            decimal dMCC;
            decimal dRLA;
            decimal dHP;
            
            string errors = String.Empty;

            if (gDupPartNumber == true)
            {
                errors += "ERROR - Duplicate Motor(Part Number)/Voltage combination already exist.";
            }

            if (cbUpdMotor_MotorType.Text.Length == 0)
            {
                errors += "Error - Motor Type is a required field.\n";
            }

            if (txtUpdMotorFLA.Text.Length == 0)
            {
                errors += "Error - FLA is a required decimal field.\n";
            }
            else
            {
                try
                {
                    dHertz = Decimal.Parse(txtUpdMotorFLA.Text);
                }
                catch
                {
                    errors += "Error - Invalid FLA value, field must be a positive decimal value.\n";
                }
            }

            try
            {
                dHertz = Decimal.Parse(txtUpdMotorHertz.Text);

                if (dHertz < 0)
                {
                    errors += "Error - Invalid Hertz value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Hertz value, field must be a positive decimal value or zero.\n";
            }

            try
            {
                dPhase = Decimal.Parse(txtUpdMotorPhase.Text);

                if (dPhase < 0)
                {
                    errors += "Error - Invalid Phase value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Phase value, field must be a positive decimal value or zero.\n";
            }

            try
            {
                dMCC = Decimal.Parse(txtUpdMotorMCC.Text);

                if (dMCC < 0)
                {
                    errors += "Error - Invalid MCC value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid MCC value, field must be a positive decimal value or zero.\n";
            }


            try
            {
                dRLA = Decimal.Parse(txtUpdMotorRLA.Text);

                if (dRLA < 0)
                {
                    errors += "Error - Invalid RLA value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid RLA value, field must be a positive decimal value or zero.\n";
            }

            try
            {
                dHP = Decimal.Parse(txtUpdMotorHP.Text);

                if (dHP < 0)
                {
                    errors += "Error - Invalid HP value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid HP value, field must be a positive decimal value or zero.\n";
            }           
            
            if (rbUpdMotor208_60_3.Checked == true)
            {
                gVoltage = "208";
            }
            else if (rbUpdMotor460_60_3.Checked == true)
            {
                gVoltage = "460";
            }
            else if (rbUpdMotor575_60_3.Checked == true)
            {
                gVoltage = "575";
            }
            else if (rbUpdMotor230_240_60_3.Checked == true)
            {
                gVoltage = "230";
            }
            else if (rbUpdMotor115_60_1.Checked == true)
            {
                gVoltage = "115";
            }
            else if (rbUpdMotor208_60_1.Checked == true)
            {
                gVoltage = "208";
            }
            else if (rbUpdMotor208_50_3.Checked == true)
            {
                gVoltage = "208";
            }
            else if (rbUpdMotor380_50_3.Checked == true)
            {
                gVoltage = "380";
            }
            else if (rbUpdMotor208_50_1.Checked == true)
            {
                gVoltage = "208";
            }
            else
            {
                errors += "Error - No Voltage selection has made.\n";
            }

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }

        private bool duplicateMotor()
        {
            bool dupFound = false;

            objMotor.RuleHeadID = gRuleHeadId;
            objMotor.PartNum = gPartNum;
            objMotor.Voltage = gVoltage;
            objMotor.Phase = gPhase;
            objMotor.Hertz = gHertz;
            DataTable dt = objMotor.GetROA_MotorDataDTL();
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];                
                dupFound = true;                
            }                     

            return dupFound;
        }

        private void ConvertMotorData()
        {
            string fla1 = "";
            string fla2 = "";
            string volt1 = "";
            string volt2 = "";
            string temp = "";
            int pos = 0;

            objMotor.RuleHeadID = "-1";
            DataTable dt = objMotor.R6_OA_GetAllMotorDataDTL();

            foreach(DataRow dr in dt.Rows)
            {
               
                temp = dr["FLA"].ToString();
                if (temp.IndexOf("-") > -1)
                {
                    pos =  temp.IndexOf("-");
                    fla1 = temp.Substring(0, pos);
                    fla2 = temp.Substring((pos + 1), (temp.Length - (pos + 1)));

                    temp = dr["Voltage"].ToString();
                    pos = temp.IndexOf("/");
                    volt1 = temp.Substring(0, 3);
                    volt2 = temp.Substring((pos + 1), (temp.Length - (pos + 1)));
                }
                else
                {
                    fla1 = dr["FLA"].ToString();                    
                    volt1 = dr["Voltage"].ToString();
                    if (volt1.Length > 2)
                    {
                        volt1 = volt1.Substring(0, 3);
                    }
                    fla2 = "";
                    volt2 = "";
                }

                objMotor.RuleHeadID = dr["RuleHeadID"].ToString();
                objMotor.FLA = fla1;
                objMotor.Voltage = volt1;
                objMotor.Hertz = "60.0";
                objMotor.Phase = "3";
                objMotor.MCC = "0.00";
                objMotor.RLA = "0.00";
                objMotor.HP = dr["HP"].ToString();
                objMotor.MotorType = dr["MotorType"].ToString();
                objMotor.Username = "tonyt";

                objMotor.InsertROA_MotorDataDTL();

                if (fla2 != "")
                {
                    objMotor.FLA = fla2;
                    objMotor.Voltage = volt2;
                    objMotor.InsertROA_MotorDataDTL();
                }
            }
        }
    }
}
