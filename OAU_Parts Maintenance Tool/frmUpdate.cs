﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdate : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        ModelNumDescBO objModel = new ModelNumDescBO();
        
        public int mRowIdx { get; set; }
        public int mSelRowIdx { get; set; }
        public int mDigitNo { get; set; }
        public decimal mQty { get; set; }
        public string mHeatType { get; set; }
        public string mUnitType {get; set;}
        public string mRevType { get; set; }
        public bool mAddRule { get; set; }
        public bool mUpdateAll { get; set; }
        public bool mChangesSaved { get; set; }
        public bool mDeleteFlag { get; set; }   
     
        public bool gAllowDigitChangeEvent { get; set; }
        public bool gInitialDisplay { get; set; }
        public bool gDeleteRow { get; set; }
        public bool gCopyAllRuleSets { get; set; }
        public bool gValuesDisplayed { get; set; }
        public bool gCheckBoxForOA { get; set; }
        public bool gCheckBoxForVKG { get; set; }        
        public bool gSubAsmMtlPart { get; set; }
        public bool gPreConfigAsm { get; set; }
        public bool gAllAsmsGood { get; set; }

        public int gUpdatedBatchId { get; set; }

        bool newPartNum = true;
        bool firstPass = true;

        const int UPD_BTN_CELL = 12;
        const int DEL_BTN_CELL = 13;       

        private frmMain m_parent;

        public frmUpdate(frmMain frmMn)
        {           
            m_parent = frmMn;
            gAllowDigitChangeEvent = false;
            gInitialDisplay = true;
            gDeleteRow = false;
            mUpdateAll = false;
            mChangesSaved = true;
            mUnitType = "";
            gValuesDisplayed = false;
            InitializeComponent();                              
        }

        private void frmUpdate_Load(object sender, EventArgs e)
        {
            int selIdx = 0;
            int rowSelect = 0;
            string strSelRowIdx = "0";

            if (this.lbScreenMode.Text != "NewPartMode" && this.lbScreenMode.Text != "CopyMode")
            {
                rowSelect = Convert.ToInt32(this.lbSelectedRow.Text);
                strSelRowIdx = lbSelectedRow.Text;

                dgvUpdateRules.Rows[rowSelect].Selected = true;
                selIdx = objPart.getDigitSelectedIndex(dgvUpdateRules.Rows[rowSelect].Cells["ColDigit"].Value.ToString(),
                                                           dgvUpdateRules.Rows[rowSelect].Cells["ColHeatType"].Value.ToString(), cbDigit);
            }
            mAddRule = false;
            cbDigit.SelectedIndex = selIdx;
            mQty = this.nudReqQty.Value;
            //mUnitType = this.cbUnitType.Text;
            mSelRowIdx = Int32.Parse(strSelRowIdx);
            mDeleteFlag = false;
            m_parent.mUpdatedValueStr = "NoUpdate";
            gAllowDigitChangeEvent = true;
            gCopyAllRuleSets = false;
            gUpdatedBatchId = 0;
            gAllAsmsGood = true;            
          
            gCheckBoxForOA = true;                      

            if (lbScreenMode.Text == "UpdateMode")
            {
                gValuesDisplayed = true;
            }            
        }

        #region Events
        private void btnUpdCancel_Click(object sender, EventArgs e)
        {            
            if (mChangesSaved == false)
            {
                DialogResult result1 = MessageBox.Show(("WARNING!!! Changes have been made that have not been saved. Leaving the\npage without pressing the 'Save All Changes' button " +
                                                        "will result in the loss of\nthese changes. Do you wish to leave the page without saving?"),
                                                       "Changes Not Saved Warning",
                                                       MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    m_parent.mUpdatedValueStr = "NoUpdate";
                    this.Close();
                }
            }
            else
            {
                if (btnAddNewRule.Text == "Add New Rule")
                {
                    m_parent.mUpdatedValueStr = "NoUpdate";
                    this.Close();
                }
                else
                {
                    cbDigit.Enabled = false;
                    btnAddNewRule.Text = "Add New Rule";
                    btnUpdCancel.Text = "Exit";
                    btnUpdate.Enabled = true;
                    cbDigit.SelectedIndex = 0;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string ruleHeadID = this.txtRuleID.Text;
            string ruleBatchId = "00";
            string currentBatchID = "";           
            string digitStr = "";
            string errorMsgs = "";
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int slashPos = modByStr.IndexOf("\\");
            int errorCode = 0;
           
            decimal circuitCharge = 0;

            modByStr = modByStr.Substring((slashPos + 1), modByStr.Length - (slashPos + 1));

            bool firstBatchID = true;
            
            List<string> batchIdList = new List<string>();

            if (objPart.RfgComponent == "true")
            {
                if (chkBoxCircuit1.Checked == false && chkBoxCircuit2.Checked == false)
                {
                    errorMsgs += "ERROR -- Rfg Component is checked, however no Circuit has been selected.";

                    if (errorCode == 1)
                    {
                        MessageBox.Show("ERROR -- Rfg Component is checked, however no Circuit has been selected.");
                    }
                    else
                    {
                        MessageBox.Show("ERROR -- Sub Assembly checkbox is checked but, No Primary Assembly part number selected.");
                    }
                }
            }
            else
            {
                objPart.RfgComponent = "false";
            }

            mUnitType = "OA";
            mRevType = "REV6";
            
            if (chkBoxSubAsm.Checked == true)
            {               
                objPart.SubAssemblyPart = true;                               

                if (cbImmediateParentPartNum.SelectedIndex > 0)
                {
                    objPart.ImmediateParent = cbImmediateParentPartNum.SelectedValue.ToString(); 
                }
                else
                {
                    objPart.ImmediateParent = "";
                }
                
                if (chkBoxMtlPart.Checked == true)
                {
                    objPart.SubAsmMtlPart = true;
                }
                else
                {
                    objPart.SubAsmMtlPart = false;
                }                                                      
            }
            else
            {
                objPart.SubAssemblyPart = false;
                objPart.SubAsmMtlPart = false;
                objPart.ImmediateParent = "";
            }            

            if (errorMsgs.Length == 0)
            {
                if (lbScreenMode.Text == "CopyMode")
                {
                    //int dupsFound = 0;
                    //int ruleCount = 0;               

                    //if (DuplicateRule(ruleHeadID, dgvUpdateRules) == false)                
                    //{                    
                    foreach (DataGridViewRow dr in dgvUpdateRules.Rows)
                    {
                        if (dr.Cells["ColUpdateType"].Value.ToString() != "Delete")
                        {
                            if (lbScreenMode.Text == "CopyMode")
                            {
                                digitStr = dr.Cells["ColDigit"].Value.ToString();
                            }
                            else
                            {
                                digitStr = mDigitNo.ToString();
                            }
                            objPart.RuleHeadID = ruleHeadID;
                            objPart.RuleBatchID = txtBatchID.Text;
                            objPart.PartNum = dr.Cells["ColPartNumber"].Value.ToString();
                            objPart.PartDescription = dr.Cells["ColPartDesc"].Value.ToString();
                            objPart.RelatedOperation = dr.Cells["ColRelatedOp"].Value.ToString();
                            objPart.VikingRelOp = dr.Cells["ColVikingRelOp"].Value.ToString();
                            objPart.PartCategory = dr.Cells["ColPartCat"].Value.ToString();                            
                            objPart.Digit = digitStr;
                            objPart.HeatType = dr.Cells["ColHeatType"].Value.ToString();
                            objPart.Value = dr.Cells["ColValue"].Value.ToString();
                            objPart.Comments = rtbComments.Text;
                            objPart.UnitType = mUnitType;
                            objPart.RevType = mRevType;
                            objPart.Qty = nudReqQty.Value.ToString();
                            objPart.UserName = modByStr;

                            if (objPart.RfgComponent == "true")
                            {
                                if (chkBoxCircuit1.Checked == true)
                                {
                                    objPart.Circuit1 = "true";
                                }
                                else
                                {
                                    objPart.Circuit1 = "false";
                                }

                                if (chkBoxCircuit2.Checked == true)
                                {
                                    objPart.Circuit2 = "true";
                                }
                                else
                                {
                                    objPart.Circuit2 = "false";
                                }
                            }
                            else
                            {
                                objPart.Circuit1 = "false";
                                objPart.Circuit2 = "false";
                            }                           

                            if (currentBatchID != dr.Cells["ColRuleBatchID"].Value.ToString())
                            {
                                currentBatchID = dr.Cells["ColRuleBatchID"].Value.ToString();
                                if (firstBatchID)
                                {
                                    firstBatchID = false;
                                }
                                else
                                {
                                    batchIdList.Add(objPart.RuleBatchID);
                                }
                                objPart.RuleBatchID = "0";
                            }

                            ruleBatchId = objPart.InsertPartConditionsDTL();
                            // Add code to isert entries into the SubAssembly table

                            if (ruleBatchId != "-1")
                            {
                                this.txtBatchID.Text = ruleBatchId;
                                objPart.RuleBatchID = ruleBatchId;
                            }

                            //if (subAsmChecked == true)
                            //{
                            //    objPart.UnitType = mUnitType;
                            //    objPart.RevType = mRevType;
                            //    objPart.InsertSubAsmParentData();
                            //}
                        }
                    
                        batchIdList.Add(objPart.RuleBatchID);

                        foreach (string batchId in batchIdList)
                        {
                            objPart.RuleBatchID = batchId;
                            DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

                            if (dt.Rows.Count > 0)
                            {
                                if (mUnitType.Contains("OA") == true)
                                {
                                    objPart.UpdatePartMaster(dt, "OA");
                                    m_parent.mRuleHeadID = this.txtRuleID.Text;
                                    m_parent.mRuleBatchID = this.txtBatchID.Text;
                                    m_parent.mPartNumber = this.cbPartNum.Text;
                                    m_parent.mQty = this.nudReqQty.Value.ToString();
                                }                              
                            }
                        }
                        this.Close();
                    }              
                }
                else // New Part or Update Mode
                {
                    foreach (DataGridViewRow dr in dgvUpdateRules.Rows)
                    {
                        objPart.RuleHeadID = dr.Cells["ColRuleHeadID"].Value.ToString();
                        if (ruleBatchId == "00")
                        {
                            objPart.RuleBatchID = dr.Cells["ColRuleBatchID"].Value.ToString();
                        }
                        else
                        {
                            objPart.RuleBatchID = ruleBatchId;
                        }

                        objPart.PartNum = dr.Cells["ColPartNumber"].Value.ToString();
                        objPart.PartDescription = dr.Cells["ColPartDesc"].Value.ToString();
                        
                        objPart.RelatedOperation = txtOARelOp.Text;
                        objPart.VikingRelOp = "";                        
                                                
                        objPart.PartCategory = dr.Cells["ColPartCat"].Value.ToString();                                               
                        objPart.Digit = dr.Cells["ColDigit"].Value.ToString();
                        objPart.HeatType = dr.Cells["ColHeatType"].Value.ToString();
                        objPart.Value = dr.Cells["ColValue"].Value.ToString();
                        objPart.Qty = dr.Cells["ColQty"].Value.ToString();
                        objPart.Comments = rtbComments.Text;
                        objPart.UnitType = mUnitType;
                        objPart.RevType = mRevType;
                        objPart.UserName = modByStr;

                        if (objPart.RfgComponent == "true")
                        {
                            if (chkBoxCircuit1.Checked == true)
                            {
                                objPart.Circuit1 = "true";
                            }
                            else
                            {
                                objPart.Circuit1 = "false";
                            }

                            if (chkBoxCircuit2.Checked == true)
                            {
                                objPart.Circuit2 = "true";
                            }
                            else
                            {
                                objPart.Circuit2 = "false";
                            }
                        }
                        else
                        {
                            objPart.Circuit1 = "false";
                            objPart.Circuit2 = "false";
                        }                       

                        if (dr.Cells["ColUpdateType"].Value.ToString() == "Insert")
                        {
                            ruleBatchId = objPart.InsertPartConditionsDTL();

                            if (ruleBatchId != "-1")
                            {
                                this.txtBatchID.Text = ruleBatchId;
                                objPart.RuleBatchID = ruleBatchId;
                                //if (subAsmChecked == true && subAsmInsertMade == false)
                                //{                                    
                                //    objPart.InsertSubAsmParentData();
                                //    subAsmInsertMade = true;
                                //}                               
                            }
                        }
                        else if (dr.Cells["ColUpdateType"].Value.ToString() == "Delete")
                        {
                            objPart.PartNum = dr.Cells["ColPartNumber"].Value.ToString();                            
                            objPart.DeleteFromPartConditionsDTL();
                            objPart.RuleBatchID = txtBatchID.Text;                           
                        }
                        else
                        {                            
                            objPart.UpdatePartConditionsDTL();                           
                        }
                    }

                    objPart.RuleBatchID = txtBatchID.Text;                   

                    m_parent.mUpdatedValueStr = "Update";
                    m_parent.mPartNumber = objPart.PartNum;
                    DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

                    if (dt.Rows.Count > 0)
                    {
                        if (mUnitType.Contains("OA") == true)
                        {
                            objPart.UpdatePartMaster(dt, "OA");
                            m_parent.mRuleHeadID = this.txtRuleID.Text;
                            m_parent.mRuleBatchID = this.txtBatchID.Text;
                            m_parent.mPartNumber = this.cbPartNum.Text;
                            m_parent.mQty = this.nudReqQty.Value.ToString();
                        }
                        else
                        {
                            if (gCheckBoxForOA == true) // if mUnitType doen not contain 'OA' however the gCheckBoxForOA equals true then delete Part numfrom OA Rev6 Part Master.
                            {
                                objPart.DeleteFromOAU_PartMaster();
                            }
                        }                        
                    }
                    else
                    {                       
                        if (gCheckBoxForOA == true) // if mUnitType doen not contain 'OA' however the gCheckBoxForOA equals true then delete Part numfrom OA Rev6 Part Master.
                        {
                            objPart.DeleteFromOAU_PartMaster();
                        }
                    }

                    this.Close();
                }
            }
            else
            {                
                MessageBox.Show(errorMsgs);                
            }
        }

        private void dgvUpdateRules_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            mRowIdx = e.RowIndex;               
            if (e.ColumnIndex == UPD_BTN_CELL)
            {               
                gAllowDigitChangeEvent = false;
                cbDigit.Enabled = false;
                displaySelectedRowData(mRowIdx);                
            }
            else if (e.ColumnIndex == DEL_BTN_CELL)
            {
                DialogResult result1 = MessageBox.Show("You are attempting to Delete a rule from the Rule Set list. Press 'Yes' to confirm delete & 'No' to cancel.",
                                                       "Deleting a Rule",
                                                       MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    dgvUpdateRules.Rows[mRowIdx].Visible = false;
                    dgvUpdateRules.Rows[mRowIdx].Cells["ColUpdateType"].Value = "Delete";
                    //dgvUpdateRules.Rows.RemoveAt(rowIdx);
                    dgvUpdateRules.Refresh();
                    cbDigit.SelectedIndex = 0;
                    gbValues.Visible = false;
                    btnUpdate.Enabled = false;
                    btnSave.Enabled = true;
                    mChangesSaved = false;
                    gDeleteRow = true;
                }
            }
        }

        private void dgvUpdateRules_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {          
            mRowIdx = e.RowIndex;            
            displaySelectedRowData(mRowIdx);           
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {                       
            string valueStr = "";
            string errorStr = "";

            bool firstValue = true;
            bool valueSelected = false;           

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(this.lbVal1);
            labelControls.Add(this.lbVal2);
            labelControls.Add(this.lbVal3);
            labelControls.Add(this.lbVal4);
            labelControls.Add(this.lbVal5);
            labelControls.Add(this.lbVal6);
            labelControls.Add(this.lbVal7);
            labelControls.Add(this.lbVal8);
            labelControls.Add(this.lbVal9);
            labelControls.Add(this.lbVal10);
            labelControls.Add(this.lbVal11);
            labelControls.Add(this.lbVal12);
            labelControls.Add(this.lbVal13);
            labelControls.Add(this.lbVal14);
            labelControls.Add(this.lbVal15);
            labelControls.Add(this.lbVal16);
            labelControls.Add(this.lbVal17);
            labelControls.Add(this.lbVal18);
            labelControls.Add(this.lbVal19);
            labelControls.Add(this.lbVal20);
            labelControls.Add(this.lbVal21);
            labelControls.Add(this.lbVal22);
            labelControls.Add(this.lbVal23);
            labelControls.Add(this.lbVal24);
            labelControls.Add(this.lbVal25);
            labelControls.Add(this.lbVal26);
            labelControls.Add(this.lbVal27);
            labelControls.Add(this.lbVal28);
            labelControls.Add(this.lbVal29);
            labelControls.Add(this.lbVal30);

            checkBoxControls.Add(this.cbVal1);
            checkBoxControls.Add(this.cbVal2);
            checkBoxControls.Add(this.cbVal3);
            checkBoxControls.Add(this.cbVal4);
            checkBoxControls.Add(this.cbVal5);
            checkBoxControls.Add(this.cbVal6);
            checkBoxControls.Add(this.cbVal7);
            checkBoxControls.Add(this.cbVal8);
            checkBoxControls.Add(this.cbVal9);
            checkBoxControls.Add(this.cbVal10);
            checkBoxControls.Add(this.cbVal11);
            checkBoxControls.Add(this.cbVal12);
            checkBoxControls.Add(this.cbVal13);
            checkBoxControls.Add(this.cbVal14);
            checkBoxControls.Add(this.cbVal15);
            checkBoxControls.Add(this.cbVal16);
            checkBoxControls.Add(this.cbVal17);
            checkBoxControls.Add(this.cbVal18);
            checkBoxControls.Add(this.cbVal19);
            checkBoxControls.Add(this.cbVal20);
            checkBoxControls.Add(this.cbVal21);
            checkBoxControls.Add(this.cbVal22);
            checkBoxControls.Add(this.cbVal23);
            checkBoxControls.Add(this.cbVal24);
            checkBoxControls.Add(this.cbVal25);
            checkBoxControls.Add(this.cbVal26);
            checkBoxControls.Add(this.cbVal27);
            checkBoxControls.Add(this.cbVal28);
            checkBoxControls.Add(this.cbVal29);
            checkBoxControls.Add(this.cbVal30);

            for (int i = 0; i < 30; ++i)
            {
                if (checkBoxControls[i].Enabled == true)
                {
                    if (checkBoxControls[i].Checked == true)
                    {
                        valueSelected = true;
                        if (firstValue)
                        {
                            valueStr = labelControls[i].Text;
                            firstValue = false;
                        }
                        else
                        {
                            valueStr += "," + labelControls[i].Text;
                        }
                    }
                }
            }

            if (lbScreenMode.Text != "CopyMode")
            {
                if (valueSelected == false)
                {
                    errorStr += "ERROR -- No values have been selected!\n";
                }

                if (nudReqQty.Value <= 0)
                {
                    errorStr += "ERROR -- Required Qty must be Greater Than 0!";
                }

                if (errorStr.Length == 0)
                {
                    int x = 0;
                    foreach (DataGridViewRow dgvr in dgvUpdateRules.Rows)
                    {
                        if (dgvr.Selected == true)
                        {
                            mRowIdx = x;
                            break;
                        }
                        ++x;
                    }
                    mChangesSaved = false;
                    //dgvUpdateRules.Rows[mSelRowIdx].Cells[6].Value = cbUnitType.Text;
                    dgvUpdateRules.Rows[mRowIdx].Cells["ColValue"].Value = valueStr;
                    dgvUpdateRules.Rows[mRowIdx].Cells["ColQty"].Value = nudReqQty.Value;
                    gUpdatedBatchId = Int32.Parse(dgvUpdateRules.Rows[mRowIdx].Cells["ColRuleBatchID"].Value.ToString());

                    if (mUpdateAll == true)
                    {
                        foreach (DataGridViewRow dr in dgvUpdateRules.Rows)
                        {
                            if (gCopyAllRuleSets == true)
                            {
                                if (Int32.Parse(dr.Cells[1].Value.ToString()) == gUpdatedBatchId)
                                {
                                    dr.Cells[10].Value = nudReqQty.Value;
                                }
                            }
                            else
                            {
                                dr.Cells[10].Value = nudReqQty.Value;
                            }
                        }
                    }

                    //mUnitType = cbUnitType.Text;
                    btnSave.Enabled = true;

                    valueStr = "";
                }
                else
                {
                    MessageBox.Show(errorStr);
                }            
            }
            else
            {
                lbScreenMode.Text = "UpdateMode";
                btnSave.Enabled = true;
            }
        }

        private void btnAddNewRule_Click(object sender, EventArgs e)
        {           
            bool dupDigitFound = false;
            int slashPos = cbDigit.Text.IndexOf('/');
            string digitStr = cbDigit.Text.Substring(0, slashPos);
            int digitInt = Int32.Parse(digitStr);
            string partDescStr = txtPartDesc.Text;

            if (btnAddNewRule.Text == "Add New Rule")
            {
                mAddRule = true;
                cbDigit.Enabled = true;
                btnUpdate.Enabled = false;
                gbValues.Visible = false;
                btnAddNewRule.Text = "Save New Rule";
                btnUpdCancel.Text = "Cancel Changes";
                cbDigit.SelectedIndex = 0;
                gAllowDigitChangeEvent = true;
                if (lbScreenMode.Text == "AddNewPart")
                {
                    cbPartNum.Enabled = true;
                }
            }
            else
            {
                string valueStr = "";
                string errorStr = "";

                bool firstValue = true;
                bool valueSelected = false;

                //if (digitStr != "0")
                //{
                    var labelControls = new List<Control>();
                    var checkBoxControls = new List<CheckBox>();

                    labelControls.Add(this.lbVal1);
                    labelControls.Add(this.lbVal2);
                    labelControls.Add(this.lbVal3);
                    labelControls.Add(this.lbVal4);
                    labelControls.Add(this.lbVal5);
                    labelControls.Add(this.lbVal6);
                    labelControls.Add(this.lbVal7);
                    labelControls.Add(this.lbVal8);
                    labelControls.Add(this.lbVal9);
                    labelControls.Add(this.lbVal10);
                    labelControls.Add(this.lbVal11);
                    labelControls.Add(this.lbVal12);
                    labelControls.Add(this.lbVal13);
                    labelControls.Add(this.lbVal14);
                    labelControls.Add(this.lbVal15);
                    labelControls.Add(this.lbVal16);
                    labelControls.Add(this.lbVal17);
                    labelControls.Add(this.lbVal18);
                    labelControls.Add(this.lbVal19);
                    labelControls.Add(this.lbVal20);
                    labelControls.Add(this.lbVal21);
                    labelControls.Add(this.lbVal22);
                    labelControls.Add(this.lbVal23);
                    labelControls.Add(this.lbVal24);
                    labelControls.Add(this.lbVal25);
                    labelControls.Add(this.lbVal26);
                    labelControls.Add(this.lbVal27);
                    labelControls.Add(this.lbVal28);
                    labelControls.Add(this.lbVal29);
                    labelControls.Add(this.lbVal30);

                    checkBoxControls.Add(this.cbVal1);
                    checkBoxControls.Add(this.cbVal2);
                    checkBoxControls.Add(this.cbVal3);
                    checkBoxControls.Add(this.cbVal4);
                    checkBoxControls.Add(this.cbVal5);
                    checkBoxControls.Add(this.cbVal6);
                    checkBoxControls.Add(this.cbVal7);
                    checkBoxControls.Add(this.cbVal8);
                    checkBoxControls.Add(this.cbVal9);
                    checkBoxControls.Add(this.cbVal10);
                    checkBoxControls.Add(this.cbVal11);
                    checkBoxControls.Add(this.cbVal12);
                    checkBoxControls.Add(this.cbVal13);
                    checkBoxControls.Add(this.cbVal14);
                    checkBoxControls.Add(this.cbVal15);
                    checkBoxControls.Add(this.cbVal16);
                    checkBoxControls.Add(this.cbVal17);
                    checkBoxControls.Add(this.cbVal18);
                    checkBoxControls.Add(this.cbVal19);
                    checkBoxControls.Add(this.cbVal20);
                    checkBoxControls.Add(this.cbVal21);
                    checkBoxControls.Add(this.cbVal22);
                    checkBoxControls.Add(this.cbVal23);
                    checkBoxControls.Add(this.cbVal24);
                    checkBoxControls.Add(this.cbVal25);
                    checkBoxControls.Add(this.cbVal26);
                    checkBoxControls.Add(this.cbVal27);
                    checkBoxControls.Add(this.cbVal28);
                    checkBoxControls.Add(this.cbVal29);
                    checkBoxControls.Add(this.cbVal30);

                    for (int i = 0; i < 30; ++i)
                    {
                        if (checkBoxControls[i].Enabled == true)
                        {
                            if (checkBoxControls[i].Checked == true)
                            {
                                valueSelected = true;
                                if (firstValue)
                                {
                                    valueStr = labelControls[i].Text;
                                    firstValue = false;
                                }
                                else
                                {
                                    valueStr += "," + labelControls[i].Text;
                                }
                            }
                        }
                    }
                //}
                //else
                //{
                //    valueSelected = true;
                //    mHeatType = "NA";
                //}
      
                //if (cbUnitType.Text.Length == 0)
                //{
                //    errorStr += "ERROR -- Unit Type is a required field!\n";
                //}               

                //if (cbDigit.SelectedIndex == 0)
                //{
                //    errorStr += "ERROR -- Digit is a required field!\n";
                //}

                foreach (DataGridViewRow dr in dgvUpdateRules.Rows)
                {
                    digitStr = dr.Cells["ColDigit"].Value.ToString();
                    if (Int32.Parse(digitStr) == digitInt)
                    {
                        dupDigitFound = true;
                        break;
                    }
                }
               
                if (dupDigitFound)
                {
                    errorStr += "ERROR -- A Rule utilizing the Selected ModelNo Digit already exist.\n";                   
                }

                if (valueSelected == false)
                {
                    errorStr += "ERROR -- No values have been selected!\n";
                }

                if (nudReqQty.Value <= 0)
                {
                    errorStr += "ERROR -- Required Qty must be Greater Than 0!";
                }

                if (errorStr.Length == 0)
                {
                    int index = dgvUpdateRules.Rows.Add();


                    dgvUpdateRules.Rows[index].Cells["ColRuleHeadID"].Value = txtRuleID.Text;
                    dgvUpdateRules.Rows[index].Cells["ColRuleBatchID"].Value = txtBatchID.Text;
                    dgvUpdateRules.Rows[index].Cells["ColPartNumber"].Value = cbPartNum.Text;
                    dgvUpdateRules.Rows[index].Cells["ColPartDesc"].Value = txtPartDesc.Text;
                    dgvUpdateRules.Rows[index].Cells["ColRelatedOp"].Value = txtOARelOp.Text;
                    dgvUpdateRules.Rows[index].Cells["ColPartCat"].Value = txtPartCategory.Text;
                    dgvUpdateRules.Rows[index].Cells["ColUnitType"].Value = mUnitType;
                    dgvUpdateRules.Rows[index].Cells["ColDigit"].Value = mDigitNo;
                    dgvUpdateRules.Rows[index].Cells["ColHeatType"].Value = mHeatType;
                    dgvUpdateRules.Rows[index].Cells["ColValue"].Value = valueStr;
                    dgvUpdateRules.Rows[index].Cells["ColQty"].Value = nudReqQty.Value;
                    dgvUpdateRules.Rows[index].Cells["ColUpdateType"].Value = "Insert";                       
                    
                    dgvUpdateRules.Rows[index].Selected = true;

                    btnSave.Enabled = true;
                    //btnUpdate.Enabled = true;
                    btnAddNewRule.Text = "Add New Rule";
                    btnUpdCancel.Text = "Exit";
                    cbDigit.Enabled = false;
                    mChangesSaved = false;
                    //mUnitType = cbUnitType.Text;

                    if (mUpdateAll == true)
                    {
                        foreach (DataGridViewRow dr in dgvUpdateRules.Rows)
                        {
                            //dr.Cells[6].Value = cbUnitType.Text;
                            dr.Cells[10].Value = nudReqQty.Value;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(errorStr);
                }
            }
        }

        private void cbDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            string digitStr = cbDigit.Text;           

            if (cbDigit.SelectedIndex != 0)
            {
                if ((gAllowDigitChangeEvent == true) || (gInitialDisplay == true))
                {                   
                    digitStr = digitStr.Substring(0, digitStr.IndexOf('/'));

                    int digitInt = Int32.Parse(digitStr);
                   
                    mDigitNo = digitInt;
                    mHeatType = cbDigit.SelectedValue.ToString();
                    gbValues.Visible = true;
                    if (mAddRule == true)
                    {
                        setupValueCheckBoxes(this, mDigitNo, mHeatType);
                    }                    
                }
            }
            else
            {
                gbValues.Visible = false;
            }
        }

        //private void cbUnitType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (mUnitType != null)
        //    {
        //        if (cbUnitType.Text != mUnitType)
        //        {
        //            mUpdateAll = true;
        //            btnUpdate.Enabled = true;
        //        }
        //    }
        //}

        private void cbPartNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            objPart.PartNum = cbPartNum.Text;
            string test = lbScreenMode.Text;
            if (newPartNum == false)
            {
                if (cbPartNum.SelectedIndex != 0)
                {
                    objPart.GetOAU_PartNumberInfo();
                    txtRuleID.Text = objPart.RuleHeadID.ToString();
                    //txtBatchID.Text = objPart.RuleBatchID.ToString();
                    txtBatchID.Text = "00";
                    txtPartCategory.Text = objPart.PartCategory;
                    txtPartDesc.Text = objPart.PartDescription;
                    txtCriticalComp.Text = objPart.CriticalComp;
                    txtOARelOp.Text = objPart.RelatedOperation.ToString();
                    lbRelatedOp.Text = objPart.RelatedOperation.ToString();
                    
                    btnUpdate.Enabled = true;
                    mAddRule = true;
                    if (lbScreenMode.Text == "CopyMode")
                    {  
                        foreach(DataGridViewRow dgvr in dgvUpdateRules.Rows)
                        {
                            dgvr.Cells["ColRuleHeadID"].Value = txtRuleID.Text;
                            dgvr.Cells["ColPartNumber"].Value = cbPartNum.Text;
                            dgvr.Cells["ColPartDesc"].Value = txtPartDesc.Text;
                            dgvr.Cells["ColRelatedOp"].Value = txtOARelOp.Text;
                            dgvr.Cells["ColPartCat"].Value = txtPartCategory.Text;                           
                        }
                    }
                    else
                    {
                        btnUpdate.Enabled = false;
                        btnUpdCancel.Text = "Cancel";
                        cbDigit.Enabled = true;
                        cbDigit.Select();
                    }
                }
                else
                {
                    MessageBox.Show("ERROR - No part number selected!");
                }
            }
            else
            {
                newPartNum = false;
            }
        }

        private void nudReqQty_ValueChanged(object sender, EventArgs e)
        {
            if (firstPass != false)
            {
                if (nudReqQty.Value != mQty)
                {
                    mUpdateAll = true;
                    if (lbScreenMode.Text == "UpdateMode")
                    {
                        btnUpdate.Enabled = true;
                    }
                }
            }
        }

        private void btnDeleteRuleSet_Click(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("WARNING! You have chosen to delete the entire Rule Set.\nClick 'Yes' to continue or 'No' to cancel.",
                                                      "Deleting a Rule",
                                                      MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                foreach (DataGridViewRow dgvr in dgvUpdateRules.Rows)
                {
                    dgvr.Visible = false;
                    dgvr.Cells["ColUpdateType"].Value = "Delete";                   
                }                
                dgvUpdateRules.Refresh();
                cbDigit.SelectedIndex = 0;
                gbValues.Visible = false;
                btnUpdate.Enabled = false;
                btnSave.Enabled = true;
                mChangesSaved = false;
                gDeleteRow = true;
            }
        }

        private void btnAllRuleSets_Click(object sender, EventArgs e)
        {           
            int iBatchId = 0;            
            int iCurBatchId = -1;

            Color prevBackground = Color.White;
            Color foreGround = Color.Black;
            Color curBackGround = Color.LightGray;
            Color nextBackGround = Color.White;  

            objPart.PartNum = cbPartNum.Text;
            DataTable dt = objPart.GetOAU_PartRulesByPartNum();
            dgvUpdateRules.Rows.Clear();

            gCopyAllRuleSets = true;

            foreach (DataRow dr in dt.Rows)
            {
                int index = dgvUpdateRules.Rows.Add();

                dgvUpdateRules.Rows[index].Cells["ColRuleHeadID"].Value = dr["RuleHeadID"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColRuleBatchID"].Value = dr["RuleBatchID"].ToString(); ;
                dgvUpdateRules.Rows[index].Cells["ColPartNumber"].Value = dr["PartNum"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColPartDesc"].Value = dr["PartDescription"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColRelatedOp"].Value = dr["RelatedOperation"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColPartCat"].Value = dr["PartCategory"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColUnitType"].Value = dr["Unit"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColDigit"].Value = dr["Digit"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColHeatType"].Value = dr["HeatType"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColValue"].Value = dr["Value"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColQty"].Value = dr["Qty"].ToString();
                dgvUpdateRules.Rows[index].Cells["ColUpdateType"].Value = "Insert";                                          

                iBatchId = Int32.Parse(dr["RuleBatchID"].ToString());

                if (iCurBatchId != iBatchId)
                {                   
                    iCurBatchId = iBatchId;

                    prevBackground = curBackGround;
                    curBackGround = nextBackGround;
                    nextBackGround = prevBackground;
                }

                dgvUpdateRules.Rows[index].DefaultCellStyle.BackColor = curBackGround;
                dgvUpdateRules.Rows[index].DefaultCellStyle.ForeColor = foreGround;
               
            }                       
        }

        private void chkBoxRfgComp_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBoxRfgComp.Checked == true)
            {
                gbCircuits.Visible = true;
                objPart.RfgComponent = "true";
            }
            else
            {
                gbCircuits.Visible = false;
                objPart.RfgComponent = "false";
            }
        }

        //private void chkBoxOA_Unit_CheckedChanged(object sender, EventArgs e)
        //{            
        //    if (gValuesDisplayed == true)
        //    {
        //        if (lbScreenMode.Text == "UpdateMode" || lbScreenMode.Text == "CopyMode")
        //        {
        //            displaySelectedRowData(mRowIdx); 
        //        }
        //        else
        //        {
        //            setupValueCheckBoxes(this, mDigitNo, mHeatType);
        //        }
        //    }
        //}

        //private void chkBoxViking_CheckedChanged(object sender, EventArgs e)
        //{                      
        //    if (gValuesDisplayed == true)
        //    {
        //        //setupValueCheckBoxes(this, mDigitNo, mHeatType);
        //        if (lbScreenMode.Text == "UpdateMode" || lbScreenMode.Text == "CopyMode")
        //        {
        //            displaySelectedRowData(mRowIdx);
        //        }
        //        else
        //        {
        //            setupValueCheckBoxes(this, mDigitNo, mHeatType);
        //        }
        //    }
        //}

        private void chkBoxSubAsm_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBoxSubAsm.Checked == true)
            {                
                gbSubAsmData.Visible = true;
                gbSubAsmData.Enabled = true;

                DataTable dt = objPart.GetSubAsmPartNums();
                cbImmediateParentPartNum.DataSource = dt;
                cbImmediateParentPartNum.DisplayMember = "PartNumDesc";
                cbImmediateParentPartNum.ValueMember = "PartNum";               
                cbImmediateParentPartNum.SelectedIndex = 0;
                cbImmediateParentPartNum.Enabled = true;                                         
            }
            else
            {
                gbSubAsmData.Visible = false;
            }

            btnUpdate.Enabled = true;
        }

        //private void cbPrimaryAsmPartNum_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (cbImmediateParentPartNum.SelectedIndex != 0)
        //    {               
        //        if (cbInnerAsmPartNum.Items.Count == 0)
        //        {
        //            //objPart.AssemblyPartType = "Inner";
        //            //DataTable dtInner = objPart.GetInnerAssemblyPartNums();                                    
                    
        //            //cbInnerAsmPartNum.DisplayMember = "PartNumDesc";
        //            //cbInnerAsmPartNum.ValueMember = "PartNum";
        //            //cbInnerAsmPartNum.DataSource = dtInner;                    
        //            //cbInnerAsmPartNum.SelectedIndex = 0;

        //            //objPart.AssemblyPartType = "Immed";
        //            //DataTable dtImmed = objPart.GetInnerAssemblyPartNums();   

        //            //cbImmediateParentPartNum.DataSource = dtImmed;
        //            //cbImmediateParentPartNum.DisplayMember = "PartNumDesc";
        //            //cbImmediateParentPartNum.ValueMember = "PartNum";
        //            //cbImmediateParentPartNum.SelectedIndex = 0; 
                  
        //            //cbInnerAsmPartNum.Enabled = true;                   
        //            //cbImmediateParentPartNum.Enabled = true;
                                                         
        //        }

        //        if (nudBOM_Level.Value < 3)
        //        {
        //            //if (nudBOM_Level.Value > 0 && nudBOM_Level.Value <= 2)
        //            if (nudBOM_Level.Value > 1)
        //            {
        //                objPart.PartNum = cbPartNum.Text;
        //                DataTable dtInner = cbInnerAsmPartNum.DataSource as DataTable;
                    
        //                int index = dtInner.AsEnumerable().Select(row => row.Field<string>("PartNum") == objPart.PartNum).ToList().FindIndex(col => col);
                        
        //                cbInnerAsmPartNum.SelectedIndex = index;
        //            }
                    
        //            cbImmediateParentPartNum.SelectedValue = cbImmediateParentPartNum.SelectedValue;
        //        }
        //        else
        //        {
        //            cbImmediateParentPartNum.SelectedIndex = 0;
        //        }
                                              
        //        // Read PartMtl table and return only InnerAssembly partNumbers
        //    }
        //}
        
        #endregion       

        #region OtherMethods
        //private int getDigitSelectedIndex(string digitStr, string heatTypeStr)
        //{
        //    int selIdx = 0;
        //    string digitHeatStr = "";

        //    digitHeatStr = digitStr + '/' + heatTypeStr;

        //    for (int x = 0; x < this.cbDigit.Items.Count; ++x)
        //    {
        //        string value = cbDigit.GetItemText(cbDigit.Items[x]);
        //        if (value.Substring(0, value.IndexOf(' ')) == digitHeatStr)
        //        {
        //            selIdx = x;
        //            break;
        //        }
        //    }

        //    return selIdx;
        //}       

        private void setupValueCheckBoxes(frmUpdate frmUpd, int digit, string heatType)
        {
            int idx = 0;
            string valueStr = "";

            DataTable dt = objModel.GetModelNoValues(digit, heatType, mUnitType);

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(frmUpd.lbVal1);
            labelControls.Add(frmUpd.lbVal2);
            labelControls.Add(frmUpd.lbVal3);
            labelControls.Add(frmUpd.lbVal4);
            labelControls.Add(frmUpd.lbVal5);
            labelControls.Add(frmUpd.lbVal6);
            labelControls.Add(frmUpd.lbVal7);
            labelControls.Add(frmUpd.lbVal8);
            labelControls.Add(frmUpd.lbVal9);
            labelControls.Add(frmUpd.lbVal10);
            labelControls.Add(frmUpd.lbVal11);
            labelControls.Add(frmUpd.lbVal12);
            labelControls.Add(frmUpd.lbVal13);
            labelControls.Add(frmUpd.lbVal14);
            labelControls.Add(frmUpd.lbVal15);
            labelControls.Add(frmUpd.lbVal16);
            labelControls.Add(frmUpd.lbVal17);
            labelControls.Add(frmUpd.lbVal18);
            labelControls.Add(frmUpd.lbVal19);
            labelControls.Add(frmUpd.lbVal20);
            labelControls.Add(frmUpd.lbVal21);
            labelControls.Add(frmUpd.lbVal22);
            labelControls.Add(frmUpd.lbVal23);
            labelControls.Add(frmUpd.lbVal24);
            labelControls.Add(frmUpd.lbVal25);
            labelControls.Add(frmUpd.lbVal26);
            labelControls.Add(frmUpd.lbVal27);
            labelControls.Add(frmUpd.lbVal28);
            labelControls.Add(frmUpd.lbVal29);
            labelControls.Add(frmUpd.lbVal30);

            checkBoxControls.Add(frmUpd.cbVal1);
            checkBoxControls.Add(frmUpd.cbVal2);
            checkBoxControls.Add(frmUpd.cbVal3);
            checkBoxControls.Add(frmUpd.cbVal4);
            checkBoxControls.Add(frmUpd.cbVal5);
            checkBoxControls.Add(frmUpd.cbVal6);
            checkBoxControls.Add(frmUpd.cbVal7);
            checkBoxControls.Add(frmUpd.cbVal8);
            checkBoxControls.Add(frmUpd.cbVal9);
            checkBoxControls.Add(frmUpd.cbVal10);
            checkBoxControls.Add(frmUpd.cbVal11);
            checkBoxControls.Add(frmUpd.cbVal12);
            checkBoxControls.Add(frmUpd.cbVal13);
            checkBoxControls.Add(frmUpd.cbVal14);
            checkBoxControls.Add(frmUpd.cbVal15);
            checkBoxControls.Add(frmUpd.cbVal16);
            checkBoxControls.Add(frmUpd.cbVal17);
            checkBoxControls.Add(frmUpd.cbVal18);
            checkBoxControls.Add(frmUpd.cbVal19);
            checkBoxControls.Add(frmUpd.cbVal20);
            checkBoxControls.Add(frmUpd.cbVal21);
            checkBoxControls.Add(frmUpd.cbVal22);
            checkBoxControls.Add(frmUpd.cbVal23);
            checkBoxControls.Add(frmUpd.cbVal24);
            checkBoxControls.Add(frmUpd.cbVal25);
            checkBoxControls.Add(frmUpd.cbVal26);
            checkBoxControls.Add(frmUpd.cbVal27);
            checkBoxControls.Add(frmUpd.cbVal28);
            checkBoxControls.Add(frmUpd.cbVal29);
            checkBoxControls.Add(frmUpd.cbVal30);

            for (int x = 0; x < 30; ++x)
            {
                checkBoxControls[x].Checked = false;
                checkBoxControls[x].Visible = false;
                checkBoxControls[x].Enabled = false;
                labelControls[x].Visible = false;
                labelControls[x].Text = "";
                toolTip1.SetToolTip(labelControls[idx], null);
                toolTip1.SetToolTip(checkBoxControls[idx], null);
                toolTip1.ShowAlways = true;
            }

            if (cbDigit.Text == "0")
            {
                ArrayList dtAry = new ArrayList();
                int aryIdx = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    dtAry.Add(Int32.Parse(dr["DigitVal"].ToString()));
                    ++aryIdx;
                }
                dtAry.Sort();

                for (int y = 0; y < aryIdx; ++y)
                {
                    valueStr = dtAry[y].ToString();
                    labelControls[y].Text = valueStr;
                    labelControls[y].Visible = true;
                    checkBoxControls[y].Visible = true;
                    checkBoxControls[y].Enabled = true;
                }
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    valueStr = dr["DigitVal"].ToString();
                    toolTip1.SetToolTip(labelControls[idx], dr["DigitDescription"].ToString());
                    toolTip1.SetToolTip(checkBoxControls[idx], dr["DigitDescription"].ToString());
                    toolTip1.ShowAlways = true;
                    labelControls[idx].Text = valueStr;
                    labelControls[idx].Visible = true;
                    checkBoxControls[idx].Enabled = true;
                    checkBoxControls[idx++].Visible = true;
                }
            }
            gValuesDisplayed = true;
        }

        private void displaySelectedRowData(int rowIdx)
        {
            string digitStr = "";
            string valueStr = "";
            string selValueStr = "";
            string qtyStr = "";
            
            int idx = 0;
            int selIdx = 0;           

            digitStr = dgvUpdateRules.Rows[rowIdx].Cells["ColDigit"].Value.ToString();
            int digitInt = Int32.Parse(digitStr);
            string heatType = dgvUpdateRules.Rows[rowIdx].Cells["ColHeatType"].Value.ToString();

            gUpdatedBatchId = Int32.Parse(dgvUpdateRules.Rows[rowIdx].Cells["ColRuleBatchID"].Value.ToString());

            qtyStr = dgvUpdateRules.Rows[rowIdx].Cells["ColQty"].Value.ToString();
           
            this.nudReqQty.Value = Decimal.Parse(qtyStr);
            
            selIdx = objPart.getDigitSelectedIndex(digitStr, heatType, cbDigit);
                       
            this.cbDigit.SelectedIndex = selIdx;
            mRowIdx = rowIdx;
            selValueStr = dgvUpdateRules.Rows[rowIdx].Cells["ColValue"].Value.ToString();

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(this.lbVal1);
            labelControls.Add(this.lbVal2);
            labelControls.Add(this.lbVal3);
            labelControls.Add(this.lbVal4);
            labelControls.Add(this.lbVal5);
            labelControls.Add(this.lbVal6);
            labelControls.Add(this.lbVal7);
            labelControls.Add(this.lbVal8);
            labelControls.Add(this.lbVal9);
            labelControls.Add(this.lbVal10);
            labelControls.Add(this.lbVal11);
            labelControls.Add(this.lbVal12);
            labelControls.Add(this.lbVal13);
            labelControls.Add(this.lbVal14);
            labelControls.Add(this.lbVal15);
            labelControls.Add(this.lbVal16);
            labelControls.Add(this.lbVal17);
            labelControls.Add(this.lbVal18);
            labelControls.Add(this.lbVal19);
            labelControls.Add(this.lbVal20);
            labelControls.Add(this.lbVal21);
            labelControls.Add(this.lbVal22);
            labelControls.Add(this.lbVal23);
            labelControls.Add(this.lbVal24);
            labelControls.Add(this.lbVal25);
            labelControls.Add(this.lbVal26);
            labelControls.Add(this.lbVal27);
            labelControls.Add(this.lbVal28);
            labelControls.Add(this.lbVal29);
            labelControls.Add(this.lbVal30);

            checkBoxControls.Add(this.cbVal1);
            checkBoxControls.Add(this.cbVal2);
            checkBoxControls.Add(this.cbVal3);
            checkBoxControls.Add(this.cbVal4);
            checkBoxControls.Add(this.cbVal5);
            checkBoxControls.Add(this.cbVal6);
            checkBoxControls.Add(this.cbVal7);
            checkBoxControls.Add(this.cbVal8);
            checkBoxControls.Add(this.cbVal9);
            checkBoxControls.Add(this.cbVal10);
            checkBoxControls.Add(this.cbVal11);
            checkBoxControls.Add(this.cbVal12);
            checkBoxControls.Add(this.cbVal13);
            checkBoxControls.Add(this.cbVal14);
            checkBoxControls.Add(this.cbVal15);
            checkBoxControls.Add(this.cbVal16);
            checkBoxControls.Add(this.cbVal17);
            checkBoxControls.Add(this.cbVal18);
            checkBoxControls.Add(this.cbVal19);
            checkBoxControls.Add(this.cbVal20);
            checkBoxControls.Add(this.cbVal21);
            checkBoxControls.Add(this.cbVal22);
            checkBoxControls.Add(this.cbVal23);
            checkBoxControls.Add(this.cbVal24);
            checkBoxControls.Add(this.cbVal25);
            checkBoxControls.Add(this.cbVal26);
            checkBoxControls.Add(this.cbVal27);
            checkBoxControls.Add(this.cbVal28);
            checkBoxControls.Add(this.cbVal29);
            checkBoxControls.Add(this.cbVal30);

            for (int x = 0; x < 30; ++x)
            {
                checkBoxControls[x].Checked = false;
                checkBoxControls[x].Visible = false;
                checkBoxControls[x].Enabled = false;
                labelControls[x].Visible = false;
                labelControls[x].Text = "";
                toolTip1.SetToolTip(labelControls[idx], null);
                toolTip1.SetToolTip(checkBoxControls[idx], null);
                toolTip1.ShowAlways = true;
            }

            DataTable dt = objModel.GetModelNoValues(digitInt, heatType, mUnitType);
            
            if (cbDigit.Text == "0")
            {
                ArrayList dtAry = new ArrayList();
                int aryIdx = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    dtAry.Add(Int32.Parse(dr["DigitVal"].ToString()));
                    ++aryIdx;
                }
                dtAry.Sort();

                for (int y = 0; y < aryIdx; ++y)
                {
                    valueStr = dtAry[y].ToString();
                    labelControls[y].Text = valueStr;
                    labelControls[y].Visible = true;
                    checkBoxControls[y].Visible = true;
                    checkBoxControls[y].Enabled = true;
                }
            }
            else
            {

                foreach (DataRow dr in dt.Rows)
                {
                    valueStr = dr["DigitVal"].ToString();
                    toolTip1.SetToolTip(labelControls[idx], dr["DigitDescription"].ToString());
                    toolTip1.SetToolTip(checkBoxControls[idx], dr["DigitDescription"].ToString());
                    toolTip1.ShowAlways = true;
                    labelControls[idx].Text = valueStr;
                    labelControls[idx].Visible = true;
                    checkBoxControls[idx].Enabled = true;
                    checkBoxControls[idx++].Visible = true;
                }
            }

            for (int i = 0; i < 30; ++i)
            {
                if (selValueStr.Contains(labelControls[i].Text))
                {
                    checkBoxControls[i].Checked = true;
                }
                else
                {
                    checkBoxControls[i].Checked = false;
                }
            }
                      
            this.nudReqQty.Enabled = true;
            btnUpdate.Enabled = true;
        }

        private bool DuplicateRule(string ruleHeadID, DataGridView dgvRules)
        {           
            string curRuleBatchID = "";
            string curDigitNo = "";
            string curValues = "";
            string curRowValues = "";
            
            bool dupRuleSetFound = false;
            bool digitNoExist = false;
            bool dupDigitValueFound = false;
            
            int dupDigitCount = 0;     
       
            List <string> batchList = new List<string>();
           
            objPart.RuleHeadID = ruleHeadID;
            objPart.UnitType = "";
            objPart.RevType = "REV6";
            DataTable dt = objPart.GetRuleBatchesForPartNum_UnitType();

            foreach (DataRow dr in dt.Rows)
            {
                if (curRuleBatchID !=  dr["RuleBatchID"].ToString())
                {                    
                    curRuleBatchID = dr["RuleBatchID"].ToString();
                    objPart.RuleBatchID = curRuleBatchID;
                    DataTable dtRules = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();
                    if (dt.Rows.Count == dtRules.Rows.Count)
                    {

                        batchList.Add(curRuleBatchID);
                    }                
                }                                        
            }

            if (batchList.Count > 0)
            {
                foreach(string batchID in batchList)
                {
                    objPart.RuleBatchID = batchID;
                    DataTable dtRules = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

                    foreach(DataRow dr in dtRules.Rows)
                    {
                        curDigitNo = dr["DigitNo"].ToString();
                        curValues =  dr["Value"].ToString();
                        digitNoExist = false;
                        foreach(DataGridViewRow row in dgvRules.Rows)
                        {
                            if (curDigitNo == row.Cells["DigitNo"].ToString())
                            {
                                curRowValues = row.Cells["Value"].ToString();
                                digitNoExist = true;
                                foreach(char ch in curValues)
                                {
                                    if (curRowValues.Contains(ch))
                                    {
                                        dupDigitValueFound = true;
                                        ++dupDigitCount;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                digitNoExist = false;
                            }
                            if (dupDigitValueFound == true)
                            {
                                break;
                            }
                        }
                        if (digitNoExist == false)
                        {
                            dupRuleSetFound = false;
                            break;
                        }
                    }
                }
            }
            return dupRuleSetFound;
        }                                                                                                             

        //private void chkBoxPreConfigAsm_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkBoxPreConfigAsm.Checked == true)
        //    {
        //        gPreConfigAsm = true;
        //    }
        //    else
        //    {
        //        gPreConfigAsm = false;
        //    }
        //    btnUpdate.Enabled = true;
        //}

        private void chkBoxMtlPart_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBoxMtlPart.Checked == true)
            {
                gSubAsmMtlPart = true;
            }
            else
            {
                gSubAsmMtlPart = false;
            }
            btnUpdate.Enabled = true;
        }

        //private void nudBOM_Level_ValueChanged(object sender, EventArgs e)
        //{
        //    if (nudBOM_Level.Value > 1)
        //    {
        //        cbInnerAsmPartNum.Enabled = true;
        //    }
        //    else
        //    {
        //        cbInnerAsmPartNum.Enabled = false;
        //    }
        //}        

        #endregion      
    }
}
