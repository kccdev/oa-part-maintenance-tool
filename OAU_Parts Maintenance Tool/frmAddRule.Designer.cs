﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmAddRule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtRuleID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAddRule = new System.Windows.Forms.Button();
            this.btnUpdCancel = new System.Windows.Forms.Button();
            this.txtBatchID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbDigit = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudReqQty = new System.Windows.Forms.NumericUpDown();
            this.lbVal30 = new System.Windows.Forms.Label();
            this.lbVal29 = new System.Windows.Forms.Label();
            this.lbVal28 = new System.Windows.Forms.Label();
            this.lbVal27 = new System.Windows.Forms.Label();
            this.lbVal26 = new System.Windows.Forms.Label();
            this.lbVal25 = new System.Windows.Forms.Label();
            this.lbVal24 = new System.Windows.Forms.Label();
            this.lbVal23 = new System.Windows.Forms.Label();
            this.lbVal22 = new System.Windows.Forms.Label();
            this.lbVal21 = new System.Windows.Forms.Label();
            this.lbVal20 = new System.Windows.Forms.Label();
            this.lbVal19 = new System.Windows.Forms.Label();
            this.lbVal18 = new System.Windows.Forms.Label();
            this.lbVal17 = new System.Windows.Forms.Label();
            this.lbVal16 = new System.Windows.Forms.Label();
            this.lbVal15 = new System.Windows.Forms.Label();
            this.lbVal14 = new System.Windows.Forms.Label();
            this.lbVal13 = new System.Windows.Forms.Label();
            this.lbVal12 = new System.Windows.Forms.Label();
            this.lbVal11 = new System.Windows.Forms.Label();
            this.lbVal10 = new System.Windows.Forms.Label();
            this.lbVal9 = new System.Windows.Forms.Label();
            this.lbVal8 = new System.Windows.Forms.Label();
            this.lbVal7 = new System.Windows.Forms.Label();
            this.lbVal6 = new System.Windows.Forms.Label();
            this.lbVal5 = new System.Windows.Forms.Label();
            this.lbVal4 = new System.Windows.Forms.Label();
            this.lbVal3 = new System.Windows.Forms.Label();
            this.lbVal2 = new System.Windows.Forms.Label();
            this.lbVal1 = new System.Windows.Forms.Label();
            this.cbVal30 = new System.Windows.Forms.CheckBox();
            this.cbVal29 = new System.Windows.Forms.CheckBox();
            this.cbVal28 = new System.Windows.Forms.CheckBox();
            this.cbVal27 = new System.Windows.Forms.CheckBox();
            this.cbVal26 = new System.Windows.Forms.CheckBox();
            this.cbVal25 = new System.Windows.Forms.CheckBox();
            this.cbVal24 = new System.Windows.Forms.CheckBox();
            this.cbVal23 = new System.Windows.Forms.CheckBox();
            this.cbVal22 = new System.Windows.Forms.CheckBox();
            this.cbVal21 = new System.Windows.Forms.CheckBox();
            this.cbVal20 = new System.Windows.Forms.CheckBox();
            this.cbVal19 = new System.Windows.Forms.CheckBox();
            this.cbVal18 = new System.Windows.Forms.CheckBox();
            this.cbVal17 = new System.Windows.Forms.CheckBox();
            this.cbVal16 = new System.Windows.Forms.CheckBox();
            this.cbVal15 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.cbVal14 = new System.Windows.Forms.CheckBox();
            this.cbVal13 = new System.Windows.Forms.CheckBox();
            this.cbVal12 = new System.Windows.Forms.CheckBox();
            this.cbVal11 = new System.Windows.Forms.CheckBox();
            this.cbVal10 = new System.Windows.Forms.CheckBox();
            this.cbVal9 = new System.Windows.Forms.CheckBox();
            this.cbVal8 = new System.Windows.Forms.CheckBox();
            this.cbVal7 = new System.Windows.Forms.CheckBox();
            this.cbVal6 = new System.Windows.Forms.CheckBox();
            this.cbVal5 = new System.Windows.Forms.CheckBox();
            this.cbVal4 = new System.Windows.Forms.CheckBox();
            this.cbVal3 = new System.Windows.Forms.CheckBox();
            this.cbVal2 = new System.Windows.Forms.CheckBox();
            this.cbVal1 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPartDesc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbUnitType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPartNum = new System.Windows.Forms.ComboBox();
            this.lbAddMsg = new System.Windows.Forms.Label();
            this.txtCriticalComp = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPartCategory = new System.Windows.Forms.TextBox();
            this.dgvAddRules = new System.Windows.Forms.DataGridView();
            this.ColRuleHeadID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRuleBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPartNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPartDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRelatedOp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPartCat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUnitType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDigit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColHeatType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUpdateButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColDelButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColDigitSelIdx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRelatedOp = new System.Windows.Forms.TextBox();
            this.btnSaveRules = new System.Windows.Forms.Button();
            this.gbValues = new System.Windows.Forms.GroupBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lbScreenMode = new System.Windows.Forms.Label();
            this.btnUpdateRule = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudReqQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddRules)).BeginInit();
            this.gbValues.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRuleID
            // 
            this.txtRuleID.Enabled = false;
            this.txtRuleID.Location = new System.Drawing.Point(446, 39);
            this.txtRuleID.Name = "txtRuleID";
            this.txtRuleID.Size = new System.Drawing.Size(115, 20);
            this.txtRuleID.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(374, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 20);
            this.label5.TabIndex = 169;
            this.label5.Text = "Rule ID:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnAddRule
            // 
            this.btnAddRule.Location = new System.Drawing.Point(1120, 12);
            this.btnAddRule.Name = "btnAddRule";
            this.btnAddRule.Size = new System.Drawing.Size(100, 30);
            this.btnAddRule.TabIndex = 60;
            this.btnAddRule.Text = "Add Rule";
            this.btnAddRule.UseVisualStyleBackColor = true;
            this.btnAddRule.Click += new System.EventHandler(this.btnAddRule_Click);
            // 
            // btnUpdCancel
            // 
            this.btnUpdCancel.Location = new System.Drawing.Point(1255, 53);
            this.btnUpdCancel.Name = "btnUpdCancel";
            this.btnUpdCancel.Size = new System.Drawing.Size(100, 30);
            this.btnUpdCancel.TabIndex = 61;
            this.btnUpdCancel.Text = "Cancel";
            this.btnUpdCancel.UseVisualStyleBackColor = true;
            this.btnUpdCancel.Click += new System.EventHandler(this.btnUpdCancel_Click);
            // 
            // txtBatchID
            // 
            this.txtBatchID.Enabled = false;
            this.txtBatchID.Location = new System.Drawing.Point(740, 39);
            this.txtBatchID.Name = "txtBatchID";
            this.txtBatchID.Size = new System.Drawing.Size(100, 20);
            this.txtBatchID.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(655, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 20);
            this.label10.TabIndex = 165;
            this.label10.Text = "Batch ID:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit
            // 
            this.cbDigit.Enabled = false;
            this.cbDigit.FormattingEnabled = true;
            this.cbDigit.Location = new System.Drawing.Point(446, 65);
            this.cbDigit.Name = "cbDigit";
            this.cbDigit.Size = new System.Drawing.Size(212, 21);
            this.cbDigit.TabIndex = 8;
            this.cbDigit.SelectedIndexChanged += new System.EventHandler(this.cbDigit_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(836, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 20);
            this.label9.TabIndex = 163;
            this.label9.Text = "Required Qty:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudReqQty
            // 
            this.nudReqQty.DecimalPlaces = 2;
            this.nudReqQty.Enabled = false;
            this.nudReqQty.Location = new System.Drawing.Point(987, 65);
            this.nudReqQty.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudReqQty.Name = "nudReqQty";
            this.nudReqQty.Size = new System.Drawing.Size(85, 20);
            this.nudReqQty.TabIndex = 10;
            this.nudReqQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbVal30
            // 
            this.lbVal30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal30.ForeColor = System.Drawing.Color.Black;
            this.lbVal30.Location = new System.Drawing.Point(936, 37);
            this.lbVal30.Name = "lbVal30";
            this.lbVal30.Size = new System.Drawing.Size(16, 68);
            this.lbVal30.TabIndex = 161;
            this.lbVal30.Text = "Heat Type:";
            this.lbVal30.Visible = false;
            // 
            // lbVal29
            // 
            this.lbVal29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal29.ForeColor = System.Drawing.Color.Black;
            this.lbVal29.Location = new System.Drawing.Point(906, 37);
            this.lbVal29.Name = "lbVal29";
            this.lbVal29.Size = new System.Drawing.Size(16, 68);
            this.lbVal29.TabIndex = 160;
            this.lbVal29.Text = "Heat Type:";
            this.lbVal29.Visible = false;
            // 
            // lbVal28
            // 
            this.lbVal28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal28.ForeColor = System.Drawing.Color.Black;
            this.lbVal28.Location = new System.Drawing.Point(876, 37);
            this.lbVal28.Name = "lbVal28";
            this.lbVal28.Size = new System.Drawing.Size(16, 68);
            this.lbVal28.TabIndex = 159;
            this.lbVal28.Text = "Heat Type:";
            this.lbVal28.Visible = false;
            // 
            // lbVal27
            // 
            this.lbVal27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal27.ForeColor = System.Drawing.Color.Black;
            this.lbVal27.Location = new System.Drawing.Point(846, 37);
            this.lbVal27.Name = "lbVal27";
            this.lbVal27.Size = new System.Drawing.Size(16, 68);
            this.lbVal27.TabIndex = 158;
            this.lbVal27.Text = "Heat Type:";
            this.lbVal27.Visible = false;
            // 
            // lbVal26
            // 
            this.lbVal26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal26.ForeColor = System.Drawing.Color.Black;
            this.lbVal26.Location = new System.Drawing.Point(816, 37);
            this.lbVal26.Name = "lbVal26";
            this.lbVal26.Size = new System.Drawing.Size(16, 68);
            this.lbVal26.TabIndex = 157;
            this.lbVal26.Text = "Heat Type:";
            this.lbVal26.Visible = false;
            // 
            // lbVal25
            // 
            this.lbVal25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal25.ForeColor = System.Drawing.Color.Black;
            this.lbVal25.Location = new System.Drawing.Point(786, 37);
            this.lbVal25.Name = "lbVal25";
            this.lbVal25.Size = new System.Drawing.Size(16, 68);
            this.lbVal25.TabIndex = 156;
            this.lbVal25.Text = "Heat Type:";
            this.lbVal25.Visible = false;
            // 
            // lbVal24
            // 
            this.lbVal24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal24.ForeColor = System.Drawing.Color.Black;
            this.lbVal24.Location = new System.Drawing.Point(756, 37);
            this.lbVal24.Name = "lbVal24";
            this.lbVal24.Size = new System.Drawing.Size(16, 68);
            this.lbVal24.TabIndex = 155;
            this.lbVal24.Text = "Heat Type:";
            this.lbVal24.Visible = false;
            // 
            // lbVal23
            // 
            this.lbVal23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal23.ForeColor = System.Drawing.Color.Black;
            this.lbVal23.Location = new System.Drawing.Point(726, 37);
            this.lbVal23.Name = "lbVal23";
            this.lbVal23.Size = new System.Drawing.Size(16, 68);
            this.lbVal23.TabIndex = 154;
            this.lbVal23.Text = "Heat Type:";
            this.lbVal23.Visible = false;
            // 
            // lbVal22
            // 
            this.lbVal22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal22.ForeColor = System.Drawing.Color.Black;
            this.lbVal22.Location = new System.Drawing.Point(696, 37);
            this.lbVal22.Name = "lbVal22";
            this.lbVal22.Size = new System.Drawing.Size(16, 68);
            this.lbVal22.TabIndex = 153;
            this.lbVal22.Text = "Heat Type:";
            this.lbVal22.Visible = false;
            // 
            // lbVal21
            // 
            this.lbVal21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal21.ForeColor = System.Drawing.Color.Black;
            this.lbVal21.Location = new System.Drawing.Point(666, 37);
            this.lbVal21.Name = "lbVal21";
            this.lbVal21.Size = new System.Drawing.Size(16, 68);
            this.lbVal21.TabIndex = 152;
            this.lbVal21.Text = "Heat Type:";
            this.lbVal21.Visible = false;
            // 
            // lbVal20
            // 
            this.lbVal20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal20.ForeColor = System.Drawing.Color.Black;
            this.lbVal20.Location = new System.Drawing.Point(636, 37);
            this.lbVal20.Name = "lbVal20";
            this.lbVal20.Size = new System.Drawing.Size(16, 68);
            this.lbVal20.TabIndex = 151;
            this.lbVal20.Text = "Heat Type:";
            this.lbVal20.Visible = false;
            // 
            // lbVal19
            // 
            this.lbVal19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal19.ForeColor = System.Drawing.Color.Black;
            this.lbVal19.Location = new System.Drawing.Point(606, 37);
            this.lbVal19.Name = "lbVal19";
            this.lbVal19.Size = new System.Drawing.Size(16, 68);
            this.lbVal19.TabIndex = 150;
            this.lbVal19.Text = "Heat Type:";
            this.lbVal19.Visible = false;
            // 
            // lbVal18
            // 
            this.lbVal18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal18.ForeColor = System.Drawing.Color.Black;
            this.lbVal18.Location = new System.Drawing.Point(576, 37);
            this.lbVal18.Name = "lbVal18";
            this.lbVal18.Size = new System.Drawing.Size(16, 68);
            this.lbVal18.TabIndex = 149;
            this.lbVal18.Text = "Heat Type:";
            this.lbVal18.Visible = false;
            // 
            // lbVal17
            // 
            this.lbVal17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal17.ForeColor = System.Drawing.Color.Black;
            this.lbVal17.Location = new System.Drawing.Point(546, 37);
            this.lbVal17.Name = "lbVal17";
            this.lbVal17.Size = new System.Drawing.Size(16, 68);
            this.lbVal17.TabIndex = 148;
            this.lbVal17.Text = "Heat Type:";
            this.lbVal17.Visible = false;
            // 
            // lbVal16
            // 
            this.lbVal16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal16.ForeColor = System.Drawing.Color.Black;
            this.lbVal16.Location = new System.Drawing.Point(516, 37);
            this.lbVal16.Name = "lbVal16";
            this.lbVal16.Size = new System.Drawing.Size(16, 68);
            this.lbVal16.TabIndex = 147;
            this.lbVal16.Text = "Heat Type:";
            this.lbVal16.Visible = false;
            // 
            // lbVal15
            // 
            this.lbVal15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal15.ForeColor = System.Drawing.Color.Black;
            this.lbVal15.Location = new System.Drawing.Point(486, 37);
            this.lbVal15.Name = "lbVal15";
            this.lbVal15.Size = new System.Drawing.Size(16, 68);
            this.lbVal15.TabIndex = 146;
            this.lbVal15.Text = "Heat Type:";
            this.lbVal15.Visible = false;
            // 
            // lbVal14
            // 
            this.lbVal14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal14.ForeColor = System.Drawing.Color.Black;
            this.lbVal14.Location = new System.Drawing.Point(456, 37);
            this.lbVal14.Name = "lbVal14";
            this.lbVal14.Size = new System.Drawing.Size(16, 68);
            this.lbVal14.TabIndex = 145;
            this.lbVal14.Text = "Heat Type:";
            this.lbVal14.Visible = false;
            // 
            // lbVal13
            // 
            this.lbVal13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal13.ForeColor = System.Drawing.Color.Black;
            this.lbVal13.Location = new System.Drawing.Point(426, 37);
            this.lbVal13.Name = "lbVal13";
            this.lbVal13.Size = new System.Drawing.Size(16, 68);
            this.lbVal13.TabIndex = 144;
            this.lbVal13.Text = "Heat Type:";
            this.lbVal13.Visible = false;
            // 
            // lbVal12
            // 
            this.lbVal12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal12.ForeColor = System.Drawing.Color.Black;
            this.lbVal12.Location = new System.Drawing.Point(396, 37);
            this.lbVal12.Name = "lbVal12";
            this.lbVal12.Size = new System.Drawing.Size(16, 68);
            this.lbVal12.TabIndex = 143;
            this.lbVal12.Text = "Heat Type:";
            this.lbVal12.Visible = false;
            // 
            // lbVal11
            // 
            this.lbVal11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal11.ForeColor = System.Drawing.Color.Black;
            this.lbVal11.Location = new System.Drawing.Point(366, 37);
            this.lbVal11.Name = "lbVal11";
            this.lbVal11.Size = new System.Drawing.Size(16, 68);
            this.lbVal11.TabIndex = 142;
            this.lbVal11.Text = "Heat Type:";
            this.lbVal11.Visible = false;
            // 
            // lbVal10
            // 
            this.lbVal10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal10.ForeColor = System.Drawing.Color.Black;
            this.lbVal10.Location = new System.Drawing.Point(336, 37);
            this.lbVal10.Name = "lbVal10";
            this.lbVal10.Size = new System.Drawing.Size(16, 68);
            this.lbVal10.TabIndex = 141;
            this.lbVal10.Text = "Heat Type:";
            this.lbVal10.Visible = false;
            // 
            // lbVal9
            // 
            this.lbVal9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal9.ForeColor = System.Drawing.Color.Black;
            this.lbVal9.Location = new System.Drawing.Point(306, 37);
            this.lbVal9.Name = "lbVal9";
            this.lbVal9.Size = new System.Drawing.Size(16, 68);
            this.lbVal9.TabIndex = 140;
            this.lbVal9.Text = "Heat Type:";
            this.lbVal9.Visible = false;
            // 
            // lbVal8
            // 
            this.lbVal8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal8.ForeColor = System.Drawing.Color.Black;
            this.lbVal8.Location = new System.Drawing.Point(276, 37);
            this.lbVal8.Name = "lbVal8";
            this.lbVal8.Size = new System.Drawing.Size(16, 68);
            this.lbVal8.TabIndex = 139;
            this.lbVal8.Text = "Heat Type:";
            this.lbVal8.Visible = false;
            // 
            // lbVal7
            // 
            this.lbVal7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal7.ForeColor = System.Drawing.Color.Black;
            this.lbVal7.Location = new System.Drawing.Point(246, 37);
            this.lbVal7.Name = "lbVal7";
            this.lbVal7.Size = new System.Drawing.Size(16, 68);
            this.lbVal7.TabIndex = 138;
            this.lbVal7.Text = "Heat Type:";
            this.lbVal7.Visible = false;
            // 
            // lbVal6
            // 
            this.lbVal6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal6.ForeColor = System.Drawing.Color.Black;
            this.lbVal6.Location = new System.Drawing.Point(216, 37);
            this.lbVal6.Name = "lbVal6";
            this.lbVal6.Size = new System.Drawing.Size(16, 68);
            this.lbVal6.TabIndex = 137;
            this.lbVal6.Text = "Heat Type:";
            this.lbVal6.Visible = false;
            // 
            // lbVal5
            // 
            this.lbVal5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal5.ForeColor = System.Drawing.Color.Black;
            this.lbVal5.Location = new System.Drawing.Point(186, 37);
            this.lbVal5.Name = "lbVal5";
            this.lbVal5.Size = new System.Drawing.Size(16, 68);
            this.lbVal5.TabIndex = 136;
            this.lbVal5.Text = "Heat Type:";
            this.lbVal5.Visible = false;
            // 
            // lbVal4
            // 
            this.lbVal4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal4.ForeColor = System.Drawing.Color.Black;
            this.lbVal4.Location = new System.Drawing.Point(156, 37);
            this.lbVal4.Name = "lbVal4";
            this.lbVal4.Size = new System.Drawing.Size(16, 68);
            this.lbVal4.TabIndex = 135;
            this.lbVal4.Text = "Heat Type:";
            this.lbVal4.Visible = false;
            // 
            // lbVal3
            // 
            this.lbVal3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal3.ForeColor = System.Drawing.Color.Black;
            this.lbVal3.Location = new System.Drawing.Point(126, 37);
            this.lbVal3.Name = "lbVal3";
            this.lbVal3.Size = new System.Drawing.Size(16, 68);
            this.lbVal3.TabIndex = 134;
            this.lbVal3.Text = "Heat Type:";
            this.lbVal3.Visible = false;
            // 
            // lbVal2
            // 
            this.lbVal2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal2.ForeColor = System.Drawing.Color.Black;
            this.lbVal2.Location = new System.Drawing.Point(96, 37);
            this.lbVal2.Name = "lbVal2";
            this.lbVal2.Size = new System.Drawing.Size(16, 68);
            this.lbVal2.TabIndex = 133;
            this.lbVal2.Text = "Heat Type:";
            this.lbVal2.Visible = false;
            // 
            // lbVal1
            // 
            this.lbVal1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal1.ForeColor = System.Drawing.Color.Black;
            this.lbVal1.Location = new System.Drawing.Point(66, 37);
            this.lbVal1.Name = "lbVal1";
            this.lbVal1.Size = new System.Drawing.Size(16, 68);
            this.lbVal1.TabIndex = 132;
            this.lbVal1.Tag = "1";
            this.lbVal1.Text = "Heat Type:";
            this.lbVal1.Visible = false;
            // 
            // cbVal30
            // 
            this.cbVal30.AutoSize = true;
            this.cbVal30.Location = new System.Drawing.Point(938, 20);
            this.cbVal30.Name = "cbVal30";
            this.cbVal30.Size = new System.Drawing.Size(15, 14);
            this.cbVal30.TabIndex = 49;
            this.cbVal30.UseVisualStyleBackColor = true;
            this.cbVal30.Visible = false;
            // 
            // cbVal29
            // 
            this.cbVal29.AutoSize = true;
            this.cbVal29.Location = new System.Drawing.Point(908, 20);
            this.cbVal29.Name = "cbVal29";
            this.cbVal29.Size = new System.Drawing.Size(15, 14);
            this.cbVal29.TabIndex = 48;
            this.cbVal29.UseVisualStyleBackColor = true;
            this.cbVal29.Visible = false;
            // 
            // cbVal28
            // 
            this.cbVal28.AutoSize = true;
            this.cbVal28.Location = new System.Drawing.Point(878, 20);
            this.cbVal28.Name = "cbVal28";
            this.cbVal28.Size = new System.Drawing.Size(15, 14);
            this.cbVal28.TabIndex = 47;
            this.cbVal28.UseVisualStyleBackColor = true;
            this.cbVal28.Visible = false;
            // 
            // cbVal27
            // 
            this.cbVal27.AutoSize = true;
            this.cbVal27.Location = new System.Drawing.Point(848, 20);
            this.cbVal27.Name = "cbVal27";
            this.cbVal27.Size = new System.Drawing.Size(15, 14);
            this.cbVal27.TabIndex = 46;
            this.cbVal27.UseVisualStyleBackColor = true;
            this.cbVal27.Visible = false;
            // 
            // cbVal26
            // 
            this.cbVal26.AutoSize = true;
            this.cbVal26.Location = new System.Drawing.Point(818, 20);
            this.cbVal26.Name = "cbVal26";
            this.cbVal26.Size = new System.Drawing.Size(15, 14);
            this.cbVal26.TabIndex = 45;
            this.cbVal26.UseVisualStyleBackColor = true;
            this.cbVal26.Visible = false;
            // 
            // cbVal25
            // 
            this.cbVal25.AutoSize = true;
            this.cbVal25.Location = new System.Drawing.Point(788, 20);
            this.cbVal25.Name = "cbVal25";
            this.cbVal25.Size = new System.Drawing.Size(15, 14);
            this.cbVal25.TabIndex = 44;
            this.cbVal25.UseVisualStyleBackColor = true;
            this.cbVal25.Visible = false;
            // 
            // cbVal24
            // 
            this.cbVal24.AutoSize = true;
            this.cbVal24.Location = new System.Drawing.Point(758, 20);
            this.cbVal24.Name = "cbVal24";
            this.cbVal24.Size = new System.Drawing.Size(15, 14);
            this.cbVal24.TabIndex = 43;
            this.cbVal24.UseVisualStyleBackColor = true;
            this.cbVal24.Visible = false;
            // 
            // cbVal23
            // 
            this.cbVal23.AutoSize = true;
            this.cbVal23.Location = new System.Drawing.Point(728, 20);
            this.cbVal23.Name = "cbVal23";
            this.cbVal23.Size = new System.Drawing.Size(15, 14);
            this.cbVal23.TabIndex = 42;
            this.cbVal23.UseVisualStyleBackColor = true;
            this.cbVal23.Visible = false;
            // 
            // cbVal22
            // 
            this.cbVal22.AutoSize = true;
            this.cbVal22.Location = new System.Drawing.Point(698, 20);
            this.cbVal22.Name = "cbVal22";
            this.cbVal22.Size = new System.Drawing.Size(15, 14);
            this.cbVal22.TabIndex = 41;
            this.cbVal22.UseVisualStyleBackColor = true;
            this.cbVal22.Visible = false;
            // 
            // cbVal21
            // 
            this.cbVal21.AutoSize = true;
            this.cbVal21.Location = new System.Drawing.Point(668, 20);
            this.cbVal21.Name = "cbVal21";
            this.cbVal21.Size = new System.Drawing.Size(15, 14);
            this.cbVal21.TabIndex = 40;
            this.cbVal21.UseVisualStyleBackColor = true;
            this.cbVal21.Visible = false;
            // 
            // cbVal20
            // 
            this.cbVal20.AutoSize = true;
            this.cbVal20.Location = new System.Drawing.Point(638, 20);
            this.cbVal20.Name = "cbVal20";
            this.cbVal20.Size = new System.Drawing.Size(15, 14);
            this.cbVal20.TabIndex = 39;
            this.cbVal20.UseVisualStyleBackColor = true;
            this.cbVal20.Visible = false;
            // 
            // cbVal19
            // 
            this.cbVal19.AutoSize = true;
            this.cbVal19.Location = new System.Drawing.Point(608, 20);
            this.cbVal19.Name = "cbVal19";
            this.cbVal19.Size = new System.Drawing.Size(15, 14);
            this.cbVal19.TabIndex = 38;
            this.cbVal19.UseVisualStyleBackColor = true;
            this.cbVal19.Visible = false;
            // 
            // cbVal18
            // 
            this.cbVal18.AutoSize = true;
            this.cbVal18.Location = new System.Drawing.Point(578, 20);
            this.cbVal18.Name = "cbVal18";
            this.cbVal18.Size = new System.Drawing.Size(15, 14);
            this.cbVal18.TabIndex = 37;
            this.cbVal18.UseVisualStyleBackColor = true;
            this.cbVal18.Visible = false;
            // 
            // cbVal17
            // 
            this.cbVal17.AutoSize = true;
            this.cbVal17.Location = new System.Drawing.Point(548, 20);
            this.cbVal17.Name = "cbVal17";
            this.cbVal17.Size = new System.Drawing.Size(15, 14);
            this.cbVal17.TabIndex = 36;
            this.cbVal17.UseVisualStyleBackColor = true;
            this.cbVal17.Visible = false;
            // 
            // cbVal16
            // 
            this.cbVal16.AutoSize = true;
            this.cbVal16.Location = new System.Drawing.Point(518, 20);
            this.cbVal16.Name = "cbVal16";
            this.cbVal16.Size = new System.Drawing.Size(15, 14);
            this.cbVal16.TabIndex = 35;
            this.cbVal16.UseVisualStyleBackColor = true;
            this.cbVal16.Visible = false;
            // 
            // cbVal15
            // 
            this.cbVal15.AutoSize = true;
            this.cbVal15.Location = new System.Drawing.Point(488, 20);
            this.cbVal15.Name = "cbVal15";
            this.cbVal15.Size = new System.Drawing.Size(15, 14);
            this.cbVal15.TabIndex = 34;
            this.cbVal15.UseVisualStyleBackColor = true;
            this.cbVal15.Visible = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(488, 20);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 115;
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.Visible = false;
            // 
            // cbVal14
            // 
            this.cbVal14.AutoSize = true;
            this.cbVal14.Location = new System.Drawing.Point(458, 20);
            this.cbVal14.Name = "cbVal14";
            this.cbVal14.Size = new System.Drawing.Size(15, 14);
            this.cbVal14.TabIndex = 33;
            this.cbVal14.UseVisualStyleBackColor = true;
            this.cbVal14.Visible = false;
            // 
            // cbVal13
            // 
            this.cbVal13.AutoSize = true;
            this.cbVal13.Location = new System.Drawing.Point(428, 20);
            this.cbVal13.Name = "cbVal13";
            this.cbVal13.Size = new System.Drawing.Size(15, 14);
            this.cbVal13.TabIndex = 32;
            this.cbVal13.UseVisualStyleBackColor = true;
            this.cbVal13.Visible = false;
            // 
            // cbVal12
            // 
            this.cbVal12.AutoSize = true;
            this.cbVal12.Location = new System.Drawing.Point(398, 20);
            this.cbVal12.Name = "cbVal12";
            this.cbVal12.Size = new System.Drawing.Size(15, 14);
            this.cbVal12.TabIndex = 31;
            this.cbVal12.UseVisualStyleBackColor = true;
            this.cbVal12.Visible = false;
            // 
            // cbVal11
            // 
            this.cbVal11.AutoSize = true;
            this.cbVal11.Location = new System.Drawing.Point(368, 20);
            this.cbVal11.Name = "cbVal11";
            this.cbVal11.Size = new System.Drawing.Size(15, 14);
            this.cbVal11.TabIndex = 30;
            this.cbVal11.UseVisualStyleBackColor = true;
            this.cbVal11.Visible = false;
            // 
            // cbVal10
            // 
            this.cbVal10.AutoSize = true;
            this.cbVal10.Location = new System.Drawing.Point(338, 20);
            this.cbVal10.Name = "cbVal10";
            this.cbVal10.Size = new System.Drawing.Size(15, 14);
            this.cbVal10.TabIndex = 29;
            this.cbVal10.UseVisualStyleBackColor = true;
            this.cbVal10.Visible = false;
            // 
            // cbVal9
            // 
            this.cbVal9.AutoSize = true;
            this.cbVal9.Location = new System.Drawing.Point(308, 20);
            this.cbVal9.Name = "cbVal9";
            this.cbVal9.Size = new System.Drawing.Size(15, 14);
            this.cbVal9.TabIndex = 28;
            this.cbVal9.UseVisualStyleBackColor = true;
            this.cbVal9.Visible = false;
            // 
            // cbVal8
            // 
            this.cbVal8.AutoSize = true;
            this.cbVal8.Location = new System.Drawing.Point(278, 20);
            this.cbVal8.Name = "cbVal8";
            this.cbVal8.Size = new System.Drawing.Size(15, 14);
            this.cbVal8.TabIndex = 27;
            this.cbVal8.UseVisualStyleBackColor = true;
            this.cbVal8.Visible = false;
            // 
            // cbVal7
            // 
            this.cbVal7.AutoSize = true;
            this.cbVal7.Location = new System.Drawing.Point(248, 20);
            this.cbVal7.Name = "cbVal7";
            this.cbVal7.Size = new System.Drawing.Size(15, 14);
            this.cbVal7.TabIndex = 26;
            this.cbVal7.UseVisualStyleBackColor = true;
            this.cbVal7.Visible = false;
            // 
            // cbVal6
            // 
            this.cbVal6.AutoSize = true;
            this.cbVal6.Location = new System.Drawing.Point(218, 20);
            this.cbVal6.Name = "cbVal6";
            this.cbVal6.Size = new System.Drawing.Size(15, 14);
            this.cbVal6.TabIndex = 25;
            this.cbVal6.UseVisualStyleBackColor = true;
            this.cbVal6.Visible = false;
            // 
            // cbVal5
            // 
            this.cbVal5.AutoSize = true;
            this.cbVal5.Location = new System.Drawing.Point(188, 20);
            this.cbVal5.Name = "cbVal5";
            this.cbVal5.Size = new System.Drawing.Size(15, 14);
            this.cbVal5.TabIndex = 24;
            this.cbVal5.UseVisualStyleBackColor = true;
            this.cbVal5.Visible = false;
            // 
            // cbVal4
            // 
            this.cbVal4.AutoSize = true;
            this.cbVal4.Location = new System.Drawing.Point(158, 20);
            this.cbVal4.Name = "cbVal4";
            this.cbVal4.Size = new System.Drawing.Size(15, 14);
            this.cbVal4.TabIndex = 23;
            this.cbVal4.UseVisualStyleBackColor = true;
            this.cbVal4.Visible = false;
            // 
            // cbVal3
            // 
            this.cbVal3.AutoSize = true;
            this.cbVal3.Location = new System.Drawing.Point(128, 20);
            this.cbVal3.Name = "cbVal3";
            this.cbVal3.Size = new System.Drawing.Size(15, 14);
            this.cbVal3.TabIndex = 22;
            this.cbVal3.UseVisualStyleBackColor = true;
            this.cbVal3.Visible = false;
            // 
            // cbVal2
            // 
            this.cbVal2.AutoSize = true;
            this.cbVal2.Location = new System.Drawing.Point(98, 20);
            this.cbVal2.Name = "cbVal2";
            this.cbVal2.Size = new System.Drawing.Size(15, 14);
            this.cbVal2.TabIndex = 21;
            this.cbVal2.UseVisualStyleBackColor = true;
            this.cbVal2.Visible = false;
            // 
            // cbVal1
            // 
            this.cbVal1.AutoSize = true;
            this.cbVal1.Location = new System.Drawing.Point(68, 20);
            this.cbVal1.Name = "cbVal1";
            this.cbVal1.Size = new System.Drawing.Size(15, 14);
            this.cbVal1.TabIndex = 20;
            this.cbVal1.UseVisualStyleBackColor = true;
            this.cbVal1.Visible = false;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(10, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 20);
            this.label8.TabIndex = 99;
            this.label8.Text = "Related Operation:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPartDesc
            // 
            this.txtPartDesc.Enabled = false;
            this.txtPartDesc.Location = new System.Drawing.Point(480, 12);
            this.txtPartDesc.Name = "txtPartDesc";
            this.txtPartDesc.Size = new System.Drawing.Size(592, 20);
            this.txtPartDesc.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(377, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 20);
            this.label7.TabIndex = 97;
            this.label7.Text = "Description:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUnitType
            // 
            this.cbUnitType.Enabled = false;
            this.cbUnitType.FormattingEnabled = true;
            this.cbUnitType.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUnitType.Location = new System.Drawing.Point(740, 65);
            this.cbUnitType.Name = "cbUnitType";
            this.cbUnitType.Size = new System.Drawing.Size(100, 21);
            this.cbUnitType.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(10, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 20);
            this.label4.TabIndex = 93;
            this.label4.Text = "Part Category:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(10, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 20);
            this.label3.TabIndex = 91;
            this.label3.Text = "Part Number:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(660, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 20);
            this.label2.TabIndex = 90;
            this.label2.Text = "Unit Type:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(327, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 89;
            this.label1.Text = "ModelNo Digit:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPartNum
            // 
            this.cbPartNum.Enabled = false;
            this.cbPartNum.FormattingEnabled = true;
            this.cbPartNum.Location = new System.Drawing.Point(170, 12);
            this.cbPartNum.Name = "cbPartNum";
            this.cbPartNum.Size = new System.Drawing.Size(200, 21);
            this.cbPartNum.TabIndex = 1;
            this.cbPartNum.SelectedIndexChanged += new System.EventHandler(this.cbPartNum_SelectedIndexChanged);
            // 
            // lbAddMsg
            // 
            this.lbAddMsg.ForeColor = System.Drawing.Color.Red;
            this.lbAddMsg.Location = new System.Drawing.Point(1091, 102);
            this.lbAddMsg.Name = "lbAddMsg";
            this.lbAddMsg.Size = new System.Drawing.Size(295, 35);
            this.lbAddMsg.TabIndex = 172;
            this.lbAddMsg.Text = "label11";
            this.lbAddMsg.Visible = false;
            // 
            // txtCriticalComp
            // 
            this.txtCriticalComp.Enabled = false;
            this.txtCriticalComp.Location = new System.Drawing.Point(987, 39);
            this.txtCriticalComp.Name = "txtCriticalComp";
            this.txtCriticalComp.Size = new System.Drawing.Size(85, 20);
            this.txtCriticalComp.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label11.Location = new System.Drawing.Point(836, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 20);
            this.label11.TabIndex = 263;
            this.label11.Text = "Critical Component:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPartCategory
            // 
            this.txtPartCategory.Enabled = false;
            this.txtPartCategory.Location = new System.Drawing.Point(170, 39);
            this.txtPartCategory.Name = "txtPartCategory";
            this.txtPartCategory.Size = new System.Drawing.Size(200, 20);
            this.txtPartCategory.TabIndex = 3;
            // 
            // dgvAddRules
            // 
            this.dgvAddRules.AllowUserToAddRows = false;
            this.dgvAddRules.AllowUserToDeleteRows = false;
            this.dgvAddRules.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAddRules.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColRuleHeadID,
            this.ColRuleBatchID,
            this.ColPartNumber,
            this.ColPartDesc,
            this.ColRelatedOp,
            this.ColPartCat,
            this.ColUnitType,
            this.ColDigit,
            this.ColHeatType,
            this.ColValue,
            this.ColQty,
            this.ColUpdateButton,
            this.ColDelButton,
            this.ColDigitSelIdx});
            this.dgvAddRules.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvAddRules.Location = new System.Drawing.Point(13, 212);
            this.dgvAddRules.MultiSelect = false;
            this.dgvAddRules.Name = "dgvAddRules";
            this.dgvAddRules.ReadOnly = true;
            this.dgvAddRules.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAddRules.Size = new System.Drawing.Size(1380, 361);
            this.dgvAddRules.TabIndex = 265;
            this.dgvAddRules.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAddRules_CellClick);
            // 
            // ColRuleHeadID
            // 
            this.ColRuleHeadID.HeaderText = "Rule Head ID";
            this.ColRuleHeadID.Name = "ColRuleHeadID";
            this.ColRuleHeadID.ReadOnly = true;
            this.ColRuleHeadID.Width = 60;
            // 
            // ColRuleBatchID
            // 
            this.ColRuleBatchID.HeaderText = "Rule Batch ID";
            this.ColRuleBatchID.Name = "ColRuleBatchID";
            this.ColRuleBatchID.ReadOnly = true;
            this.ColRuleBatchID.Width = 60;
            // 
            // ColPartNumber
            // 
            this.ColPartNumber.HeaderText = "Part Number";
            this.ColPartNumber.Name = "ColPartNumber";
            this.ColPartNumber.ReadOnly = true;
            this.ColPartNumber.Width = 130;
            // 
            // ColPartDesc
            // 
            this.ColPartDesc.HeaderText = "Part Description";
            this.ColPartDesc.Name = "ColPartDesc";
            this.ColPartDesc.ReadOnly = true;
            this.ColPartDesc.Width = 280;
            // 
            // ColRelatedOp
            // 
            this.ColRelatedOp.HeaderText = "Related Op";
            this.ColRelatedOp.Name = "ColRelatedOp";
            this.ColRelatedOp.ReadOnly = true;
            this.ColRelatedOp.Width = 60;
            // 
            // ColPartCat
            // 
            this.ColPartCat.HeaderText = "Part Category";
            this.ColPartCat.Name = "ColPartCat";
            this.ColPartCat.ReadOnly = true;
            this.ColPartCat.Width = 150;
            // 
            // ColUnitType
            // 
            this.ColUnitType.HeaderText = "Unit Type";
            this.ColUnitType.Name = "ColUnitType";
            this.ColUnitType.ReadOnly = true;
            this.ColUnitType.Width = 50;
            // 
            // ColDigit
            // 
            this.ColDigit.HeaderText = "Digit";
            this.ColDigit.Name = "ColDigit";
            this.ColDigit.ReadOnly = true;
            this.ColDigit.Width = 60;
            // 
            // ColHeatType
            // 
            this.ColHeatType.HeaderText = "Heat Type";
            this.ColHeatType.Name = "ColHeatType";
            this.ColHeatType.ReadOnly = true;
            // 
            // ColValue
            // 
            this.ColValue.HeaderText = "Value";
            this.ColValue.Name = "ColValue";
            this.ColValue.ReadOnly = true;
            this.ColValue.Width = 200;
            // 
            // ColQty
            // 
            this.ColQty.HeaderText = "Quantity";
            this.ColQty.Name = "ColQty";
            this.ColQty.ReadOnly = true;
            // 
            // ColUpdateButton
            // 
            this.ColUpdateButton.HeaderText = "Upd";
            this.ColUpdateButton.Name = "ColUpdateButton";
            this.ColUpdateButton.ReadOnly = true;
            this.ColUpdateButton.Text = "Upd";
            this.ColUpdateButton.UseColumnTextForButtonValue = true;
            this.ColUpdateButton.Width = 40;
            // 
            // ColDelButton
            // 
            this.ColDelButton.HeaderText = "Del";
            this.ColDelButton.Name = "ColDelButton";
            this.ColDelButton.ReadOnly = true;
            this.ColDelButton.Text = "Del";
            this.ColDelButton.UseColumnTextForButtonValue = true;
            this.ColDelButton.Width = 40;
            // 
            // ColDigitSelIdx
            // 
            this.ColDigitSelIdx.HeaderText = "DigitSelIdx";
            this.ColDigitSelIdx.Name = "ColDigitSelIdx";
            this.ColDigitSelIdx.ReadOnly = true;
            this.ColDigitSelIdx.Visible = false;
            // 
            // txtRelatedOp
            // 
            this.txtRelatedOp.Enabled = false;
            this.txtRelatedOp.Location = new System.Drawing.Point(170, 65);
            this.txtRelatedOp.Name = "txtRelatedOp";
            this.txtRelatedOp.Size = new System.Drawing.Size(115, 20);
            this.txtRelatedOp.TabIndex = 7;
            // 
            // btnSaveRules
            // 
            this.btnSaveRules.Enabled = false;
            this.btnSaveRules.Location = new System.Drawing.Point(1255, 12);
            this.btnSaveRules.Name = "btnSaveRules";
            this.btnSaveRules.Size = new System.Drawing.Size(100, 30);
            this.btnSaveRules.TabIndex = 266;
            this.btnSaveRules.Text = "Save Rules";
            this.btnSaveRules.UseVisualStyleBackColor = true;
            this.btnSaveRules.Click += new System.EventHandler(this.btnSaveRules_Click);
            // 
            // gbValues
            // 
            this.gbValues.Controls.Add(this.cbVal18);
            this.gbValues.Controls.Add(this.cbVal1);
            this.gbValues.Controls.Add(this.cbVal2);
            this.gbValues.Controls.Add(this.cbVal3);
            this.gbValues.Controls.Add(this.cbVal4);
            this.gbValues.Controls.Add(this.cbVal5);
            this.gbValues.Controls.Add(this.cbVal6);
            this.gbValues.Controls.Add(this.cbVal7);
            this.gbValues.Controls.Add(this.cbVal8);
            this.gbValues.Controls.Add(this.cbVal9);
            this.gbValues.Controls.Add(this.cbVal10);
            this.gbValues.Controls.Add(this.cbVal11);
            this.gbValues.Controls.Add(this.cbVal12);
            this.gbValues.Controls.Add(this.cbVal13);
            this.gbValues.Controls.Add(this.cbVal14);
            this.gbValues.Controls.Add(this.checkBox9);
            this.gbValues.Controls.Add(this.cbVal15);
            this.gbValues.Controls.Add(this.cbVal16);
            this.gbValues.Controls.Add(this.cbVal17);
            this.gbValues.Controls.Add(this.lbVal30);
            this.gbValues.Controls.Add(this.cbVal19);
            this.gbValues.Controls.Add(this.lbVal29);
            this.gbValues.Controls.Add(this.cbVal20);
            this.gbValues.Controls.Add(this.lbVal28);
            this.gbValues.Controls.Add(this.cbVal21);
            this.gbValues.Controls.Add(this.lbVal27);
            this.gbValues.Controls.Add(this.cbVal22);
            this.gbValues.Controls.Add(this.lbVal26);
            this.gbValues.Controls.Add(this.cbVal23);
            this.gbValues.Controls.Add(this.lbVal25);
            this.gbValues.Controls.Add(this.cbVal24);
            this.gbValues.Controls.Add(this.lbVal24);
            this.gbValues.Controls.Add(this.cbVal25);
            this.gbValues.Controls.Add(this.lbVal23);
            this.gbValues.Controls.Add(this.cbVal26);
            this.gbValues.Controls.Add(this.lbVal22);
            this.gbValues.Controls.Add(this.cbVal27);
            this.gbValues.Controls.Add(this.lbVal21);
            this.gbValues.Controls.Add(this.cbVal28);
            this.gbValues.Controls.Add(this.lbVal20);
            this.gbValues.Controls.Add(this.cbVal29);
            this.gbValues.Controls.Add(this.lbVal19);
            this.gbValues.Controls.Add(this.cbVal30);
            this.gbValues.Controls.Add(this.lbVal18);
            this.gbValues.Controls.Add(this.lbVal1);
            this.gbValues.Controls.Add(this.lbVal17);
            this.gbValues.Controls.Add(this.lbVal2);
            this.gbValues.Controls.Add(this.lbVal16);
            this.gbValues.Controls.Add(this.lbVal3);
            this.gbValues.Controls.Add(this.lbVal15);
            this.gbValues.Controls.Add(this.lbVal4);
            this.gbValues.Controls.Add(this.lbVal14);
            this.gbValues.Controls.Add(this.lbVal5);
            this.gbValues.Controls.Add(this.lbVal13);
            this.gbValues.Controls.Add(this.lbVal6);
            this.gbValues.Controls.Add(this.lbVal12);
            this.gbValues.Controls.Add(this.lbVal7);
            this.gbValues.Controls.Add(this.lbVal11);
            this.gbValues.Controls.Add(this.lbVal8);
            this.gbValues.Controls.Add(this.lbVal10);
            this.gbValues.Controls.Add(this.lbVal9);
            this.gbValues.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbValues.ForeColor = System.Drawing.Color.RoyalBlue;
            this.gbValues.Location = new System.Drawing.Point(107, 89);
            this.gbValues.Name = "gbValues";
            this.gbValues.Size = new System.Drawing.Size(965, 117);
            this.gbValues.TabIndex = 267;
            this.gbValues.TabStop = false;
            this.gbValues.Text = "Values:";
            this.gbValues.Visible = false;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ShowAlways = true;
            // 
            // lbScreenMode
            // 
            this.lbScreenMode.ForeColor = System.Drawing.Color.Red;
            this.lbScreenMode.Location = new System.Drawing.Point(1091, 137);
            this.lbScreenMode.Name = "lbScreenMode";
            this.lbScreenMode.Size = new System.Drawing.Size(101, 18);
            this.lbScreenMode.TabIndex = 268;
            this.lbScreenMode.Text = "label11";
            this.lbScreenMode.Visible = false;
            // 
            // btnUpdateRule
            // 
            this.btnUpdateRule.Location = new System.Drawing.Point(1120, 53);
            this.btnUpdateRule.Name = "btnUpdateRule";
            this.btnUpdateRule.Size = new System.Drawing.Size(100, 30);
            this.btnUpdateRule.TabIndex = 269;
            this.btnUpdateRule.Text = "Update Rule";
            this.btnUpdateRule.UseVisualStyleBackColor = true;
            this.btnUpdateRule.Click += new System.EventHandler(this.btnUpdateRule_Click);
            // 
            // frmAddRule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1409, 587);
            this.Controls.Add(this.btnUpdateRule);
            this.Controls.Add(this.lbScreenMode);
            this.Controls.Add(this.gbValues);
            this.Controls.Add(this.btnSaveRules);
            this.Controls.Add(this.txtRelatedOp);
            this.Controls.Add(this.dgvAddRules);
            this.Controls.Add(this.txtPartCategory);
            this.Controls.Add(this.txtCriticalComp);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lbAddMsg);
            this.Controls.Add(this.cbPartNum);
            this.Controls.Add(this.txtRuleID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnAddRule);
            this.Controls.Add(this.btnUpdCancel);
            this.Controls.Add(this.txtBatchID);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cbDigit);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.nudReqQty);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPartDesc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbUnitType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmAddRule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Part/Rule";
            ((System.ComponentModel.ISupportInitialize)(this.nudReqQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddRules)).EndInit();
            this.gbValues.ResumeLayout(false);
            this.gbValues.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtRuleID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddRule;
        private System.Windows.Forms.Button btnUpdCancel;
        public System.Windows.Forms.TextBox txtBatchID;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox cbDigit;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.NumericUpDown nudReqQty;
        public System.Windows.Forms.Label lbVal30;
        public System.Windows.Forms.Label lbVal29;
        public System.Windows.Forms.Label lbVal28;
        public System.Windows.Forms.Label lbVal27;
        public System.Windows.Forms.Label lbVal26;
        public System.Windows.Forms.Label lbVal25;
        public System.Windows.Forms.Label lbVal24;
        public System.Windows.Forms.Label lbVal23;
        public System.Windows.Forms.Label lbVal22;
        public System.Windows.Forms.Label lbVal21;
        public System.Windows.Forms.Label lbVal20;
        public System.Windows.Forms.Label lbVal19;
        public System.Windows.Forms.Label lbVal18;
        public System.Windows.Forms.Label lbVal17;
        public System.Windows.Forms.Label lbVal16;
        public System.Windows.Forms.Label lbVal15;
        public System.Windows.Forms.Label lbVal14;
        public System.Windows.Forms.Label lbVal13;
        public System.Windows.Forms.Label lbVal12;
        public System.Windows.Forms.Label lbVal11;
        public System.Windows.Forms.Label lbVal10;
        public System.Windows.Forms.Label lbVal9;
        public System.Windows.Forms.Label lbVal8;
        public System.Windows.Forms.Label lbVal7;
        public System.Windows.Forms.Label lbVal6;
        public System.Windows.Forms.Label lbVal5;
        public System.Windows.Forms.Label lbVal4;
        public System.Windows.Forms.Label lbVal3;
        public System.Windows.Forms.Label lbVal2;
        public System.Windows.Forms.Label lbVal1;
        public System.Windows.Forms.CheckBox cbVal30;
        public System.Windows.Forms.CheckBox cbVal29;
        public System.Windows.Forms.CheckBox cbVal28;
        public System.Windows.Forms.CheckBox cbVal27;
        public System.Windows.Forms.CheckBox cbVal26;
        public System.Windows.Forms.CheckBox cbVal25;
        public System.Windows.Forms.CheckBox cbVal24;
        public System.Windows.Forms.CheckBox cbVal23;
        public System.Windows.Forms.CheckBox cbVal22;
        public System.Windows.Forms.CheckBox cbVal21;
        public System.Windows.Forms.CheckBox cbVal20;
        public System.Windows.Forms.CheckBox cbVal19;
        public System.Windows.Forms.CheckBox cbVal18;
        public System.Windows.Forms.CheckBox cbVal17;
        public System.Windows.Forms.CheckBox cbVal16;
        public System.Windows.Forms.CheckBox cbVal15;
        public System.Windows.Forms.CheckBox checkBox9;
        public System.Windows.Forms.CheckBox cbVal14;
        public System.Windows.Forms.CheckBox cbVal13;
        public System.Windows.Forms.CheckBox cbVal12;
        public System.Windows.Forms.CheckBox cbVal11;
        public System.Windows.Forms.CheckBox cbVal10;
        public System.Windows.Forms.CheckBox cbVal9;
        public System.Windows.Forms.CheckBox cbVal8;
        public System.Windows.Forms.CheckBox cbVal7;
        public System.Windows.Forms.CheckBox cbVal6;
        public System.Windows.Forms.CheckBox cbVal5;
        public System.Windows.Forms.CheckBox cbVal4;
        public System.Windows.Forms.CheckBox cbVal3;
        public System.Windows.Forms.CheckBox cbVal2;
        public System.Windows.Forms.CheckBox cbVal1;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtPartDesc;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cbUnitType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbPartNum;
        public System.Windows.Forms.TextBox txtCriticalComp;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtPartCategory;
        public System.Windows.Forms.TextBox txtRelatedOp;
        public System.Windows.Forms.Label lbAddMsg;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.GroupBox gbValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRuleHeadID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRuleBatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPartNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPartDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRelatedOp;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPartCat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUnitType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDigit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColHeatType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColQty;
        private System.Windows.Forms.DataGridViewButtonColumn ColUpdateButton;
        private System.Windows.Forms.DataGridViewButtonColumn ColDelButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDigitSelIdx;
        public System.Windows.Forms.DataGridView dgvAddRules;
        public System.Windows.Forms.Button btnSaveRules;
        public System.Windows.Forms.Label lbScreenMode;
        public System.Windows.Forms.Button btnUpdateRule;
    }
}