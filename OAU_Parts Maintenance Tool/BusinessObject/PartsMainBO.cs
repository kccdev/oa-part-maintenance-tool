﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    class PartsMainBO : PartsMainDTO
    {
        #region SQL Methods
        public DataTable GetOAU_PartsAndRulesList()
        {
            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartsAndRulesList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetVMEASM_PartsWithRules()
        {
            DataTable dt = new DataTable();

            try
            {                
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetVMEASM_PartsWithRules", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }



        public DataTable GetOAU_PartRulesByPartNum()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRulesByPartNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartRulesDataByPartNum()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRulesDataByPartNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


        public DataTable GetOAU_PartRulesByPartNumber()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRulesByPartNumber", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNumber", PartNum);
                        //cmd.Parameters.AddWithValue("@UnitType", UnitType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartDetail()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartDetail", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


        public DataTable GetRuleBatchesForPartNum_UnitType()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetRuleBatchesForPartNum_UnitType", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        cmd.Parameters.AddWithValue("@RevType", RevType);
                        cmd.Parameters.AddWithValue("@UnitType", UnitType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }       

        public DataTable GetOAU_PartRulesByRuleHeadID_AndRuleBatchID()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRulesByRuleHeadID_AndRuleBatchID", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartCategoryList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartCategoryList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                                     
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetModelNoDigitList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetModelNoDigitList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }        

        public DataTable GetOAU_MainPartList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetMainPartList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Category", PartCategory);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void  GetOAU_PartNumberInfo()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartNumberInfo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                                if (dt.Rows.Count > 0)
                                {
                                    DataRow dr = dt.Rows[0];
                                    RuleHeadID = dr["RuleHeadID"].ToString();
                                    RuleBatchID = dr["RuleBatchID"].ToString();
                                    CriticalComp = dr["CriticalComp"].ToString();
                                    PartDescription = dr["PartDescription"].ToString();
                                    RelatedOperation = dr["RelatedOperation"].ToString();
                                    PartCategory = dr["PartCategory"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return;
        }        

        public DataTable GetOAU_PartNumbersByCategory(string categoryList)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartNumbersByCategory", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CategoryList", categoryList);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetCompressorPartNumbers()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetCompressorPartNumbers", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }      

        public void UpdatePartConditionsDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;


                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdatePartConditionsDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        command.Parameters.AddWithValue("@Digit", Int32.Parse(Digit));
                        command.Parameters.AddWithValue("@HeatType", HeatType);
                        command.Parameters.AddWithValue("@UnitType", UnitType);
                        command.Parameters.AddWithValue("@Value", Value);
                        command.Parameters.AddWithValue("@Qty", decimal.Parse(Qty));

                        if (RelatedOperation.Length == 0)
                        {
                            command.Parameters.AddWithValue("@RelatedOp", 0);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@RelatedOp", Int32.Parse(RelatedOperation));
                        }
                        
                        command.Parameters.AddWithValue("@RfgComp", Boolean.Parse(RfgComponent));
                        command.Parameters.AddWithValue("@Circuit1", Boolean.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit2", Boolean.Parse(Circuit2));                                               
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@SubAssemblyPart", SubAssemblyPart);
                        command.Parameters.AddWithValue("@SubAsmMtlPart", SubAsmMtlPart);
                        command.Parameters.AddWithValue("@ImmediateParent", ImmediateParent);
                        command.Parameters.AddWithValue("@Comments", Comments);
                        command.Parameters.AddWithValue("@UserName", UserName);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string InsertPartConditionsDTL()
        {            
            string ruleBatchIdOut = "0";

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_InsertPartConditionsDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        command.Parameters.AddWithValue("@Digit", Int32.Parse(Digit));
                        command.Parameters.AddWithValue("@HeatType", HeatType);
                        command.Parameters.AddWithValue("@Unit", UnitType);
                        command.Parameters.AddWithValue("@Value", Value);
                        command.Parameters.AddWithValue("@Qty", decimal.Parse(Qty));

                        if (RelatedOperation.Length == 0)
                        {
                            command.Parameters.AddWithValue("@RelatedOp", 0);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@RelatedOp", Int32.Parse(RelatedOperation));
                        }
                                       
                        command.Parameters.AddWithValue("@RfgComp", Boolean.Parse(RfgComponent));
                        command.Parameters.AddWithValue("@Circuit1", Boolean.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit2", Boolean.Parse(Circuit2));
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@SubAssemblyPart", SubAssemblyPart);
                        command.Parameters.AddWithValue("@SubAsmMtlPart", SubAsmMtlPart);
                        command.Parameters.AddWithValue("@ImmediateParent", ImmediateParent);
                        command.Parameters.AddWithValue("@Comments", Comments);
                        command.Parameters.AddWithValue("@UserName", UserName);
                        command.Parameters.Add("@RuleBatchIdOut", SqlDbType.BigInt);
                        command.Parameters["@RuleBatchIdOut"].Direction = ParameterDirection.Output;
                        connection.Open();
                        command.ExecuteNonQuery();
                        ruleBatchIdOut = command.Parameters["@RuleBatchIdOut"].Value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ruleBatchIdOut;
        }

        public void Insert_PartMaster(DataTable dt, string unitType)
        {
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                    //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                    if (unitType == "OA")
                    {
                        using (var connection = new SqlConnection(connectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_InsertPartMaster", connection))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@PartMasterTbl", dt);
                                connection.Open();
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }
                        }
                    }
                    else
                    {
                        using (var connection = new SqlConnection(connectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand("KCC.VKG_InsertPartMaster", connection))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@PartMasterTbl", dt);
                                connection.Open();
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }
                        }
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error inserting data into PartMaster table - " + f);
                }
            }
        }

        public void DeleteFromOAU_PartMaster()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeleteFromPartMaster", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPartRulesHead()
        {
            
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_InsertPartRulesHead", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@PartCategory", PartCategory);
                        cmd.Parameters.AddWithValue("@RelatedOp", Int32.Parse(RelatedOperation));
                        cmd.Parameters.AddWithValue("@AssemblySeq", Int32.Parse(AssemblySeq));
                        cmd.Parameters.AddWithValue("@LinesideBin", LinesideBin);
                        cmd.Parameters.AddWithValue("@PartType", PartType);
                        cmd.Parameters.AddWithValue("@StationLoc", StationLoc);
                        cmd.Parameters.AddWithValue("@PickList", PickList);
                        cmd.Parameters.AddWithValue("@ConfigPart", ConfigurablePart);
                        cmd.Parameters.AddWithValue("@ModBy", UserName);
                        connection.Open();
                        cmd.ExecuteNonQuery();                       
                    }
                }
            }
                catch (Exception f)
                {
                    MessageBox.Show("Error inserting data into R6_OA_PartRulesHead table - " + f);
                }
            
        }

        public DataTable GetPartRulesHead()
        {
            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRulesHead", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }       

        public void DeleteROA_PartRulesHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeletePartRulesHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(RuleHeadID));                       
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePartRulesHead()
        {
            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdatePartRulesHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@PartCategory", PartCategory);
                        command.Parameters.AddWithValue("@RelatedOp", Int32.Parse(RelatedOperation));
                        command.Parameters.AddWithValue("@AssemblySeq", Int32.Parse(AssemblySeq));
                        command.Parameters.AddWithValue("@PartType", PartType);
                        command.Parameters.AddWithValue("@StationLoc", StationLoc);
                        command.Parameters.AddWithValue("@LinesideBin", LinesideBin);
                        command.Parameters.AddWithValue("@PickList", PickList);
                        command.Parameters.AddWithValue("@ConfigPart", ConfigurablePart);
                        command.Parameters.AddWithValue("@ModBy", UserName);
                        command.Parameters.AddWithValue("@ID", Int32.Parse(RuleHeadID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteFromPartConditionsDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeleteFromPartConditionsDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        command.Parameters.AddWithValue("@Digit", Int32.Parse(Digit));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetOA_PartNumbersBasedOnClassID()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartNumbersBasedOnClassID", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region OA_VKG Sql
        public DataTable VikingGetModelNoDigitList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.VKG_GetModelNoDigitList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable VikingGetModelNumDesc()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.VKG_GetModelNumDesc", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                       
                        cmd.Parameters.AddWithValue("@DigitNum", Int32.Parse(Digit));
                        cmd.Parameters.AddWithValue("@HeatType", HeatType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSubAsmPartNums()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_GetSubAsmPartNums", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetInnerAssemblyPartNums()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_GetInnerAssemblyPartNums", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AsmPartNumType", AssemblyPartType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetAssemblyPartNumDetail()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_VKG_GetAssemblyPartNumDetail", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", AssemblyPartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSubAsmParentData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_GetSubAsmParentData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@BatchID", Int32.Parse(RuleBatchID));
                        cmd.Parameters.AddWithValue("@RevType", RevType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertSubAsmParentData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_InsertSubAsmParentData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@BatchID", Int32.Parse(RuleBatchID));
                        cmd.Parameters.AddWithValue("@BOM_Level", BOM_Level);
                        cmd.Parameters.AddWithValue("@PrimaryAssemblyPartNum", PrimaryAssemblyPartNum);
                        cmd.Parameters.AddWithValue("@InnerAssemblyPartNum", InnerAssemblyPartNum);
                        cmd.Parameters.AddWithValue("@ImmediateParent", ImmediateParent);
                        cmd.Parameters.AddWithValue("@UnitType", UnitType);
                        cmd.Parameters.AddWithValue("@RevType", RevType);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error inserting data into R6_OA_VKG_SubAsmParentData table - " + f);
            }
        }

        public void UpdateSubAsmParentData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_UpdateSubAsmParentData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@BatchID", Int32.Parse(RuleBatchID));
                        cmd.Parameters.AddWithValue("@BOM_Level", BOM_Level);
                        cmd.Parameters.AddWithValue("@PrimaryAssemblyPartNum", PrimaryAssemblyPartNum);
                        cmd.Parameters.AddWithValue("@InnerAssemblyPartNum", InnerAssemblyPartNum);
                        cmd.Parameters.AddWithValue("@ImmediateParent", ImmediateParent);
                        cmd.Parameters.AddWithValue("@UnitType", UnitType);
                        cmd.Parameters.AddWithValue("@RevType", RevType);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error updating data into R6_VKG_SubAssemblyParentData table - " + f);
            }
        }

        public void DeleteSubAsmParentData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["FubarDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_VKG_DeleteSubAsmParentData", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@BatchID", Int32.Parse(RuleBatchID));
                        command.Parameters.AddWithValue("@UnitType", UnitType);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteFromVKG_PartMaster()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.VKG_DeleteFromPartMaster", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Other Methods
        public int getDigitSelectedIndex(string digitStr, string heatTypeStr, ComboBox cbDigit)
        {
            int selIdx = 0;
            string digitHeatStr = "";

            digitHeatStr = digitStr + '/' + heatTypeStr;

            for (int x = 0; x < cbDigit.Items.Count; ++x)
            {
                string value = cbDigit.GetItemText(cbDigit.Items[x]);
                if (value.Substring(0, value.IndexOf(' ')) == digitHeatStr)
                {
                    selIdx = x;
                    break;
                }
            }

            return selIdx;
        }

        public void UpdatePartMaster(DataTable dt, string curUnitType)
        {           
            int curRuleBatchID = Int32.Parse(RuleBatchID);
            int curRuleHeadID = Int32.Parse(RuleHeadID);
            int curRelatedOperation = 0;
            int newRowsNeededCount = 1;
            int numValues = 0;                    
            int batchRuleCount = 0;           
            int digitInt;                                            
            int valueLen;           
            int tonageNumValues = 0;           
            int rdi = 0;
            int sri = 0;            

            bool firstRow = true;
            bool unitTypeDigitFound = false;
            
            string curPartNum = "";
            string curPartCategory = "";
            string modelNo = "";
            string createBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string curPartType = "";
            string qtyStr = "";
            string valueStr = "";
            string tonageValueStr = "";
            string unitType = "";  
            string heatType = "";         
            string digitVal = "";
            string tmp1 = "";
            string tmp2 = "";
            string tmp3 = "";
            string[,] ruleData = new string[10, 3];
            string[] tonageData = new string[10];
            string[] unitTypeData = new string[5];
            string[,] singleValueRuleData = new string[10, 2];

            decimal qtyDec = 0;

            ModelNumDescBO objModel = new ModelNumDescBO();

            DateTime curDate = DateTime.Now;

            DataTable dtNew = new DataTable();           
            dtNew.Columns.Add("CurrentRuleHeadID");
            dtNew.Columns.Add("CurrentRuleBatchID");
            dtNew.Columns.Add("PartNum");
            dtNew.Columns.Add("ModelNo");
            dtNew.Columns.Add("RelatedOperation");
            dtNew.Columns.Add("PartCategory");
            dtNew.Columns.Add("PartType");
            dtNew.Columns.Add("Qty");
            dtNew.Columns.Add("CreateBy", typeof(string)).MaxLength = 50;
            dtNew.Columns.Add("CreateDate", typeof(DateTime));
            dtNew.Columns.Add("ModBy", typeof(string)).MaxLength = 50;
            dtNew.Columns.Add("ModDate", typeof(DateTime));       
           
            foreach (DataRow row in dt.Rows)
            {
                curPartNum = row["PartNum"].ToString();;
                curRelatedOperation = Int32.Parse(row["RelatedOperation"].ToString());
                curPartCategory = row["PartCategory"].ToString();               
                unitType = "***";
                heatType = row["HeatType"].ToString();
                digitVal = row["Digit"].ToString();
                digitInt = Int32.Parse(digitVal);               
                valueStr = row["Value"].ToString();
                qtyStr = row["Qty"].ToString();

                //if (unitType.Contains("OA") == true && unitType.Contains("VKG") == true)
                //{

                //}

                if ((curPartNum == "INS2.00X2.10LBS-PIC") && (curRuleBatchID == 10))
                    curPartNum = "INS2.00X2.10LBS-PIC";

                if (qtyStr == "" || qtyStr == null)
                {
                    qtyDec = 0;
                }
                else
                {
                    qtyDec = decimal.Parse(qtyStr);
                }
               
                numValues = 0;                
              
                string validValueStr = "";
                string[] values = valueStr.Split(',');                

                DataTable dtVal = objModel.GetModelNoValues(digitInt, heatType, curUnitType);
               
                bool firstDigVal = true;
                foreach(string digVal in values)
                {                       
                    foreach(DataRow drVal in dtVal.Rows)
                    {
                        if (digVal == drVal["DigitVal"].ToString())
                        {
                            if (firstDigVal == true)
                            {
                                validValueStr = digVal;
                                firstDigVal = false;
                            }
                            else
                            {
                                validValueStr += "," + digVal;
                            }                                
                            break;
                        }
                    }                        
                }
                values = validValueStr.Split(',');
                numValues = values.Length;
                valueStr = validValueStr;

                if (curPartCategory == "Motor" || curPartCategory.Contains("Mtr"))
                {
                    if (digitInt == 14 || digitInt == 44)  // Outdoor Coil Type (Condenser)
                    {
                        curPartType = "Condensor";
                    }
                    else if (digitInt == 21) // Supply Fan Motor (Indoor Fan)
                    {
                        curPartType = "SupplyFan";
                    }
                    else if (digitInt == 25) // Exhaust Fan Motor (Powered Exhaust)
                    {
                        curPartType = "PoweredExhaust";
                    }
                    else if (digitInt == 36 || digitInt == 34) // Energy Recovery Wheel Size (ERV Size) 
                    {
                        curPartType = "ERV";
                    }
                    //else
                    //{
                    //    curPartType = "";
                    //}
                }
                else if (curPartCategory == "Compressor")
                {
                    curPartType = "Compressor";
                }
                else if (curPartCategory == "DigitalScroll")
                {
                    curPartType = "DigitalScroll";
                }
                else if (curPartCategory == "DigitalScrollTandem")
                {
                    curPartType = "DigitalScrollTandem";
                }

                if (digitInt == 3)
                {
                    unitTypeDigitFound = true;
                }

                if (digitInt == 567)
                {
                    tonageData = valueStr.Split(',');
                    tonageNumValues = tonageData.Length;
                }
                //else if (digitInt == 3)
                //{
                //    unitTypeDigitFound = true;
                //    unitTypeData = valueStr.Split(',');
                //    unitTypeNumValues = unitTypeData.Length;                   
                //}
                else if (numValues == 1)
                {
                    singleValueRuleData[sri, 0] = valueStr;
                    singleValueRuleData[sri++, 1] = digitVal;
                }
                else
                {
                    ruleData[rdi, 0] = valueStr;
                    ruleData[rdi, 1] = digitVal;
                    ruleData[rdi++, 2] = numValues.ToString();
                    ++batchRuleCount;
                }

                if (firstRow == true)
                {
                    newRowsNeededCount = numValues;
                    firstRow = false;
                }
                else
                {
                    newRowsNeededCount *= numValues;
                }
                          
            }

            for (int i = 0; i < rdi; i++)
            {
                for (int j = 0; j < rdi - 1; j++)
                {
                    if (Int32.Parse(ruleData[j, 2]) < Int32.Parse(ruleData[j + 1, 2]))
                    {
                        tmp1 = ruleData[j, 0];
                        tmp2 = ruleData[j, 1];
                        tmp3 = ruleData[j, 2];
                        ruleData[j, 0] = ruleData[j + 1, 0];
                        ruleData[j, 1] = ruleData[j + 1, 1];
                        ruleData[j, 2] = ruleData[j + 1, 2];
                        ruleData[j + 1, 0] = tmp1;
                        ruleData[j + 1, 1] = tmp2;
                        ruleData[j + 1, 2] = tmp3;
                    }
                }
            }

            int tonageIdx = 0;
            int tonageSwitchVal = 0;
            if (tonageNumValues > 1)
            {
                tonageSwitchVal = newRowsNeededCount / tonageNumValues;
                tonageValueStr = tonageData[tonageIdx++];
            }
            else
            {
                tonageSwitchVal = newRowsNeededCount;
                tonageIdx = 1;
                if (tonageNumValues == 1)
                {
                    tonageValueStr = tonageData[0];
                }
                else
                {
                    tonageValueStr = "***";
                }
            }           

            List<string> modelNoLst = new List<string>();

            if (unitTypeDigitFound == true && curUnitType == "OA")
            {
                unitType = "OA*";
            }
            else if (unitTypeDigitFound == true && curUnitType == "VKG")
            {
                unitType = "HA*";
            }
            else
            {
                unitType = "***";
            }

            for (int x = 0; x < newRowsNeededCount; ++x)
            {               
                if ((x / tonageIdx) == tonageSwitchVal)
                {
                    tonageValueStr = tonageData[tonageIdx++];
                }
                modelNo = unitType + "*" + tonageValueStr + "******************************************************************";
                modelNoLst.Add(modelNo);
            }

            for (int a = 0; a < sri; ++a)
            {
                valueStr = singleValueRuleData[a, 0];
                valueLen = valueStr.Length;                

                digitInt = getDigitValue(Int32.Parse(singleValueRuleData[a, 1].ToString()));

                for (int b = 0; b < newRowsNeededCount; ++b)
                {
                    modelNo = modelNoLst[b];
                    
                    if (digitInt == 70)
                    {
                        modelNo = modelNo.Remove((digitInt - 1), 1).Insert((digitInt - 1), "0");
                        valueLen = valueStr.Length;
                        modelNo = modelNo.Remove(digitInt, valueLen).Insert(digitInt, valueStr);
                    }
                    else
                    {
                        modelNo = modelNo.Remove((digitInt - 1), valueLen).Insert((digitInt - 1), valueStr);
                    }
                    
                    modelNoLst[b] = modelNo;
                }
            }

            string[] r1Vals = new string[25];
            string[] r2Vals = new string[25];
            string[] r3Vals = new string[25];
            string[] r4Vals = new string[25];
            string[] r5Vals = new string[25];
            string[] r6Vals = new string[25];
            string[] r7Vals = new string[25];

            int r1Digit = 0;
            int r2Digit = 0;
            int r3Digit = 0;
            int r4Digit = 0;
            int r5Digit = 0;
            int r6Digit = 0;
            int r7Digit = 0;

            int r1Len = 0;
            int r2Len = 0;
            int r3Len = 0;
            int r4Len = 0;
            int r5Len = 0;
            int r6Len = 0;
            int r7Len = 0;

            if (batchRuleCount > 0)
            {
                r1Vals = ruleData[0, 0].Split(',');
                r1Digit = getDigitValue(Int32.Parse(ruleData[0, 1].ToString()));
                r1Len = r1Vals[0].Length;
            }
            if (batchRuleCount > 1)
            {
                r2Vals = ruleData[1, 0].Split(',');
                r2Digit = getDigitValue(Int32.Parse(ruleData[1, 1].ToString()));
                r2Len = r2Vals[0].Length;
            }
            if (batchRuleCount > 2)
            {
                r3Vals = ruleData[2, 0].Split(',');
                r3Digit = getDigitValue(Int32.Parse(ruleData[2, 1].ToString()));
                r3Len = r3Vals[0].Length;
            }
            if (batchRuleCount > 3)
            {
                r4Vals = ruleData[3, 0].Split(',');
                r4Digit = getDigitValue(Int32.Parse(ruleData[3, 1].ToString()));
                r4Len = r4Vals[0].Length;
            }
            if (batchRuleCount > 4)
            {
                r5Vals = ruleData[4, 0].Split(',');
                r5Digit = getDigitValue(Int32.Parse(ruleData[4, 1].ToString()));
                r5Len = r5Vals[0].Length;
            }
            if (batchRuleCount > 5)
            {
                r6Vals = ruleData[5, 0].Split(',');
                r6Digit = getDigitValue(Int32.Parse(ruleData[5, 1].ToString()));
                r6Len = r6Vals[0].Length;
            }
            if (batchRuleCount > 6)
            {
                r7Vals = ruleData[6, 0].Split(',');
                r7Digit = getDigitValue(Int32.Parse(ruleData[6, 1].ToString()));
                r7Len = r7Vals[0].Length;
            }

            List<string> modelNoLst2 = new List<string>();

            int loopIdx = 0;
            if (tonageNumValues > 0)
            {
                loopIdx = tonageNumValues;
            }
            else
            {
                loopIdx = 1;
                tonageSwitchVal = 1;
            }

            string modelNo2 = "";
            if (batchRuleCount > 0)
            {
                int y = 0;
                for (int x = 0; x < loopIdx; ++x)
                {
                    y = 0;
                    while (y < tonageSwitchVal)
                    {
                        tonageIdx = (x * tonageSwitchVal) + y;
                        modelNo = modelNoLst[tonageIdx];
                        foreach (string r1 in r1Vals)
                        {
                            if (batchRuleCount > 1)
                            {
                                foreach (string r2 in r2Vals)
                                {
                                    if (batchRuleCount > 2)
                                    {
                                        foreach (string r3 in r3Vals)
                                        {
                                            if (batchRuleCount > 3)
                                            {
                                                foreach (string r4 in r4Vals)
                                                {
                                                    if (batchRuleCount > 4)
                                                    {
                                                        foreach (string r5 in r5Vals)
                                                        {
                                                            if (batchRuleCount > 5)
                                                            {
                                                                foreach (string r6 in r6Vals)
                                                                {
                                                                    if (batchRuleCount > 6)
                                                                    {
                                                                        foreach (string r7 in r7Vals)
                                                                        {
                                                                            modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                                                            modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                                                            modelNo2 = modelNo2.Remove((r3Digit - 1), r3Len).Insert((r3Digit - 1), r3);
                                                                            modelNo2 = modelNo2.Remove((r4Digit - 1), r4Len).Insert((r4Digit - 1), r4);
                                                                            modelNo2 = modelNo2.Remove((r5Digit - 1), r5Len).Insert((r5Digit - 1), r5);
                                                                            modelNo2 = modelNo2.Remove((r6Digit - 1), r6Len).Insert((r6Digit - 1), r6);
                                                                            modelNo2 = modelNo2.Remove((r7Digit - 1), r7Len).Insert((r7Digit - 1), r7);
                                                                            modelNoLst2.Add(modelNo2);
                                                                            ++y;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                                                        modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                                                        modelNo2 = modelNo2.Remove((r3Digit - 1), r3Len).Insert((r3Digit - 1), r3);
                                                                        modelNo2 = modelNo2.Remove((r4Digit - 1), r4Len).Insert((r4Digit - 1), r4);
                                                                        modelNo2 = modelNo2.Remove((r5Digit - 1), r5Len).Insert((r5Digit - 1), r5);
                                                                        modelNo2 = modelNo2.Remove((r6Digit - 1), r6Len).Insert((r6Digit - 1), r6);
                                                                        modelNoLst2.Add(modelNo2);
                                                                        ++y;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                                                modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                                                modelNo2 = modelNo2.Remove((r3Digit - 1), r3Len).Insert((r3Digit - 1), r3);
                                                                modelNo2 = modelNo2.Remove((r4Digit - 1), r4Len).Insert((r4Digit - 1), r4);
                                                                modelNo2 = modelNo2.Remove((r5Digit - 1), r5Len).Insert((r5Digit - 1), r5);
                                                                modelNoLst2.Add(modelNo2);
                                                                ++y;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                                        modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                                        modelNo2 = modelNo2.Remove((r3Digit - 1), r3Len).Insert((r3Digit - 1), r3);
                                                        modelNo2 = modelNo2.Remove((r4Digit - 1), r4Len).Insert((r4Digit - 1), r4);
                                                        modelNoLst2.Add(modelNo2);
                                                        ++y;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                                modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                                modelNo2 = modelNo2.Remove((r3Digit - 1), r3Len).Insert((r3Digit - 1), r3);
                                                modelNoLst2.Add(modelNo2);
                                                ++y;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                        modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                        modelNoLst2.Add(modelNo2);
                                        ++y;
                                    }

                                }
                            }
                            else
                            {
                                modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                modelNoLst2.Add(modelNo2);
                                ++y;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int c = 0; c < newRowsNeededCount; ++c)
                {
                    modelNoLst2.Add(modelNoLst[c]);
                }
            }


            //int unitTypeIdx = 0;
            //int unitTypeSwitchVal = 0;
            //if (unitTypeNumValues > 1)
            //{
            //    unitTypeSwitchVal = newRowsNeededCount / unitTypeNumValues;
            //    unitType = "OA" + unitTypeData[unitTypeIdx++];
            //}
            //else
            //{
            //    unitTypeSwitchVal = newRowsNeededCount;
            //    unitTypeIdx = 1;
            //    if (unitTypeNumValues == 1)
            //    {
            //        unitType = "OA" + unitTypeData[0];
            //    }
            //    else
            //    {
            //        unitType = "***";
            //    }
            //}

            //if (unitTypeNumValues > 0)
            //{
            //    for (int x = 0; x < newRowsNeededCount; ++x)
            //    {
            //        if (unitTypeNumValues > 1)
            //        {
            //            if ((x / unitTypeIdx) == unitTypeSwitchVal)
            //            {
            //                unitType = "OA" + unitTypeData[unitTypeIdx++];
            //            }
            //        }

            //        modelNoLst2[x] = modelNoLst2[x].Remove(0, 3).Insert(0, unitType);                   
                    
            //    }
            //}

            for (int x = 0; x < newRowsNeededCount; ++x)
            {
                modelNo = modelNoLst2[x];
                if (firstRow == false)
                {
                    var dr = dtNew.NewRow();                  
                    dr["CurrentRuleHeadID"] = curRuleHeadID;
                    dr["CurrentRuleBatchID"] = curRuleBatchID;
                    dr["PartNum"] = curPartNum;
                    dr["ModelNo"] = modelNo;
                    dr["RelatedOperation"] = curRelatedOperation;
                    dr["PartCategory"] = curPartCategory;
                    dr["PartType"] = curPartType;
                    dr["Qty"] = qtyDec;
                    dr["CreateBy"] = createBy;
                    dr["CreateDate"] = curDate;
                    dr["ModBy"] = createBy;
                    dr["ModDate"] = curDate;
                    dtNew.Rows.Add(dr);
                }
            }

            if (curUnitType == "OA")
            {
                DeleteFromOAU_PartMaster();
                Insert_PartMaster(dtNew, "OA");
            }
            else
            {
                DeleteFromVKG_PartMaster();
                Insert_PartMaster(dtNew, "VKG");
            }
        }

        private int getDigitValue(int digitVal)
        {
            int retDigitVal = 0;

            if (digitVal == 567)
            {
                retDigitVal = 5;
            }
            else if (digitVal == 2324)
            {
                retDigitVal = 23;
            }
            else if (digitVal == 2728)
            {
                retDigitVal = 27;
            }             
            else if (digitVal == 0)
            {
                retDigitVal = 70;
            }
            else
            {
                retDigitVal = digitVal;
            }

            return retDigitVal;
        }       
        #endregion
    }
}
