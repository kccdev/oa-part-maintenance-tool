﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    class MotorBO : MotorDTO
    {
        public DataTable GetROA_MotorDataDTL()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetMotorDataDTL", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@Voltage", Int32.Parse(Voltage));
                        cmd.Parameters.AddWithValue("@Hertz", Decimal.Parse(Hertz));
                        cmd.Parameters.AddWithValue("@Phase", Int32.Parse(Phase));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable R6_OA_GetAllMotorDataDTL()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetAllMotorDataDTL", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void Update_ROA_MotorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateMotorDataDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@FLA", FLA);
                        command.Parameters.AddWithValue("@Hertz", Decimal.Parse(Hertz));
                        command.Parameters.AddWithValue("@Phase", Decimal.Parse(Phase));
                        command.Parameters.AddWithValue("@MCC", Decimal.Parse(MCC));
                        command.Parameters.AddWithValue("@RLA", Decimal.Parse(RLA));
                        command.Parameters.AddWithValue("@Voltage", Voltage);
                        command.Parameters.AddWithValue("@HP", Decimal.Parse(HP));                                               
                        command.Parameters.AddWithValue("@ModBy", Username);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertROA_MotorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_InsertMotorDataDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@MotorType", MotorType);
                        command.Parameters.AddWithValue("@FLA", Decimal.Parse(FLA));
                        command.Parameters.AddWithValue("@Hertz", Decimal.Parse(Hertz));
                        command.Parameters.AddWithValue("@Phase", Int32.Parse(Phase));
                        command.Parameters.AddWithValue("@MCC", Decimal.Parse(MCC));
                        command.Parameters.AddWithValue("@RLA", Decimal.Parse(RLA));
                        command.Parameters.AddWithValue("@Voltage", Int32.Parse(Voltage));
                        command.Parameters.AddWithValue("@HP", Decimal.Parse(HP));
                        command.Parameters.AddWithValue("@ModBy", Username);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteROA_MotorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeleteMotorDataDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
