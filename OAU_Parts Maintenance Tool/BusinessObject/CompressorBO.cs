﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    class CompressorBO : CompressorDTO
    {
        public DataTable GetROA_CompressorDataDtl(string partNum, int iVoltage)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetCompressorDataDtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@Voltage", iVoltage);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);                              
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void GetOAU_CompressorDetailData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetCompressorDetailData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@Voltage", Int32.Parse(Voltage));
                        cmd.Parameters.AddWithValue("@Phase", Int32.Parse(Phase));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);

                                if (dt.Rows.Count > 0)
                                {
                                    DataRow dr = dt.Rows[0];
                                    RuleHeadID = dr["RuleheadID"].ToString();
                                    PartNum = dr["PartNum"].ToString();
                                    PartDescription = dr["PartDescription"].ToString();
                                    PartCategory = dr["PartCategory"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }       

        public void Update_ROA_CompressorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateCompressorDataDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@Voltage", Decimal.Parse(Voltage));
                        command.Parameters.AddWithValue("@Phase", Decimal.Parse(Phase));
                        command.Parameters.AddWithValue("@RLA", Decimal.Parse(RLA));
                        command.Parameters.AddWithValue("@LRA", Decimal.Parse(LRA));
                        command.Parameters.AddWithValue("@ModBy", ModBy);                       
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete_CompressorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeleteCompressorDataDtl", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));                        
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertROA_CompressorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_InsertCompressorDataDtl", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@Voltage", Decimal.Parse(Voltage));
                        command.Parameters.AddWithValue("@Phase", Decimal.Parse(Phase));
                        command.Parameters.AddWithValue("@RLA", Decimal.Parse(RLA));
                        command.Parameters.AddWithValue("@LRA", Decimal.Parse(LRA));
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetROA_CompressorChargeData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetCompressorChargeData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UnitModel", UnitModel);
                        cmd.Parameters.AddWithValue("@CoolingCap", CoolingCap);
                        cmd.Parameters.AddWithValue("@HeatType", HeatType);   
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertROA_CompressorChargeDataHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_InsertCompressorChargeDataHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UnitModel", UnitModel);
                        command.Parameters.AddWithValue("@CoolingCap", CoolingCap);
                        command.Parameters.AddWithValue("@HeatType", HeatType);
                        command.Parameters.AddWithValue("@Circuit1", Decimal.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit1_Reheat", Decimal.Parse(Circuit1_Reheat));
                        command.Parameters.AddWithValue("@Circuit2", Decimal.Parse(Circuit2));
                        command.Parameters.AddWithValue("@Circuit2_Reheat", Decimal.Parse(Circuit2_Reheat));
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateROA_CompressorChargeDataHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateCompressorChargeDataHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@Circuit1", Decimal.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit1_Reheat", Decimal.Parse(Circuit1_Reheat));
                        command.Parameters.AddWithValue("@Circuit2", Decimal.Parse(Circuit2));
                        command.Parameters.AddWithValue("@Circuit2_Reheat", Decimal.Parse(Circuit2_Reheat));
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteROA_CompressorChargeDataHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeleteCompressorChargeDataHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));                       
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetCompCircuitChargeData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetCompCircuitChargeData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Digit3", Digit3);
                        cmd.Parameters.AddWithValue("@Digit4", Digit4);
                        cmd.Parameters.AddWithValue("@Digit567", Digit567);
                        cmd.Parameters.AddWithValue("@Digit9", Digit9);
                        cmd.Parameters.AddWithValue("@Digit11", Digit11);
                        cmd.Parameters.AddWithValue("@Digit12", Digit12);
                        cmd.Parameters.AddWithValue("@Digit13", Digit13);
                        cmd.Parameters.AddWithValue("@Digit14", Digit14);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable R6_OA_GetDuplicateCompChargeDataEntries()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetDuplicateCompChargeDataEntries", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertR6_OA_CompCircuitChargeData()
        {                  
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_InsertCompCircuitChargeData", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Digit3", Digit3);
                        command.Parameters.AddWithValue("@Digit4", Digit4);
                        command.Parameters.AddWithValue("@Digit567", Digit567);
                        command.Parameters.AddWithValue("@Digit9", Digit9);
                        command.Parameters.AddWithValue("@Digit11", Digit11);
                        command.Parameters.AddWithValue("@Digit12", Digit12);
                        command.Parameters.AddWithValue("@Digit13", Digit13);
                        command.Parameters.AddWithValue("@Digit14", Digit14);
                        command.Parameters.AddWithValue("@Circuit1_1_Compressor", Circuit1_1_Compressor);
                        command.Parameters.AddWithValue("@Circuit1_2_Compressor", Circuit1_2_Compressor);
                        command.Parameters.AddWithValue("@Circuit1_3_Compressor", Circuit1_3_Compressor);
                        command.Parameters.AddWithValue("@Circuit2_1_Compressor", Circuit2_1_Compressor);
                        command.Parameters.AddWithValue("@Circuit2_2_Compressor", Circuit2_2_Compressor);
                        command.Parameters.AddWithValue("@Circuit2_3_Compressor", Circuit2_3_Compressor);
                        command.Parameters.AddWithValue("@Circuit1", Decimal.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit1_Reheat", Decimal.Parse(Circuit1_Reheat));
                        command.Parameters.AddWithValue("@Circuit2", Decimal.Parse(Circuit2));
                        command.Parameters.AddWithValue("@Circuit2_Reheat", Decimal.Parse(Circuit2_Reheat));
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateR6_OA_CompCircuitChargeData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateCompCircuitChargeData", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@Digit3", Digit3);
                        command.Parameters.AddWithValue("@Digit4", Digit4);
                        command.Parameters.AddWithValue("@Digit567", Digit567);
                        command.Parameters.AddWithValue("@Digit9", Digit9);
                        command.Parameters.AddWithValue("@Digit11", Digit11);
                        command.Parameters.AddWithValue("@Digit12", Digit12);
                        command.Parameters.AddWithValue("@Digit13", Digit13);
                        command.Parameters.AddWithValue("@Digit14", Digit14);
                        command.Parameters.AddWithValue("@Circuit1_1_Compressor", Circuit1_1_Compressor);
                        command.Parameters.AddWithValue("@Circuit1_2_Compressor", Circuit1_2_Compressor);
                        command.Parameters.AddWithValue("@Circuit1_3_Compressor", Circuit1_3_Compressor);
                        command.Parameters.AddWithValue("@Circuit2_1_Compressor", Circuit2_1_Compressor);
                        command.Parameters.AddWithValue("@Circuit2_2_Compressor", Circuit2_2_Compressor);
                        command.Parameters.AddWithValue("@Circuit2_3_Compressor", Circuit2_3_Compressor);
                        command.Parameters.AddWithValue("@Circuit1", Decimal.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit1_Reheat", Decimal.Parse(Circuit1_Reheat));
                        command.Parameters.AddWithValue("@Circuit2", Decimal.Parse(Circuit2));
                        command.Parameters.AddWithValue("@Circuit2_Reheat", Decimal.Parse(Circuit2_Reheat));
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteR6_OA_CompCircuitChargeData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeleteCompCircuitData", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region LineSize Sql
        public DataTable GetLineSizeList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetLineSizeList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Digit3", Digit3);
                        cmd.Parameters.AddWithValue("@Digit4", Digit4);
                        cmd.Parameters.AddWithValue("@CoolingCap", Digit567);                        
                        cmd.Parameters.AddWithValue("@Digit11", Digit11);                    
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetLineSizeCoolingCap()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetLineSizeCoolingCap", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertLineSize()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_InsertLineSize", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Digit3", Digit3);
                        command.Parameters.AddWithValue("@Digit4", Digit4);
                        command.Parameters.AddWithValue("@CoolingCap", Digit567);                        
                        command.Parameters.AddWithValue("@Digit11", Digit11);
                        command.Parameters.AddWithValue("@Circuit1Suction", Circuit1Suction);
                        command.Parameters.AddWithValue("@Circuit1LiquidLine", Circuit1LiquidLine);
                        command.Parameters.AddWithValue("@Circuit1DischargeLine", Circuit1DischargeLine);
                        command.Parameters.AddWithValue("@Circuit2Suction", Circuit2Suction);
                        command.Parameters.AddWithValue("@Circuit2LiquidLine", Circuit2LiquidLine);
                        command.Parameters.AddWithValue("@Circuit2DischargeLine", Circuit2DischargeLine);
                        command.Parameters.AddWithValue("@ModifiedBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateLineSize()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateLineSize", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@Digit3", Digit3);
                        command.Parameters.AddWithValue("@Digit4", Digit4);
                        command.Parameters.AddWithValue("@CoolingCap", Digit567);
                        command.Parameters.AddWithValue("@Digit11", Digit11);
                        command.Parameters.AddWithValue("@Circuit1Suction", Circuit1Suction);
                        command.Parameters.AddWithValue("@Circuit1LiquidLine", Circuit1LiquidLine);
                        command.Parameters.AddWithValue("@Circuit1DischargeLine", Circuit1DischargeLine);
                        command.Parameters.AddWithValue("@Circuit2Suction", Circuit2Suction);
                        command.Parameters.AddWithValue("@Circuit2LiquidLine", Circuit2LiquidLine);
                        command.Parameters.AddWithValue("@Circuit2DischargeLine", Circuit2DischargeLine);
                        command.Parameters.AddWithValue("@ModifiedBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLineSizeCoolingCap()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeleteLineSizeCoolingCap", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));                       
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
