﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmAddRule : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        ModelNumDescBO objModel = new ModelNumDescBO();

        const int UPD_BTN_CELL = 11;
        const int DEL_BTN_CELL = 12;

        private frmMain m_parent;
        public string mHeatType { get; set; }
        public string mOAUTypeCode { get; set; }
        public int mDigitNo { get; set; }
        public int mRowIdx { get; set; }
        
        public string gScreenMode { get; set; }
        public string gUpdate { get; set; }
        
        bool newPartNum = true;

        public frmAddRule(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
            mHeatType = "";
            mOAUTypeCode = "";
            gScreenMode = lbScreenMode.Text;
        }

        #region Events       

        private void cbDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            string digitStr = cbDigit.Text;

            digitStr = digitStr.Substring(0, digitStr.IndexOf('/'));
           
            int digitInt = Int32.Parse(digitStr);
            mDigitNo = digitInt;
            mHeatType = cbDigit.SelectedValue.ToString();
            gbValues.Visible = true;
            setupValueCheckBoxes(this, digitInt, mHeatType);           
        }
      
        private void btnUpdCancel_Click(object sender, EventArgs e)
        {
            m_parent.mQty = "-1";            
            this.Close();
        }

        private void btnAddRule_Click(object sender, EventArgs e)
        {            
            int index = 0;
            string valueStr = "";
            string errorStr = "";

            bool firstValue = true;
            bool valueSelected = false;

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(this.lbVal1);
            labelControls.Add(this.lbVal2);
            labelControls.Add(this.lbVal3);
            labelControls.Add(this.lbVal4);
            labelControls.Add(this.lbVal5);
            labelControls.Add(this.lbVal6);
            labelControls.Add(this.lbVal7);
            labelControls.Add(this.lbVal8);
            labelControls.Add(this.lbVal9);
            labelControls.Add(this.lbVal10);
            labelControls.Add(this.lbVal11);
            labelControls.Add(this.lbVal12);
            labelControls.Add(this.lbVal13);
            labelControls.Add(this.lbVal14);
            labelControls.Add(this.lbVal15);
            labelControls.Add(this.lbVal16);
            labelControls.Add(this.lbVal17);
            labelControls.Add(this.lbVal18);
            labelControls.Add(this.lbVal19);
            labelControls.Add(this.lbVal20);
            labelControls.Add(this.lbVal21);
            labelControls.Add(this.lbVal22);
            labelControls.Add(this.lbVal23);
            labelControls.Add(this.lbVal24);
            labelControls.Add(this.lbVal25);
            labelControls.Add(this.lbVal26);
            labelControls.Add(this.lbVal27);
            labelControls.Add(this.lbVal28);
            labelControls.Add(this.lbVal29);
            labelControls.Add(this.lbVal30);

            checkBoxControls.Add(this.cbVal1);
            checkBoxControls.Add(this.cbVal2);
            checkBoxControls.Add(this.cbVal3);
            checkBoxControls.Add(this.cbVal4);
            checkBoxControls.Add(this.cbVal5);
            checkBoxControls.Add(this.cbVal6);
            checkBoxControls.Add(this.cbVal7);
            checkBoxControls.Add(this.cbVal8);
            checkBoxControls.Add(this.cbVal9);
            checkBoxControls.Add(this.cbVal10);
            checkBoxControls.Add(this.cbVal11);
            checkBoxControls.Add(this.cbVal12);
            checkBoxControls.Add(this.cbVal13);
            checkBoxControls.Add(this.cbVal14);
            checkBoxControls.Add(this.cbVal15);
            checkBoxControls.Add(this.cbVal16);
            checkBoxControls.Add(this.cbVal17);
            checkBoxControls.Add(this.cbVal18);
            checkBoxControls.Add(this.cbVal19);
            checkBoxControls.Add(this.cbVal20);
            checkBoxControls.Add(this.cbVal21);
            checkBoxControls.Add(this.cbVal22);
            checkBoxControls.Add(this.cbVal23);
            checkBoxControls.Add(this.cbVal24);
            checkBoxControls.Add(this.cbVal25);
            checkBoxControls.Add(this.cbVal26);
            checkBoxControls.Add(this.cbVal27);
            checkBoxControls.Add(this.cbVal28);
            checkBoxControls.Add(this.cbVal29);
            checkBoxControls.Add(this.cbVal30);

            for (int i = 0; i < 30; ++i)
            {
                if (checkBoxControls[i].Enabled == true)
                {
                    if (checkBoxControls[i].Checked == true)
                    {
                        valueSelected = true;
                        if (firstValue)
                        {
                            valueStr = labelControls[i].Text;
                            firstValue = false;
                        }
                        else
                        {
                            valueStr += "," + labelControls[i].Text;
                        }
                    }
                }
            }           

            if (cbUnitType.Text.Length == 0)
            {
                errorStr += "ERROR -- Unit Type is a required field!\n";
            }
            
            if (cbPartNum.SelectedIndex == 0)
            {
                errorStr += "ERROR -- Part Number is a required field!\n";
            }            

            if (cbDigit.SelectedIndex == 0)
            {
                errorStr += "ERROR -- Digit is a required field!\n";
            }            

            if (valueSelected == false)
            {
                errorStr += "ERROR -- No values have been selected!\n";
            }

            if (nudReqQty.Value <= 0)
            {
                errorStr += "ERROR -- Required Qty must be Greater Than 0!";
            }

            if (errorStr.Length == 0)
            {
                if (this.btnAddRule.Text == "Update Rule")
                {
                    index = mRowIdx;
                    this.btnAddRule.Text = "Add Rule";
                }
                else
                {
                    index = dgvAddRules.Rows.Add();
                }

                dgvAddRules.Rows[index].Cells[0].Value = txtRuleID.Text;
                dgvAddRules.Rows[index].Cells[1].Value = txtBatchID.Text;
                dgvAddRules.Rows[index].Cells[2].Value = cbPartNum.Text;
                dgvAddRules.Rows[index].Cells[3].Value = txtPartDesc.Text;
                dgvAddRules.Rows[index].Cells[4].Value = txtRelatedOp.Text;
                dgvAddRules.Rows[index].Cells[5].Value = txtPartCategory.Text;
                dgvAddRules.Rows[index].Cells[6].Value = cbUnitType.Text;
                dgvAddRules.Rows[index].Cells[7].Value = mDigitNo;
                dgvAddRules.Rows[index].Cells[8].Value = mHeatType;
                dgvAddRules.Rows[index].Cells[9].Value = valueStr;
                dgvAddRules.Rows[index].Cells[10].Value = nudReqQty.Value;
                dgvAddRules.Rows[index].Cells[13].Value = cbDigit.SelectedIndex;

                foreach (DataGridViewRow dr in dgvAddRules.Rows)
                {
                    dr.Cells[6].Value = cbUnitType.Text;
                    dr.Cells[10].Value = nudReqQty.Value;
                }                
                                
                btnSaveRules.Enabled = true;
                                                                               
                valueStr = "";
                this.gbValues.Visible = false;
                this.cbPartNum.Enabled = false;
                this.cbUnitType.Enabled = false;
                this.nudReqQty.Enabled = false;
                this.cbDigit.Select();
                //this.cbDigit.SelectedIndex = 0;
                this.cbDigit.Text = "";                             
            }
            else
            {               
                MessageBox.Show(errorStr);
            }
        }
       
        private void btnUpdateRule_Click(object sender, EventArgs e)
        {
            //string valueStr = "";
            //string errorStr = "";

            //bool firstValue = true;
            //bool valueSelected = false;

            //var labelControls = new List<Control>();
            //var checkBoxControls = new List<CheckBox>();

            //labelControls.Add(this.lbVal1);
            //labelControls.Add(this.lbVal2);
            //labelControls.Add(this.lbVal3);
            //labelControls.Add(this.lbVal4);
            //labelControls.Add(this.lbVal5);
            //labelControls.Add(this.lbVal6);
            //labelControls.Add(this.lbVal7);
            //labelControls.Add(this.lbVal8);
            //labelControls.Add(this.lbVal9);
            //labelControls.Add(this.lbVal10);
            //labelControls.Add(this.lbVal11);
            //labelControls.Add(this.lbVal12);
            //labelControls.Add(this.lbVal13);
            //labelControls.Add(this.lbVal14);
            //labelControls.Add(this.lbVal15);
            //labelControls.Add(this.lbVal16);
            //labelControls.Add(this.lbVal17);
            //labelControls.Add(this.lbVal18);
            //labelControls.Add(this.lbVal19);
            //labelControls.Add(this.lbVal20);
            //labelControls.Add(this.lbVal21);
            //labelControls.Add(this.lbVal22);
            //labelControls.Add(this.lbVal23);
            //labelControls.Add(this.lbVal24);
            //labelControls.Add(this.lbVal25);
            //labelControls.Add(this.lbVal26);
            //labelControls.Add(this.lbVal27);
            //labelControls.Add(this.lbVal28);
            //labelControls.Add(this.lbVal29);
            //labelControls.Add(this.lbVal30);

            //checkBoxControls.Add(this.cbVal1);
            //checkBoxControls.Add(this.cbVal2);
            //checkBoxControls.Add(this.cbVal3);
            //checkBoxControls.Add(this.cbVal4);
            //checkBoxControls.Add(this.cbVal5);
            //checkBoxControls.Add(this.cbVal6);
            //checkBoxControls.Add(this.cbVal7);
            //checkBoxControls.Add(this.cbVal8);
            //checkBoxControls.Add(this.cbVal9);
            //checkBoxControls.Add(this.cbVal10);
            //checkBoxControls.Add(this.cbVal11);
            //checkBoxControls.Add(this.cbVal12);
            //checkBoxControls.Add(this.cbVal13);
            //checkBoxControls.Add(this.cbVal14);
            //checkBoxControls.Add(this.cbVal15);
            //checkBoxControls.Add(this.cbVal16);
            //checkBoxControls.Add(this.cbVal17);
            //checkBoxControls.Add(this.cbVal18);
            //checkBoxControls.Add(this.cbVal19);
            //checkBoxControls.Add(this.cbVal20);
            //checkBoxControls.Add(this.cbVal21);
            //checkBoxControls.Add(this.cbVal22);
            //checkBoxControls.Add(this.cbVal23);
            //checkBoxControls.Add(this.cbVal24);
            //checkBoxControls.Add(this.cbVal25);
            //checkBoxControls.Add(this.cbVal26);
            //checkBoxControls.Add(this.cbVal27);
            //checkBoxControls.Add(this.cbVal28);
            //checkBoxControls.Add(this.cbVal29);
            //checkBoxControls.Add(this.cbVal30);

            //for (int i = 0; i < 30; ++i)
            //{
            //    if (checkBoxControls[i].Enabled == true)
            //    {
            //        if (checkBoxControls[i].Checked == true)
            //        {
            //            valueSelected = true;
            //            if (firstValue)
            //            {
            //                valueStr = labelControls[i].Text;
            //                firstValue = false;
            //            }
            //            else
            //            {
            //                valueStr += "," + labelControls[i].Text;
            //            }
            //        }
            //    }
            //}

            //if (valueSelected == false)
            //{
            //    errorStr += "ERROR -- No values have been selected!\n";
            //}

            //if (nudReqQty.Value <= 0)
            //{
            //    errorStr += "ERROR -- Required Qty must be Greater Than 0!";
            //}

            //if (errorStr.Length == 0)
            //{
            //    //mChangesSaved = false;
            //    //dgvAddRules.Rows[mSelRowIdx].Cells[6].Value = cbUnitType.Text;
            //    //dgvAddRules.Rows[mSelRowIdx].Cells[9].Value = valueStr;
            //    //dgvAddRules.Rows[mSelRowIdx].Cells[10].Value = nudReqQty.Value;

            //    //if (mUpdateAll == true)
            //    //{
            //    //    foreach (DataGridViewRow dr in dgvAddRules.Rows)
            //    //    {
            //    //        dr.Cells[6].Value = cbUnitType.Text;
            //    //        dr.Cells[10].Value = nudReqQty.Value;
            //    //    }
            //    //}

            //    //btnSave.Enabled = true;

            //    valueStr = "";
            //}
            //else
            //{
            //    MessageBox.Show(errorStr);
            //}
        }       

        private void btnSaveRules_Click(object sender, EventArgs e)
        {
            int dupsFound = 0;
            int ruleCount = 0;           
            string unitType = "";
            string digitStr = "";

            foreach (DataGridViewRow dr in dgvAddRules.Rows)
            {
                if (cbUnitType.Text != "")
                {
                    unitType = cbUnitType.Text;
                }
                else
                {
                    unitType = dr.Cells[6].Value.ToString();
                }

                ++ruleCount;
                if (DuplicateRule(dr.Cells[2].Value.ToString(), unitType, dr.Cells[9].Value.ToString()) == true)
                {
                    ++dupsFound;
                    //duplicateRuleFound = true;
                    break;
                }
            }

            if (dupsFound != ruleCount)  // If dupFound does not equal ruleCount then the ruleConditions are unique
            {
                string ruleBatchId = "0";
                foreach (DataGridViewRow dr in dgvAddRules.Rows)
                {
                     if (lbScreenMode.Text == "CopyMode")
                    {
                        digitStr = dr.Cells[7].Value.ToString();
                    }
                    else
                    {
                        digitStr = mDigitNo.ToString();
                    }
                    objPart.RuleHeadID = dr.Cells[0].Value.ToString();
                    objPart.RuleBatchID = txtBatchID.Text;
                    objPart.PartNum = dr.Cells[2].Value.ToString();
                    objPart.PartDescription = dr.Cells[3].Value.ToString();
                    objPart.RelatedOperation = dr.Cells[4].Value.ToString();
                    objPart.PartCategory = dr.Cells[5].Value.ToString();
                    objPart.UnitType = unitType;
                    objPart.Digit = digitStr;
                    objPart.HeatType = dr.Cells[8].Value.ToString();
                    objPart.Value = dr.Cells[9].Value.ToString();
                    objPart.Qty = nudReqQty.Value.ToString();
                    objPart.UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

                    ruleBatchId = objPart.InsertPartConditionsDTL();
                    if (ruleBatchId != "-1")
                    {
                        this.txtBatchID.Text = ruleBatchId;
                        objPart.RuleBatchID = ruleBatchId;
                    }
                }
                DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

                if (dt.Rows.Count > 0)
                {
                    objPart.UpdatePartMaster(dt, "OA");
                    m_parent.mRuleHeadID = this.txtRuleID.Text;
                    m_parent.mRuleBatchID = this.txtBatchID.Text;
                    m_parent.mPartNumber = cbPartNum.Text;
                    m_parent.mQty = this.nudReqQty.Value.ToString();
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("ERROR - A ruleSet with this same values already exist for this Unit Type."); 
            }
           
        }

        private void cbPartNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            objPart.PartNum = cbPartNum.Text;
            if (newPartNum == false)
            {
                if (cbPartNum.SelectedIndex != 0)
                {
                    objPart.GetOAU_PartNumberInfo();
                    txtRuleID.Text = objPart.RuleHeadID.ToString();
                    //txtBatchID.Text = objPart.RuleBatchID.ToString();
                    txtBatchID.Text = "00";
                    txtPartCategory.Text = objPart.PartCategory;
                    txtPartDesc.Text = objPart.PartDescription;
                    txtCriticalComp.Text = objPart.CriticalComp;
                    txtRelatedOp.Text = objPart.RelatedOperation.ToString();
                    cbDigit.Select();
                }
                else
                {
                    MessageBox.Show("ERROR - No part number selected!");
                }
            }
            else
            {
                newPartNum = false;
            }
        }

        private void dgvAddRules_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIdx = e.RowIndex;            

            if (e.ColumnIndex == UPD_BTN_CELL)
            {
                this.cbDigit.SelectedIndex = (int)dgvAddRules.Rows[rowIdx].Cells[12].Value;
                mRowIdx = rowIdx;
                string valueStr = dgvAddRules.Rows[rowIdx].Cells[8].Value.ToString();

                var labelControls = new List<Control>();
                var checkBoxControls = new List<CheckBox>();

                labelControls.Add(this.lbVal1);
                labelControls.Add(this.lbVal2);
                labelControls.Add(this.lbVal3);
                labelControls.Add(this.lbVal4);
                labelControls.Add(this.lbVal5);
                labelControls.Add(this.lbVal6);
                labelControls.Add(this.lbVal7);
                labelControls.Add(this.lbVal8);
                labelControls.Add(this.lbVal9);
                labelControls.Add(this.lbVal10);
                labelControls.Add(this.lbVal11);
                labelControls.Add(this.lbVal12);
                labelControls.Add(this.lbVal13);
                labelControls.Add(this.lbVal14);
                labelControls.Add(this.lbVal15);
                labelControls.Add(this.lbVal16);
                labelControls.Add(this.lbVal17);
                labelControls.Add(this.lbVal18);
                labelControls.Add(this.lbVal19);
                labelControls.Add(this.lbVal20);
                labelControls.Add(this.lbVal21);
                labelControls.Add(this.lbVal22);
                labelControls.Add(this.lbVal23);
                labelControls.Add(this.lbVal24);
                labelControls.Add(this.lbVal25);
                labelControls.Add(this.lbVal26);
                labelControls.Add(this.lbVal27);
                labelControls.Add(this.lbVal28);
                labelControls.Add(this.lbVal29);
                labelControls.Add(this.lbVal30);

                checkBoxControls.Add(this.cbVal1);
                checkBoxControls.Add(this.cbVal2);
                checkBoxControls.Add(this.cbVal3);
                checkBoxControls.Add(this.cbVal4);
                checkBoxControls.Add(this.cbVal5);
                checkBoxControls.Add(this.cbVal6);
                checkBoxControls.Add(this.cbVal7);
                checkBoxControls.Add(this.cbVal8);
                checkBoxControls.Add(this.cbVal9);
                checkBoxControls.Add(this.cbVal10);
                checkBoxControls.Add(this.cbVal11);
                checkBoxControls.Add(this.cbVal12);
                checkBoxControls.Add(this.cbVal13);
                checkBoxControls.Add(this.cbVal14);
                checkBoxControls.Add(this.cbVal15);
                checkBoxControls.Add(this.cbVal16);
                checkBoxControls.Add(this.cbVal17);
                checkBoxControls.Add(this.cbVal18);
                checkBoxControls.Add(this.cbVal19);
                checkBoxControls.Add(this.cbVal20);
                checkBoxControls.Add(this.cbVal21);
                checkBoxControls.Add(this.cbVal22);
                checkBoxControls.Add(this.cbVal23);
                checkBoxControls.Add(this.cbVal24);
                checkBoxControls.Add(this.cbVal25);
                checkBoxControls.Add(this.cbVal26);
                checkBoxControls.Add(this.cbVal27);
                checkBoxControls.Add(this.cbVal28);
                checkBoxControls.Add(this.cbVal29);
                checkBoxControls.Add(this.cbVal30);

                for (int i = 0; i < 30; ++i)
                {
                    if (valueStr.Contains(labelControls[i].Text))
                    {
                        checkBoxControls[i].Checked = true;                        
                    }
                    else
                    {
                        checkBoxControls[i].Checked = false;     
                    }
                }
                this.cbUnitType.Enabled = true;
                this.nudReqQty.Enabled = true;
                this.btnAddRule.Text = "Update Rule";
            }
            else if (e.ColumnIndex == DEL_BTN_CELL)
            {
                DialogResult result1 = MessageBox.Show("You are attempting to Delete a rule from the Add Rules Grid. Are you sure you want to do this?",
                                                       "Deleting a Rule",
                                                       MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    dgvAddRules.Rows.RemoveAt(rowIdx);
                    dgvAddRules.Refresh();
                }               
            }
        }
     
        #endregion

        #region Other Methods
        private void setupValueCheckBoxes(frmAddRule frmAdd, int digit, string heatType)
        {
            int idx = 0;
            string valueStr = "";
            string unitType = cbUnitType.Text;

            DataTable dt = objModel.GetOAU_ModelNoValues(digit, heatType);

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();
                
            labelControls.Add(frmAdd.lbVal1);
            labelControls.Add(frmAdd.lbVal2);
            labelControls.Add(frmAdd.lbVal3);
            labelControls.Add(frmAdd.lbVal4);
            labelControls.Add(frmAdd.lbVal5);
            labelControls.Add(frmAdd.lbVal6);
            labelControls.Add(frmAdd.lbVal7);
            labelControls.Add(frmAdd.lbVal8);
            labelControls.Add(frmAdd.lbVal9);
            labelControls.Add(frmAdd.lbVal10);
            labelControls.Add(frmAdd.lbVal11);
            labelControls.Add(frmAdd.lbVal12);
            labelControls.Add(frmAdd.lbVal13);
            labelControls.Add(frmAdd.lbVal14);
            labelControls.Add(frmAdd.lbVal15);
            labelControls.Add(frmAdd.lbVal16);
            labelControls.Add(frmAdd.lbVal17);
            labelControls.Add(frmAdd.lbVal18);
            labelControls.Add(frmAdd.lbVal19);
            labelControls.Add(frmAdd.lbVal20);
            labelControls.Add(frmAdd.lbVal21);
            labelControls.Add(frmAdd.lbVal22);
            labelControls.Add(frmAdd.lbVal23);
            labelControls.Add(frmAdd.lbVal24);
            labelControls.Add(frmAdd.lbVal25);
            labelControls.Add(frmAdd.lbVal26);
            labelControls.Add(frmAdd.lbVal27);
            labelControls.Add(frmAdd.lbVal28);
            labelControls.Add(frmAdd.lbVal29);
            labelControls.Add(frmAdd.lbVal30);

            checkBoxControls.Add(frmAdd.cbVal1);
            checkBoxControls.Add(frmAdd.cbVal2);
            checkBoxControls.Add(frmAdd.cbVal3);
            checkBoxControls.Add(frmAdd.cbVal4);
            checkBoxControls.Add(frmAdd.cbVal5);
            checkBoxControls.Add(frmAdd.cbVal6);
            checkBoxControls.Add(frmAdd.cbVal7);
            checkBoxControls.Add(frmAdd.cbVal8);
            checkBoxControls.Add(frmAdd.cbVal9);
            checkBoxControls.Add(frmAdd.cbVal10);
            checkBoxControls.Add(frmAdd.cbVal11);
            checkBoxControls.Add(frmAdd.cbVal12);
            checkBoxControls.Add(frmAdd.cbVal13);
            checkBoxControls.Add(frmAdd.cbVal14);
            checkBoxControls.Add(frmAdd.cbVal15);
            checkBoxControls.Add(frmAdd.cbVal16);
            checkBoxControls.Add(frmAdd.cbVal17);
            checkBoxControls.Add(frmAdd.cbVal18);
            checkBoxControls.Add(frmAdd.cbVal19);
            checkBoxControls.Add(frmAdd.cbVal20);
            checkBoxControls.Add(frmAdd.cbVal21);
            checkBoxControls.Add(frmAdd.cbVal22);
            checkBoxControls.Add(frmAdd.cbVal23);
            checkBoxControls.Add(frmAdd.cbVal24);
            checkBoxControls.Add(frmAdd.cbVal25);
            checkBoxControls.Add(frmAdd.cbVal26);
            checkBoxControls.Add(frmAdd.cbVal27);
            checkBoxControls.Add(frmAdd.cbVal28);
            checkBoxControls.Add(frmAdd.cbVal29);
            checkBoxControls.Add(frmAdd.cbVal30);                       

            for (int x = 0; x < 30; ++x)
            {
                checkBoxControls[x].Checked = false;
                checkBoxControls[x].Visible = false;
                checkBoxControls[x].Enabled = false;
                labelControls[x].Visible = false;
                labelControls[x].Text = "";
                toolTip1.SetToolTip(labelControls[idx], null);
                toolTip1.SetToolTip(checkBoxControls[idx], null);
            }

            if (cbDigit.Text == "0")
            {
                ArrayList dtAry = new ArrayList();
                int aryIdx = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    dtAry.Add(Int32.Parse(dr["DigitVal"].ToString()));
                    ++aryIdx;
                }
                dtAry.Sort();

                for (int y = 0; y < aryIdx; ++y)
                {
                    valueStr = dtAry[y].ToString();
                    labelControls[y].Text = valueStr;
                    labelControls[y].Visible = true;
                    checkBoxControls[y].Visible = true;
                    checkBoxControls[y].Enabled = true;
                }
            }
            else
            {
               
                foreach (DataRow dr in dt.Rows)
                {                                         
                    valueStr = dr["DigitVal"].ToString();
                    toolTip1.SetToolTip(labelControls[idx], dr["DigitDescription"].ToString());
                    toolTip1.SetToolTip(checkBoxControls[idx], dr["DigitDescription"].ToString());
                    labelControls[idx].Text = valueStr;
                    labelControls[idx].Visible = true;
                    checkBoxControls[idx].Enabled = true;
                    checkBoxControls[idx++].Visible = true;
                }
            }            
        }

        private bool DuplicateRule(string partNumber, string unitType, string valueStr)
        {
            string[] newValues = valueStr.Split(',');
            bool dupRuleFound = false;
            objPart.PartNum = partNumber; 
            //objPart.UnitType = unitType;

            DataTable dt = objPart.GetOAU_PartRulesByPartNumber();

            foreach(DataRow dr in dt.Rows)
            {
                foreach(string newValStr in  newValues)
                {
                    string[] dbValues = dr["Value"].ToString().Split(',');
                    foreach(string dbStr in dbValues)
                    {
                        if (dbStr == newValStr)
                        {
                            dupRuleFound = true;
                            //MessageBox.Show("ERROR - A rule with this value already exist for this Unit Type.\n" +
                            //                "RuleHeadID = " + dr["RuleHeadID"].ToString() + "  RuleBatchID = " + dr["RuleBatchID"].ToString() + "  Digit# = " + dr["Digit"].ToString());
                            break;
                        }
                    }
                    if (dupRuleFound == true)
                    {
                        break;
                    }
                }
                if (dupRuleFound == true)
                {
                    break;
                }
            }

            return dupRuleFound;
        }
       
        #endregion                                  
    }
}
