﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{    
    public partial class frmUpdateCompCircuit : Form
    {
        ModelNumDescBO objModel = new ModelNumDescBO();
        CompressorBO objComp = new CompressorBO();
        decimal circuit1Dec1;
        decimal circuit1Dec1Reheat;
        decimal circuit1Dec2;
        decimal circuit1Dec2Reheat;
        string majorDesignSeq = "";
        string revision = "REV5";

        private frmMain m_parent;

        public frmUpdateCompCircuit(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
            //initializeComboBoxes("REV5");
        }

        private void btnUpdCCD_Save_Click(object sender, EventArgs e)
        {
            if (btnUpdCCD_Save.Text == "Save")
            {
                saveNewCoolingCap();
            }
            else
            {
                updateCoolingCap();
            }

            this.Close();
        }

        private void btnUpdCCD_Cancel_Click(object sender, EventArgs e)
        {
            m_parent.mCoolingCap = "0";
            this.Close();
        }

        //private void rbRev5_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbRev5.Checked == true && lbUpdCCDEventFire.Text == "Yes")
        //    {
        //        setupDigitValuesComboBoxes("REV5");
        //        cbUpdCCD_Digit567.Focus();
        //        cbUpdCCD_Digit9.Enabled = false;
        //        cbUpdCCD_Digit13.Enabled = false;
        //    }
        //    lbUpdCCDEventFire.Text = "Yes";

        //}

        //private void rbRev6_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbRev6.Checked == true && lbUpdCCDEventFire.Text == "Yes")
        //    {                
        //        setupDigitValuesComboBoxes("REV6");
        //        cbUpdCCD_Digit567.Focus();
        //        cbUpdCCD_Digit9.Enabled = true;
        //        cbUpdCCD_Digit13.Enabled = true;
        //    }
        //    lbUpdCCDEventFire.Text = "Yes";
        //}

        private void updateCoolingCap()
        {
            string errors = validateInputData();

            if (errors.Length == 0)
            {
                bindDataToModel();
                objComp.ID = lbUpdCCD_ID.Text;
                objComp.UpdateR6_OA_CompCircuitChargeData();
                m_parent.mUnitType = objComp.Digit3;
                m_parent.mRevision = objComp.Digit4;
                m_parent.mCoolingCap = objComp.Digit567;
                m_parent.mVoltage = objComp.Digit9;
                m_parent.mIndoorCoilType = objComp.Digit11;
                m_parent.mHotGasReheat = objComp.Digit12;
                m_parent.mCompType = objComp.Digit13;
                m_parent.mOutdoorCoilType = objComp.Digit14;
                m_parent.mComp1_1 = objComp.Circuit1_1_Compressor;
                m_parent.mComp1_2 = objComp.Circuit1_2_Compressor;
                m_parent.mComp1_3 = objComp.Circuit1_3_Compressor;
                m_parent.mComp2_1 = objComp.Circuit2_1_Compressor;
                m_parent.mComp2_2 = objComp.Circuit2_2_Compressor;
                m_parent.mComp2_3 = objComp.Circuit2_3_Compressor;
                m_parent.mCircuit1 = objComp.Circuit1;
                m_parent.mCircuit1_Reheat = objComp.Circuit1_Reheat;
                m_parent.mCircuit2 = objComp.Circuit2;
                m_parent.mCircuit2_Reheat = objComp.Circuit2_Reheat;
                m_parent.mModBy = objComp.ModBy;
                m_parent.mLastModDate = DateTime.Now.ToString();
            }
            else
            {
                MessageBox.Show(errors);
            }
        }

        private void saveNewCoolingCap()
        {
            string errors = validateInputData();

            if (errors.Length == 0)
            {
                bindDataToModel();
                objComp.InsertR6_OA_CompCircuitChargeData();
                m_parent.mCoolingCap = objComp.Digit567;
            }
            else
            {
                MessageBox.Show(errors);
            }
        }

        private void bindDataToModel()
        {
            objComp.Digit3 = cbUpdCCD_Digit3.Text.ToString();
            //objComp.Digit4 = majorDesignSeq;
            objComp.Digit4 = cbUpdCCD_Digit4.Text.Substring(0,1);
            objComp.Digit567 = cbUpdCCD_Digit567.SelectedValue.ToString();    
            objComp.Digit9 = cbUpdCCD_Digit9.SelectedValue.ToString();
            objComp.Digit11 = cbUpdCCD_Digit11.SelectedValue.ToString();
            objComp.Digit12 = cbUpdCCD_Digit12.SelectedValue.ToString(); 
            objComp.Digit13 = cbUpdCCD_Digit13.SelectedValue.ToString();                                           
            objComp.Digit14 = cbUpdCCD_Digit14.SelectedValue.ToString();

            objComp.Circuit1_1_Compressor = cbUpdCCDComp1_1.SelectedValue.ToString();
            if (cbUpdCCDComp1_2.SelectedIndex != 0)
            {
                objComp.Circuit1_2_Compressor = cbUpdCCDComp1_2.SelectedValue.ToString();
            }
            else
            {
                objComp.Circuit1_2_Compressor = "NA";
            }

            if (cbUpdCCDComp1_3.SelectedIndex != 0)
            {
                objComp.Circuit1_3_Compressor = cbUpdCCDComp1_3.SelectedValue.ToString();
            }
            else
            {
                objComp.Circuit1_3_Compressor = "NA";
            }

            if (cbUpdCCDComp2_1.SelectedIndex != 0)
            {
                objComp.Circuit2_1_Compressor = cbUpdCCDComp2_1.SelectedValue.ToString();
            }
            else
            {
                objComp.Circuit2_1_Compressor = "NA";
            }

            if (cbUpdCCDComp2_2.SelectedIndex != 0)
            {
                objComp.Circuit2_2_Compressor = cbUpdCCDComp2_2.SelectedValue.ToString();
            }
            else
            {
                objComp.Circuit2_2_Compressor = "NA";
            }

            if (cbUpdCCDComp2_3.SelectedIndex != 0)
            {
                objComp.Circuit2_3_Compressor = cbUpdCCDComp2_3.SelectedValue.ToString();
            }
            else
            {
                objComp.Circuit2_3_Compressor = "NA";
            }

            objComp.Circuit1 = txtUpdCCD_Circuit1.Text;

            objComp.Circuit1_Reheat = "0";
            if (txtUpdCCD_Circuit1Reheat.Text.Length > 0)
            {
                objComp.Circuit1_Reheat = txtUpdCCD_Circuit1Reheat.Text;
            }

            objComp.Circuit2 = "0";
            if (txtUpdCCD_Circuit2.Text.Length > 0)
            {
                objComp.Circuit2 = txtUpdCCD_Circuit2.Text;
            }

            objComp.Circuit2_Reheat = "0";
            if (txtUpdCCD_Circuit2Reheat.Text.Length > 0)
            {
                objComp.Circuit2_Reheat = txtUpdCCD_Circuit2Reheat.Text;
            }

            objComp.ModBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
        }
       
        private string validateInputData()
        {
            string errors = "";

            if (cbUpdCCD_Digit3.SelectedIndex == 0 || cbUpdCCD_Digit3.Text.Length == 0)
            {
                errors += "No selection made for Unit Type (Digit 3).\n";
            }

            if (cbUpdCCD_Digit4.SelectedIndex == 0 || cbUpdCCD_Digit4.Text.Length == 0)
            {
                errors += "No selection made for Major Design Sequence (Digit 4).\n";
            }

            if (cbUpdCCD_Digit567.SelectedIndex == 0 || cbUpdCCD_Digit567.Text.Length == 0)
            {
                errors += "No selection made for Cooling Capacity (Digit 567).\n";
            }
           
            if (cbUpdCCD_Digit9.SelectedIndex == 0 || cbUpdCCD_Digit9.Text.Length == 0)
            {
                errors += "No selection made for Voltage (Digit 9).\n";
            }

            if (cbUpdCCD_Digit11.SelectedIndex == 0 || cbUpdCCD_Digit11.Text.Length == 0)
            {
                errors += "No selection made for Indoor Coil Type (Digit 11).\n";
            }

            if (cbUpdCCD_Digit13.SelectedIndex == 0 || cbUpdCCD_Digit13.Text.Length == 0)
            {
                errors += "No selection made for Compressor (Digit 13).\n";
            }                       

            if (cbUpdCCD_Digit14.SelectedIndex == 0 || cbUpdCCD_Digit14.Text.Length == 0)
            {
                errors += "No selection made for Outdoor Coil Type (Digit 14).\n";
            }

            if (cbUpdCCDComp1_1.SelectedIndex == 0 && cbUpdCCDComp1_2.SelectedIndex == 0 && cbUpdCCDComp1_3.SelectedIndex == 0 &&
                cbUpdCCDComp2_1.SelectedIndex == 0 && cbUpdCCDComp2_2.SelectedIndex == 0 && cbUpdCCDComp2_3.SelectedIndex == 0)
            {
                errors += "No compressors have been selected.\n";
            }

            if (txtUpdCCD_Circuit1.Text.Length == 0)
            {
                errors += "Circuit1 is a required field.\n";
            }
            else
            {
                try
                {
                    circuit1Dec1 = decimal.Parse(txtUpdCCD_Circuit1.Text);
                }
                catch
                {
                    errors += "Circuit1 contains invalid data.\n";
                }
            }

            if (txtUpdCCD_Circuit1Reheat.Text.Length > 0)
            {
                try
                {
                    circuit1Dec1Reheat = decimal.Parse(txtUpdCCD_Circuit1Reheat.Text);
                }
                catch
                {
                    errors += "Circuit1 Reheat contains invalid data.\n";
                }
            }

            if (txtUpdCCD_Circuit2.Text.Length > 0)
            {
                try
                {
                    circuit1Dec2 = decimal.Parse(txtUpdCCD_Circuit2.Text);
                }
                catch
                {
                    errors += "Circuit2 contains invalid data.\n";
                }
            }

            if (txtUpdCCD_Circuit2Reheat.Text.Length > 0)
            {
                try
                {
                    circuit1Dec2Reheat = decimal.Parse(txtUpdCCD_Circuit2Reheat.Text);
                }
                catch
                {
                    errors += "Circuit2 Reheat contains invalid data.\n";
                }
            }

            if ((errors.Length == 0) && (btnUpdCCD_Save.Text == "Add"))
            {
                objComp.Digit3 = cbUpdCCD_Digit3.Text;
                objComp.Digit4 = cbUpdCCD_Digit4.Text.Substring(0,1);
                objComp.Digit567 = cbUpdCCD_Digit567.SelectedValue.ToString();
                objComp.Digit9 = cbUpdCCD_Digit9.SelectedValue.ToString();
                objComp.Digit11 = cbUpdCCD_Digit11.SelectedValue.ToString();
                objComp.Digit12 = cbUpdCCD_Digit12.SelectedValue.ToString();
                objComp.Digit13 = cbUpdCCD_Digit13.SelectedValue.ToString();
                objComp.Digit14 = cbUpdCCD_Digit14.SelectedValue.ToString();
                DataTable dt = objComp.GetCompCircuitChargeData();
                if (dt.Rows.Count > 0)
                {
                    errors += "ERROR - Duplicate record exist utiizing this digit combination.\n";
                    m_parent.mCoolingCap = "0";
                }
            }

            return errors;
        }

        private void setupDigitValuesComboBoxes(string revision, string unitType)
        {
            if (unitType == "B" || unitType == "G")
            {
                objModel.OAUTypeCode = "OALBG";
            }
            else
            {
                objModel.OAUTypeCode = "OAU123DKN";
            }

            DataTable dtDigit = objModel.GetCoolingCapByRevision(revision, 567);
            DataRow dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            cbUpdCCD_Digit567.ValueMember = "DigitVal";
            cbUpdCCD_Digit567.DisplayMember = "DigitValDesc";
            cbUpdCCD_Digit567.DataSource = dtDigit;

            dtDigit = objModel.GetVoltageByRevision(revision, 9);
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            cbUpdCCD_Digit9.ValueMember = "DigitVal";
            cbUpdCCD_Digit9.DisplayMember = "DigitValDesc";
            cbUpdCCD_Digit9.DataSource = dtDigit;

            objModel.DigitNo = "11";
            objModel.HeatType = "NA";           
            objModel.Revision = revision;

            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            cbUpdCCD_Digit11.ValueMember = "DigitVal";
            cbUpdCCD_Digit11.DisplayMember = "DigitValDesc";
            cbUpdCCD_Digit11.DataSource = dtDigit;

            objModel.DigitNo = "12";
            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            cbUpdCCD_Digit12.ValueMember = "DigitVal";
            cbUpdCCD_Digit12.DisplayMember = "DigitValDesc";
            cbUpdCCD_Digit12.DataSource = dtDigit;

            objModel.DigitNo = "13";
            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            cbUpdCCD_Digit13.ValueMember = "DigitVal";
            cbUpdCCD_Digit13.DisplayMember = "DigitValDesc";
            cbUpdCCD_Digit13.DataSource = dtDigit;

            objModel.DigitNo = "14";
            dtDigit = objModel.GetModelNumDescByRevision();
            dr = dtDigit.NewRow();
            dr["DigitVal"] = -1;
            dr["DigitValDesc"] = "Select a Value.";
            dtDigit.Rows.InsertAt(dr, 0);
            cbUpdCCD_Digit14.ValueMember = "DigitVal";
            cbUpdCCD_Digit14.DisplayMember = "DigitValDesc";
            cbUpdCCD_Digit14.DataSource = dtDigit;

        }       

        private int findSelectIndex(DataTable dt, string searchVal)
        {
            int retIdx = 0;

            foreach (DataRow dr in dt.Rows)
            {
                if (searchVal == dr["DigitVal"].ToString())
                {
                    break;
                }
                ++retIdx;
            }

            return retIdx;
        }

        private void cbUpdCCD_Digit4_SelectedIndexChanged(object sender, EventArgs e)
        {
            string unitType = cbUpdCCD_Digit3.Text;
            string OAUTypeCode = "";

            bool enableAll = false;

            if (unitType == "B" || unitType == "G")
            {
                OAUTypeCode = "OALBG";
            }
            else
            {
                OAUTypeCode = "OAU123DKN";
            }

            if (cbUpdCCD_Digit4.SelectedIndex != 0)
            {
                switch (cbUpdCCD_Digit4.SelectedIndex)
                {
                    case 1:
                        majorDesignSeq = "D";
                        setupDigitValuesComboBoxes("REV5", OAUTypeCode);
                        cbUpdCCD_Digit567.Focus();
                        enableAll = true;
                        break;
                    case 2:
                        majorDesignSeq = "E";
                        setupDigitValuesComboBoxes("REV5", OAUTypeCode);
                        cbUpdCCD_Digit567.Focus();
                        enableAll = true;
                        break;
                    case 3:
                        majorDesignSeq = "F";
                        setupDigitValuesComboBoxes("REV5", OAUTypeCode);
                        cbUpdCCD_Digit567.Focus();
                        enableAll = true;
                        break;
                    case 4:
                        majorDesignSeq = "G";
                        setupDigitValuesComboBoxes("REV6", "");
                        cbUpdCCD_Digit567.Focus();
                        enableAll = true;
                        break;
                    default:
                        break;
                }

                if (enableAll == true)
                {
                    cbUpdCCD_Digit567.Enabled = true;
                    cbUpdCCD_Digit9.Enabled = true;
                    cbUpdCCD_Digit11.Enabled = true;
                    cbUpdCCD_Digit12.Enabled = true;
                    cbUpdCCD_Digit13.Enabled = true;
                    cbUpdCCD_Digit14.Enabled = true;
                }
                
            }
        }

        private void cbUpdCCD_Digit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbUpdCCD_Digit3.SelectedIndex != 0)
            {
                cbUpdCCD_Digit4.Enabled = true;
            }
            else
            {
                cbUpdCCD_Digit4.Enabled = false;
            }
        }                
    }
}
