﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdateCompCharge : Form
    {
        private frmMain m_parent;
        CompressorBO objComp = new CompressorBO();
       
        public frmUpdateCompCharge(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
        }

        private void btnCompDtlCancel_Click(object sender, EventArgs e)
        {
            m_parent.mUnitType = "NoUpdate";
            this.Close();
        }

        private void btnCompDtlSave_Click(object sender, EventArgs e)
        {
            string heatType = "";
            string coolingCap = "";
            string unitModel = "";

            if (validCompChargeUpdates() == true)
            {
                objComp.ID = txtUpdCCD_ID.Text;
                unitModel = cbUpdCCD_UnitModel.Text;
                coolingCap = cbUpdCCD_CoolingCapacity.Text;
                objComp.Circuit1 = txtUpdCCD_Circuit1.Text;
                objComp.Circuit1_Reheat = txtUpdCCD_Circuit1Reheat.Text;
                objComp.Circuit2 = txtUpdCCD_Circuit2.Text;
                objComp.Circuit2_Reheat = txtUpdCCD_Circuit2Reheat.Text;
                objComp.ModBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                objComp.DateMod = DateTime.Now.ToString("g");  // 2/27/2009 12:12 PM format

                if (rbUpdCCD_Air.Checked == true)
                {
                    heatType = "Air";
                }
                else if (rbUpdCCD_Normal.Checked == true)
                {
                    heatType = "Normal";
                }
                else if (rbUpdCCD_Water.Checked == true)
                {
                    heatType = "Water";
                }

                if (this.btnUpdCCD_Save.Text == "Update")
                {
                    objComp.UpdateROA_CompressorChargeDataHead();
                    m_parent.mUnitType = objComp.UnitModel;
                    m_parent.mCircuit1 = objComp.Circuit1;
                    m_parent.mCircuit1_Reheat = objComp.Circuit1_Reheat;
                    m_parent.mCircuit2 = objComp.Circuit2;
                    m_parent.mCircuit2_Reheat = objComp.Circuit2_Reheat;
                    m_parent.mModBy = objComp.ModBy;
                    m_parent.mLastModDate = objComp.DateMod;
                    this.Close();

                }
                else if (this.btnUpdCCD_Save.Text == "Add")
                {
                    objComp.UnitModel = unitModel;
                    objComp.CoolingCap = coolingCap;
                    objComp.HeatType = heatType;
                    DataTable dt = objComp.GetROA_CompressorChargeData();
                    if (dt.Rows.Count > 0)
                    {
                        MessageBox.Show("ERROR - Compressor already exist in ROA_CompressorChargeDataHead table!");
                    }
                    else
                    {
                        m_parent.mUnitType = objComp.UnitModel;
                        objComp.InsertROA_CompressorChargeDataHead();
                        this.Close();
                    }
                }
                else if (this.btnUpdCCD_Save.Text == "Delete")
                {
                     DialogResult result1 = MessageBox.Show("Are you sure you want to do the Compressor Charge Date: " +  unitModel +  " - " + coolingCap + " - " + heatType + "? Press Yes to delete; No to cancel!",
                                                       "Deleting a PartRulesHead Row",
                                                       MessageBoxButtons.YesNo);
                     if (result1 == DialogResult.Yes)
                     {
                         objComp.ID = txtUpdCCD_ID.Text;
                         objComp.UnitModel = "ALL";
                         objComp.DeleteROA_CompressorChargeDataHead();
                         this.Close();
                     }
                }
            }
        }

        private bool validCompChargeUpdates()
        {
            bool retVal = true;            
            decimal dCircuitVal;
            string errors = String.Empty;

            if (cbUpdCCD_UnitModel.Text.Length == 0)
            {
                errors += "Error - Unit Model is a required field.\n";
            }

            if (cbUpdCCD_CoolingCapacity.Text.Length == 0)
            {
                errors += "Error - Cooling Capacity is a required field.\n";
            }

            if (rbUpdCCD_Air.Checked == false && rbUpdCCD_Normal.Checked == false && rbUpdCCD_Water.Checked == false)
            {
                errors += "Error - No Heat Type has been selected.\n";
            }
           
            try
            {
                dCircuitVal = Decimal.Parse(txtUpdCCD_Circuit1.Text);

                if (dCircuitVal < 0)
                {
                    errors += "Error - Invalid Circuit1 value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Circuit1 value, field must be a positive decimal value or zero.\n";
            }

            try
            {
                dCircuitVal = Decimal.Parse(txtUpdCCD_Circuit1Reheat.Text);

                if (dCircuitVal < 0)
                {
                    errors += "Error - Invalid Circuit1 Reheat value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Circuit1 Reheat value, field must be a positive decimal value or zero.\n";
            }


            try
            {
                dCircuitVal = Decimal.Parse(txtUpdCCD_Circuit2.Text);

                if (dCircuitVal < 0)
                {
                    errors += "Error - Invalid Circuit2 value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Circuit2 value, field must be a positive decimal value or zero.\n";
            }


             try
            {
                dCircuitVal = Decimal.Parse(txtUpdCCD_Circuit2Reheat.Text);

                if (dCircuitVal < 0)
                {
                    errors += "Error - Invalid Circuit2 Reheat value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Circuit2 Reheat value, field must be a positive decimal value or zero.\n";
            }

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }
    }
}
